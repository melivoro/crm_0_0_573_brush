﻿/****************************************************************************
 *
 * Project:   name
 *
 * @file      filename.c
 * @author    author
 * @date      [Creation date in format %02d.%02d.20%02d]
 *
 * @brief     description
 *
 ****************************************************************************/

#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definitions:                    */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project.            */
                          /* vartab.h is generated automatically.           */

#include "objtab.h"       /* Object ID definitions:                         */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects.         */
                          /* objtab.h is generated automatically.           */
#include "visu.h"
#include "visu_material.h"
/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes ************************************************/

/* functions ****************************************************************/

/****************************************************************************
 * @brief  description
 *
 * @param  yourParam1
 * @param  yourParam1
 * @return none
 ****************************************************************************/
void SpreadingSettings_Draw(void)
{
  const tReagent *Reagent = GetActualReagent();
  if(NULL != Reagent)
  {
    SetVisObjData(OBJ_MATERIALNAME, Reagent->Name, strlen(Reagent->Name)+1);
  }
}

void Visu_SpreadSettings_Close(const tVisuData * VData)
{
  VarDisableEvent(HDL_ACTUALSETTINGS);
}



void Visu_SpreadSettings_Open(const tVisuData * VData)
{
  PrioMaskOn(MSK_SPREADINGSETTINGS);
  SetVar( HDL_ACTUALSETTINGS, Reagent_GetIdx()+1);
  SetVar(HDL_SETTINGSNUM, Reagent_GetNum());
  VarEnableEvent(HDL_ACTUALSETTINGS);
  SpreadingSettings_Draw();
}
void Visu_SpreadSettings_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evt)
{
  for(uint32_t i = 0; i< evtc; i++)
  {
    if(evt[i].Type == CEVT_VAR_CHANGE)
    {
      if(evt[i].Content.mVarChange.VarHandle == HDL_ACTUALSETTINGS)
      {
        Reagent_SetIdx(GetVar(HDL_ACTUALSETTINGS)-1);
        SpreadingSettings_Draw();
      }
    }
  }
  Visu_HomeKeyPressed();
}
