/****************************************************************************
 *
 * File:         VISU_INSTRUMENT.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu_instrument.h"
#include "visu.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Visu_Instrument_Init(const tVisuData * VData)
{

}

void Visu_Instrument_Open(const tVisuData * VData)
{
  PrioMaskOn(MSK_INSTRUMENTS);
}

void Visu_Instrument_Close(const tVisuData * VData)
{

}

void Visu_Instrument_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{
  Visu_HomeKeyPressed();
}
