﻿/****************************************************************************
 *
 * File:         project.c
 * Project:      name
 * Author(s):    author
 * Date:         10.04.2013
 *
 * Description:  description
 *
 *               Copyright (c) 2013 by your company
 *                         your address
 *
 *
 ****************************************************************************/

/* include files ************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* API-Function declarations                      */
#include <UserCAPI.h>

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the objects                 */
                          /* objtab.h is generated automatically            */
#include "visu.h"
#include "io.h"
#include "gseDebug.h"
#include "profile.h"

#include "control.h"
#include "param.h"
#include "visu_ctrl.h"
#include "gsUsbHid.h"
/* macro definitions ********************************************************/
#define PASSWORD_ADMIN_STANDARD    1
#define PASSWORD_MERKATOR_STANDARD 4848
/* type definitions *********************************************************/

/* local prototypes *********************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/
/*void _gs_event_trj1939_event(void);
void _gs_event_trj1939_preinit(void);
void _gs_event_trj1939_thrinit(void);
void _gs_event_trj1939_deinit(void);
void _gs_event_trj1939_cycle(void);*/
/* local constants **********************************************************/

/* local variables **********************************************************/

/* functions ****************************************************************/

tVisuData VisuData;
int32_t init = 0;
/****************************************************************************
  UserC Initialisation
 ****************************************************************************/
int UserCInit(uint32_t initFlags)
{
  //First device start.
  //SetDebugWindow(1);
  if(GS_INIT_FLAG_FIRST_PROJECT_START & initFlags)
  {

    //Set Standard Passwords at first device start.
    if(0 == GetVar(HDL_PASSWORD_MERKATOR))
    {
      SetVar(HDL_PASSWORD_MERKATOR, PASSWORD_MERKATOR_STANDARD);
      SetVar(HDL_PASSWORD_ADMIN   , PASSWORD_ADMIN_STANDARD);
    }
    //Set Brightness of Displaybacklight
    SetVar(HDL_BRIGHTNESS, 100);
    SetVar(HDL_MATERIALDENSITY, 1000);
  }
  /* The return value is the interval of UserC-Timer in steps of 10 ms */
  /* Returning 0 disables the function UserCTimer */
  return 1;
}

/****************************************************************************
  UserC cyclic part
 ****************************************************************************/
void UserCCycle(uint32_t evtc, tUserCEvt * evtv)
{
  //The UserCInit is limited by a timeout. That's why the system initialisation will be done in the first cycle.
  if(!init)
  {
    //SetDebugWindow(1); //Use this to show debug output on the screen
    db_init(1, 10000, 0, 0,1,NULL); //Inits debug output. Output via Ethernet and USB-TO-Serial-Converter possible
    db_out_time("Start Init\r\n");

    //Read Parameters
    InitParam();
    db_out_time("Init Param ready\r\n");


    db_out_time("IO_Init  ready\r\n");

    //Init Profiles
    Prof_Init();
    db_out_time("Prof_Init  ready\r\n");
    Ctrl_Init();
    db_out_time("Ctrl_Init  ready\r\n");
    Visu_Init(&VisuData);

    db_out_time("Init complete\r\n");
    init = 1;
    gsUsbHidSetLayout(0, GS_USB_HID_KEY_LAYOUT_US_INTERNATIONAL);

  }
  else
  {
    Visu_Ctrl_Cycle(evtc, evtv);
    Visu_Cycle(evtc, evtv);
    Ctrl_Cycle(evtc, evtv);
  }
  db_cycle(evtc, evtv);
}

/****************************************************************************
  UserC timer process
 ****************************************************************************/
void UserCTimer(void)
{
  /* Enter your time critical code here ... */
  /* Be aware that the execution time must not exceed the timer interval */

  //Call timer functions only if init is complete
	  if(init == 1)
  {
    Visu_Timer();
    Ctrl_Timer_MS();
  }
}


void UserCDeInit(void)
{
  Ctrl_DeInit();
}
