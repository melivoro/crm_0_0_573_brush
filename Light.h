/************************************************************************
 *
 * File:         LIGHT.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef LIGHT_H
#define LIGHT_H

#include <config\libconfig.h>
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
uint32_t Light_Init(config_t *cfg);
uint32_t Light_Cycle(uint32_t evtc,  tUserCEvt *evt);



/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef LIGHT_H
