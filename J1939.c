﻿/****************************************************************************
 *
 * File:         J1939.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "BNSO.h"
#include "UserJ1939.h"
#include "param.h"
#include "control.h"
#include "J1939.h"
#include "gseDebug.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/
#define J1939_THIS_DEVICE_ID 0x81
/* prototypes ***************************************************************/

typedef enum eagEngineType
{
  ENGINE_TYPE_UNKNOWN = 0,
  ENGINE_TYPE_DEFAULT,
  ENGINE_TYPE_J1939,
}eEngineType;

eEngineType EngineType;



typedef struct tagPGN_F004
{
  unsigned int   res_0                       : 24;
  unsigned int   EngineSpeed_LB              : 8; //SPN 190
  unsigned int   EngineSpeed_HB              : 8; //SPN 190
  unsigned int   res_1                       : 24;
}tPGN_F004;

//Air Supply Pressure
typedef struct tagPGN_FEAE
{
  uint8_t        PneumaticSupplyPressure    ; //SPN 46
  uint8_t        res_0                      ;
  uint8_t        ServicePracePressure_1;        //1087
  uint8_t        ServicePracePressure_2;        //1088
  uint32_t       res_1;
}tPGN_FEAE;

//Engine Temperature 1
typedef struct tagPGN_FEEE
{
  uint8_t  EngineCoolantTemp     ; //SPN 110
  uint8_t  EngineFuelTemp        ; //SPN 174
  uint16_t EngineOilTemp         ;//SPN 175
  uint32_t       res_0;
}tPGN_FEEE;

//Engine Fluid Level/Pressure 1
typedef struct tagPGN_FEEF
{
  uint16_t      res_0                     ;
  uint8_t       OilLevel                  ; //SPN 98
  uint8_t       OilPressure               ; //SPN 100
  uint16_t      res_1                     ;
  uint8_t       CoolantPressure           ; //SPN 109
  uint8_t       CoolantLevel              ; //SPN 111
}tPGN_FEEF;


//Ambient Conditions
typedef struct tagPGN_FEF5
{
  uint16_t      res_0                     ;
  uint8_t       res_1                     ;
  uint8_t       AmbientAirTemp_LB          ; //SPN 171
  uint8_t       AmbientAirTemp_HB          ; //SPN 171
  uint8_t       res_2                     ;
  uint16_t      res_3                     ;
}tPGN_FEF5;

//speed
typedef struct tagPGN_FEF1
{
  unsigned int res_0 : 8;
  unsigned int WheelSpeed :16 ;
  unsigned int res_1 :24/*8*/ ;
  unsigned int PTOstate: 8; //AK NOT TESTED actually size is 5
  unsigned int res_2 : 8/*32*/; //actually size is 11
}tPGN_FEF1;

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

tPG PG_FF01;
tPG PG_FF02;
tPG PG_FF03;

tPG PG_F004;
tPG PG_FEAE;
tPG PG_FEEE;
tPG PG_FEEF;
tPG PG_FEF5;
tPG PG_FEF1;

int32_t JFifo = 0;
//PGNs for BNSO receive
tPGN_FF01 PGN_FF01;
tPGN_FF02 PGN_FF02;

//RPM OF ENGINE
tPGN_F004 PGN_F004;

//Air pressure
tPGN_FEAE PGN_FEAE;

//OIL TEMPERATURE OF ENGINE
//COOLING TEMPERATURE OF ENGINE
tPGN_FEEE PGN_FEEE;

//COOLING PRESSURE OF ENGINE
//OIL PRESSURE OF ENGINE
tPGN_FEEF PGN_FEEF;


//AMBIENT TEMPERATURE
tPGN_FEF5 PGN_FEF5;
//SPEED
tPGN_FEF1 PGN_FEF1;

tPGN_FF01 *J1939_GetPGN_FF01(void)
{
  return &PGN_FF01;
}

tPGN_FF02 *J1939_GetPGN_FF02(void)
{
  return &PGN_FF02;
}


void J1939_SendPGN(uint32_t id, void * data, size_t size_data)
{
  if(size_data > 8)
  {
    return;
  }
  tCanMessage msg;
  msg.channel = 1;
  msg.ext     = 1;
  msg.id      = id;
  msg.len     = 8;
  msg.res     = 0;
  msg.data.u32[0] = 0;
  msg.data.u32[1] = 0;
  memcpy(& msg.data, data, size_data);
  CANSendMsg(&msg);
}

void J1939_Engine_Standard(void)
{

}

void J1939_Init(uint32_t CAN, tControl * Ctrl)
{
  const tParamData *Parameter = ParameterGet();
  if(0 == strcmpi("DEFAULT", Parameter->Engine.Name))
  {
    EngineType = ENGINE_TYPE_DEFAULT;
    JFifo = CANCreateFiFo(256);
    CANAddRxFilterFiFo(1,0,0,1,JFifo);
  }
  else if(0 == strcmpi("J1939", Parameter->Engine.Name))
  {
    tECUName ThisDevice;
    ThisDevice.ArbAdrCap =    0;
    ThisDevice.IndGroup  =    0;
    ThisDevice.VehSys    =    0;
    ThisDevice.VehSysInst=    0;
    ThisDevice.Func      =   30;
    ThisDevice.FuncInst  =    0;
    ThisDevice.ManufCode =  338;
    ThisDevice.ECUInst   =    0;
    ThisDevice.IdNumber  =    3;

    Ctrl->BNSO.Address = 0x20;
    UserJ1939_Create(1);

    db_out("Add BNSO ECU\r\n");
    Ctrl->BNSO.Hdl    = UserJ1939_Add_ECU_Fix_Address(Ctrl->BNSO.Address );
    db_out("Add Engine ECU\r\n");

    Ctrl->Engine.Hdl2 = UserJ1939_Add_ECU_Fix_Address(Parameter->Engine.Id2);
    db_out("Add Engine ECU2\r\n");
    Ctrl->Engine.Hdl  = UserJ1939_Add_ECU_Fix_Address(Parameter->Engine.Id);
    db_out("Add this ECU\r\n");
    UserJ1939_Add_This_ECU(&ThisDevice,J1939_THIS_DEVICE_ID);
      //BNSO
    //Add PGN of BNSO
    UserJ1939_Add_SendPGN(0xFF01, Ctrl->BNSO.Hdl, &PG_FF01,1, 6, 500, sizeof(tPGN_FF01) );
    UserJ1939_Add_SendPGN(0xFF02, Ctrl->BNSO.Hdl, &PG_FF02,1, 6, 500, sizeof(tPGN_FF02) );

    EngineType = ENGINE_TYPE_J1939;
    //Add Engine ECU
    //Add PGN of Engine
    UserJ1939_Add_RecvPGN(0xF004, Ctrl->Engine.Hdl , &PG_F004,1, 6, 100, sizeof(tPGN_F004) );
    UserJ1939_Add_RecvPGN(0xFEAE, Ctrl->Engine.Hdl2, &PG_FEAE,1, 6, 100, sizeof(tPGN_FEAE) );
    UserJ1939_Add_RecvPGN(0xFEEE, Ctrl->Engine.Hdl , &PG_FEEE,1, 6, 100, sizeof(tPGN_FEEE) );
    UserJ1939_Add_RecvPGN(0xFEEF, Ctrl->Engine.Hdl , &PG_FEEF,1, 6, 100, sizeof(tPGN_FEEF) );
    UserJ1939_Add_RecvPGN(0xFEF1, Ctrl->Engine.Hdl , &PG_FEF1,1, 6, 100, sizeof(tPGN_FEF1) );
    UserJ1939_Add_RecvPGN(0xFEF5, Ctrl->Engine.Hdl , &PG_FEF5,1, 6, 100, sizeof(tPGN_FEF5) );

    UserJ1939_Start();
  }
}

void J1939_SetData(int16_t *Destination, uint32_t Source,uint8_t SourceSize,  float Factor, float Offset)
{
  uint32_t MaxSize = (1 << (SourceSize * 8 )) - 1;
  if(Source < MaxSize)
  {
    *Destination = ((double)Source * Factor) + Offset;
  }
}

uint32_t J1939_GetPGN(uint32_t id)
{
  int32_t pgn = id & 0xFFFF00;
  pgn = pgn >> 8;
  return pgn;
}
void J1939_Cycle(tEngine *Engine)
{
  //Set Data for BNSO
  if(EngineType == ENGINE_TYPE_DEFAULT)
  {
    tCanMessage msg;
    static int32_t EngineTime =  10000;
    while(CANReadFiFo(JFifo,&msg,1))
    {
      EngineTime = GetMSTick();
      switch(J1939_GetPGN(msg.id))
      {
        case 0xF004:
          memcpy(&PGN_F004,&msg.data.u32[0],sizeof(msg.data));
        break;

        case 0xFEAE:
          memcpy(&PGN_FEAE,&msg.data.u32[0],sizeof(msg.data));
        break;

        case 0xFEEE:
          memcpy(&PGN_FEEE,&msg.data.u32[0],sizeof(msg.data));
        break;

        case 0xFEEF:
          memcpy(&PGN_FEEF,&msg.data.u32[0],sizeof(msg.data));
        break;

        case 0xFEF5:
          memcpy(&PGN_FEF5,&msg.data.u32[0],sizeof(msg.data));
        break;

        case 0xFEF1:
          memcpy(&PGN_FEF1,&msg.data.u32[0],sizeof(msg.data));
        break;

        default:
        break;
      }
    }
    if(1000 < (GetMSTick()- EngineTime))
    {
      Engine->Active = 0;
    }
    else Engine->Active = 1;

    uint16_t rpm             = PGN_F004.EngineSpeed_LB | (PGN_F004.EngineSpeed_HB << 8);
    J1939_SetData(&Engine->rpm, rpm, sizeof(uint16_t), 0.125,0);
    //Engine->AirPressure = PGN_FEAE.PneumaticSupplyPressure * 8;  //mBar
    //Engine->AirPressure = PGN_FEAE.ServicePracePressure_1 * 8;  //mBar
    J1939_SetData(&Engine->AirPressure,     PGN_FEAE.ServicePracePressure_2 ,sizeof(uint8_t) , 8           ,0);  //mBar
    J1939_SetData(&Engine->speed,           PGN_FEF1.WheelSpeed             ,sizeof(uint16_t), (1.0/256.0) ,0);       //m/h
    J1939_SetData(&Engine->CoolentTemp,     PGN_FEEE.EngineCoolantTemp      ,sizeof(uint8_t) , 1           ,-40);     //°C
    J1939_SetData(&Engine->FuelTemp,        PGN_FEEE.EngineFuelTemp         ,sizeof(uint8_t) , 1           ,-40);     //°C
    J1939_SetData(&Engine->OilTemp,         PGN_FEEE.EngineOilTemp          ,sizeof(uint16_t), (1.0 / 32.0),-273);    //°C
    J1939_SetData(&Engine->OilLevel,        PGN_FEEF.OilLevel               ,sizeof(uint8_t) ,  0.4        ,0  );     //0-100%
    J1939_SetData(&Engine->OilPressure,     PGN_FEEF.OilPressure            ,sizeof(uint8_t) ,  4          ,0  );
    J1939_SetData(&Engine->CoolentLevel,    PGN_FEEF.CoolantLevel           ,sizeof(uint8_t) , 0.4         ,0  );
    J1939_SetData(&Engine->CoolentPressure, PGN_FEEF.CoolantPressure        ,sizeof(uint8_t) , 2           ,0  );
    int16_t AmbTemp         = PGN_FEF5.AmbientAirTemp_LB | (PGN_FEF5.AmbientAirTemp_HB << 8);
   J1939_SetData(&Engine->AmbientAirTemp,  AmbTemp                         ,sizeof(uint16_t), (1.0/32.0)  ,-273); //°C
   //J1939_SetData(&Engine->pto_active, PGN_FEF1.PTOstate      ,sizeof(uint8_t) , 1           ,0  );//AK not tested
//&Engine->AmbientAirTemp=0;
      static int32_t timer =0;
    if(500 < (GetMSTick() -timer))
    {
      timer = GetMSTick();

      msg.id = 0x18FF0181;
      msg.channel = 1;
      msg.ext = 1;
      msg.len = 8;
      msg.res = GS_CAN_DATA_FRAME;
      memcpy(&msg.data.u32[0], &PGN_FF01, sizeof(msg.data));
      CANSendMsg(&msg);
      //
      msg.id = 0x18FF0281;
      memcpy(&msg.data.u32[0], &PGN_FF02, sizeof(msg.data));
      CANSendMsg(&msg);
    }
  }
  else if(EngineType == ENGINE_TYPE_J1939)
  {
    UserJ1939_PGN_SetData(&PG_FF01, (uint8_t*) &PGN_FF01, sizeof(PGN_FF01));
    UserJ1939_PGN_SetData(&PG_FF02, (uint8_t*) &PGN_FF02, sizeof(PGN_FF02));

    UserJ1939_PGN_GetData(&PG_F004, (uint8_t*) &PGN_F004, sizeof(PGN_F004));
    UserJ1939_PGN_GetData(&PG_FEAE, (uint8_t*) &PGN_FEAE, sizeof(PGN_FEAE));
    UserJ1939_PGN_GetData(&PG_FEEE, (uint8_t*) &PGN_FEEE, sizeof(PGN_FEEE));
    UserJ1939_PGN_GetData(&PG_FEEF, (uint8_t*) &PGN_FEEF, sizeof(PGN_FEEF));
    UserJ1939_PGN_GetData(&PG_FEF5, (uint8_t*) &PGN_FEF5, sizeof(PGN_FEF5));
    UserJ1939_PGN_GetData(&PG_FEF1, (uint8_t*) &PGN_FEF1, sizeof(PGN_FEF1));
            uint16_t rpm             = PGN_F004.EngineSpeed_LB | (PGN_F004.EngineSpeed_HB << 8);
    
    J1939_SetData(&Engine->rpm, rpm, sizeof(uint16_t), 0.125,0);
        //Engine->AirPressure = PGN_FEAE.PneumaticSupplyPressure * 8;  //mBar
    //Engine->AirPressure = PGN_FEAE.ServicePracePressure_1 * 8;  //mBar
    J1939_SetData(&Engine->AirPressure,     PGN_FEAE.ServicePracePressure_2 ,sizeof(uint8_t) , 8           ,0);  //mBar
    J1939_SetData(&Engine->speed,           PGN_FEF1.WheelSpeed             ,sizeof(uint16_t), (1.0/256.0) ,0);       //m/h
    J1939_SetData(&Engine->CoolentTemp,     PGN_FEEE.EngineCoolantTemp      ,sizeof(uint8_t) , 1           ,-40);     //°C
    J1939_SetData(&Engine->FuelTemp,        PGN_FEEE.EngineFuelTemp         ,sizeof(uint8_t) , 1           ,-40);     //°C
    J1939_SetData(&Engine->OilTemp,         PGN_FEEE.EngineOilTemp          ,sizeof(uint16_t), (1.0 / 32.0),-273);    //°C
    J1939_SetData(&Engine->OilLevel,        PGN_FEEF.OilLevel               ,sizeof(uint8_t) ,  0.4        ,0  );     //0-100%
    J1939_SetData(&Engine->OilPressure,     PGN_FEEF.OilPressure            ,sizeof(uint8_t) ,  4          ,0  );
    J1939_SetData(&Engine->CoolentLevel,    PGN_FEEF.CoolantLevel           ,sizeof(uint8_t) , 0.4         ,0  );
    J1939_SetData(&Engine->CoolentPressure, PGN_FEEF.CoolantPressure        ,sizeof(uint8_t) , 2           ,0  );
  uint16_t AmbTemp         = PGN_FEF5.AmbientAirTemp_LB | (PGN_FEF5.AmbientAirTemp_HB << 8);
   J1939_SetData(&Engine->AmbientAirTemp,  AmbTemp                         ,sizeof(uint16_t), (1.0/32.0)  ,-273);                 //Â°C

  }
}

void J1939_Timer(tControl *Ctrl)
{
  Ctrl->Engine.Active = UserJ1939_ECU_Active(Ctrl->Engine.Hdl);
  Ctrl->BNSO.Active   = UserJ1939_ECU_Active(Ctrl->BNSO.Hdl);

}
