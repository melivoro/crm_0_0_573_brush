﻿/************************************************************************
 *
 * File:         VISU_PROFILE.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef VISU_PROFILE_H
#define VISU_PROFILE_H

/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
void Visu_Profile_Open(const tVisuData * VData);

void Visu_Profile_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv);


void Visu_ProfileEdit_Open(const tVisuData * VData);

void Visu_ProfileEdit_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv);



/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef VISU_PROFILE_H
