﻿/****************************************************************************
 *
 * File:         CTRL_PLOUGH.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "ctrl_plough.h"
#include "io_types.h"
#include "control.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Plough_UpDown_Init(tABC_Attach * Plough,uint32_t Device1, uint32_t Pos1, uint32_t Device2, uint32_t Pos2  )
{
    Valv_Init(&Plough->ValveUpDown, Device1, Pos1, Device2, Pos2);
}

void Plough_UpDown_Second_Init(tABC_Attach * Plough,uint32_t Device1, uint32_t Pos1, uint32_t Device2, uint32_t Pos2  )
{
    Valv_Init(&Plough->ValveUpDownSecondary, Device1, Pos1, Device2, Pos2);
}



void Plough_Float_Init(tABC_Attach * Plough, int32_t Device, int32_t Pos)
{
    ValvDisk_Init(&Plough->ValveFloat, Device, Pos);
}

void Plough_LeftRight_Init(tABC_Attach *Plough, uint32_t DeviceTurnLeft, uint32_t PosTurnLeft,  uint32_t DeviceTurnRight, uint32_t PosTurnRight )
{
    Valv_Init(&Plough->ValveLeftRight,DeviceTurnLeft, PosTurnLeft, DeviceTurnRight, PosTurnRight );
}

void Plough_SteelRubber_Init(tABC_Attach *Plough, uint32_t DeviceSteelUp, uint32_t PosSteelUp,  uint32_t DeviceSteelDown, uint32_t PosSteelDown )
{
    Valv_Init(&Plough->ValveSteelUpDown,DeviceSteelUp, PosSteelUp, DeviceSteelDown, PosSteelDown );
}

void Plough_WorkTransport(tABC_Attach * Plough,uint32_t Device1, uint32_t Pos1, uint32_t Device2, uint32_t Pos2  )
{
    Valv_Init(&Plough->ValveWorkTransport,Device1, Pos1, Device2,Pos2 );
}

void Plough_TiltUpDown(tABC_Attach * Plough,uint32_t Device1, uint32_t Pos1, uint32_t Device2, uint32_t Pos2  )
{
    Valv_Init(&Plough->ValveTilt,Device1, Pos1, Device2,Pos2 );
}

void Plough_ExRetract_Init(tABC_Attach *Plough, uint32_t DeviceRetract, uint32_t PosRetract
                                          , uint32_t DeviceExtract, uint32_t PosExtract
                                           )
{
    Valv_Init(&Plough->ValveExRetract,DeviceRetract, PosRetract, DeviceExtract, PosExtract );

}

void Plough_ExRetract_RightSection(tABC_Attach *Attachment,uint32_t DeviceRightSection, uint32_t PosRightSection)
{
    ValvDisk_Init(&Attachment->ValveSecondSection , DeviceRightSection, PosRightSection);
}


void Plough_UpDown_Timer_10ms(tABC_Attach * Plough)
{
    Plough->timer_cnt++;
    //call only every 10 times
    if(0 == (Plough->timer_cnt%10))
    {
        Valv_Timer_100ms(&Plough->ValveUpDown);
    }
}


/**
 * @brief Standard function to controle the plough.
 * Joystick is read,
 *
 * @param Plough
 */
void Plough_Cycle(tABC_Attach *Plough, tJSCommands *Cmd)
{
    //MoveUpDown;
    ValvProp_25_50_75_Set(&Plough->ValveUpDown.Valve.PVEH, -Cmd->Updown );
    //Floating:
    if(FLOAT_ON  == Cmd->Float)
    {
        Valv_Set(&Plough->ValveFloat,1);
        Ctrl_SetMaxSpeed(Plough->MaxSpeed);
    }
    else Valv_Set(&Plough->ValveFloat,0);
    //Left right:
    Valv_Set(&Plough->ValveLeftRight, -Cmd->LeftRight );
    //Steel Rubber
    Valv_Set(&Plough->ValveSteelUpDown,Cmd->Z );
    //Extract/Retract
    Valv_Set(&Plough->ValveExRetract,Cmd->Z);
}

void Plough_2_Sections_Cycle(tABC_Attach *Plough_A, tJSCommands *Cmd_A, tABC_Attach *Plough_C, tJSCommands *Cmd_C)
{
    if(Cmd_C->Active)
    {
        Valv_Set(&Plough_C->ValveSecondSection,1);
        ValvProp_25_50_75_Set(&Plough_A->ValveUpDown.Valve.PVEH, -Cmd_C->Updown  );
        //Floating:
        if(FLOAT_ON  == Cmd_C->Float)
        {
             Valv_Set(&Plough_A->ValveFloat,1);
             Ctrl_SetMaxSpeed(Plough_A->MaxSpeed);
        }
        else Valv_Set(&Plough_A->ValveFloat,0);
        //Sync float of A & C
        Cmd_A->Float = Cmd_C->Float;
        //Left right:
        Valv_Set(&Plough_A->ValveLeftRight, -Cmd_C->LeftRight );
        //Steel Rubber
        Valv_Set(&Plough_A->ValveSteelUpDown,Cmd_C->Z );
        //Extract/Retract
        Valv_Set(&Plough_A->ValveExRetract,Cmd_C->Z);
    }
    else
    {
        Valv_Set(&Plough_C->ValveSecondSection,0);
        ValvProp_25_50_75_Set(&Plough_A->ValveUpDown.Valve.PVEH, -Cmd_A->Updown  );
        //Floating:
        if(FLOAT_ON  == Cmd_A->Float)
        {
             Valv_Set(&Plough_A->ValveFloat,1);
             Ctrl_SetMaxSpeed(Plough_A->MaxSpeed);
        }
        else Valv_Set(&Plough_A->ValveFloat,0);
        //Sync float of A & C
        Cmd_C->Float = Cmd_A->Float;

        //Left right:
        Valv_Set(&Plough_A->ValveLeftRight, -Cmd_A->LeftRight );
        //Steel Rubber
        Valv_Set(&Plough_A->ValveSteelUpDown,Cmd_A->Z );
        //Extract/Retract
        Valv_Set(&Plough_A->ValveExRetract,Cmd_A->Z);
    }

}

