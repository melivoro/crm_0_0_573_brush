﻿/****************************************************************************
 *
 * File:         VISU_START.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu.h"
#include "profile.h"
#include "gseDebug.h"
/****************************************************************************/

/* macro definitions ********************************************************/
#define VISU_START_SHOWN_MS 1000
/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
uint32_t timer_start;

void Visu_Start_Open(const tVisuData * VData)
{
  timer_start = GetMSTick();
  PrioMaskOn(MSK_START);
  tProfile * CurrentProfile = Profile_GetCurrentProfile();
  if(NULL == CurrentProfile)
  {
    db_out("No profile found!\r\n");
    return;
  }
  SetVisObjData(OBJ_PROFILE,  CurrentProfile->name, strlen(CurrentProfile->name) + 1);
  uint32_t value;
  if(0 == ConfigGetInt("System.Version.User",&value))    //Version of User Project
    SetVar(HDL_APP, value);
  if(0 == ConfigGetInt("System.Version.Boot",&value))    //Version of App of OS
    SetVar(HDL_OS, value);

}



void VisuStart_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evt)
{
  if(VISU_START_SHOWN_MS < (GetMSTick() - timer_start))
  {
    Visu_OpenScreen(SCREEN_MAIN);
  }
}

