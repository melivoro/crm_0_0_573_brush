﻿/****************************************************************************
 *
 * File:         PROFILE.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "profile.h"
#include "gsToVisu.h"
#include "RCColor.h"
#include "RCText.h"
#include "gseList.h"
#include "gseDebug.h"
#include "control.h"
#include "Ctrl_Front_Attach.h"
#include "Ctrl_Inter_Attach.h"
#include "Ctrl_Side_Attach.h"
#include "Ctrl_Rear_Attach.h"
#include "ctrl_top_attachment.h"

/****************************************************************************/

/* macro definitions ********************************************************/
#define PROFILE_PATH    "/gs/data/profiles.cfg"
#define PROFILE_PATHUSB "/gs/usb/profiles.cfg"

#define INACTIVE INT32_MAX
#define ARROW_INVISIBLE 0
#define ARROW_WHITE     1
#define ARROW_GREEN_0   2
#define ARROW_GREEN_1   3



/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/*******************************************************************************
 * Modules definitions
 *******************************************************************************
 *
 * These arrays define the behavior of the modules / attachment.
 * For every module
 * The Size of an array is defined by a Textlist of the RC_Text-Ressource. You can
 * choose the actual Module by selecting a different one out of this list. That's why the
 * size of this array is defined by the list with parameters. You have a list for each Module group.
 * (A, Top, C, B, rear)
 *
 * ****************************************************************************/



tProfileModule Module_A[RCTEXT_CNT_LT_A_ATTACHMENT] = {
  //Name of the Attachment                                        //Idx of Attachments.png    //Container showning
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_NONE            , .Icon = ATPIC_NONE        , .Cnt = CNT_EMPTY,              .Init_cb = NULL                    , .Cycle_cb = NULL                            , .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_BUCHERFKPLOUGH  , .Icon = ATPIC_PLOUGH_FK   , .Cnt = CNT_A_PLOUGH_FK,        .Init_cb = Ctrl_Front_FK_Init      , .Cycle_cb = Ctrl_Front_Plough_Standard_Cycle, .Timer_cb = Ctrl_Front_Plough_Standard_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_CORMZ           , .Icon = ATPIC_PLOUGH_FK   , .Cnt = CNT_A_PLOUGH_FK,        .Init_cb = Ctrl_Front_CORMZ_Init   , .Cycle_cb = Ctrl_Front_Plough_Standard_Cycle, .Timer_cb = Ctrl_Front_Plough_Standard_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_SMOLENSK        , .Icon = ATPIC_PLOUGH_FK   , .Cnt = CNT_A_PLOUGH_FK,        .Init_cb = Ctrl_Front_SMOLENSK_Init, .Cycle_cb = Ctrl_Front_Plough_Standard_Cycle, .Timer_cb = Ctrl_Front_Plough_Standard_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_BUCHERTN34      , .Icon = ATPIC_PLOUGH_NT   , .Cnt = CNT_A_PLOUGH_TN,        .Init_cb = Ctrl_Front_TN34_Init    , .Cycle_cb = Ctrl_Front_Plough_Standard_Cycle, .Timer_cb = Ctrl_Front_Plough_Standard_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_MN              , .Icon = ATPIC_PLOUGH_NT   , .Cnt = CNT_A_PLOUGH_MN,        .Init_cb = Ctrl_Front_MN_Init      , .Cycle_cb = Ctrl_Front_Plough_Standard_Cycle, .Timer_cb = Ctrl_Front_Plough_Standard_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_TE              , .Icon = ATPIC_PLOUGH_TE   , .Cnt = CNT_A_PLOUGH_TE,        .Init_cb = Ctrl_Front_TE_Init      , .Cycle_cb = Ctrl_Front_Plough_2_Sections_Cycle, .Timer_cb = Ctrl_Front_Plough_Standard_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_VTYPE           , .Icon = ATPIC_PLOUGH_TE   , .Cnt = CNT_A_PLOUGH_VA,        .Init_cb = Ctrl_Front_VTYPE_Init   , .Cycle_cb = Ctrl_Front_Plough_2_Sections_Cycle, .Timer_cb = Ctrl_Front_Plough_Standard_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_OFF_OPF_245     , .Icon = /*ATPIC_OPF*/ATPIC_OMB         , .Cnt = CNT_A_BRUSH_OPF245,     .Init_cb = Ctrl_Front_OFF_OPF_245  , .Cycle_cb = Ctrl_Front_Brush_Standard_Cycle , .Timer_cb = Ctrl_Front_Brush_Standard_Timer_10ms },
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_OMB1            , .Icon = ATPIC_OMB         , .Cnt = CNT_A_BRUSH_OMB1,       .Init_cb = Ctrl_Front_OMB1         , .Cycle_cb = Ctrl_Front_Brush_Standard_Cycle , .Timer_cb = Ctrl_Front_Brush_Standard_Timer_10ms },
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_OMT1            , .Icon = ATPIC_OMT         , .Cnt = CNT_A_BRUSH_OMT1,       .Init_cb = Ctrl_Front_OMT1         , .Cycle_cb = Ctrl_Front_Brush_Standard_Cycle , .Timer_cb = Ctrl_Front_Brush_Standard_Timer_10ms },
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_MF300           , .Icon = ATPIC_MF300_OUT    , .Cnt = CNT_A_WASHER_MF300,     .Init_cb = Ctrl_Front_MF300        , .Cycle_cb = Ctrl_Front_Brush_Standard_Cycle , .Timer_cb = Ctrl_Front_Brush_Standard_Timer_10ms },
  { .RC_Name =  RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_MF500           , .Icon = ATPIC_MF500        , .Cnt = CNT_A_WASHER_MF500,     .Init_cb = Ctrl_Front_MF500        , .Cycle_cb = Ctrl_Front_Brush_Standard_Cycle , .Timer_cb = Ctrl_Front_Brush_Standard_Timer_10ms },
};

tProfileModule Module_C [RCTEXT_CNT_LT_C_ATTACHMENT] = {
  { .RC_Name =  RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_NONE       , .Icon = ATPIC_NONE       , .Cnt = CNT_EMPTY      , .Init_cb = NULL                    , .Cycle_cb = NULL                     , .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_AA_AC      , .Icon = ATPIC_PLOUGH_SIDE, .Cnt = CNT_C_PLOUGH_AA, .Init_cb = Ctrl_Side_AA_AC_Init    , .Cycle_cb = Ctrl_Side_Plough_Cycle   , .Timer_cb = Ctrl_Side_Plough_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_CORMZ_OBA  , .Icon = ATPIC_PLOUGH_SIDE, .Cnt = CNT_C_PLOUGH_AK, .Init_cb = Ctrl_Side_CORMZ_OBA_Init, .Cycle_cb = Ctrl_Side_Plough_Cycle   , .Timer_cb = Ctrl_Side_Plough_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_BUCHER_AK  , .Icon = ATPIC_PLOUGH_SIDE, .Cnt = CNT_C_PLOUGH_AK, .Init_cb = Ctrl_Side_BUCHER_AK_Init, .Cycle_cb = Ctrl_Side_AK_Cycle       , .Timer_cb = Ctrl_Side_Plough_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_MSN37      , .Icon = ATPIC_PLOUGH_SIDE, .Cnt = CNT_C_PLOUGH_AK, .Init_cb = Ctrl_Side_MSN37_Init    , .Cycle_cb = Ctrl_Side_Plough_Cycle   , .Timer_cb = Ctrl_Side_Plough_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_TE         , .Icon = ATPIC_PLOUGH_TE  , .Cnt = CNT_A_PLOUGH_TE, .Init_cb = Ctrl_Side_TE_Init       , .Cycle_cb = NULL                     , .Timer_cb = NULL},//Cycle and timer function don in attachment A
  { .RC_Name =  RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_VTYPE      , .Icon = ATPIC_PLOUGH_TE  , .Cnt = CNT_A_PLOUGH_VA, .Init_cb = Ctrl_Side_VType_Init    , .Cycle_cb = NULL                     , .Timer_cb = NULL},//Cycle and timer function don in attachment A
};

tProfileModule Module_B[RCTEXT_CNT_LT_B_ATTACHMENT] = {
  { .RC_Name =  RCTEXT_LI_B_ATTACHMENT_ATTACHMENT_NONE    , .Icon = ATPIC_NONE           , .Cnt = CNT_EMPTY,             .Init_cb = NULL                  , .Cycle_cb = NULL                            , .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_B_ATTACHMENT_ATTACHMENT_OMP253  , .Icon = ATPIC_OMP253         , .Cnt = CNT_B_BRUSH_OMP253  ,  .Init_cb = Ctrl_Inter_OMP253_Init, .Cycle_cb = Ctrl_Inter_Brush_Standard_Cycle , .Timer_cb = Ctrl_Inter_Brush_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_B_ATTACHMENT_ATTACHMENT_OMP220  , .Icon = ATPIC_OMP253         , .Cnt = CNT_B_BRUSH_OMP253  ,  .Init_cb = Ctrl_Inter_OMP220_Init, .Cycle_cb = Ctrl_Inter_Brush_Standard_Cycle , .Timer_cb = Ctrl_Inter_Brush_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_B_ATTACHMENT_ATTACHMENT_CH2600  , .Icon = ATPIC_PLOUGH_CH2600  , .Cnt = CNT_B_GRADER_CH2600 ,  .Init_cb = Ctrl_Inter_CH2600_Init, .Cycle_cb = Ctrl_Inter_Plough_Standard_Cycle, .Timer_cb = Ctrl_Inter_Plough_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_B_ATTACHMENT_ATTACHMENT_OZP231  , .Icon = ATPIC_OMP253         , .Cnt = CNT_B_BRUSH_OZP231  ,  .Init_cb = Ctrl_Inter_OZP231_Init, .Cycle_cb = Ctrl_Inter_Brush_Standard_Cycle, .Timer_cb = Ctrl_Inter_Plough_Timer_10ms},
};

tProfileModule Module_Top  [RCTEXT_CNT_LT_TOPATTACHMENT] = {
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_NONE                   , .Icon = ATPIC_NONE, .Cnt = CNT_EMPTY          , .Init_cb = NULL                             , .Cycle_cb = NULL, .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_RTR_KH                 , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_KH         , .Init_cb = Ctrl_top_RTR_KH_Init             , .Cycle_cb = Ctrl_top_RTR_Cycle              , .Timer_cb = Ctrl_top_RTR_SH_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_RTR_SH                 , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_SH         , .Init_cb = Ctrl_top_RTR_SH_Init             , .Cycle_cb = Ctrl_top_RTR_Cycle              , .Timer_cb = Ctrl_top_RTR_SH_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_LRS_CK                 , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_CK         , .Init_cb = Ctrl_top_LRS_CK_Init             , .Cycle_cb = Ctrl_top_LRS_CK_Cycle           , .Timer_cb = Ctrl_top_LRS_CK_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_LRS_CK_BP300           , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_CK         , .Init_cb = Ctrl_top_LRS_CK_Init             , .Cycle_cb = Ctrl_top_LRS_CK_Cycle           , .Timer_cb = Ctrl_top_LRS_CK_Timer_10ms},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_E2000_BP300            , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_E2000      , .Init_cb = Ctrl_top_E2000_BP300_Init        , .Cycle_cb = Ctrl_top_E2000_BP300_Cycle      , .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_E2000_C610H            , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_E2000      , .Init_cb = Ctrl_top_E2000_C610H_Init        , .Cycle_cb = Ctrl_top_E2000_C610H_Cycle      , .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_E2000_BP300_C610H      , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_E2000      , .Init_cb = Ctrl_top_E2000_BP300_C610H_Init  , .Cycle_cb = Ctrl_top_E2000_BP300_C610H_Cycle, .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_E2000_C610H_RR300      , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_RR300_E2000, .Init_cb = Ctrl_top_E2000_C610H_RR300_Init  , .Cycle_cb = Ctrl_top_E2000_C610H_RR300_Cycle, .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_E2000_C610H_RR400      , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_RR300_E2000, .Init_cb = Ctrl_top_E2000_C610H_RR400_Init  , .Cycle_cb = Ctrl_top_E2000_C610H_RR400_Cycle, .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_E2000_BP300_C610H_RR400, .Icon = ATPIC_NONE, .Cnt = CNT_TOP_RR300_E2000, .Init_cb = Ctrl_top_E2000_BP300_RR400_Init  , .Cycle_cb = Ctrl_top_E2000_BP300_RR400_Cycle, .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_E2000_BP300_C610H_RR300, .Icon = ATPIC_NONE, .Cnt = CNT_TOP_RR300_E2000, .Init_cb = Ctrl_top_E2000_BP300_RR300_Init  , .Cycle_cb = Ctrl_top_E2000_BP300_RR300_Cycle, .Timer_cb = NULL},
  { .RC_Name =  RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_CF                     , .Icon = ATPIC_NONE, .Cnt = CNT_TOP_CF,          .Init_cb = NULL                             , .Cycle_cb = Ctrl_top_CF_Cycle               , .Timer_cb = NULL},
};




tModules ProfModules[] =
{
  {Module_Top,  GS_ARRAYELEMENTS(Module_Top)       , RCTEXT_L_TOPATTACHMENT},
  {Module_A,GS_ARRAYELEMENTS(Module_A)             , RCTEXT_L_A_ATTACHMENT},
  {Module_B,GS_ARRAYELEMENTS(Module_B)             , RCTEXT_L_B_ATTACHMENT},
  {Module_C, GS_ARRAYELEMENTS(Module_C)            , RCTEXT_L_C_ATTACHMENT},
};


tGSList      ProfileList;


/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

/**
 * @brief These are the standard profiles of a new device. If the project is installed on a D3510
 * the first time, these Profiles can be found.
 */
tProfile StandardProfiles[] =
{
  { .name = "123", .Module =  {RCTEXT_LI_TOPATTACHMENT_ATTACHMENT_RTR_SH
                                   ,RCTEXT_LI_A_ATTACHMENT_ATTACHMENT_MN
                                   ,RCTEXT_LI_B_ATTACHMENT_ATTACHMENT_CH2600
                                   ,RCTEXT_LI_C_ATTACHMENT_ATTACHMENT_BUCHER_AK}},
};

tProfile * CurrentProfile = NULL;



void Prof_AddStandardProfiles(void)
{
  for(uint32_t i = 0; i < GS_ARRAYELEMENTS(StandardProfiles); i++)
  {
    GSList_AddData(&ProfileList, &StandardProfiles[i], sizeof(tProfile));
  }
  /*GSList_AddData(&ProfileList, &StandardProfiles[0], sizeof(tProfile));
  GSList_AddData(&ProfileList, &StandardProfiles[1], sizeof(tProfile));
  GSList_AddData(&ProfileList, &StandardProfiles[2], sizeof(tProfile));*/
}

tProfileModule *GetCurrentModule(uint32_t idx)
{
  return &ProfModules[idx].p[CurrentProfile->Module[idx]];
}

const char * Prof_GetModuleName( uint32_t ModuleIdx, uint32_t idx, uint32_t language)
{
  if(ModuleIdx >= GS_ARRAYELEMENTS(ProfModules))
  {
    return "Prof_GetModuleName: Invalid Module Index\r\n";
  }
  if(idx >= ProfModules[ModuleIdx].p_num)
  {
    return "Prof_GetModuleName: Invalid Listindex in Module \r\n";
  }
  return RCTextGetListElement(ProfModules[ModuleIdx].RCtListOffset,ProfModules[ModuleIdx].p[idx].RC_Name, language );;
}

void Prof_Write(tProfile * Profile, tGsFile **fp)
{
  FilePrintf(*fp, "PROFNAME=\"%s\"\r\n", Profile->name);
  for(uint32_t i = 0; i < GS_ARRAYELEMENTS(Profile->Module) ; i++)
  {
    FilePrintf(*fp, "MODULE%u=\"%s\"\r\n", i + 1, Prof_GetModuleName(i,Profile->Module[i], 0));
  }
}



/**
 * @brief This Function loads profiles form a File and writes them into a list.
 *  Profiles are saved like this:
 * PROFNAME="ProfileName"
 * MODULE1="ABC"
 * MODULE2="EFG"
 * MODUEL3="HIJ"
 * MODUEL4="KLM"
 * MODUEL5="NOP"
 * PROFNAME="ProfileName2"
 * ...
 *
 * @return int32_t
 */
int32_t Prof_Load(void)
{
  //Open File with configturation.
   tGsFile *fp = FileOpen(PROFILE_PATH, "r");
   if(fp == NULL)
   {
     db_out_time("Can't open \"%s\"\r\n",PROFILE_PATH );

     return -1;
   }
   tProfile *ActProfile = NULL;
   char buffer[128];
   db_out_time("Destroy  list\r\n");
   //Destroy existing Profile List
   GSList_Destroy(&ProfileList);
   db_out_time("List destroyed\r\n");
   //Read lines
   while(FileGets(buffer, sizeof(buffer), fp))
   {
     //check for new Profile
     if(strstr(buffer, "PROFNAME"))
     {
       db_out_time("Found PROFNAME in %s\r\n", buffer);
       tProfile Profile;
       memset(&Profile,0, sizeof(tProfile));
       char *ptr;
       char delimeter[] = "=\r\n\"";
       //Get profile name
       ptr = strtok(buffer, delimeter);
       ptr = strtok(NULL, delimeter);
       strncpy(Profile.name,ptr, sizeof(Profile.name));
       //Save Profile in the List
       ActProfile = GSList_AddData(&ProfileList,&Profile, sizeof(Profile));
     }
     //Check for Module Entries
     else if(strstr(buffer, "MODULE"))
     {
       db_out_time("Found MODULE in %s\r\n", buffer);
       //Check Number of Module
       if(NULL != ActProfile)
       {
          uint32_t idx;
          if(strstr(buffer, "MODULE1"))
          {
            idx = 0;
          }
          else if(strstr(buffer, "MODULE2"))
          {
            idx = 1;
          }
          else if(strstr(buffer, "MODULE3"))
          {
            idx = 2;
          }
          else if(strstr(buffer, "MODULE4"))
          {
            idx = 3;
          }
          else return -1;


          char *ptr;
          char delimeter[] = "=\r\n\"";
          //Get module name
          ptr = strtok(buffer, delimeter);
          ptr = strtok(NULL, delimeter);
          if(NULL == ptr)
          {
              db_out("Error at loading profile. could not read module name in :\"%s\"", buffer);
          }
         for(uint32_t i = 0; i < ProfModules[idx].p_num; i++)
         {
            if(0 == strcmp(ptr,Prof_GetModuleName( idx, i, 0)))
            {
              db_out("Found Module!");
              ActProfile->Module[idx] = i;
              break;
            }
         }
       }
     }
   }
   FileClose(fp);
   return 0;
}




int32_t Prof_Save(void)
{
  tGsFile *fp = FileOpen(PROFILE_PATH, "w");
  if(fp == NULL)
  {
    db_out("Can't open \"%s\"\r\n");
    return -1;
  }
  tProfile * ActProf = GSList_GetFirstData(&ProfileList);
  while(ActProf != NULL)
  {
    Prof_Write(ActProf, &fp);
    ActProf = GSList_GetNextData(&ProfileList);
  }

  FileClose(fp);
  int32_t ProfIdx = Profile_GetIndex(Profile_GetCurrentProfile());
  if(-1 < ProfIdx)
    SetVarIndexed(IDX_CURRENTPROFILE, ProfIdx); //saves the idx of the current profile as remanent variable

  return 0;
}

int32_t Prof_CopyToUsb(void)
{
  return CopyFile(PROFILE_PATH,PROFILE_PATHUSB,NULL );
}

int32_t Prof_LoadFromUsb(void)
{
  return CopyFile(PROFILE_PATHUSB,PROFILE_PATH,NULL );
}



/**
 * @brief Initalises the profiles. Loads existing Profiles from the flash
 * or generates standard profiles, if no profiles exist, jet.
 * This will also load find the profile, which had been active the last time and
 * will select it as current
 *
 */
void Prof_Init(void)
{

    db_out("Start init profile");
    GSList_Init(&ProfileList);
    db_out_time("ProfileList initialized\r\n");
    if(-1 == Prof_Load())
    {
      db_out_time("Load Standard Profile List\r\n");
      Prof_AddStandardProfiles();
      Prof_Save();
    }
    //Get current Profile
    CurrentProfile = GSList_GetFirstData(&ProfileList);
    for(int32_t i = 0 ; i < GetVarIndexed(IDX_CURRENTPROFILE); i++)
    {
      CurrentProfile = GSList_GetNextData(&ProfileList);
    }
    if(CurrentProfile == NULL)
    {
      CurrentProfile = GSList_GetFirstData(&ProfileList);
    }
}
/**
 * @brief This Functions draws the container, showing the actual movement of the attachments.
 * In the center of the container the actual selected equipment is shown (depending on the pressed Joystick Button A, B or C)
 * Now it is checked, which valves are in use for this attachment. If a Valve is used, the arrows to this valve function will be shown.
 *
 * Example:
 * A snow Plough is used. Its picture will be shown in the middle, if Key A is pressed. (A for front Attachment)
 * The Plough can move up and down. So there is a Valve for the movment UpDown. If no movmemnt the arrows for up and down are shown in white.
 * If the Plough moves up, the Arrow for up will become green.
 *
 */
void Prof_DrawMove(void)
{
  eAttachmentPicIdx Pic = 0;
  int32_t updown = INACTIVE;
  int32_t leftright= INACTIVE;
  int32_t extract = INACTIVE;
  int32_t steelblade = INACTIVE;
  int32_t work       = INACTIVE;
  int32_t sideupdown = INACTIVE;
  int32_t sidetilt = INACTIVE;
  const tControl *Ctrl= CtrlGet();
  //Check updown
  tProfileModule *Module;


  int32 Attachment = EQUIP_A;
  if(1 == Ctrl->ABC_Buttons_Pressed)//only A XOR B XOR C
  {
    for(int32_t i = 0; i<= EQUIP_C; i++)
    {
      //check if Button A,B or C is down
      Attachment = i;
      if(IOT_Button_IsDown(&Ctrl->Joystick.Button[i]))
      {
        //check for movement depending on the connected valve.
        if((Module = GetCurrentModule(i+1)))
        {
          Pic = Module->Icon;
          if(!Pic)  continue;
          //set Arrows only if available for Attachment
          if(Ctrl->ABC[i].ABC_Attach.ValveUpDown.Type       != VALVE_TYPE_NONE)
          {
            updown =  Ctrl->CmdABC[i].Updown;
          }
          //set Arrows only if available for Attachment
          if(Ctrl->ABC[i].ABC_Attach.ValveLeftRight.Type  != VALVE_TYPE_NONE)
          {
            leftright =  Ctrl->CmdABC[i].LeftRight;
          }
          if(Ctrl->ABC[i].ABC_Attach.ValveExRetract.Type  != VALVE_TYPE_NONE)
          {
            extract =  Ctrl->CmdABC[i].Z;
          }
          //Steel blade arrows
          if(Ctrl->ABC[i].ABC_Attach.ValveSteelUpDown.Type  != VALVE_TYPE_NONE)
          {
            steelblade =  Ctrl->CmdABC[i].Z;
            //Spezial Icon at use of Steelblade for opening and closing
            if(steelblade > 0)
              Pic = ATPIC_STEELBLADE_DOWN;
            else if(steelblade < 0 )
              Pic = ATPIC_STEELBLADE_UP;
          }
          // work arrows
          if(Ctrl->ABC[i].ABC_Attach.ValveWorkTransport.Type  != VALVE_TYPE_NONE)
          {
            work =  Ctrl->CmdABC[i].LeftRight;
            //Spezial Icon at use of Steelblade for opening and closing
          }
          if(Ctrl->ABC[i].ABC_Attach.ValveSideUpDown.Type  != VALVE_TYPE_NONE)
          {
            sideupdown = Ctrl->CmdABC[i].Z;
                      //special picture for MF300 while retracting
            if(Pic == ATPIC_MF300_OUT)
            {
              if(sideupdown < 0)
              {
                Pic = ATPIC_MF300_IN;
              }
            }
          }
        }
      }
      if(Pic)
      {
        break;
      }
    }
  }
  //draw the arrows, if a picture is shown.
  if(Pic)
  {
    if(!IsInfoContainerOn(CNT_ATTACHMOVE))
    {
      InfoContainerOn(CNT_ATTACHMOVE);
    }

    //____________________
    // Draw Arrows for possible, and active functions into the container.

    //Up / Down
    if(updown == INACTIVE)  SetVarIndexed(IDX_ATTACHMENT_UP ,ARROW_INVISIBLE);
    else if(updown > 0) SetVarIndexed(IDX_ATTACHMENT_UP ,ARROW_GREEN_0);
    else           SetVarIndexed(IDX_ATTACHMENT_UP ,ARROW_WHITE);

    if(updown == INACTIVE)  SetVarIndexed(IDX_ATTACHMENT_DOWN ,ARROW_INVISIBLE);
    else if(updown < 0) SetVarIndexed(IDX_ATTACHMENT_DOWN ,ARROW_GREEN_0);
    else           SetVarIndexed(IDX_ATTACHMENT_DOWN ,ARROW_WHITE);

    //Left /Right
    if(leftright == INACTIVE)  SetVarIndexed(IDX_ATTACHMENT_RIGHT ,ARROW_INVISIBLE);
    else if(leftright > 0) SetVarIndexed(IDX_ATTACHMENT_RIGHT ,ARROW_GREEN_0);
    else               SetVarIndexed(IDX_ATTACHMENT_RIGHT ,ARROW_WHITE);

    if(leftright == INACTIVE)  SetVarIndexed(IDX_ATTACHMENT_LEFT ,ARROW_INVISIBLE);
    else if(leftright < 0) SetVarIndexed(IDX_ATTACHMENT_LEFT ,ARROW_GREEN_0);
    else               SetVarIndexed(IDX_ATTACHMENT_LEFT ,ARROW_WHITE);

    //Extract /Retract
    if(steelblade == INACTIVE)  SetVarIndexed(IDX_ATTACHMENT_STEELUPDOWN ,ARROW_INVISIBLE);
    else if(steelblade > 0) SetVarIndexed(IDX_ATTACHMENT_STEELUPDOWN ,ARROW_GREEN_0);
    else if(steelblade < 0) SetVarIndexed(IDX_ATTACHMENT_STEELUPDOWN ,ARROW_GREEN_1);
    else                 SetVarIndexed(IDX_ATTACHMENT_STEELUPDOWN ,ARROW_WHITE);
    //Workposition
    if(work == INACTIVE)  SetVarIndexed(IDX_ATTACHMENT_WORK ,ARROW_INVISIBLE);
    else if(work > 0) SetVarIndexed(IDX_ATTACHMENT_WORK ,ARROW_GREEN_0);
    else if(work < 0) SetVarIndexed(IDX_ATTACHMENT_WORK ,ARROW_GREEN_1);
    else                 SetVarIndexed(IDX_ATTACHMENT_WORK ,ARROW_WHITE);
    //Steelblade
    if(extract == INACTIVE)  SetVarIndexed(IDX_ATTACHMENT_EXTRACT ,ARROW_INVISIBLE);
    else if(extract == 0) SetVarIndexed(IDX_ATTACHMENT_EXTRACT ,ARROW_WHITE);
    else if(Attachment == EQUIP_A)
    {
      if     (extract > 0) SetVarIndexed(IDX_ATTACHMENT_EXTRACT ,ARROW_GREEN_0);
      else if(extract < 0) SetVarIndexed(IDX_ATTACHMENT_EXTRACT ,ARROW_GREEN_1);
      //change pic if ATPIC_PLOUGH_TE
      if(Pic == ATPIC_PLOUGH_TE)
         Pic =  ATPIC_PLOUGH_TE_L;
    }
    else if(Attachment == EQUIP_C)
    {
      if     (extract > 0) SetVarIndexed(IDX_ATTACHMENT_EXTRACT ,ARROW_GREEN_1);
      else if(extract < 0) SetVarIndexed(IDX_ATTACHMENT_EXTRACT ,ARROW_GREEN_0);
      //change pic if ATPIC_PLOUGH_TE
      if(Pic == ATPIC_PLOUGH_TE)
         Pic =  ATPIC_PLOUGH_TE_R;
    }
    else                 SetVarIndexed(IDX_ATTACHMENT_EXTRACT ,ARROW_WHITE);

    if(sideupdown == INACTIVE) SetVarIndexed(IDX_ATTACHMENT_SIDEUPDOWN, ARROW_INVISIBLE);
    else if(sideupdown < 0 )   SetVarIndexed(IDX_ATTACHMENT_SIDEUPDOWN, ARROW_GREEN_1);
    else if(sideupdown > 0 )   SetVarIndexed(IDX_ATTACHMENT_SIDEUPDOWN, ARROW_GREEN_0);
    else                       SetVarIndexed(IDX_ATTACHMENT_SIDEUPDOWN, ARROW_WHITE);


    SetVarIndexed(IDX_ATTACHPIC, Pic);
  }
  else//switch off container, if no key is pressed.
  {
    if(IsInfoContainerOn(CNT_ATTACHMOVE))
    {
      InfoContainerOff(CNT_ATTACHMOVE);
    }
  }
}



void Prof_Draw(void)
{
  //SetIcons
  for(int32_t i = 0; i < MODULES_NUM; i++)
  {
    tProfileModule *ThisModule = &ProfModules[i].p[CurrentProfile->Module[i]];
    if(ThisModule != NULL)
    {
      if(CNT_EMPTY != ThisModule->Cnt)
      {

        if(!IsInfoContainerOn(ThisModule->Cnt))
        {
          InfoContainerOn(ThisModule->Cnt);
        }
      }
    }

  }
}


void ProfHide(void)
{
  //Close Container
  for(int32_t i = 0; i < MODULES_NUM; i++)
  {
    tProfileModule *ThisModule = &ProfModules[i].p[CurrentProfile->Module[i]];
    InfoContainerOff(ThisModule->Cnt);
  }
}

/**
 * @brief This function will generate a default name out of a configuration.
 *
 * @param prof
 */
void Prof_GenerateName(tProfile *prof)
{
  memset(prof->name, 0, sizeof(prof->name));
  for(uint32_t i = 0; i < GS_ARRAYELEMENTS(ProfModules); i++)
  {
    uint32_t idx = prof->Module[i];
    if(0 < idx)
    {
      if(idx < ProfModules[i].p_num)
      {
        const char * name = RCTextGetListElement(ProfModules[i].RCtListOffset, ProfModules[i].p[idx].RC_Name,GetVarIndexed(IDX_SYS_LANGUAGE));
        strncat(prof->name, name, sizeof(prof->name) - strlen(prof->name));
      }
    }
  }
}

tProfile * Profile_GetCurrentProfile(void)
{
  return CurrentProfile;
}


void Prof_SetCurrentProfile(tProfile *Prof)
{
  CurrentProfile = Prof;
}

tGSList * Profile_GetProfileList(void)
{
  return &ProfileList;
}



void Prof_Cycle(tControl *Ctrl,  uint32_t evtc, tUserCEvt *evtv)
{
  //Call init Functions of Profiles, if the profile changed.
  static tProfile *LastProfile = NULL;
  if(LastProfile != CurrentProfile)
  {
    LastProfile = CurrentProfile;
    if(NULL != CurrentProfile)
    {
      for(uint32_t i = 0; i < GS_ARRAYELEMENTS(CurrentProfile->Module); i++)
      {
          if(ProfModules[i].p[CurrentProfile->Module[i]].Init_cb)
          {
            ProfModules[i].p[CurrentProfile->Module[i]].Init_cb(Ctrl);
          }
      }
    }
  }
  //Call Cycle functions of the different moduels of the current modules.
  if(NULL != CurrentProfile)
  {
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(CurrentProfile->Module); i++)
    {
        if(ProfModules[i].p[CurrentProfile->Module[i]].Cycle_cb)
        {
          ProfModules[i].p[CurrentProfile->Module[i]].Cycle_cb(Ctrl, evtc,evtv);
        }
    }
  }
}


void Prof_Timer_10ms(tControl *Ctrl)
{
  //Call init Functions of Modules, if the profile changed.
  static tProfile *LastProfile = NULL;
  if(LastProfile != CurrentProfile)
  {
    LastProfile = CurrentProfile;
    if(NULL != CurrentProfile)
    {
      for(uint32_t i = 0; i < GS_ARRAYELEMENTS(CurrentProfile->Module); i++)
      {
          if(ProfModules[i].p[CurrentProfile->Module[i]].Init_cb)
          {
            ProfModules[i].p[CurrentProfile->Module[i]].Init_cb(Ctrl);
          }
      }
    }
  }
  //Call Timer Functions of Modules
  if(NULL != CurrentProfile)
  {
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(CurrentProfile->Module); i++)
    {
        if(ProfModules[i].p[CurrentProfile->Module[i]].Timer_cb)
        {
          ProfModules[i].p[CurrentProfile->Module[i]].Timer_cb(Ctrl);
        }
    }
  }
}


/**
 * @brief Gets idx of the current profile
 *
 * @param ProfSearch, Pointer of the profile to be searched for.
 */
int32_t Profile_GetIndex(tProfile *ProfSearch)
{
  tGSList *ProfList =  Profile_GetProfileList();
  if(ProfSearch == GSList_GetFirstData(ProfList))
    return 0;
  for(uint32_t i = 1; i < ProfList->numItems; i++)
  {
    if(ProfSearch == GSList_GetNextData(ProfList))
    {
      return i;
    }
  }
  return -1;
}

