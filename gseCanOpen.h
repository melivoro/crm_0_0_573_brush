﻿/************************************************************************
 *
 * File:        CANOPEN.h
 * Project:		EasyLibs
 * Author(s):	Marc Schartel
 * Date:		08.05.2018
 *
 * Description: Lib for creating a CanOpen-Master. Does not support the Object Directory
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CANOPEN_H
#define CANOPEN_H
#include "gsdebug.h"
#include <UserCAPI.h>

//#include "canopen.c"
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

#define CAN0          0          ///<
#define CAN1          1          ///<
#define CAN2          2          ///<
#define CAN3          3          ///<

#define CBIT11ID      0          ///<
#define CBIT29ID      1          ///<





#define PDO1           0
#define PDO2           1
#define PDO3           2
#define PDO4           3
#define PDO5           4
#define PDO6           5
#define PDO7           6
#define PDO8           7
#define PDO9           8
#define PDO10          9


#define CO_OBJ_DEVICETYPE     0x1000
#define CO_OBJ_ERRORREGSTER   0x1001
#define CO_OBJ_SYNC_COM_CYCLE 0x1006
#define CO_OBJ_MANDEVNAME     0x1008
#define CO_OBJ_MANHARDREV     0x1009
#define CO_OBJ_MANFIRMREV     0x100A
#define CO_OBJ_GUARD_TIME     0x100C
#define CO_OBJ_GUARD_FACTOR   0x100D
#define CO_OBJ_HEARTBEAT      0x1017
#define CO_OBJ_IDENTITYDATA   0x1018



#define TRANSMISSION_TYPE_SYNC         0
#define TRANSMISSION_TYPE_SYNC_NUM(x) (x & 0xFF)
#define TRANSMISSION_TYPE_ASYNC        0xFE


#define LSS_BAUDRATE_1000  0
#define LSS_BAUDRATE_800   1
#define LSS_BAUDRATE_500   2
#define LSS_BAUDRATE_250   3
#define LSS_BAUDRATE_125   4
#define LSS_BAUDRATE_100   5
#define LSS_BAUDRATE_50    6
#define LSS_BAUDRATE_20    7
#define LSS_BAUDRATE_10    8

/*************************
Debug Level
*************************/

/**
 * @brief Different Debuglevel to differ the number of Outputs
 *
 */
typedef enum eCO_DebugLevel{
  CO_DBGLVL_INFO,     /**< All informations*/
  CO_DBGLVL_WARNING,  /**< Warnings, Errors and critical Errors*/
  CO_DBGLVL_ERROR,    /**<  Errors and critical Errors*/
  CO_DBGLVL_CRITICAL, /**<  Critical Errors*/
}eCO_DebugLevel;

/*************************
Node Command
************************/
typedef enum eNMT_Command
{
  NMT_START_REMOTE_NODE      =     0x01, ///<Operational mode
  NMT_STOP_REMOTE_NODE       =     0x02, ///<Prepared mode
  NMT_ENTER_PRE_OPERATIONAL  =     0x80, ///<Pre-Operational Node
  NMT_RESET_NODE             =     0x81, ///<Command for rebooting the Node.
  NMT_RESET_COMMUNICATION    =     0x82, ///<Command for restarting the CAN Commuication.
}eNMT_Command;


/**
 * @brief Possible Node states of a CanOpenSlave device.
 *
 */
typedef enum eNodeState
{
  NODE_INITIALISATION = 0x01, ///<device intializes or booting
  NODE_STOPPED        = 0x04, ///<Device stopped by NMT_STOP_REMOTE_NODE
  NODE_OPERATIONAL    = 0x05, ///<Device is running and is full operation. Device can send and receive PDOs.
  NODE_PREOPERATIONAL = 0x7F, ///<Device started. SDOs can be exchanged. Sending PDOs doesn't work in this mode
  NODE_ERROR          = -1,   ///<Device is in error state and has to be resetted.
  NODE_UNKNOWN        = 0x02,
}eNodeState;

typedef enum ePDOConnectionSet
{
  PDO_CONNECTOINSET_DEFAULT,
  PDO_CONNECTOINSET_EMPTY,
}ePDOConnectionSet;



typedef enum eConfigType
{                               ///<Param 1             | Param 2             | value                      | Comment
                                ///<--------------------|---------------------|----------------------------|-------------------------------------------------------------------------------------------------
  PDO_CFG_TRANSMISSION_TYPE,    ///<PDO                 | 1 = TX-PDO of Slave | Use TRANSMISSION_TYPE_...  | Sets the PDO Transmission Mode. Async or Sync
  PDO_CFG_TRANSMISSION_ID,      ///<PDO                 | 1 = TX-PDO of Slave | New Transmission ID of PDO | Sets the ID for Sending/Receiving PDOs
  PDO_CFG_INHIBIT_TIME,         ///<PDO                 | always 1            | time in ms                 | minimum send time between two event triggert PDO-Messages
  PDO_CFG_EVENT_TIME,           ///<PDO                 | always 1            | time in ms                 | maximum time  a PDO won't be sent if it's event triggerts (ASYNC transmission)
  PDO_CFG_STORE_PARAMETERS,     ///<eCfgSlaveParameters | 0                   | 0                          | Restores the Parasmers of the node. Use eCfgSlaveParameters to select which parameters will be stored.
  PDO_CFG_RESTORE_PARAMETERS,   ///<eCfgSlaveParameters | 0                   | 0                          | Saves    the Parasmers of the node. Use eCfgSlaveParameters to select which parameters will be restored.
}eConfigType;


typedef enum eCfgSlaveParameters
{
  CFG_SLAVE_PARAMERTS_ALL = 1,
  CFG_SLAVE_PARAMERTS_COMMUNICATION,
  CFG_SLAVE_PARAMERTS_APPLICATION,
  CFG_SLAVE_PARAMERTS_MANUFACTURER,

}eCfgSlaveParameters;

//Objects

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

typedef int32_t tCanOpenMaster;
typedef void* tCanOpenSlave;

typedef union tagsData
  {
      uint32_t u32[2];
      int32_t  s32[2];
      uint16_t u16[4];
      int16_t  s16[4];
      uint8_t  u8[8];
      int8_t   s8[8];
  }tsData;



typedef struct tagEmcy
{
  uint16_t Code;
  uint8_t  Register;
  uint8_t  InfoByte[5];
}tEmcy;




typedef enum etagLSS
{
  LSS_START       = 0x00010004,
  LSS_ACT_NODE_ID = 0x5E,
  LSS_NEW_NODE_ID = 0x11,
  LSS_NEW_BAUDRATE= 0x13,
  LSS_SAVE        = 0x17,
  LSS_STOP        = 0x04,
  LSS_ERROR       = -1,
  LSS_NO_MSG      = 0,
}etLSS;



/* global function prototypes ********************************************/

/**
* @brief Initialises the CANOpen Master. Call this function befor adding Slaves
*
* @param Channel CAN-Interface CAN0, CAN1, ...
* @param Ext     Address extention Use CBIT11ID or CBIT29ID
* @param DbOut   Debugfunction lie PrintToDebug
*
* @return        Handle of the CanOpen-Master
*/
tCanOpenMaster  CO_Init( int32_t  Channel, int32_t Ext,  void (*DbOut)(const char *, ...));

/**
* @brief Initialises the CANOpen Master. Call this function befor adding Slaves
*
* @param Channel CAN-Interface CAN0, CAN1, ...
* @param Ext     Address extention Use CBIT11ID or CBIT29ID
* @param DbOut   Debugfunction lie PrintToDebug
*
* @return        Handle of the CanOpen-Master
*/

int32_t CO_SetDebugLevel(tCanOpenMaster Hdl, eCO_DebugLevel DebugLevel);

/**
* @brief Sets how many debug messages will be send to the debug-Function defined in CO_Init
*
* @param DebugLevel defines, which messages will be send to DbOut
* @return       1 if the debuglevel has been set successfully, else -1
*/
tCanOpenSlave   CO_AddSlave(int32_t Id,tCanOpenMaster Hdl, ePDOConnectionSet Set);


/**
* @brief Adds a TX-Pdo to a Slave.
*
* @param Slave   Handle of the Slave
* @param Hdl     Offset to the NodeID (0x200, 0x300, ...)

* @return        -1 if an error occured, >=0 index of the new PDO
*/
int32_t         CO_AddTXPdo( tCanOpenSlave Slave, int32_t Offset);

/**
* @brief Adds a RX-Pdo to a Slave.
*
* @param Slave   Handle of the Slave
* @param Hdl     Offset to the NodeID (0x180, 0x280, ...)

* @return        -1 if an error occured, >=0 index of the new PDO
*/
int32_t         CO_AddRXPdo( tCanOpenSlave Slave, int32_t Offset);

/**
* @brief Cycle of the CanOpen_Master. Should be called at the beginning of each
* UserCCycle.
*
* @param Hdl     Handle of the CanOpen-Master

*/
void            CO_Cycle(tCanOpenMaster Hdl);

/**
* @brief Returns the actual State of a node. To get the NodeState you have to activate
* Heardbeat or Guarding protocoll of the Slave
*
* @param Slave   Handle of the Slave
*
* @return        Actual Node State of the protocoll
*/
eNodeState CO_GetNodeState(tCanOpenSlave Slave);


/**
* @brief Sends a NMT-Command to a Slave to change the state.
*
* @param  Slave   Handle of the Slave
* @param  Command the NMT-Command
*
* @return 1 if the message was successfully put to the CAN send FiFo
*/
int32_t CO_NMT_Send( tCanOpenSlave Slave,eNMT_Command Command);

/**
* @brief Use this function to write data to an Object. The Data will be written into
* the SDO-Write fifo.
*
* @param  Slave    Handle of the Slave
* @param  Index    Index of the Object to be written
* @param  SubIndex SubIndex of the Object to be written
* @param  *data    Pointer to the Data to be written
* @param  size     Size of the data to be written
*
* @return 1 if the message was successfully put to the SDO-Write-Fifo
*/
int32_t CO_Sdo_Set(tCanOpenSlave Slave, int16_t Index, int8_t SubIndex, void *data, int32_t size);

/**
* @brief Use this function to request data from an Object. The Request will be written into
* the SDO-Write fifo. You have to use the funciton  CO_Sdo_Read to receive the message.
*
* @param  Slave    Handle of the Slave
* @param  Index    Index of the Object to be written
* @param  SubIndex SubIndex of the Object to be written

* @param  size     Enter the size here if, it is known. otherwise 0.
*
* @return 1 if the message was successfully put to the SDO-Write-Fifo
*/
int32_t CO_Sdo_Req(tCanOpenSlave Slave, int16_t Index, int8_t SubIndex, int32_t size);

/**
* @brief returns data read from a SOD-Object.
*
*
* @param  Slave     Handle of the Slave
* @param  *Index    The Index of the read object will be written into this.
* @param  *SubIndex The SubIndex of the read object will be written into this.
* @param  *Data     Pointer to where the data will be written to.
* @param  size      Maximum size of data which can be written to *Data
*
* @return           >0 size of data read form object, -1 if an SDO transmision
*                   has been aborted.
*/
int32_t CO_Sdo_Read(tCanOpenSlave Slave,int32_t *Index, int32_t *SubIndex, void * Data, size_t DataSize);

/**
* @brief Tells, if the Master and Slave are communicating via SDOs in the moment.
*
*
* @param  Slave     Handle of the Slave

*
* @return           1 if Slave is busy with communicating via SDOs
*/
int32_t CO_SDO_Busy(tCanOpenSlave Slave);

/**
* @brief returns the abort code of the last aborted SDO-Transmission.
*
*
* @param  Slave     Handle of the Slave

* @return           Abort code
*
*/
int32_t CO_Sdo_GetErrorCode(tCanOpenSlave Slave);


/**
* @brief returns the data of an received pdo-message. The function has to be called
* each UserCCycle.
*
* @param  Slave     Handle of the Slave
* @param  Pdo       PDO the data is comming from
* @param  *Data     where the data should be saved

* @return 1 if new data has been received and copied into Data.
*
*/
int32_t CO_Pdo_Read(tCanOpenSlave Slave, int32_t Pdo,tsData *Data);


/**
* @brief Receive emcy messgaes.The function has to be called
* each UserCCycle.
*
*
* @param  Slave         Handle of the Slave
* @param  *EmcyMessage  the EMCY-Message will be stored in this stucture.

* @return 1 if a new EMCY-Message has been received.
*
*/
int32_t CO_Emcy_Read(tCanOpenSlave Slave, tEmcy *EmcyMessage);

/**
* @brief Starts the node guarding protocoll at the Slave. The Master sets the required
* SDOs and begins to send Remote Telegramms automatically. To deactivate
* NodeGuarding, enter 0 for GuardTime and factor
*
* @param  Slave         Handle of the Slave
* @param  GuardTime     timeperiode for sending RTR-Messages
* @param  factor        if the Slave didn't receive a RTR-Message for
*                       (GuardTime *  factor) ms, it will go to the mode "Stopped"
*                       Use a value >= 2 for factor.
*
*/
void CO_NodeGuarding_Start(tCanOpenSlave Slave, int32_t GuardTime, int32_t facotor);

/**
* @brief Starts the Heartbeat protocoll. The Master sets the required
* SDOs to the node and monitores the state of the node.
*
* @param  Slave         Handle of the Slave
* @param  Time_ms       Timeperiode the Slave is sending a HeartBeat. Enter 0
                        for diabeling the Heartbeat protocoll.
* @param  factor        if the Slave didn't send a HeartBeat for
*                       (Time_ms *  factor) ms, the state of the Slave will be
*                       switched to "Error". Use a value >= 2 for factor.
*
*/
void CO_HeartBeat_Start   (tCanOpenSlave Slave, int32_t Time_ms, int32_t factor);

/**
* @brief The Master will beginn to send Sync_Message to the selected Node.
* Use the function CO_PdoCfg_SetParam to set a PDO to SYNC-Mode.
*
* @param  Slave         Handle of the Slave
* @param  CycleTime     Timeperiode a SYNC-Message will be sent to a Slave
*
*/
void CO_SYNC_Start(tCanOpenSlave Slave, int32_t CycleTime);

/**
* @brief Properties of the Slave and its PDOs can be set by this function.
*
* @param  Slave         Handle of the Slave
* @param  Pdo           of which PDO the Config is changed. First PDO = 1
*                       (only for some properties)
* @param  SlaveTx       1 for PDOs send from Slave, else 0
* @param  eType         type of propertie
* @param  value         new value of the propertie
*
* @return 0 at success, else -1
*/
int32_t  CO_PdoCfg_SetParam(tCanOpenSlave Slave,  eConfigType eType,int32_t Param_1, int32_t Param_2, int32_t value);

void CO_DeInit(void);

int32_t CO_Pdo_Send(tCanOpenSlave Slave,uint16_t PDO_Idx, tsData *Data);
/**
* @brief Changes the length of a PDO. Default length is 8.
*
* @param  Slave         Handle of the Slave
* @param  PDO_Idx       Of which PDO the Lenght will be changed. First PDO = 1
*                       (only for some properties)
* @param  len           length of pdo [0..8]
*
* @return 0 at success, else -1
*/
int32_t CO_Pdo_Send_SetLen(tCanOpenSlave Slave,uint16_t PDO_Idx, uint8_t len);
/***********************************
LSS
***********************************/

int32_t LSS_Start(tCanOpenMaster Hdl);
int32_t LSS_Stop(tCanOpenMaster Hdl);
int32_t LSS_Req_ID(tCanOpenMaster Hdl);
int32_t LSS_Set_ID(tCanOpenMaster Hdl, int8_t NodeId);
int32_t LSS_Set_Baudrate(tCanOpenMaster Hdl, int16_t Baudrate);
int32_t LSS_Save(tCanOpenMaster Hdl);
etLSS   LSS_Receive(tCanOpenMaster Hdl, int16_t *data);






/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CANOPEN_H
