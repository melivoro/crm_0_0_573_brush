﻿/************************************************************************
 *
 * File:         CTRL_REAR_ATTACH.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CTRL_REAR_ATTACH_H
#define CTRL_REAR_ATTACH_H
#include "control.h"
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/

void Ctrl_Rear_OZP221_Init(tControl *Ctrl);

void Ctrl_Rear_Brush_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);
void Ctrl_Rear_Brush_Timer_10ms(tControl *Ctrl);

/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CTRL_REAR_ATTACH_H
