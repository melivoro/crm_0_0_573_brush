﻿/**
 * @file subConsole.h
 *
 * This lib supports you a console.
 * 

 * @author Marc Schartel<br>
 *         Graf-Syteco GmbH & Co. KG
 *
 * @date   created: 11.10.2016
 * 
 */
 
/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef SUBCONSOLE_H
#define SUBCONSOLE_H

//-----------------------------------------------------------------------------
//Includes
//-----------------------------------------------------------------------------

#include "gsSocketTCPServer.h"

//-----------------------------------------------------------------------------
//Datatypes
//-----------------------------------------------------------------------------

/**
 * Function Pointer for an Output stream working similar to printf
 */
typedef void  (*tCon_StreamOut)(const char *string, ...);


/**
 * Function Pointer of funcitons which can be called by the console.
 */
typedef void  (*tCon_ComFunc)( uint32_t Hdl, char ** data);

 /**
 * Functions which can be called in the console are listed in arrays of this type
 */
typedef struct tagsCon_ComArray
{
 const char command[64];            //console command to call the function
 int32_t arg;                       //nuber of arguments
 tCon_ComFunc function;             //pointer to the called function
 const char description[128];       //description of function for help
}tsCon_ComArray;


 /**
 * Initializes the streams for the console. 
 *
 * 
 *
 * @param BufSize       Size of the buffer, for incomming Console commands
 * @param StreamOut_cb  Callback function for Stream Output
 *
 * @return              Handle of the console. importent for using further console functions. 
 */
int32_t sCon_InitStream(int32_t BufSize, tCon_StreamOut StreamOut_cb);

 /**
 * Adds an Array of functions, which can be called by the console
 *
 * 
 *
 * @param Hdl           Handle of the console
 * @param ComArray      Array of the commands
 * @param numComArray   Size of array
 * 
 */
void    sCon_AddCommandList(uint32_t Hdl, tsCon_ComArray *ComArray, int32_t numComArray);

 /**
 * Incoming commands have to be passed to this function.  
 *
 * 
 *
 * @param Hdl           Handle of the console
 * @param string        incomming string
 * @param n             size of string
 *
 * @return              -1 if an error occured, else 0
 */
int32_t sCon_GetString (uint32_t Hdl,  char * string, int32_t n);

 /**
 * Writes a string to the console. Use this function for own created Conolse commands 
 *
 * 
 *
 * @param Hdl           Handle of the console
 * @param fmt           formated string like printf
 * 
 *
 * @return              -1 if an error occured, else 0
 */
void    sCon_WriteString(uint32_t Hdl, const char * fmt,...);


/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef SUBCONSOLE_H
