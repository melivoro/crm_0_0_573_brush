﻿/****************************************************************************
 *
 * File:         CTRL_INTER_ATTACH.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "control.h"
#include "io.h"
#include "Ctrl_Brush.h"

/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Ctrl_Inter_OMP253_Init(tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach   = &Ctrl->ABC[EQUIP_B].ABC_Attach;
    Brush_UpDown_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach,MCM250_2, 1,-1,-1);
    Brush_Float_Init( ABC_Attach  , MCM250_2,8);
    Brush_Rotate_Init( ABC_Attach  , MCM250_1, 3);
    Brush_LeftRight_Init(ABC_Attach  ,-1,-1,-1,-1 );
    Brush_ExRetract_Init(ABC_Attach  , -1,-1 ,-1, -1);
    Brush_SideSections_Init(ABC_Attach  , -1,-1,-1,-1);
    ABC_Attach->MaxSpeed = 35;
    Brush_A_B(ABC_Attach, PVE_PORT_A);
}


void Ctrl_Inter_OMP220_Init(tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach   = &Ctrl->ABC[EQUIP_B].ABC_Attach;
    Brush_UpDown_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach,MCM250_2, 1,-1,-1);
    Brush_Float_Init( ABC_Attach  , MCM250_2,8);
    Brush_Rotate_Init( ABC_Attach  , MCM250_1, 3);
    Brush_LeftRight_Init(ABC_Attach  ,-1,-1,-1,-1 );
    Brush_ExRetract_Init(ABC_Attach  , -1,-1 ,-1, -1);
    Brush_SideSections_Init(ABC_Attach  , -1,-1,-1,-1);
    Brush_A_B(ABC_Attach, PVE_PORT_A);
    ABC_Attach->MaxSpeed = 35;
}

void Ctrl_Inter_OZP231_Init(tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach   = &Ctrl->ABC[EQUIP_B].ABC_Attach;
    Brush_UpDown_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach,MCM250_2, 1,-1,-1);
    Brush_Float_Init( ABC_Attach  , MCM250_2,8);
    Brush_Rotate_Init( ABC_Attach  , MCM250_1, 3);
    Brush_LeftRight_Init(ABC_Attach  ,-1,-1,-1,-1 );
    Brush_ExRetract_Init(ABC_Attach  , -1,-1 ,-1, -1);
    Brush_SideSections_Init(ABC_Attach  , -1,-1,-1,-1);
    Brush_A_B(ABC_Attach,PVE_PORT_A);
    ABC_Attach->MaxSpeed = 30;
}

void Ctrl_Inter_CH2600_Init(tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach   = &Ctrl->ABC[EQUIP_B].ABC_Attach;
   // Plough_UpDown_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach,MCM250_2,2,MCM250_2,0);
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach,MCM250_2, 1,-1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach,0, 0 );
    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach, -1,-1,-1,-1);
    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach,-1,-1,-1,-1 );
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_B].ABC_Attach, -1,-1,-1,-1);
    ABC_Attach->MaxSpeed = 50;

}

void Ctrl_Inter_Brush_Draw(tControl *Ctrl)
{
    if(Brush_Active(&Ctrl->ABC[EQUIP_B].ABC_Attach))
    {
        static int32_t _time = 0;
        if(250 < GetMSTick()-_time)
        {
            _time = GetMSTick();
            int32_t pos = GetVarIndexed(IDX_BRUSHON_INTER) + 1;
            if(pos > 2)
            {
                pos = 1;
            }
            SetVarIndexed(IDX_BRUSHON_INTER,pos);
        }
    }
    else
    {
        SetVarIndexed(IDX_BRUSHON_INTER, 0);
    }

    if(Valv_GetPos(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveFloat) > 0)
        SetVarIndexed(IDX_FLOATING_INTERAX,1 );
    else
        SetVarIndexed(IDX_FLOATING_INTERAX,0 );
}

void Ctrl_Inter_Brush_Standard_Cycle(tControl*Ctrl, uint32_t evtc,tUserCEvt *evtv)
{
    Brush_Cycle(&Ctrl->ABC[EQUIP_B].ABC_Attach, &Ctrl->CmdABC[EQUIP_B]);
    Ctrl_Inter_Brush_Draw(Ctrl);
}

void Ctrl_Inter_Plough_Standard_Cycle(tControl*Ctrl, uint32_t evtc,tUserCEvt *evtv)
{
    Brush_Cycle(&Ctrl->ABC[EQUIP_B].ABC_Attach, &Ctrl->CmdABC[EQUIP_B]);

}

void Ctrl_Inter_Plough_Timer_10ms(tControl *Ctrl)
{
    static int32_t count = 0;
    count++;
    if(0 == (count %10))
    {
        Valv_Timer_100ms(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveUpDown);
    }
}
void Ctrl_Inter_Brush_Timer_10ms(tControl *Ctrl)
{
    static int32_t count = 0;
    count++;
    if(0 == (count %10))
    {
        Valv_Timer_100ms(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveUpDown);
    }
}
