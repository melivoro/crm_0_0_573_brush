﻿/****************************************************************************
 *
 * Project:   name
 *
 * @file      filename.c
 * @author    author
 * @date      [Creation date in format %02d.%02d.20%02d]
 *
 * @brief     description
 *
 ****************************************************************************/

#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definitions:                    */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project.            */
                          /* vartab.h is generated automatically.           */

#include "objtab.h"       /* Object ID definitions:                         */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects.         */
                          /* objtab.h is generated automatically.           */
#include "Ctrl_Top_ADrive.h"
#include "gseMCM.h"
#include "control.h"
#include "param.h"
/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes ************************************************/

/* functions ****************************************************************/

/****************************************************************************
 * @brief  description
 *
 * @param  yourParam1
 * @param  yourParam1
 * @return none
 ****************************************************************************/
#define ASYM_DRIVE_MOVE_STOP  0
#define ASYM_DRIVE_MOVE_LEFT  1
#define ASYM_DRIVE_MOVE_RIGHT 2


#define ASYM_DRIVE_LEFT  ( -ASYM_DRIVE_TIME)
#define ASYM_DRIVE_RIGHT    ASYM_DRIVE_TIME
#define ASYM_DRIVE_HOME (2 * ASYM_DRIVE_RIGHT)

#define ASYM_DRIVE_STARTENGINE_TIME 400

void AsymerticDrive_Init(tAsymerticDrive *ad, uint8_t MCM_Device_Engine, uint8_t MCM_Output_Engine, uint8_t MCM_Device_Direction, uint8_t MCM_Output_Direction)
{
  ad->Engine_Device =  MCM_Device_Engine;
  ad->Engine_Output =  MCM_Output_Engine;
  ad->Polarity_Device =  MCM_Device_Direction;
  ad->Polarity_Output =  MCM_Output_Direction;
  ad->PosActual       =  ASYM_DRIVE_RIGHT;
  ad->Pos             =  ASYM_DRIVE_TIME;
}



void AsymetricDrive_Move(tAsymerticDrive *ad ,uint8_t  Direction)
{
  if(ASYM_DRIVE_MOVE_STOP == Direction)
  {
      GSeMCM_SetDigitalOut(ad->Engine_Device, ad->Engine_Output, 0);
      GSeMCM_SetDigitalOut(ad->Polarity_Device, ad->Polarity_Output, 0);
  }
  else if(ASYM_DRIVE_MOVE_LEFT == Direction)
  {
      ad->MaxRightPos = 0;
      if(!ad->MaxLeftPos)//only if it is not at the left.
      {
        GSeMCM_SetDigitalOut(ad->Engine_Device, ad->Engine_Output, 1);
        GSeMCM_SetDigitalOut(ad->Polarity_Device, ad->Polarity_Output, 0);
      }
  }
  else if(ASYM_DRIVE_MOVE_RIGHT == Direction)
  {
      ad->MaxLeftPos = 0;
      if(!ad->MaxRightPos)//only if it is not at the right.
      {
        GSeMCM_SetDigitalOut(ad->Engine_Device, ad->Engine_Output, 1);
        GSeMCM_SetDigitalOut(ad->Polarity_Device, ad->Polarity_Output, 1);
      }
  }
  if(Direction != ad->Direction_Old)
  {
    ad->Direction_Old = Direction;
    ad->I_Max_Time = GetMSTick() + ASYM_DRIVE_STARTENGINE_TIME;
  }
}

void AsymetricDrive_CurrentControle_Init(tAsymerticDrive *ad, int32_t mA_Max)
{
  ad->I_Max = mA_Max;
}

int32_t AsymetricDrive_CurrentControle(tAsymerticDrive *ad)
{
  if(!ad->I_Max)
  {
    ad->I_Max_Time = GetMSTick();
  }
  int16_t mA = GSeMCM_GetAnalogIn(ad->Engine_Device, ad->Engine_Output + 4);


  if(ad->I_Max)
  {
    //check if start time is still running
    if( 0 > (int32_t)(GetMSTick() - ad->I_Max_Time)) //if I_Max_Time > GetMSTick()
    {

      return mA;
    }
    if(mA < ad->I_Max)//reset timeout if in parameter.
    {
      ad->I_Max_Time = GetMSTick();
      return mA;
    }

    if(50 < GetMSTick() - ad->I_Max_Time)   // if overcurrent for more than 100 ms
    {

      //switch of engine.
      if(ad->Direction_Old == ASYM_DRIVE_MOVE_RIGHT)
      {
        ad->PosActual   = ASYM_DRIVE_RIGHT;
        ad->MaxRightPos = 1;
        ad->I_Max_Time  = GetMSTick();
        AsymetricDrive_Move(ad,ASYM_DRIVE_MOVE_STOP);
      }
      else if(ad->Direction_Old == ASYM_DRIVE_MOVE_LEFT)
      {
        ad->PosActual = ASYM_DRIVE_LEFT;
        ad->MaxLeftPos = 1;
        ad->I_Max_Time = GetMSTick();
        AsymetricDrive_Move(ad,ASYM_DRIVE_MOVE_STOP);
        // stop goning left at switch off.
      }
    }
  }
  return mA;
}


void AsymerticDrive_Timer_10ms(tAsymerticDrive *ad)
{

  if((!ad->On))
  {
    //go home

    if( ad->PosActual >= ASYM_DRIVE_HOME)
    {
      ad->PosActual = ASYM_DRIVE_RIGHT;
      if( ad->Direction_Old == ASYM_DRIVE_MOVE_RIGHT)
      {
        AsymetricDrive_Move(ad,ASYM_DRIVE_MOVE_STOP);
      }
    }
    //change visu
    if( ad->Direction_Old == ASYM_DRIVE_MOVE_RIGHT)
    {
      ad->PosActual  = ad->PosActual + 10;
    }
  }
  else if(ad->On)
  {

    //direction
    if(10 < (ad->Pos - ad->PosActual))
    {
      AsymetricDrive_Move(ad,ASYM_DRIVE_MOVE_RIGHT);
      ad->PosActual  = ad->PosActual + 10;
    }
    else if(-10 > (ad->Pos - ad->PosActual))
    {
      AsymetricDrive_Move(ad,ASYM_DRIVE_MOVE_LEFT);
      ad->PosActual  = ad->PosActual - 10;
    }
    else
    {
      AsymetricDrive_Move(ad,ASYM_DRIVE_MOVE_STOP);
    }
  }
  SetVar(HDL_TESTVAR_3,  AsymetricDrive_CurrentControle(ad));
  SetVar(HDL_TESTVAR_4, ad->Direction_Old);
  const tParamData * Param = ParameterGet();
  SetVar(HDL_TESTVAR_5, Param->AsymDrive.I_Max);
}




//Open Asymetic Drive and go to middle position.
void AsymeticDriveSwitchOn(tAsymerticDrive *ad)
{
  if(!ad->On)
  {
      ad->On = 1;
      ad->Pos = 0;
  }
}

//Swith Asymetic drive off, and set Endposition.
void AsymeticDriveSwitchOff(tAsymerticDrive *ad)
{
  if(ad->On)
  {

    ad->On = 0;
    ad->Pos = ASYM_DRIVE_HOME;
    AsymetricDrive_Move(ad,ASYM_DRIVE_MOVE_RIGHT);

  }
}


/**
 * @brief This will change the nominal position of the asymetic drive.
 * It has 5 position. Left, Half left, Middle, Half right, Right.
 * @param MovePos 0 = don't change position.
 *               -1 One position to the right,
 *               1 One position to the left.
 */
void AsymetricDrive_Command(tAsymerticDrive *ad,int32_t MovePos )
{
  if(ad->On)
  {
    if(-1 == MovePos)
    {
      ad->Pos = ad->Pos + ASYM_DRIVE_RIGHT/2;
    }
    else if(1 == MovePos)
    {
      ad->Pos = ad->Pos + ASYM_DRIVE_LEFT/2;
    }

    if(ad->Pos < ASYM_DRIVE_LEFT)
    {
      ad->Pos = ASYM_DRIVE_LEFT;
    }
    if(ad->Pos > ASYM_DRIVE_RIGHT)
    {
      ad->Pos = ASYM_DRIVE_RIGHT;
    }
  }
}



void AsymetricDrive_Draw(const tAsymerticDrive *ad)
{
  //Spreading Sector,
  const tControl *Ctrl = CtrlGet();
  if((Ctrl->Top.Conveyor.on)
  ||(ad->PosActual < (ASYM_DRIVE_RIGHT - 10 ))) //Actual position is shown in 5 directions. left, half left, center, half right, right. Home position is to the right
  {
    SetVarIndexed(IDX_SPREADINGSECTOR, 1 + (ad->Pos + ASYM_DRIVE_TIME)*5/(2 * ASYM_DRIVE_TIME));
    SetVarIndexed(IDX_SPREADINGSECTOR_ACT, 1 + (ad->PosActual + ASYM_DRIVE_TIME)*5/(2 * ASYM_DRIVE_TIME));
  }
  else//no colour, if  conveyor off.
  {
    SetVarIndexed(IDX_SPREADINGSECTOR,0);
    SetVarIndexed(IDX_SPREADINGSECTOR_ACT,0);
  }
}
