﻿/****************************************************************************
 *
 * File:         VISU_PROFILE.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu.h"
#include "profile.h"
#include "gsToVisu.h"
#include "GSe_MsgBox.h"
#include "RCText.h"

/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

uint32_t ProfileTextList[8] = {
                      OBJ_PROFILENAME_1,
                      OBJ_PROFILENAME_2,
                      OBJ_PROFILENAME_3,
                      OBJ_PROFILENAME_4,
                      OBJ_PROFILENAME_5,
                      OBJ_PROFILENAME_6,
                      OBJ_PROFILENAME_7,
                      OBJ_PROFILENAME_8
};

uint32_t offset = 0;

tProfile *ProfileEdit;
tProfile *ProfileSelect;


void Profile_Change_Draw(void)
{

  SetVisObjData ( OBJ_PROFILECHANGEACTUALPROFILE,Profile_GetCurrentProfile()->name,strlen(Profile_GetCurrentProfile()->name)+1 );
  tGSList *List = Profile_GetProfileList();
  //read first profile;
  tProfile * Profile =  GSList_GetFirstData(List);
  uint32_t i;

  for(i = 0 ; i < GS_ARRAYELEMENTS(ProfileTextList); i++)
  {
    if(NULL == Profile )
    {
      break;
    }
    SetVisObjData(ProfileTextList[i], Profile->name, strlen(Profile->name)+1);
    Profile = GSList_GetNextData(List);
    SendToVisuObj(ProfileTextList[i], GS_TO_VISU_SET_ATTR_ALL,   ATTR_VISIBLE |ATTR_TRANSPARENT | ATTR_NOMINAL);

  }
  //fill empty entries;
  for(; i < GS_ARRAYELEMENTS(ProfileTextList); i++)
  {
    SetVisObjData(ProfileTextList[i], " ", strlen(" ")+1);
    SendToVisuObj(ProfileTextList[i],GS_TO_VISU_SET_ATTR_ALL, 0);
  }
}

void Visu_Profile_Open(const tVisuData * VData)
{
  PrioMaskOn(MSK_PROFILE_CHANGE);
  Profile_Change_Draw();
}

tProfile * Visu_Profile_GetSelectedProfile(uint32_t line)
{
  tGSList *List = Profile_GetProfileList();
  tProfile *Profile = GSList_GetFirstData(List);
  line = line + offset;
  if(line >= List->numItems)
    return NULL;
  for(uint32_t k = 0; k < line; k++)
  {
    Profile = GSList_GetNextData(List);
    if(Profile == NULL)
    {
      break;
    }
  }
  return Profile;

}


void Visu_Profile_Add(void)
{
  tGSList *List = Profile_GetProfileList();
  GSList_GetLastData(List);
  tProfile Profile;
  memset(&Profile, 0, sizeof(tProfile));
  strncpy(Profile.name, RCTextGetText(RCTEXT_T_NEWPROFILE, GetVarIndexed(IDX_SYS_LANGUAGE)), sizeof(Profile.name));
  GSList_AddData(List, &Profile, sizeof(tProfile));
  Profile_Change_Draw();
}

void Visu_ProfileDel(void)
{
  if(NULL != ProfileSelect)
  {
    tGSList *List = Profile_GetProfileList();
    GSList_DeleteData(List, ProfileSelect);
    if(ProfileSelect == Profile_GetCurrentProfile())
    {
      Prof_SetCurrentProfile(GSList_GetFirstData(List));
    }
    ProfileSelect = NULL;
    Profile_Change_Draw();
  }
}

void Visu_Profile_OpenEdit_cb(void *arg)
{
  Visu_OpenScreen(SCREEN_PROFILE_EDIT);
}




void Visu_Profile_MsgBoxNewProfile_cb(void *arg)
{
  Prof_SetCurrentProfile(ProfileSelect);
  Profile_Change_Draw();
  int32_t ProfIdx = Profile_GetIndex(ProfileSelect);
  if(-1 < ProfIdx)
    SetVarIndexed(IDX_CURRENTPROFILE, ProfIdx); //saves the idx of the current profile as remanent variable
}

/*
*/

void Visu_Profile_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{
  Visu_HomeKeyPressed();
  for(uint32_t i = 0; i < evtc; i++)
  {
    if(CEVT_MENU_INDEX == evtv[i].Type)
    {
      for (uint32_t j = 0; j < GS_ARRAYELEMENTS(ProfileTextList); j++)
      {
        if(ProfileTextList[j] == evtv[i].Content.mMenuIndex.ObjID)
        {

          tProfile *Profile = Visu_Profile_GetSelectedProfile(j);
          if(NULL  != Profile)
          {
            const char * Head = RCTextGetText(RCTEXT_T_CHANGEPROFILE, GetVarIndexed(IDX_SYS_LANGUAGE));
            char text[64];
            snprintf(text, sizeof(text),RCTextGetText(RCTEXT_T_USETHISPROFILE, GetVarIndexed(IDX_SYS_LANGUAGE)), Profile->name);

            MsgBoxOkCancel(Head, text, Visu_Profile_MsgBoxNewProfile_cb, NULL, NULL, NULL );

          }
        }
      }
    }
    else if(CEVT_SELECTED == evtv[i].Type)
    {
      for (uint32_t j = 0; j < GS_ARRAYELEMENTS(ProfileTextList); j++)
      {
        if(ProfileTextList[j] == evtv[i].Content.mSelected.ObjID)
        {

          tProfile *Profile = Visu_Profile_GetSelectedProfile(j);
          if(NULL  != Profile)
          {

            ProfileSelect = Profile;
            break;
          }
        }
      }
    }
  }
  if(GetVarIndexed(IDX_ATT_USER_ADMIN)||GetVarIndexed(IDX_ATTR_USER_MERKATOR))
  {

if(IsKeyPressedNew(3))
    {
      Prof_Save();
    }

    if(IsKeyPressedNew(5))
    {
      if(1 == Prof_CopyToUsb())
      {
        MsgBoxOK_RCText(RCTEXT_T_INFO, RCTEXT_T_COPYTOUSBSUCCESS, NULL, NULL);
      }
      else  MsgBoxOK_RCText(RCTEXT_T_INFO, RCTEXT_T_COPYTOUSBERROR, NULL, NULL);
    }
    if(IsKeyPressedNew(6))
    {
      if(1 == Prof_LoadFromUsb())
      {
        MsgBoxOK_RCText(RCTEXT_T_INFO, RCTEXT_T_COPYFROMUSBSUCCESS, NULL, NULL);
      }
      else  MsgBoxOK_RCText(RCTEXT_T_INFO, RCTEXT_T_COPYFROMUSBERROR, NULL, NULL);
      Prof_Load();
      Profile_Change_Draw();
    }
  }

  if (GetVarIndexed(IDX_ATTR_USER_MERKATOR))
  {
    if(IsKeyPressedNew(1))
    {
      Visu_Profile_Add();
    }

    if(IsKeyPressedNew(2))
    {
      Visu_ProfileDel();
    }
    if(IsKeyPressedNew(4))
    {
      if(NULL != ProfileSelect)
      {
        ProfileEdit = ProfileSelect;
        Visu_OpenScreen(SCREEN_PROFILE_EDIT);
      }
    }
  }
}


void Visu_ProfileEdit_ToVisu(void)
{
  if(NULL != ProfileEdit)
  {
    SetVarIndexed(IDX_ATTACH_TOP, ProfileEdit->Module[0]);
    SetVarIndexed(IDX_ATTACH_A , ProfileEdit->Module[1]);
    SetVarIndexed(IDX_ATTACH_B, ProfileEdit->Module[2]);
    SetVarIndexed(IDX_ATTACH_C  , ProfileEdit->Module[3]);


    SetVisObjData(OBJ_PROFILEEDITNAME, ProfileEdit->name, strlen(ProfileEdit->name)+1);
  }
}

void Visu_ProfileEdit_OK(void *arg)
{
  if(NULL != ProfileEdit)
  {
    ProfileEdit->Module[0] = GetVarIndexed(IDX_ATTACH_TOP);
    ProfileEdit->Module[1] = GetVarIndexed(IDX_ATTACH_A);
    ProfileEdit->Module[2] = GetVarIndexed(IDX_ATTACH_B);
    ProfileEdit->Module[3] = GetVarIndexed(IDX_ATTACH_C);

    GetVisObjData(OBJ_PROFILEEDITNAME,  ProfileEdit->name, sizeof(ProfileEdit->name));
  }
  Visu_OpenScreen(SCREEN_PROFILE);
}


void Visu_ProfileEdit_Cancel(void *arg)
{
  Visu_OpenScreen(SCREEN_PROFILE);
}

void Visu_ProfileEdit_MsgBoxClose(void)
{
  if(!MsgBox_IsAnyBoxActive())
  {
    MsgBoxOkCancel(RCTextGetText(RCTEXT_T_CHANGEPROFILE, GetVarIndexed(IDX_SYS_LANGUAGE)),
                   RCTextGetText(RCTEXT_T_CHANGEPROFILETEXT, GetVarIndexed(IDX_SYS_LANGUAGE)),
                   Visu_ProfileEdit_OK, NULL, Visu_ProfileEdit_Cancel, NULL );
  }
}

void Visu_ProfileEdit_Open(const tVisuData * VData)
{
  Visu_ProfileEdit_ToVisu();
  PrioMaskOn(MSK_PROFILEEDIT);
}



void Visu_ProfileEdit_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{
  if(IsKeyPressedNew(10))
  {
    Visu_ProfileEdit_MsgBoxClose();
    Profile_Change_Draw();
  }
}

