﻿/************************************************************************
 *
 * File:         DRAWFUNCTIONS.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef DRAWFUNCTIONS_H
#define DRAWFUNCTIONS_H

/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
typedef struct tagPaintText
{
  uint32_t colour_active;
  uint32_t colour_inactive;
  uint32_t obj;
  uint32_t val_old;
}tPaintText;

void PaintText_Cycle(tPaintText *pt,int8_t active);
void PaintText_Init(tPaintText *pt,  uint32_t colour_active,  uint32_t colour_inactive,  uint32_t obj);

/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef DRAWFUNCTIONS_H
