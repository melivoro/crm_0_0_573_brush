﻿/****************************************************************************
 *
 * File:         VISU_SENSORS.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu.h"
#include "visu_sensors.h"
#include "control.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Visu_Sensor_Open(const tVisuData *VData)
{
  PrioMaskOn(MSK_ACTIVESENSORS);
}

void Visu_Sensor_Cycle(const tVisuData *VData, uint32_t evtc, tUserCEvt *evtv)
{
  const tControl * Ctrl = CtrlGet();
  static int32_t timer = 0;
  if(333 < (GetMSTick()  - timer))
  {
    timer = GetMSTick();
    SetVarIndexed(IDX_IO_VIEW_DI_00, Ctrl->Sensors.DI.Spreading);
    SetVarIndexed(IDX_IO_VIEW_DI_01, Ctrl->Sensors.DI.Humidifier);
    SetVarIndexed(IDX_IO_VIEW_DI_02, Ctrl->Sensors.DI.MaterialPresense);
    SetVarIndexed(IDX_IO_VIEW_DI_03, Ctrl->Sensors.DI.Ignition);
    SetVarIndexed(IDX_IO_VIEW_DI_04, Ctrl->Emcy.value);
    SetVarIndexed(IDX_IO_VIEW_DI_05, Ctrl->Sensors.DI.OilFilter);
    SetVarIndexed(IDX_IO_VIEW_DI_06, Ctrl->Sensors.DI.UpperOilLevel);
    SetVarIndexed(IDX_IO_VIEW_DI_07, Ctrl->Sensors.DI.LowerOilLevel);
    SetVarIndexed(IDX_IO_VIEW_DI_08, Ctrl->Sensors.DI.CheckOilLevelSensor);

    SetVarIndexed(IDX_IO_VIEW_AI_00, Ctrl->Sensors.Freq.ConveryRotation); //AK NOT TESTED div by reductor ratio returned 0. How to make it showing decimals

    SetVarIndexed(IDX_IO_VIEW_AI_01, (IOT_NormAI(&Ctrl->Sensors.AI.OilTemp)+5) / 10 * 10);
    SetVarIndexed(IDX_IO_VIEW_AI_02, IOT_NormAI(&Ctrl->Sensors.AI.OilPressure));
  }
  Visu_HomeKeyPressed();
}
