﻿/************************************************************************
 *
 * File:         CTRL_ABC_ATTACH.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CTRL_ABC_ATTACH_H
#define CTRL_ABC_ATTACH_H
#include "gseMaint_Runtime.h"
#include "hydvalvectrl.h"
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
typedef struct tagABCAttach_
{
    tMaintRuntime * MaintRunntime;

    tValv ValveUpDown;
    tValv ValveUpDownSecondary;
    tValv ValveFloat;
    tValv ValveRotation;
    tValv ValveLeftRight;
    tValv ValveExRetract;
    tValv ValveSteelUpDown;
    tValv ValveTilt;
    tValv ValveWorkTransport;
    tValv ValveSideUpDown;
    tValv ValveSecondSection;

    
    uint32_t ValuePWM; //(добавлено)
    uint32_t timer_cnt;
    uint32_t MaxSpeed;
    int8_t   BrushInvert;
}tABC_Attach;

void ABC_Attach_Clear(tABC_Attach *ABC_Attach);

/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CTRL_ABC_ATTACH_H
