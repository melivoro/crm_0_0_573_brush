﻿/************************************************************************
 *
 * File:         CTRL_BRUSH.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CTRL_BRUSH_H
#define CTRL_BRUSH_H

/*************************************************************************/

#include "gseMaint_Runtime.h"
#include "HydValveCtrl.h"
#include "io_types.h"
#include "errorlist.h"
#include "commands.h"
#include "Ctrl_ABC_Attach.h"
/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/

/**
 * @brief Creates the posibility for maintanance for the brush.
 *
 * @param Brush Pointer to a Structure of the type tABC_Attach
 * @param Maint Pointer to a Structure of the type tMaint. With this function, conditions will be added to the maint-structure.
 * @param ID    Error ID for a error messaage. A Warning and a Service message will be set with the same error id.
 * @param RCText_Warn RC-Text of a Warning. The Warning will become active, 500 h before the next service.
 * @param RCText_Service RC-Text for Service.
 * @param HdlRemVar_Warn Remanent variable to save the time for a warning
 * @param Hdl_RemVar_Service Remanent variable to save the time of the service
 */
void Brush_Maint_Create(tABC_Attach *Brush, tMaint *Maint,eErrorID ID,  uint32_t RCText_Warn, uint32_t RCText_Service , uint32_t HdlRemVar_Warn, uint32_t Hdl_RemVar_Service);

/**
 * @brief Sets the Valve for UP-Down movement.
 * If Device1 != -1 Device2 == -1     , an propotional valve will be initialized
 * If Device1 != -1 Device2 != -1     , two discret valves will be initialized
 * If Device1 == -1 Device2 == -1     , no valves will be initialized
 * @param Brush
 * @param Device1 MCM-Device
 * @param Pos1    Index of the digital Output
 * @param Device2 MCM-Device
 * @param Pos2    Index of the digital Output
 */
void Brush_UpDown_Init(tABC_Attach * Brush,uint32_t Device1, uint32_t Pos1,uint32_t Device2, uint32_t Pos2 );


/**
 * @brief Sets the valve for floating
 *
 * @param Brush
 * @param Device Index of the digital Output
 * @param Pos    MCM-Device
 */
void Brush_Float_Init(tABC_Attach * Brush, int32_t Device, int32_t Pos);

/**
 * @brief Sets the valve for Rotation
 *
 * @param Brush
 * @param Device Index of the digital Output
 * @param Pos    MCM-Device
 */
void Brush_Rotate_Init(tABC_Attach * Brush, int32_t Device, int32_t Pos);

/**
 * @brief Sets the Valve for Left-Right movement.
 * If Device1 != -1 Device2 == -1     , an propotional valve will be initialized
 * If Device1 != -1 Device2 != -1     , two discret valves will be initialized
 * If Device1 == -1 Device2 == -1     , no valves will be initialized
 * @param Brush
 * @param Device1 MCM-Device
 * @param Pos1    Index of the digital Output
 * @param Device2 MCM-Device
 * @param Pos2    Index of the digital Output
 */
void Brush_LeftRight_Init(tABC_Attach *Brush, uint32_t DeviceTurnLeft, uint32_t PosTurnLeft,  uint32_t DeviceTurnRight, uint32_t PosTurnRight );

/**
 * @brief Sets the Valve for movement of the side section.
 * If Device1 != -1 Device2 == -1     , an propotional valve will be initialized
 * If Device1 != -1 Device2 != -1     , two discret valves will be initialized
 * If Device1 == -1 Device2 == -1     , no valves will be initialized
 * @param Brush
 * @param Device1 MCM-Device
 * @param Pos1    Index of the digital Output
 * @param Device2 MCM-Device
 * @param Pos2    Index of the digital Output
 */
void Brush_SideSections_Init(tABC_Attach *Brush, uint32_t DeviceUp, uint32_t PosUp,  uint32_t DeviceDown, uint32_t PosDown );

/**
 * @brief Sets the Valve for Extracting and Retracting
 * If Device1 != -1 Device2 == -1     , an propotional valve will be initialized
 * If Device1 != -1 Device2 != -1     , two discret valves will be initialized
 * If Device1 == -1 Device2 == -1     , no valves will be initialized
 * @param Brush
 * @param Device1 MCM-Device
 * @param Pos1    Index of the digital Output
 * @param Device2 MCM-Device
 * @param Pos2    Index of the digital Output
 */
void Brush_ExRetract_Init(tABC_Attach *Brush, uint32_t DeviceRetract, uint32_t PosRetract , uint32_t DeviceExtract, uint32_t PosExtract);


/**
 * @brief The Brush is controled by a PVG-Valve. If Brush is connected to output A, use inverted. Else not.
 *
 * @param ABC_Attach
 * @param value 0 if Output A , 1 Output B
 */
void Brush_A_B(tABC_Attach *ABC_Attach, int32_t value);

void Brush_UpDown_Timer_10ms(tABC_Attach * Brush);

/**
 * @brief This function should be called every cycle for each used attachment ( A-C)
 *
 * @param Brush
 * @param Cmd Pointer to the Command Structure of the Attachment A, B or C
 */
void Brush_Cycle(tABC_Attach *Brush, tJSCommands *Cmd);

int32_t Brush_Active(const tABC_Attach *ABC_Attach);

//void Ctrl_Brush_Init(tControl * Ctrl);


/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CTRL_BRUSH_H
