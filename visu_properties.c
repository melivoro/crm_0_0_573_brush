﻿/****************************************************************************
 *
 * File:         VISU_PROPERTIES.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu_properties.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Visu_Properties_Init(const tVisuData * VData)
{


}


void Visu_Properties_Open(const tVisuData * VData)
{
  PrioMaskOn(MSK_PROPERTIES);
}


void Visu_Properties_Close(const tVisuData * VData)
{

}


void Visu_Properties_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{

  Visu_HomeKeyPressed();
}

