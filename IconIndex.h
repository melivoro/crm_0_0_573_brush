﻿/************************************************************************
 *
 * File:         ICONINDEX.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef ICONINDEX_H
#define ICONINDEX_H

/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/


typedef enum eagAttachmentPicIdx
{
  ATPIC_NONE,
  ATPIC_PLOUGH_NT,
  ATPIC_PLOUGH_FK = 2,
  ATPIC_PLOUGH_SIDE = 3,
  ATPIC_OMP253 = 4,
  ATPIC_OMB    = 5,
  ATPIC_OMT    = 6,
  ATPIC_MF500  = 7,
  ATPIC_MF300_OUT= 8,
  ATPIC_MF300_IN = 9,
  ATPIC_STEELBLADE_DOWN = 10,
  ATPIC_STEELBLADE_UP   = 11,
  ATPIC_PLOUGH_TE       = 12,
  ATPIC_PLOUGH_TE_L     = 13,
  ATPIC_PLOUGH_TE_R     = 14,
  ATPIC_PLOUGH_CH2600   = 15,
 // ATPIC_OPF = 16,
}eAttachmentPicIdx;





/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef ICONINDEX_H
