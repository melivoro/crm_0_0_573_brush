﻿/************************************************************************
 *
 * File:         CONTROL.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CONTROL_H
#define CONTROL_H
#include "io_types.h"
#include "Ctrl_Plough.h"
#include "ctrl_top.h"
#include "gseMaint.h"
#include "gseMaint_Lifetime.h"
#include "IO_Joystick_V85_B25.h"
#include "Commands.h"
#include "ctrl_light.h"

/*************************************************************************/

/* macro definitions *****************************************************/



#define ABCAttach_ON    0
#define ABCAttach_OFF   1

/* type definitions ******************************************************/

typedef enum eagABC_Equipment
{
  EQUIP_A = 0,
  EQUIP_B = 1,
  EQUIP_C = 2
}eagABC_Equipment;

typedef struct tagCtrlAutomatic
{
    uint8_t on;
}tCtrlAutomatic;










typedef struct tagEngine
{
  char     name[48];
  uint32_t Active;
  uint32_t Address;
  uint32_t Hdl;
  uint32_t Hdl2;
  int16_t rpm;
  int16_t AirPressure;
  int16_t OilTemp;
  int16_t OilPressure;
  int16_t OilLevel;
  int16_t CoolentTemp;
  int16_t CoolentPressure;
  int16_t CoolentLevel;
  int16_t AmbientAirTemp;
  int16_t FuelTemp;
  int16_t speed;
  int8_t PTO_Engaged;
  //int8_t PTO_state; //AK not tested
}tEngine;

typedef struct tagBNSO
{
  uint32_t Hdl;
  uint32_t Active;
  uint32_t Address;
}tBNSO;


typedef struct tagABCEquip
{
  int32_t  AttachPicIdx;
  tABC_Attach  ABC_Attach;
}tABCEquip;

typedef uint8_t DI_Sensor;

typedef enum eagDISensorState
{
  DI_SENSOR_OFF = 0,
  DI_SENSOR_ON  = 1,
  DI_SENSOR_UNKNOWN  = 2,
  DI_SENSOR_INACTIVE = 3

}eDISensorState;
/*
typedef struct tagSensorDI
{
  DI_Sensor Spreading;
  DI_Sensor Ignition;
  DI_Sensor Humidifier;
  DI_Sensor MaterialPresense;
  DI_Sensor OilFilter;
  DI_Sensor UpperOilLevel;
  DI_Sensor LowerOilLevel;
  DI_Sensor CheckOilLevelSensor;
  DI_Sensor PTO_Active;
}tSensorDI;
*/

typedef struct tagSensorDI
{
  eDISensorState Spreading;
  eDISensorState Ignition;
  eDISensorState Humidifier;
  eDISensorState MaterialPresense;
  eDISensorState OilFilter;
  eDISensorState UpperOilLevel;
  eDISensorState LowerOilLevel;
  eDISensorState CheckOilLevelSensor;
  eDISensorState PTO_Active;
}tSensorDI;
typedef struct tagSensorAI
{
  tIOT_AI  Liquidpresence;
  tIOT_AI  OilTemp;                  //in 0.1°C-Steps
  tIOT_AI  OilPressure;
  tIOT_AI  ValveErr[4];
}tSensorAI;

typedef struct tagSensorFreq
{
  uint32_t ConveryRotation;
}tSensorFreq;



typedef struct tagSensors
{
  tSensorDI   DI;
  tSensorAI   AI;
  tSensorFreq Freq;
}tSensors;



typedef struct tagRearEquip
{
  tABC_Attach ABC_Attach;
  int8_t Float;
}tRearEquip;

typedef struct tagHydraulicSystem
{
  tMaintRuntime *Load;
  tMaintRuntime *UnLoad;
}tHydraulicSystem;


typedef struct tagPlougKeyboard
{
  tIOT_Button Up;
  tIOT_Button Down;
}tPloughKeyboard;



typedef struct tagControl
{
    tIOT_Button      Emcy;
    uint8_t          Emcy_Active;
    tCtrlAutomatic   Auto;
    t_Joystick       Joystick;
    tPloughKeyboard  PloughKeys;
    tJSCommands      CmdNoKey;
    tJSCommands      CmdABC[3];
    uint8_t          ABC_Buttons_Pressed;

    tLight            Light;
    tEngine           Engine;
    tBNSO             BNSO;
    tHydraulicSystem  Hydr;
    tSensors          Sensors;
    tMaint            Maint;
    tMaint            MaintHourcounter;
    tMaintLifetime   *AnualMaint;
    tABCEquip         ABC[3];
    tTopEquip         Top;
    tRearEquip        Rear;

    uint32_t          MaxSpeed;
}tControl;





typedef void (*fpModuleInit)(tControl *Ctrl);
typedef void (*fpModuleCycle)(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);
typedef void (*fpModuleTimer)(tControl *Ctrl);
typedef void (*fpModuleDeInit)(tControl *Ctrl);

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/


const tControl*  CtrlGet(void);

void Ctrl_Init(void);
void Ctrl_Cycle(uint32_t evtc, tUserCEvt *evtv);
void Ctrl_Timer_MS(void);

void CheckRange(int32_t *value, int32_t min,int32_t max);

/**
 * @brief Toggles between manual mode and automode. At manual Mode Attachments will
 * use the speed of 30km/h
 *
 */
void Ctrl_Automatic_Toggle(void);

/**
 * @brief Use this function in the attachments to set the maximum speed of a vehicle.
 * If the acutally set maximum speed is smaller then Maxspeed, the maximum Speed
 * won't be overwritten.
 *
 * @param MaxSpeed
 */
void Ctrl_SetMaxSpeed(uint32_t MaxSpeed);

/**
 * @brief Resets the maximum speed of the vehicle. It has no Limit, now anymore,
 * until you set a new maximum speed with Ctrl_SetMaxSpeed
 */
void Ctrl_ResetMaxSpeed(void);

/**
 * @brief Activates a Sensor. It's state will change to DI_SENSOR_UNKNOWN
 *
 * @param di Pointer to the Sensor of the Type DI_Sensor
 */
void Ctrl_DISensor_Activate(DI_Sensor *di);
void Ctrl_DISensor_DeActivate(DI_Sensor *di);
int8_t Ctrl_IsDISensor_Active(DI_Sensor di);

/**
 * @brief Deinits the Controle structure at device shut down.
 *
 */
void Ctrl_DeInit(void);
/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CONTROL_H
