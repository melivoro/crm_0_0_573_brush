﻿/****************************************************************************
 *
 * File:         DEVICE.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */

#include "gsSocket.h"
#include "gsSocketTCPServer.h"
#include "gseDebug.h"
#include "gseConsole.h"
#include "gseEth_functions.h"
#include "gsDebug.h"
#include "gsUsbSer.h"





/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/
int32_t db_streams = 0;
int32_t db_COM     = -1;
int32_t cmd = 0;

tGsSocketTcpServer DebugServer = NULL;
tGsUsbSerHdl       UsbHdl = NULL;

int32_t DebugServer_Port;
int32_t Hdl_CMD_Eth, Hdl_CMD_COM;

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/



int32_t db_Eth_cb(const char * msg)
{
  if(Upload_over_ETH(0, NULL, -1, -1))
  {
    if(NULL == DebugServer)
    {
      DebugServer = gsSocketTcpServerCreate(DebugServer_Port, 0);
    }
    else
    {
      if(gsSocketTcpServerIsConnected(DebugServer))
       {
          gsSocketTcpServerWrite(DebugServer,msg,strlen(msg));
       }
    }
  }
  return 0;
}

int32_t db_SerCom(const char * msg)
{
  if(!UsbHdl)
  {
    UsbHdl = gsUsbSerCreate(0);
    if(UsbHdl)
    {
      gsUsbSerSetBaudrate(UsbHdl, 115200);
      gsUsbSerSetParity(UsbHdl, GS_USBSER_PARITY_NONE);
      gsUsbSerSetStopbits(UsbHdl,1);
      gsUsbSerOpen(UsbHdl);
    }
  }
  if(UsbHdl)
  {
    if(0 > gsUsbSerSend(UsbHdl, msg,strlen(msg)))
    {
      gsUsbSerDestroy(UsbHdl);
      UsbHdl = NULL;
    }
  }
  return 0;
}

int32_t db_cb(const char * msg)
{
  db_Eth_cb(msg);
  db_SerCom(msg);
  return 0;
}


void db_out(const char * format, ...)
{
  char buffer[256];
  va_list args;
  va_start (args, format);
  vsnprintf (buffer,256,format, args);
  va_end (args);
  gsDebugStreamsPrint(buffer);
}

void db_out_time(const char * format, ...)
{
  char buffer[256];
  tSysTime _time;
  tSysDate _date;
  RTCGetDate(&_date);  RTCGetTime(&_time);
  snprintf(buffer, 20, "%02d.%02d.%02d %02d:%02d:%02d "
                                              ,_date.Day
                                              ,_date.Month
                                              ,_date.Year
                                              ,_time.Hours
                                              ,_time.Minutes
                                              ,_time.Seconds
                                               );
  char *p = buffer + strlen(buffer);
  va_list args;
  va_start (args, format);
  vsnprintf (p,256 - strlen(buffer),format, args);
  va_end (args);
  gsDebugStreamsPrint(buffer);
}



int32_t db_init(int32_t Console, int32_t EthPort, int32_t ComPort, int32_t usb, int32_t window,  const char *filepath)
{
  if(Console)
    cmd = 1;

  if(EthPort)
  {
    DebugServer_Port = EthPort;

    db_streams = db_streams | GS_DBG_STREAM_CALLBACK;
  }
  else
    db_streams = db_streams &(~GS_DBG_STREAM_CALLBACK);


  if(ComPort >= 0)
  {

    db_streams = db_streams | GS_DBG_STREAM_SERIAL;
    db_COM     = ComPort;
    gsDebugStreamsAddSerial(ComPort);
  }
  else
    db_streams = db_streams &(~GS_DBG_STREAM_SERIAL);


  if(usb)
  {
    db_streams = db_streams | GS_DBG_STREAM_USB_MEM;
  }
  else
    db_streams = db_streams &(~GS_DBG_STREAM_USB_MEM);

  if(window)
  {
    db_streams = db_streams | GS_DBG_STREAM_WINDOW;
  }
  else
    db_streams = db_streams &(~GS_DBG_STREAM_WINDOW);

  if(NULL!= filepath)
  {

    db_streams = db_streams | GS_DBG_STREAM_FILE;
  }
  else
    db_streams = db_streams &(~GS_DBG_STREAM_FILE);

  gsDebugStreamsInit(db_streams);
  gsDebugStreamsAddCallback(db_cb);

  gsDebugStreamsSetLineMode(GS_DBG_STREAM_LINE_MODE_DISABLED);
  if(NULL != filepath)
    gsDebugStreamsAddFile(filepath);
  Hdl_CMD_Eth = sCon_InitStream(128, db_out);
  Hdl_CMD_COM = sCon_InitStream(128, db_out);
  return 0;
}


void db_cycle(int32_t evtc,tUserCEvt * evtv)
{
  //Server for CMD
  if(!cmd)
    return;

  if(Upload_over_ETH(evtc, evtv,-1,-1))
  {
    if(NULL == DebugServer)
    {
      if(DebugServer_Port)
      {
        DebugServer = gsSocketTcpServerCreate(DebugServer_Port,0);
      }
    }
    else if(gsSocketTcpServerIsConnected(DebugServer))
    {
      char buffer[128] = {0};
      int32_t n;
      if((n = gsSocketTcpServerRead(DebugServer, buffer, 128)))
      {
        sCon_GetString (Hdl_CMD_Eth, buffer,n);
      }

    }
  }
  if(db_COM >= 0)
  {
    char buffer[128] = {0};
    int32_t n;
    if((n = SerialRecv(db_COM, buffer, 128)))
    {
      sCon_GetString (Hdl_CMD_COM, buffer,n);
    }
  }
  if(NULL != UsbHdl)
  {
    char buffer[128];
    int32_t n;
    if((n = gsUsbSerRecv(UsbHdl, buffer, 128)))
    {
      sCon_GetString (Hdl_CMD_COM, buffer,n);
    }
  }
}


int32_t GetEthConHdl(void)
{
  return Hdl_CMD_Eth;
}
