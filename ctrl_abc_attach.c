﻿/****************************************************************************
 *
 * File:         CTRL_ABC_ATTACH.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */
#include "Ctrl_ABC_Attach.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Attachment_Clear(tABC_Attach *ABC_Attach)
{
    //Reset all valves. Don't delete Pointer to Runtime.
    tMaintRuntime *Runtime = ABC_Attach->MaintRunntime;

    memset(ABC_Attach, 0, sizeof(tABC_Attach));

    ABC_Attach->MaintRunntime = Runtime;
}

