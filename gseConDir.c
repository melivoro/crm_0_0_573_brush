﻿/****************************************************************************
 *
 * File:         GSECONDIR.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include "gseConsole.h"
#include "gseConDir.h"
#include "UserCAPI.h"


/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/
typedef struct tagConDir
{
    char    root[32];
    char    path[128];
    tGsDir *dir;
}tConDir;

#define DRIVE_DATA  0
#define DRIVE_TMP   1
#define DRIVE_USB   2

int32_t ActualDir = 0;

tConDir Drives[] =
{
    { .root = "/gs/data/", .path = "" ,.dir = NULL},
    { .root = "/tmp/"    , .path = "" ,.dir = NULL},
    { .root = "/gs/usb/" , .path = "" ,.dir = NULL}
};






tConDir *_GetActualDir(void)
{
    if(!strlen(Drives[ActualDir].path))
    {
        strcpy(Drives[ActualDir].path, Drives[ActualDir].root);
    }
    return &Drives[ActualDir];
}

void ConDir_dir(uint32_t Hdl, char **data)
{
    tConDir *ActDir = _GetActualDir();
    tGsDir *dir =  DirOpen(ActDir->path);
    if(NULL == dir)
    {
        strcpy(ActDir->path,ActDir->root);
        dir =  DirOpen(ActDir->path);
        if(NULL == dir)
        {
            sCon_WriteString(Hdl, "%s is invalid root\r\n", ActDir->path);
            return;
        }
    }


    tGsDirEntry Entry;
    sCon_WriteString(Hdl, "Directory of %s: \r\n\n", ActDir->path );
    while(0 == DirRead(dir, &Entry))
    {
        if(Entry.mType == DIR_ENTRY_TYPE_FILE)
        {
            sCon_WriteString(Hdl,"%08d B     %s\r\n",Entry.mSize, Entry.mName);
        }
        else
        {
            sCon_WriteString(Hdl,"<DIR>          %s\r\n", Entry.mName);
        }
    }
    DirClose(dir);
    sCon_WriteString(Hdl, ActDir->path);
}

void ConDir_cd(uint32_t Hdl, char **data)
{
    tConDir *ActDir = _GetActualDir();
    tGsDir *dir =  DirOpen(ActDir->path);
    if(NULL != dir)
    {
        tGsDirEntry Entry;
        while(0 == DirRead(dir, &Entry))
        {
            if(0 == strcmpi(data[0], Entry.mName ))
            {
                if(Entry.mType != DIR_ENTRY_TYPE_DIR)
                {
                    sCon_WriteString(Hdl, "%s is not a valid folder", Entry.mPath);
                }
                else
                {
                    if(NULL != strstr(data[0], ".."))
                    {
                        if(strlen(ActDir->path) > strlen(ActDir->root) + 1)
                        {
                            uint32_t lastsign = strlen(ActDir->path)  - 1;

                            do
                            {
                                ActDir->path[lastsign] = '\0';
                                lastsign--;

                            }while(ActDir->path[lastsign] != '/');
                        }
                    }
                    else if(NULL != strstr(data[0], "."))
                    {

                    }
                    else
                    {
                        char newpath[256];
                        snprintf(newpath, 256, "%s%s/",ActDir->path, Entry.mName );
                        tGsDir *NewDir = DirOpen(newpath);
                        if(NULL !=NewDir)
                        {
                            strcpy(ActDir->path, newpath);
                            DirClose(NewDir);
                        }
                    }

                    sCon_WriteString(Hdl, "%s", ActDir->path);
                    DirClose(dir);
                }
                return;
            }
        }
        DirClose(dir);
    }
    if(strchr(data[0], ':'))
    {
        if((strstr(data[0], "usb"))
         ||(strstr(data[0], "usb_t_0")))
         {
            ActualDir = DRIVE_USB;
         }
        else if((strstr(data[0], "flash"))
         ||(strstr(data[0], "gs/data")))
         {
            ActualDir = DRIVE_DATA;
         }
        else if((strstr(data[0], "temp"))
         ||(strstr(data[0], "tmp"))
         ||(strstr(data[0], "ram")))
         {
            ActualDir = DRIVE_TMP;
         }
    }
    ActDir = _GetActualDir();
    sCon_WriteString(Hdl, "%s", ActDir->path);
    return;
}


int32_t _remDir(uint32_t Hdl,const char *path)
{
    tGsDir *Dir = DirOpen(path);
    if(NULL == Dir)
    {
        return -1;
    }
    tGsDirEntry Entry;
    while(0 == DirRead(Dir, &Entry))
    {
        if(DIR_ENTRY_TYPE_FILE ==  Entry.mType)
        {

            sCon_WriteString(Hdl, "Removed %s\r\n",Entry.mPath);
            FileUnlink(Entry.mPath);
        }
        else
        {
            if((!strstr(Entry.mName, ".."))
             &&(!strstr(Entry.mName, ".")))
            {
                _remDir( Hdl,Entry.mPath);
            }
        }

    }
    DirClose(Dir);
    RemoveDir(path);
    sCon_WriteString(Hdl, "Removed %s\r\n",path);
    return 0;
}


void ConDir_rm(uint32_t Hdl, char **data)
{
    char *path;
    //Lösche über absoluten Pfad
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(Drives); i++)
    {
        path = strstr(data[0], Drives[0].root);
        if(path != NULL)
            break;
    }
    if(NULL != path)
    {
        if(strrchr(path, '.'))
        {
            if(0 == FileUnlink(path))
            {
                sCon_WriteString(Hdl,"Removed %s\r\n", path);
            }
            else sCon_WriteString(Hdl,"Error removed %s\r\n", path);

        }
        else //letztes Zeichen = '/'-->Lösche ordner
        {
            _remDir( Hdl,path);
        }
    }
    else // kein Pfad enthalten, suche nach Datei im aktuellen Pfad
    {
        tConDir *ActDir = _GetActualDir();
        tGsDir * pDir = DirOpen(ActDir->path);
        if(NULL != pDir)
        {
            tGsDirEntry DirEntry;
            while(0 == DirRead(pDir, &DirEntry))
            {
                if(0 == strcmpi(DirEntry.mName, data[0] ))
                {

                    if(DIR_ENTRY_TYPE_FILE == DirEntry.mType)
                    {
                      int32_t ret =  FileUnlink(DirEntry.mPath);
                      if(0 != ret)
                      {
                        sCon_WriteString(Hdl,"Error at FileUnlink(\"%s\");, returned with %d\r\n",DirEntry.mPath,ret);
                        DirClose(pDir);
                        return;
                      }
                      sCon_WriteString(Hdl,"Removed %s\r\n", DirEntry.mPath);
                      return;
                    }
                    else
                    {
                        _remDir(Hdl, DirEntry.mPath);
                    }
                    DirClose(pDir);
                    return;
                }

            }
            DirClose(pDir);
        }
    }
    return;
}


tsCon_ComArray GS_ConDir[] = {{"dir"                , 0 ,  ConDir_dir           , "shows files in a folder"   },
                              {"cd"                 , 1 ,  ConDir_cd            , "opens new folder path"     },
                              {"rm"                 , 1,   ConDir_rm            , "removes file or folder"     },
};


int32_t gseConDirAdd(uint32_t Hdl)
{
    sCon_AddCommandList( Hdl,GS_ConDir, GS_ARRAYELEMENTS(GS_ConDir));
    return 0;
}
