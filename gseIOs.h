﻿/************************************************************************
 *
 * File:         GSe_IOs.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef GSe_IOs_H
#define GSe_IOs_H

#define IO_FALSE 0
#define IO_TRUE  1
#define IO_ERR   2
#define IO_NAN   3
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

#define CAN0 0
#define CAN1 1
#define BIT11 0
#define BIT29 1

typedef enum etagGSe_IOsDoError
{
  DO_ERROR_UNKNOWN = -1,
  DO_ERROR_NONE,
  DO_ERROR_OPENLOAD,
  DO_ERROR_OVERCURRENT,
}eGSe_IOsDoError;


typedef enum etagGSe_IOsAIType
{
  AI_0_10_VOLT = 0,
  AI_0_20_MA,
}eGSe_IOsAIType;


typedef enum etagGSe_IO_Type
{
  IO_TYPE_DI,
  IO_TYPE_AI,
  IO_TYPE_DO,
  IO_TYPE_AO,
  IO_TYPE_PWM,
  IO_TYPE_FREQ,
  IO_TYPE_COUNT,
}etGSe_IO_Type;

typedef struct tagGSe_IOs_Pos
{
  int32_t idxDevice;
  int32_t idxIO;
}tGSe_IOs_Pos;

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
typedef struct tagGSe_IOs_DI
{
  char   *description;
  uint8_t value;
  tGSe_IOs_Pos Pos;
}
tGSe_IOs_DI;

typedef struct tagGSe_IOs_DO
{
  char * description;
  int8_t value;
  int8_t changed;
  eGSe_IOsDoError error;
  int32_t     current;
  tGSe_IOs_Pos Pos;
}
tGSe_IOs_DO;

typedef struct tagGSe_IOs_PWM
{
   char * description;
  int32_t enable;
  int32_t duty;
  int32_t freq;
  tGSe_IOs_Pos Pos;
}tGSe_IOs_PWM;



typedef struct tagGSe_IOs_AI
{
  char * description;
  int32_t value;
  tGSe_IOs_Pos Pos;
}tGSe_IOs_AI;

typedef struct tagGSe_IOs_AO
{
  char * description;
  int32_t value;
  tGSe_IOs_Pos Pos;
}tGSe_IOs_AO;

typedef struct tagGSe_IOs_Count
{
  char * description;
  int32_t value;
  int32_t overflow;
  tGSe_IOs_Pos Pos;
}tGSe_IOs_Count;

typedef struct tagGSe_IOs_FreqIn
{
  char * description;
  int32_t value;
  tGSe_IOs_Pos Pos;
}tGSe_IOs_FreqIn;

typedef struct tagDevices tDevices;



typedef void (*GSe_IOs_init_cb) (tDevices *pIODevice, void *arg);
typedef void (*GSe_IOs_cycle_cb) (tDevices *pIODevice);

struct tagDevices
{
 char     DeviceName[32];
 char     Firmware[32];
 char     SNr[32];
 uint32_t idx;
 int32_t active;
 GSe_IOs_init_cb  fpInit_IODevice;
 GSe_IOs_cycle_cb fpCycle_IODevice;
 uint32_t numDI;
 tGSe_IOs_DI *DI;
 uint32_t numDO;
 tGSe_IOs_DO *DO;
 uint32_t numPWM;
 tGSe_IOs_PWM *PWM;
 uint32_t numAI;
 tGSe_IOs_AI  *AI;
 uint32_t numAO;
 tGSe_IOs_AO  *AO;
 uint32_t numFreqIn;
 tGSe_IOs_FreqIn *FreqIn;
 uint32_t numCount;
 tGSe_IOs_Count *Count;
 void *   DeviceData;
 uint32_t volt;
 uint32_t temp;
};



int32_t GSe_IOs_Add_Device(GSe_IOs_init_cb fpDeviceInit,void * arg, GSe_IOs_cycle_cb fpDeviceCycle );
void GSe_IOs_Cycle(void);

int32_t GSe_IO_Add_DI(uint32_t DeviceHdl, const char * description);
int32_t GSe_IO_Add_DO(uint32_t DeviceHdl, const char * description);
int32_t GSe_IO_Add_AI(uint32_t DeviceHdl, const char * description);
int32_t GSe_IO_Add_PWM(uint32_t DeviceHdl, const char * description);
int32_t GSe_IO_Add_AO(uint32_t DeviceHdl, const char * description);
int32_t GSe_IO_Add_FreqIn(uint32_t DeviceHdl, const char * description);
int32_t GSe_IO_Add_Counter(uint32_t DeviceHdl, const char * description);


tGSe_IOs_DI  * GSe_IOs_GetDIp(int32_t Hdl_Device , uint32_t n);
tGSe_IOs_DO  * GSe_IOs_GetDOp(int32_t Hdl_Device , uint32_t n);
tGSe_IOs_AI  * GSe_IOs_GetAIp(int32_t Hdl_Device , uint32_t n);
tGSe_IOs_PWM * GSe_IOs_GetPWMp(int32_t Hdl_Device , uint32_t n);

int32_t GSe_IOs_GetDI(tGSe_IOs_DI  *pDi);
int32_t GSe_IOs_GetAI(tGSe_IOs_AI  *pAi);

void    GSe_IOs_SetDO(tGSe_IOs_DO  *pDo, int32_t value);
void    GSe_IOs_SetPWM(tGSe_IOs_PWM  *pPWM, int32_t duty, int32_t freq);
int32_t GSe_IOs_GetActiveState( int32_t Hdl_Device);
int32_t GSe_IOs_GetVolt       ( int32_t Hdl_Device);
int32_t GSe_IOs_GetTemp       ( int32_t Hdl_Device);


uint32_t GSe_IO_GetNumDevices(void);
const char * GSe_IO_GetDeviceName(uint32_t DeviceHdl);

const tDevices * GSe_IO_GetDevice(uint32_t DeviceHdl);

/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef GSe_IOs_H
