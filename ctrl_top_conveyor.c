﻿/****************************************************************************
 *
 * Project:   name
 *
 * @file      filename.c
 * @author    author
 * @date      [Creation date in format %02d.%02d.20%02d]
 *
 * @brief     description
 *
 ****************************************************************************/

#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definitions:                    */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project.            */
                          /* vartab.h is generated automatically.           */

#include "objtab.h"       /* Object ID definitions:                         */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects.         */
                          /* objtab.h is generated automatically.           */
#include "control.h"
#include "gseDebug.h"
#include "gsToVisu.h"
#include "ctrl_top_conveyor.h"
#include "visu_material.h"

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/
/*tSQLTable ReagentTable_50C ={   .TableName = "Reagent 50C" ,
  .ColumnNames = {"Quantity [g/min]","RPM", "PWM",  "EMPTY"},
  .Cell = {
   {250000    , 45,   95 , 30},
    {625000   , 88,   110, 31},
    {1250000    , 152,  126, 32},
    {1875000    , 208,  141, 33},
    {2500000   , 248,  157, 34},
    {6250000   , 296,  172, 35},
    {12500000   , 360,  188, 36},
    {18750000   , 385,  203, 37},
    {25000000  , 500,  219, 38},
    {37500000  , 590,  234, 39},
    {50000000  , 8200, 250, 40},
    }
};

tSQLTable ReagentTable_0C ={   .TableName = "Reagent 0°C" ,
  .ColumnNames = {"Quantity [g/min]","RPM", "PWM",  "EMPTY"},
  .Cell = {
    {1000    , 45,   95 , 30},
    {2500    , 88,   110, 31},
    {5000    , 152,  126, 32},
    {7500    , 208,  141, 33},
    {10000   , 248,  157, 34},
    {25000   , 296,  172, 35},
    {50000   , 360,  188, 36},
    {75000   , 385,  203, 37},
    {100000  , 500,  219, 38},
    {150000  , 590,  234, 39},
    {200000  , 8200, 250, 40},
    }
};*/

tSQLTable ReagentTable_Min20C ={   .TableName = "Conveyor" ,
  .ColumnNames = {"Quantity [g/min]","RPM", "PWM",  "EMPTY"},
  .Cell = {
    {250000    , 45,   95 , 30},
    {5225000   , 88,   110, 31},
    {10200000    , 152,  126, 32},
    {15175000    , 208,  141, 33},
    {20150000   , 248,  157, 34},
    {25125000   , 296,  172, 35},
    {30100000   , 360,  188, 36},
    {35075000   , 385,  203, 37},
    {40050000  , 500,  219, 38},
    {45025000  , 590,  234, 39},
    {50000000  , 8200, 250, 40},
    }
};
/* local function prototypes ************************************************/

/* functions ****************************************************************/










tTemperatureTable* SetTemperature(tConveyor *Conveyor, uint32_t Idx_Reagent, uint32_t Idx_Temperature , int8_t Temperature, tSQLTable * Table)
{
  if(Idx_Reagent >= NUMREAGENTS)
  {
    return NULL;
  }
  if(Idx_Temperature >= TEMPERATURE_PER_REAGENT)
  {
    return NULL;
  }
  Conveyor->TempTables[Idx_Temperature].Table =  SQL_Tabel_Init(Table);
 Conveyor->TempTables[Idx_Temperature].temp  = Temperature;
//Idx_Temperature = 0; added by AK
  return &Conveyor->TempTables[Idx_Temperature];
}


void ConveyorCreate(tConveyor *Conveyor)
{

 SetTemperature(Conveyor, 0, 0, 15, &ReagentTable_Min20C);
  //SetTemperature(Conveyor, 0, 1, 30, &ReagentTable_0C);
  //SetTemperature(Conveyor, 0, 2, 50 , &ReagentTable_50C);

  Conveyor->Density_max = UINT32_MAX;
}

void ConveyorInit(tConveyor *Conveyor,uint32_t Device, uint32_t Pos)
{
  ValveProp12V_Init(&Conveyor->Valve, Device, Pos, 1000, 5);
}


int32_t  ConveyorGetMaxDensity(tConveyor *Conveyor)
{
  uint32_t val = 0;
  const tReagent *Reagent = GetActualReagent();
  if(NULL != Reagent)
  {
    val = Reagent->SpreadingDensity_Max;
  }

  if(val > Conveyor->Density_max)
    val = Conveyor->Density_max;
  return val;;
}


void ConveyorCycle(tConveyor *Conveyor, int32_t temp)
{
  const tControl *Ctrl = CtrlGet();
  int32_t Density = Ctrl->Top.Density;
  if(Ctrl->Top.Boost == BOOST_ON)
  {
    Density = ConveyorGetMaxDensity(Conveyor);
  }

  const tReagent *Reagent = GetActualReagent();;
  if(Reagent == NULL)
  {
    return;
  }
  //km/h * g/m² * m  = 1000 / 60 * g / min
  if(Ctrl->Top.Conveyor.on)
  {
    double speed = 0;
    speed = TopGetSpeed();
    double mDensity = Reagent->MaterialDensity;
    //prevent div/0
    if(fabs(mDensity) < 0.001 )
    {
      mDensity = 1;
    }
    double shield_factor = Conveyor_GetShieldFactor();
    //uint32_t g_min = (speed / 60.0 * 1000.0) *  (double) Density /1000.0 * (double) Ctrl->Top.Disk.width * (double) Conveyor->p * shield_factor * 0.65 * 0.4075;
                    //   km/min            *   g/m²            *   m / factor
    double a = speed *(double) Ctrl->Top.Disk.width * mDensity / shield_factor / 0.65 / 0.4075/ 60.0;
    uint32_t g_min = (double) Density * a;
    SetVar(HDL_TESTVAR_GMIN, g_min);
    if(g_min != Conveyor->g_min)
    {
      Conveyor->g_min =g_min;
      //choose eagent

      tTemperatureTable * TempTables = Conveyor->TempTables;
      //temp is colder then the table with the coldest temperature-> take table with the coldest temperature
      if(temp <= TempTables[0].temp)
      {
        Conveyor->pwm = TableGetLSEValue(TempTables[0].Table,0,2,Conveyor->g_min);
        Conveyor->g_min_max = TempTables[0].Table->Cell[10][0];
      }
      //temp > then table with the hightest temperature -> Use Table with the biggest temperature.
     /* else if(temp >= TempTables[TEMPERATURE_PER_REAGENT-1].temp)
      {
        Conveyor->pwm = TableGetLSEValue(TempTables[TEMPERATURE_PER_REAGENT-1].Table,0,2,Conveyor->g_min);
        Conveyor->g_min_max = TempTables[TEMPERATURE_PER_REAGENT-1].Table->Cell[10][0];
      }
      else//Temperature is between the range of the coldest and warmest table. Read the Values out of the two tables with the nearest temperature and calculate the PWM->value by lineare equation
      {
        for(int32_t i = 1; i < TEMPERATURE_PER_REAGENT; i++)
        {
          if(temp < TempTables[i].temp)
          {
            if(TempTables[i].temp ==  TempTables[i-1].temp)//would be division through 0
              return;
            double y1 =  TableGetLSEValue(TempTables[i    ].Table,0,2,Conveyor->g_min);
            double y2 =  TableGetLSEValue(TempTables[i - 1].Table,0,2,Conveyor->g_min);
            double m = (y1 - y2)/
                        (double)(TempTables[i].temp- (TempTables[i - 1].temp));

            double b = y1  - m *TempTables[i].temp;
            Conveyor->pwm = m * temp + b;
            //calc maximum g/min:
            y1 = TempTables[i].Table->Cell[10][0];
            y2 = TempTables[i-1].Table->Cell[10][0];
            m = (y1 - y2)/
                  (double)(TempTables[i].temp- (TempTables[i - 1].temp));
            b = y1  - m * TempTables[i].temp;
            Conveyor->g_min_max = m * temp + b;
            //calc maximum density at current speed and width

            break;
          }
        }
      }*/

      if(a < 0.001)
      {
        a = 1;
      }
      Conveyor->Density_max = Conveyor->g_min_max  / a;
    }
  }
  else
  {
    Conveyor->pwm = 0;
    Conveyor->g_min = -1;
  }
  ValvProp_Set(&Conveyor->Valve.Valve.PVEH,Conveyor->pwm);

}

void ConveyorOn(tConveyor *Conveyor,uint32_t on)
{
  Conveyor->on = on;
}

void ConveyorTimer(tConveyor *Conveyor)
{
  ValvProp_Timer_100ms(&Conveyor->Valve.Valve.PVEH);
}


void ConveyorDraw(const tConveyor *Conveyor)
{

  static int32_t msk_old = -1;
  if(GetCurrentMaskShown() != msk_old)
  {
    msk_old = GetCurrentMaskShown();
    const tReagent *ActualReagent = GetActualReagent();
    if(NULL != ActualReagent)
    {
      if(NULL != ActualReagent->Name)
      {
        SetVisObjData(OBJ_MATERIAL_KH, ActualReagent->Name, strlen(ActualReagent->Name) + 1 );
        SetVisObjData(OBJ_MATERIAL_SH, ActualReagent->Name, strlen(ActualReagent->Name) + 1 );
      }
    }
  }
}
