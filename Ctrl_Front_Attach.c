﻿/****************************************************************************
 *
 * File:         CTRL_FRONT_ATTACH.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "Ctrl_Front_Attach.h"
#include "control.h"
#include "io.h"
#include "RCText.h"
#include "Ctrl_Brush.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/


void Ctrl_Front_FK_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1, -1, -1 );
    Plough_Float_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 8 );

    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1 );
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 60;

}

void Ctrl_Front_CORMZ_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1, -1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,0, 0 );

    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,-1,-1,-1,-1 );
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
}

void Ctrl_Front_SMOLENSK_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1 ,-1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 8 );

    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1 );
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
}

void Ctrl_Front_TN34_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1 ,-1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 8 );
    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, MCM250_1,0, MCM250_1,2);

    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1 );
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 40;
}


void Ctrl_Front_MN_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1 ,-1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 8 );
    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, MCM250_1,0,MCM250_1,2);

    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, MCM250_1, 7,  MCM250_1, 5 );
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 40;
}
void Ctrl_Front_TE_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1 ,-1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 8 );
    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, MCM250_1,0,MCM250_1,2);

    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, -1,-1,-1,-1 );
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, MCM250_1,5,MCM250_1,7);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 50;
}
void Ctrl_Front_VTYPE_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1 ,-1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 8 );
    Plough_LeftRight_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, MCM250_1,0,MCM250_1,2);

    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,-1,-1,-1,-1);
    Plough_ExRetract_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach, MCM250_1,5,MCM250_1,7);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 50;
}


void Ctrl_Front_OFF_OPF_245(tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach = &Ctrl->ABC[EQUIP_A].ABC_Attach;
    Brush_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1,-1,-1);
    Brush_Float_Init( ABC_Attach, MCM250_1, 8);
    Brush_Rotate_Init( ABC_Attach, MCM250_1, 3);
    Brush_LeftRight_Init(ABC_Attach,MCM250_1, 0,MCM250_1, 2 );
    Brush_ExRetract_Init(ABC_Attach,-1,-1,-1,-1);
    Brush_SideSections_Init(ABC_Attach, 0-1,-1,-1,-1);
    Brush_A_B(ABC_Attach, PVE_PORT_B);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 35;
}


void Ctrl_Front_OMB1       (tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach = &Ctrl->ABC[EQUIP_A].ABC_Attach;
    Brush_UpDown_Init(ABC_Attach,MCM250_1, 1,-1,-1);
    Brush_Float_Init( ABC_Attach, -1, 0);
    Brush_Rotate_Init( ABC_Attach, MCM250_1, 3);
    Brush_LeftRight_Init(ABC_Attach, MCM250_1,0 ,MCM250_1, 2);
    Brush_ExRetract_Init(ABC_Attach, -1,-1,-1,-1);
    Brush_SideSections_Init(ABC_Attach, -1,-1,-1,-1);
    Brush_A_B(ABC_Attach, PVE_PORT_B);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 5;
    WaterPump_SetMaxLiter(20000);
}


void Ctrl_Front_OMT1       (tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach = &Ctrl->ABC[EQUIP_A].ABC_Attach;
    Brush_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1,-1,-1);
   // Brush_Float_Init( ABC_Attach, MCM250_1, 8);
    Brush_Float_Init( ABC_Attach, -1, 0);
    Brush_Rotate_Init( ABC_Attach, MCM250_1, 3);
    Brush_LeftRight_Init(ABC_Attach,MCM250_1, 0,MCM250_1, 2 );
    //Brush_LeftRight_Init(ABC_Attach,-1,-1,-1,-1 );
    Brush_ExRetract_Init(ABC_Attach, MCM250_1, 5,MCM250_1, 7);
   //Brush_ExRetract_Init(ABC_Attach,-1,-1,-1,-1);
    Brush_SideSections_Init(ABC_Attach,-1,-1,-1,-1);
    Brush_A_B(ABC_Attach, PVE_PORT_B);
    Ctrl->ABC[EQUIP_A].ABC_Attach.MaxSpeed = 5;
}


void Ctrl_Front_MF300      (tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach = &Ctrl->ABC[EQUIP_A].ABC_Attach;
    Brush_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,MCM250_1, 1,-1,-1);
    Brush_Float_Init( ABC_Attach, -1, -1);
    Brush_Rotate_Init( ABC_Attach, -1, -1);
    Brush_LeftRight_Init(ABC_Attach,MCM250_1, 0,MCM250_1, 2);
    Brush_ExRetract_Init(ABC_Attach, -1, -1, -1, -1);
    Brush_SideSections_Init(ABC_Attach, MCM250_1, 5, MCM250_1, 7);
   //Brush_SideSections_Init(ABC_Attach,-1,-1,-1,-1);
}



void Ctrl_Front_MF500      (tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach = &Ctrl->ABC[EQUIP_A].ABC_Attach;
    Brush_UpDown_Init(&Ctrl->ABC[EQUIP_A].ABC_Attach,-1, -1,-1,-1);
    Brush_Float_Init( ABC_Attach, -1, -1);
    Brush_Rotate_Init( ABC_Attach, -1, -1);
    Brush_LeftRight_Init(ABC_Attach, MCM250_1, 5, -1, -1);
    Brush_ExRetract_Init(ABC_Attach, -1,-1,-1,-1);
    Brush_SideSections_Init(ABC_Attach,-1,-1,-1,-1);
    //Brush_SideSections_Init(ABC_Attach, MCM250_1,0,MCM250_1,2);
}

void Ctrl_Front_Brush_Draw(tControl *Ctrl)
{
    if(Brush_Active(&Ctrl->ABC[EQUIP_A].ABC_Attach))
    {
        static int32_t _time = 0;
        if(250 < GetMSTick()-_time)
        {
            _time = GetMSTick();
            int32_t pos = GetVarIndexed(IDX_BRUSHON_FRONT) + 1;
            if(pos > 2)
            {
                pos = 1;
            }
            SetVarIndexed(IDX_BRUSHON_FRONT,pos);
        }
    }
    else
    {
        SetVarIndexed(IDX_BRUSHON_FRONT, 0);
    }

    if(0 != (Valv_GetState(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveFloat) > 0))
        SetVarIndexed(IDX_FLOATING_FRONT,1 );
    else
        SetVarIndexed(IDX_FLOATING_FRONT,0 );
}

void Ctrl_Front_Brush_Standard_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
    Ctrl_Front_Brush_Draw(Ctrl);
    Brush_Cycle(&Ctrl->ABC[EQUIP_A].ABC_Attach, &Ctrl->CmdABC[EQUIP_A]);
}

void Ctrl_Front_Brush_Standard_Timer_10ms(tControl *Ctrl)
{
    Brush_UpDown_Timer_10ms(&Ctrl->ABC[EQUIP_A].ABC_Attach);
}

void Ctrl_Front_Plough_Draw(tControl *Ctrl)
{
    if(Valv_GetValue(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveFloat))
    {
        SetVarIndexed(IDX_FLOATING_FRONT,1);
    }
    else
    {
        SetVarIndexed(IDX_FLOATING_FRONT,0);
    }

}


void Ctrl_Front_Plough_Standard_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
    Ctrl_Front_Plough_Draw(Ctrl);
    Plough_Cycle(&Ctrl->ABC[EQUIP_A].ABC_Attach, &Ctrl->CmdABC[EQUIP_A]);

}


void Ctrl_Front_Plough_2_Sections_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
    Plough_2_Sections_Cycle(&Ctrl->ABC[EQUIP_A].ABC_Attach, &Ctrl->CmdABC[EQUIP_A], &Ctrl->ABC[EQUIP_C].ABC_Attach, &Ctrl->CmdABC[EQUIP_C]);
}

void Ctrl_Front_Plough_Standard_Timer_10ms(tControl *Ctrl)
{
    Plough_UpDown_Timer_10ms(&Ctrl->ABC[EQUIP_A].ABC_Attach);
}





