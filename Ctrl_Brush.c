﻿/****************************************************************************
 *
 * File:         CTRL_tABCAttach.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "Ctrl_Plough.h"
#include "RCText.h"

#include "errorlist.h"
#include "control.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/
int8_t BrushOn;
int32_t timer; //(добавлен)
/* local function prototypes*************************************************/

#define VALVE_NOT_USED 0
#define VALVE_DISKRET  1
#define VALVE_PROP     2
#define MAX_VALUE_PWM  1000 //(добавлено)
#define DELAY 500           //(добавлено)(0.5 сек)
/**#define**************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

void Brush_Maint_Create(tABC_Attach * ABC_Attach, tMaint *Maint,eErrorID ID,  uint32_t RCText_Warn, uint32_t RCText_Service , uint32_t HdlRemVar_Warn, uint32_t Hdl_RemVar_Service)
{
    ABC_Attach->MaintRunntime = Maint_Runtime_Add(Maint,ID, RCText_Warn, RCText_Service,HdlRemVar_Warn, Hdl_RemVar_Service,500  );
}



void Brush_UpDown_Init(tABC_Attach * ABC_Attach,uint32_t Device1, uint32_t Pos1,uint32_t Device2, uint32_t Pos2 )
{
  Valv_Init(&ABC_Attach->ValveUpDown,Device1, Pos1, Device2, Pos2);

}





void Brush_Float_Init(tABC_Attach * ABC_Attach, int32_t Device, int32_t Pos)
{
    ValvDisk_Init(&ABC_Attach->ValveFloat, Device, Pos);
    
}


void Brush_Rotate_Init(tABC_Attach * ABC_Attach, int32_t Device, int32_t Pos)
{

    Valv_Init(&ABC_Attach->ValveRotation, Device, Pos,-1,-1);
    //ValvDisk_Init(&ABC_Attach->ValveRotation, Device, Pos);
}

void Brush_LeftRight_Init(tABC_Attach * ABC_Attach, uint32_t DeviceTurnLeft, uint32_t PosTurnLeft,  uint32_t DeviceTurnRight, uint32_t PosTurnRight )
{
    Valv_Init(&ABC_Attach->ValveLeftRight,  DeviceTurnLeft,  PosTurnLeft,   DeviceTurnRight,  PosTurnRight );
}

void Brush_SideSections_Init(tABC_Attach * ABC_Attach, uint32_t DeviceUp, uint32_t PosUp,  uint32_t DeviceDown, uint32_t PosDown )
{
    Valv_Init(&ABC_Attach->ValveSideUpDown,  DeviceUp,  PosUp, DeviceDown,PosDown );
}

void Brush_ExRetract_Init(tABC_Attach * ABC_Attach, uint32_t DeviceRetract, uint32_t PosRetract
                                          , uint32_t DeviceExtract, uint32_t PosExtract)
{
    Valv_Init(&ABC_Attach->ValveExRetract, DeviceRetract,PosRetract,DeviceExtract,PosExtract);
}



void Brush_UpDown_Timer_10ms(tABC_Attach * ABC_Attach)
{
    ABC_Attach->timer_cnt++;
    //call only every 10 times
    if(0 == (ABC_Attach->timer_cnt%10))
    {
        Valv_Timer_100ms(&ABC_Attach->ValveUpDown);
        Valv_Timer_100ms(&ABC_Attach->ValveLeftRight);
        Valv_Timer_100ms(&ABC_Attach->ValveExRetract);
        Valv_Timer_100ms(&ABC_Attach->ValveSideUpDown);
    }
}

void Brush_A_B(tABC_Attach *ABC_Attach, int32_t value)
{
        ABC_Attach->BrushInvert = value;
}


int32_t Brush_Active(const tABC_Attach *ABC_Attach)
{
    if(BrushOn && (ABC_Attach->BrushInvert == BrushOn))
     return 1;
    else return 0;
}

void Brush_Cycle(tABC_Attach * ABC_Attach, tJSCommands *Cmd)
{
    //F-Key pressed for toggle brush.
    if(Cmd->Active)
    {
        if(ABC_Attach->ValveRotation.Type != VALVE_TYPE_NONE) 
        {
            if(Cmd->F_New)
            {
                if(PVE_PORT_A == ABC_Attach->BrushInvert)
                {
                    if(PVE_PORT_A == BrushOn)
                    {
                         BrushOn    = PVE_PORT_NONE;
                         Cmd->Float = FLOAT_OFF;
                    }
                    else BrushOn = PVE_PORT_A;
                }
                else
                {
                    if(PVE_PORT_B == BrushOn)
                        BrushOn = PVE_PORT_NONE;
                    else BrushOn = PVE_PORT_B;
                }
            }
        }
    }
    //switch of rotation if we have a floting valve.
    if(ABC_Attach->ValveFloat.Type != VALVE_TYPE_NONE)
    {
        int32_t move = Cmd->Updown;
        if(move > 0)
        {
            BrushOn = PVE_PORT_NONE;
        }
    }

    //MoveUpDown;
    Valv_Set(&ABC_Attach->ValveUpDown,-Cmd->Updown );
    //Floating:
    if(!Brush_Active(ABC_Attach))
    {
        Cmd->Float = FLOAT_OFF;
      //  Valv_Set(&ABC_Attach->ValveRotation, 0);  //kav. tried to stop rotation when  it's up
    }
    if(FLOAT_ON  == Cmd->Float)
         Valv_Set(&ABC_Attach->ValveFloat,1);
    else Valv_Set(&ABC_Attach->ValveFloat,0);
    //Left right:
    Valv_Set(&ABC_Attach->ValveLeftRight,-Cmd->LeftRight );
    //Steel Rubber
    Valv_Set(&ABC_Attach->ValveSideUpDown,Cmd->Z );
    //Extract/Retract
    Valv_Set(&ABC_Attach->ValveExRetract,Cmd->Z);
    //tBrush
    //check if correct Brush Valve is on.
    /*Valv_Set(&ABC_Attach->ValveRotation, BrushOn * 1000);
    if(  (ABC_Attach->BrushInvert == BrushOn )
     && BrushOn)
        Ctrl_SetMaxSpeed(ABC_Attach->MaxSpeed);

    if(FLOAT_ON == Cmd->Float)
    {
        Ctrl_SetMaxSpeed(ABC_Attach->MaxSpeed);
    }*/
    
                                                                      
    Valv_Set(&ABC_Attach->ValveRotation, ABC_Attach->ValuePWM); //изменено(убран BrushOn)
    //Valv_Set(&ABC_Attach->ValveRotation, BrushOn * ABC_Attach->ValuePWM); 
    
    if(  (ABC_Attach->BrushInvert == BrushOn )
     && BrushOn)
        Ctrl_SetMaxSpeed(ABC_Attach->MaxSpeed);

    if(FLOAT_ON == Cmd->Float)
    {
        Ctrl_SetMaxSpeed(ABC_Attach->MaxSpeed);
    }
    
}     

 void Brush_Timer_100ms(tABC_Attach *ABC_Attach, tJSCommands *Cmd) //(в структуру ABC_Attach добавлено ValuePWM) 
//(добавлена функция Brush_Timer_100ms)
{                                                            //(добавлено)
  if (BrushOn == PVE_PORT_A){                                //(добавлено) (-1 - порт A)
    timer = timer + 100;                                     //(добавлено) время прошедшее с начала нажатия на кнопку
    if (timer > DELAY){                                      //(добавлено) задержка 0.5с 
      if (ABC_Attach->ValuePWM > -(MAX_VALUE_PWM)){          //(добавлено)
        ABC_Attach->ValuePWM -= 100;                         //(добавлено) 100 - из расч.того,что плав. пуск щетки осущ за 1 сек         
      }                                                      //(добавлено)
      else                                                   //(добавлено)
        ABC_Attach->ValuePWM == -(MAX_VALUE_PWM);            //(добавлено)
    }                                                        //(добавлено)
  }                                                          //(добавлено)
  if (BrushOn == PVE_PORT_B){                                //(добавлено) (1 - порт B)
    timer = timer + 100;                                     //(добавлено) время прошедшее с начала нажатия на кнопку
    if (timer > DELAY){                                      //(добавлено)
      if (ABC_Attach->ValuePWM < MAX_VALUE_PWM){             //(добавлено)
        ABC_Attach->ValuePWM += 100;                         //(добавлено) 100 - из расч.того,что плав. пуск щетки осущ за 1 сек         
      }                                                      //(добавлено)
      else                                                   //(добавлено)
        ABC_Attach->ValuePWM == MAX_VALUE_PWM;               //(добавлено)
    }                                                        //(добавлено)
  }                                                          //(добавлено)
  if (BrushOn == PVE_PORT_NONE){                             //(добавлено) (0 - порт отсутствует) плавная остановка щетки
    timer = 0;                                               //(добавлено)
    if (ABC_Attach->ValuePWM > 0){                           //(добавлено)
        ABC_Attach->ValuePWM -= 100;                         //(добавлено) 100 - из расч.того,что плав. пуск щетки осущ за 1 сек         
      }                                                      //(добавлено)
    else if (ABC_Attach->ValuePWM < 0){                      //(добавлено)
          ABC_Attach->ValuePWM += 100;                       //(добавлено)
      }                                                      //(добавлено)
  }                                                          //(добавлено)
}                                                            //(добавлено)