﻿/****************************************************************************
 *
 * File:         VISU_STATISTIK.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu_statistik.h"
#include "gseMaint_Lifetime.h"
#include "control.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Visu_Statistic_Open(const tVisuData *Data)
{
    PrioMaskOn(MSK_STATISTIK);
}

void Visu_Statistic_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{
    const tControl *Ctrl = CtrlGet();
    SetVar(HDL_NEXTSERVICE, Maint_Lifetime_TimeUntilNextService_d(Ctrl->AnualMaint));
    for(uint32_t i = 0; i < evtc; i++)
    {
        if(CEVT_MENU_INDEX ==  evtv[i].Type)
        {
            //Set next Service
            if(OBJ_SETSERVICE == evtv[i].Content.mMenuIndex.ObjID)
            {
                Maint_Lifetime_SetNextServiceInSeconds(Ctrl->AnualMaint, 30 * 60 *60 * 24,GetVar(HDL_SETNEXTSERVICE) * 60 *60 * 24);
            }
        }
    }
    Visu_HomeKeyPressed();
}
