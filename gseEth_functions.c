﻿/****************************************************************************
 *
 * File:         ETH_UPLOAD.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */

#include "gseEth_functions.h"


/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/
tGsSocketTcpServer mUploadServer;
tGsSocketIntfInfo socketInfo;

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      : Upload_over_ETH IP
**
**    Description   : When the device get an IP or/and MAC Adress it will
**                    write it into the given object
**
**
**    Returnvalues  : 0 = waiting for IP Adress / 1 = get an IP Adress
**                    -1 = failure
*****************************************************************************/
int32_t Upload_over_ETH(uint32_t evtc, tUserCEvt * evtv, int32_t ip_obj_id, int32_t mac_obj_id)
{
  static int32_t Ethernet_Up = 0;
  if(!Ethernet_Up)
  {
    for(uint32_t i=0; i<evtc; i++)
    {
      if(CEVT_NETWORK == evtv[i].Type)
      {
        if(CEVT_NETWORK_STATE_CONFIG == evtv[i].Content.mNetwork.mState)
        {
          if(0 == gsSocketGetIntfInfo(NULL, &socketInfo))
          {
            if(ip_obj_id > -1)
              SetVisObjData(ip_obj_id, socketInfo.mIpAddr, strlen(socketInfo.mIpAddr) + 1);

            if(mac_obj_id > -1)
              SetVisObjData(mac_obj_id, socketInfo.mHwAddr, strlen(socketInfo.mHwAddr) + 1);
            if(5 < strlen(socketInfo.mIpAddr))
              Ethernet_Up = 1;
            else
              Ethernet_Up = -1;

            if(NULL == mUploadServer)
            {
              mUploadServer = gsSocketTcpServerCreate(23000,0);
              Ethernet_Up = 1;
            }
          }
        }
      }
    }
  }

  //UploadServer
  if(NULL != mUploadServer)
  {
    char buf[64] = {0};

    if((gsSocketTcpServerRead(mUploadServer, buf, sizeof(buf))) > 0)
    {
      if(strstr(buf, "updaterequest"))
      {
        SetProjectUpdate(GS_PRJ_UPDATE_TRIGGER);
      }
    }
  }
  return Ethernet_Up;
}

/****************************************************************************
**
**    Function      : DeInit_Upload_over_ETH
**
**    Description   : destroy the TCP Server which is used for the ETH Upload
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void DeInit_Upload_over_ETH(void)
{
  gsSocketTcpServerDestroy (mUploadServer);
}

char * Upload_over_ETH_Get_IP(void)
{
  return socketInfo.mIpAddr;
}

char * Upload_over_ETH_Get_Mac(void)
{
  return socketInfo.mHwAddr;
}

/****************************************************************************
**
**    Function      : Start_Debug_ETH
**
**    Description   : Open an TCPIP Server to send the debug information over the
**                    function Debug_Send.
**
**
**    Returnvalues  : 1 if device have no IP adress
**                    0 if a TCP server was added to the possible debug streams
**                   -1 if a TCP Server can´t added
**
*****************************************************************************/


/****************************************************************************
**
**    Function      : DeInit_Debug_ETH
**
**    Description   : destroy the TCP Server which is used for the ETH Debug
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void DeInit_Debug_ETH(void)
{
  gsDebugStreamsDeInit();
}

