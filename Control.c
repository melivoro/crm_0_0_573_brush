﻿/****************************************************************************
 *
 * File:         CONTROL.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "control.h"

#include "IO.h"
#include "io_types.h"

#include "IO.h"
#include "IO_Joystick_V85_B25.h"
#include "gseMCM.h"
#include "GSe_MsgBox.h"
#include "profile.h"
#include "RCText.h"
#include "gseErrorlist.h"
#include "errorlist.h"
#include "J1939.h"
#include "param.h"
#include "gseMaint.h"
#include "gseMaint_Lifetime.h"
#include "gseDebug.h"
#include "ctrl_brush.h"
#include "bnso.h"





/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/
typedef struct tagCtrlIO_Buttons
{
    tIOT_Button *Button;
    uint8_t Device;
    uint8_t idx;
}tCtrlIO_Buttons;

typedef struct tagCtrlIO_DOs
{
    uint8_t *DO;
    uint8_t Device;
    uint8_t idx;
}
tCtrlIO_DOs;
/*
typedef struct tagCtrlIO_DIs
{
    uint8_t *DI;
    uint8_t Device;
    uint8_t idx;
}
tCtrlIO_DIs;
*/
typedef struct tagCtrlIO_DIs
{
    eDISensorState *DI;
    uint8_t Device;
    uint8_t idx;
}
tCtrlIO_DIs;

typedef struct tagCtrlIO_AIs
{
    tIOT_AI *AI;
    uint8_t Device;
    uint8_t idx;
}
tCtrlIO_AIs;

typedef struct tagCtrlIO_Freqs
{
    uint32_t *Freq;
    uint8_t Device;
    uint8_t idx;
}
tCtrlIO_Freqs;

//Main structure for maintanance


/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


tControl _Ctrl;


tCtrlIO_Buttons MCM_StandardButtons[] =
{
    {.Button = &_Ctrl.Light.Key.PloughHead         , .Device = K2_4X3   ,.idx = 4-1},  //old 3
    {.Button = &_Ctrl.Light.Key.FrontBeaconCab     , .Device = K2_4X3   ,.idx = 8-1},  //old 2	Beacons or light bar on the vehicle's cab ON/OFF
    {.Button = &_Ctrl.Light.Key.RearBeaconTopEquip , .Device = K2_4X3   ,.idx = 6-1},  //old 8  Beacons/strobes at the back of the main equipment ON/OFF
    {.Button = &_Ctrl.Light.Key.LeftRoadSign       , .Device = K2_4X3   ,.idx = 10-1}, //old 7
    {.Button = &_Ctrl.Light.Key.RightRoadSign      , .Device = K2_4X3   ,.idx = 2-1},  //old 9
    {.Button = &_Ctrl.Light.Key.Work               , .Device = K2_4X3   ,.idx = 5-1},  //old 11
    {.Button = &_Ctrl.Light.Key.All                , .Device = K2_4X3   ,.idx = 12-1}, //old 1
    {.Button = &_Ctrl.Light.Key.WorkSide           , .Device = K2_4X3   ,.idx = 3-1},  //old count 6
    {.Button = &_Ctrl.Emcy                         , .Device = MCM250_3 ,.idx = 1},
    {.Button = &_Ctrl.PloughKeys.Up                , .Device = K2_PLOUGH ,.idx = 1},
    {.Button = &_Ctrl.PloughKeys.Down              , .Device = K2_PLOUGH ,.idx = 2}
};


tCtrlIO_DOs MCM_StandardDOs[] =
{

     {.DO = &_Ctrl.Light.On.PloughHead[0]        , .Device = MCM250_3 ,.idx = 1},
     {.DO = &_Ctrl.Light.On.PloughHead[1]        , .Device = MCM250_3 ,.idx = 3},
     {.DO = &_Ctrl.Light.On.FrontBeaconCab       , .Device = MCM250_3 ,.idx = 0},
     {.DO = &_Ctrl.Light.On.WorkSide             , .Device = MCM250_3 ,.idx = 2},
//LRS
     {.DO = &_Ctrl.Light.On.LRS_LeftRoadSign     , .Device = MCM250_4 ,.idx = 0},
     {.DO = &_Ctrl.Light.On.LRS_RightRoadSign    , .Device = MCM250_4 ,.idx = 2},
     {.DO = &_Ctrl.Light.On.LRS_RearBeacon       , .Device = MCM250_4 ,.idx = 6},
     {.DO = &_Ctrl.Light.On.LRS_WorkRear         , .Device = MCM250_4 ,.idx = 4},
//RTR
     {.DO = &_Ctrl.Light.On.RTR_LeftRoadSign     , .Device = MCM250_5 ,.idx = 0},
     {.DO = &_Ctrl.Light.On.RTR_RightRoadSign    , .Device = MCM250_5 ,.idx = 2},
     {.DO = &_Ctrl.Light.On.RTR_RearBeacon       , .Device = MCM250_5 ,.idx = 1},
     {.DO = &_Ctrl.Light.On.RTR_WorkRear         , .Device = MCM250_5 ,.idx = 3}

};

/*********
 * Sensors
 * ******/
tCtrlIO_DIs MCM_DIs[] =
{
    {.DI = &_Ctrl.Sensors.DI.Spreading          , .Device = MCM250_4, .idx = 0},
    {.DI = &_Ctrl.Sensors.DI.Humidifier         , .Device = MCM250_4, .idx = 1},
    {.DI = &_Ctrl.Sensors.DI.MaterialPresense   , .Device = MCM250_4, .idx = 2},
    {.DI = &_Ctrl.Sensors.DI.Ignition           , .Device = MCM250_2, .idx = 0},
    {.DI = &_Ctrl.Sensors.DI.OilFilter          , .Device = MCM250_1, .idx = 0},
    {.DI = &_Ctrl.Sensors.DI.UpperOilLevel      , .Device = MCM250_1, .idx = 1},
    {.DI = &_Ctrl.Sensors.DI.LowerOilLevel      , .Device = MCM250_1, .idx = 2},
    {.DI = &_Ctrl.Sensors.DI.PTO_Active         , .Device = MCM250_3, .idx = 0},
    {.DI = &_Ctrl.Sensors.DI.CheckOilLevelSensor, .Device = MCM250_1, .idx = 3}

};

tCtrlIO_AIs MCM_AIs[] =
{
    {.AI = &_Ctrl.Sensors.AI.Liquidpresence, .Device = MCM250_5, .idx = 0},
    {.AI = &_Ctrl.Sensors.AI.OilTemp       , .Device = MCM250_1, .idx = 0},
    {.AI = &_Ctrl.Sensors.AI.OilPressure   , .Device = MCM250_1, .idx = 1},
    {.AI = &_Ctrl.Sensors.AI.ValveErr[0]   , .Device = MCM250_2, .idx = 0},
    {.AI = &_Ctrl.Sensors.AI.ValveErr[1]   , .Device = MCM250_2, .idx = 1},
    {.AI = &_Ctrl.Sensors.AI.ValveErr[2]   , .Device = MCM250_2, .idx = 2},
    {.AI = &_Ctrl.Sensors.AI.ValveErr[3]   , .Device = MCM250_2, .idx = 3},
};

tCtrlIO_Freqs MCM_FreqIn[] =
{
    {.Freq = &_Ctrl.Sensors.Freq.ConveryRotation, .Device = MCM250_4, .idx = 3}
};

/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
enum DeviceState
{
    DEVICESTATE_MCM250_0,
    DEVICESTATE_MCM250_1,
    DEVICESTATE_MCM250_2,
    DEVICESTATE_MCM250_3,
    DEVICESTATE_MCM250_4,
    DEVICESTATE_K2_3X4,
    DEVICESTATE_K2_PLOUGH,
    DEVICESTATE_JOYSTICK,
    DEVICESTATE_ENGINE,
    DEVICESTATE_BNSO,
    DEVICESTATE_NUM
};




const tControl*  CtrlGet(void)
{
    return &_Ctrl;
}


void Ctrl_DeviceStates(tControl *Ctrl)
{
    //Check MCM-Modules

    static struct
    {
        uint32_t DeviceStates;
        int32_t DeviceTemp;
        uint32_t MCM_Handle;
        eErrorID ErrID_Offline;
        eErrorID ErrID_Temp;
    }MCM_States[]
    = {
        //state    //temp,device  , Error ID offline      , Error ID Temperature
        {0         ,0    ,MCM250_1, ERRID_MCM250_0_OFFLINE, ERRID_MCM250_0_TEMP},
        {0         ,0    ,MCM250_2, ERRID_MCM250_1_OFFLINE, ERRID_MCM250_1_TEMP},
        {0         ,0    ,MCM250_3, ERRID_MCM250_2_OFFLINE, ERRID_MCM250_2_TEMP},
        {0         ,0    ,MCM250_4, ERRID_MCM250_3_OFFLINE, ERRID_MCM250_3_TEMP},
        {0         ,0    ,MCM250_5, ERRID_MCM250_4_OFFLINE, ERRID_MCM250_4_TEMP}
    };

    const tParamData *Parameter = ParameterGet();
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(MCM_States); i++)
    {
        if(MCM_States[i].DeviceStates  != (uint32_t)GSeMCM_GetActiveStatus(MCM_States[i].MCM_Handle))
        {
            MCM_States[i].DeviceStates = GSeMCM_GetActiveStatus(MCM_States[i].MCM_Handle);
            if(i < MCM250_4)
            {
                if(MCM_States[i].DeviceStates)
                {
                    EList_ResetErr(MCM_States[i].ErrID_Offline);
                    //Check temperature only, if device is online
                    MCM_States[i].DeviceTemp = GSeMCM_GetTemperature(MCM_States[i].MCM_Handle);
                    if(Parameter->MCM.temp_max < (MCM_States[i].DeviceTemp / 10))
                    {
                        EList_SetErrLevByRCText(VBL_WARNING, MCM_States[i].ErrID_Temp, RCTEXT_T_MCM250_TEMPTOHIGH,i+1);
                    }
                    else
                    {
                        EList_ResetErr( MCM_States[i].ErrID_Temp);
                    }
                }
                else
                {
                    EList_SetErrLevByRCText(VBL_ERROR_CRITICAL, MCM_States[i].ErrID_Offline, RCTEXT_T_MCM250_OFFLINE, i+1);
                }
            }
            else if(i == MCM250_5)
            {
                //Reset MCM-Errors if MCM250_4 or MCM250_5 are online
                if(MCM_States[MCM250_4].DeviceStates
                 ||MCM_States[MCM250_5].DeviceStates)
                {
                    EList_ResetErr(MCM_States[MCM250_4].ErrID_Offline);
                    EList_ResetErr(MCM_States[MCM250_5].ErrID_Offline);
                    int32_t idx;
                    //get active device
                    if(MCM_States[MCM250_4].DeviceStates)
                    {
                        idx = MCM250_4;
                    }
                    else
                    {
                        idx = MCM250_5;
                    }
                    MCM_States[idx].DeviceTemp = GSeMCM_GetTemperature(MCM_States[idx].MCM_Handle);
                    if(Parameter->MCM.temp_max < (MCM_States[idx].DeviceTemp / 10))
                    {
                        EList_SetErrLevByRCText(VBL_WARNING, MCM_States[idx].ErrID_Temp, RCTEXT_T_MCM250_TEMPTOHIGH,idx+1);
                    }
                    else
                    {
                        EList_ResetErr( MCM_States[idx].ErrID_Temp);
                    }
                }
                else
                {
                    EList_SetErrLevByRCText(VBL_ERROR_CRITICAL, MCM_States[MCM250_4].ErrID_Offline, RCTEXT_T_MCM250_OFFLINE, MCM250_4+1);
                    EList_SetErrLevByRCText(VBL_ERROR_CRITICAL, MCM_States[MCM250_5].ErrID_Offline, RCTEXT_T_MCM250_OFFLINE, MCM250_5+1);
                }
            }
        }
    }
    //only MCM250_4 or MCM250_5 have to be online



    //Check Keyboard
    static struct
    {
        uint32_t DeviceStates;
        uint32_t DeviceTemp;
        uint32_t MCM_Handle;
        eErrorID ErrID_Offline;
        eErrorID ErrID_Temp;
    }K2_State = {0,0,K2_4X3, ERRID_K2_OFFLINE, ERRID_K2_TEMP};

  /*  if(K2_State.DeviceStates  != (uint32_t) GSeMCM_GetActiveStatus(K2_State.MCM_Handle))
    {
        K2_State.DeviceStates = GSeMCM_GetActiveStatus(K2_State.MCM_Handle);
        if(K2_State.DeviceStates)
        {
            EList_ResetErr(K2_State.ErrID_Offline);
            //check temperature only if device is online.
            K2_State.DeviceTemp = GSeMCM_GetTemperature(K2_State.MCM_Handle);
            if(Parameter->MCM.temp_max < ((int32_t) K2_State.DeviceTemp) / 10)
            {
                EList_SetErrLevByRCText(VBL_WARNING,K2_State.ErrID_Temp, RCTEXT_T_K2_TEMP);
            }
            else EList_ResetErr(K2_State.ErrID_Temp);
        }
        else
        {
            EList_SetErrLevByRCText(VBL_ERROR_CRITICAL,K2_State.ErrID_Offline, RCTEXT_T_K2_OFFLINE);
        }
    }*/


    static uint32_t JoystickState;
    if(JoystickState  != IO_Joystick_IsActive(&Ctrl->Joystick))
    {
        JoystickState =  IO_Joystick_IsActive(&Ctrl->Joystick);
        if(JoystickState)
        {
            EList_ResetErr(ERRID_JOYSTICK_OFFLINE);
        }
        else
        {
            EList_SetErrLevByRCText(VBL_ERROR_CRITICAL,  ERRID_JOYSTICK_OFFLINE, RCTEXT_T_JOYSTICKOFFLLINE);
        }
    }
    static uint32_t EngineState;
    if(EngineState  != Ctrl->Engine.Active)
    {
        EngineState = Ctrl->Engine.Active;
        if(EngineState)
        {
            EList_ResetErr(ERRID_ENGINE_OFFLINE);
        }
        else
        {
            EList_SetErrLevByRCText(VBL_ERROR_CRITICAL,  ERRID_ENGINE_OFFLINE, RCTEXT_T_ENGINEOFFLINE);
        }
    }
    static uint32_t BNSOState;
    if(BNSOState  != Ctrl->BNSO.Active)
    {
        BNSOState = Ctrl->BNSO.Active;
        if(BNSOState)
        {
            EList_ResetErr(ERRID_BNSO_OFFLINE);
        }
        else
        {
            EList_SetErrLevByRCText(VBL_ERROR_CRITICAL,  ERRID_BNSO_OFFLINE, RCTEXT_T_BNSO_OFFLINE);
        }
    }
}

void Ctrl_TippingError(void)
{
    static int32_t TippingErrors[4] = {0};
    for(int32_t i = 0; i < 4; i++)
    {
        if(_Ctrl.Sensors.AI.ValveErr[i].value > 500) // > 5V
        {
            if(TippingErrors[i] != 1)
            {
                TippingErrors[i] = 1;
                EList_SetErrLevByRCText(VBL_ERROR, ERRID_HYDRAULICVALVETIPPINGERROR_0 + i, RCTEXT_T_TIPPINGERROR, i, _Ctrl.Sensors.AI.ValveErr[i].value );
            }
        }
        else
        {
            if(TippingErrors[i] != 0)
            {
                TippingErrors[i] = 0;
                EList_ResetErr(ERRID_HYDRAULICVALVETIPPINGERROR_0 + i);
            }
        }
    }

}


void CheckRange(int32_t *value, int32_t min,int32_t max)
{
  if(*value < min)
    *value = min;
  else if(*value > max)
  {
    *value = max;
   // *value=min;//AK not tested
  }
}




/**
 * @brief this Function checks temperature and
 *
 * @param Ctrl
 */
void Ctrl_SystemErrors(tControl *Ctrl)
{
    static uint32_t count =  10000;// first 10 seconds no errordetection
    //Every 2 Seconds
    if(2000 < (int32_t)(GetMSTick() - count))
    {
        count = GetMSTick();
        //db_out("Check Errors\r\n");


        //Task 238 Critical oil level MCM250#1 pin26 HIGH
        if(0 == Ctrl->Sensors.DI.CheckOilLevelSensor)
        {
            //db_out("Setting Error RCTEXT_T_DECREASEINOILLEVEL\r\n");
            EList_SetErrLevByRCText(VBL_ERROR, ERRID_NO_OIL_SENSOR,  RCTEXT_T_OILSENSORNOTAVAILABLE);
            SetVar(HDL_OILLEVEL,-1);//Warning, grey Icon
        }
        else
        {
            EList_ResetErr(ERRID_NO_OIL_SENSOR);
            //Task 236 Decrease oil level, PIN25 & 26 Low
            if((0 == Ctrl->Sensors.DI.UpperOilLevel)
             &&(0 == Ctrl->Sensors.DI.LowerOilLevel))
            {
                //db_out("Setting Error RCTEXT_T_DECREASEINOILLEVEL\r\n");
                EList_SetErrLevByRCText(VBL_ERROR, ERRID_DECREASINGOILEVEL,  RCTEXT_T_DECREASEINOILLEVEL);
                EList_ResetErr(ERRID_CRITICAL_OIL_LEVEL);
                SetVar(HDL_OILLEVEL,1);//Warning, orange Icon
             }
            else if(1 == Ctrl->Sensors.DI.UpperOilLevel)
            {

                SetVar(HDL_OILLEVEL,0);//Warning, green Icon
                EList_ResetErr(ERRID_DECREASINGOILEVEL);
                EList_ResetErr(ERRID_CRITICAL_OIL_LEVEL);
            }
            else
            {
                //db_out("Setting Error RCTEXT_T_CRITICALOILLEVEL\r\n");
                EList_SetErrLevByRCText(VBL_ERROR, ERRID_CRITICAL_OIL_LEVEL,  RCTEXT_T_CRITICALOILLEVEL);
                EList_ResetErr(ERRID_DECREASINGOILEVEL);
                SetVar(HDL_OILLEVEL,2);//Critical, red Icon
            }
        }




        //Task 243 Low Voltage
       /* if(210 > GSeMCM_GetVoltage(MCM250_2))
        {
           // db_out("Set Low Voltage Error\r\n.");
           EList_SetErrLevByRCText(VBL_ERROR, ERRID_LOW_VOLTAGE,  RCTEXT_T_LOWVOLTAGE);
        }
        else
            EList_ResetErr(ERRID_LOW_VOLTAGE);
        //Task 243 High Voltage
        if(290 < GSeMCM_GetVoltage(MCM250_2))
            EList_SetErrLevByRCText(VBL_ERROR, ERRID_HIGH_VOLTAGE,  RCTEXT_T_HIGHVOLTAGE);
        else
            EList_ResetErr(ERRID_HIGH_VOLTAGE);
*/
        //Humidity Sensor
        /*if(DI_SENSOR_OFF == Ctrl->Sensors.DI.Humidifier)
            EList_SetErrLevByRCText(VBL_ERROR, ERRID_NO_WATERSENSOR,  RCTEXT_T_WATERSENSORNOTAVAILABLE);
        else
            EList_ResetErr(ERRID_NO_WATERSENSOR);*/
        Ctrl_TippingError();

    }

}





int32_t  EmcyBuzzer = 0;

void Emcy_OK_cb(void *arg)
{
   tControl *Ctrl = arg;
   if(!Ctrl->Emcy.value)
   {
        MsgBoxOk(RCTextGetText(RCTEXT_T_EMCY_BUTTON,GetVarIndexed(IDX_SYS_LANGUAGE) )
                , RCTextGetText(RCTEXT_T_EMCY_MSG,GetVarIndexed(IDX_SYS_LANGUAGE) )
                ,Emcy_OK_cb,Ctrl);
        EmcyBuzzer = 0;
   }
   else
   {
       GSeMCM_Emcy_Set(0);
       Ctrl->Emcy_Active = 0;
   }

}


void Ctrl_EMCY_Cycle(tControl *Ctrl,uint32_t evtc, tUserCEvt *evtv)
{

    if(Ctrl->Sensors.DI.Ignition)
    {
        if(!Ctrl->Emcy.value)
        {
            if(!Ctrl->Emcy_Active)
            {
                Ctrl->Emcy_Active = 1;
                MsgBoxOk(RCTextGetText(RCTEXT_T_EMCY_BUTTON,GetVarIndexed(IDX_SYS_LANGUAGE) )
                ,RCTextGetText(RCTEXT_T_EMCY_MSG,GetVarIndexed(IDX_SYS_LANGUAGE) )
                ,Emcy_OK_cb,Ctrl);
                GSeMCM_Emcy_Set(1);
                SetVar(HDL_EMCYSTOPNUM, 1 + GetVar(HDL_EMCYSTOPNUM));
                EmcyBuzzer = 1;
            }
            if(EmcyBuzzer)
            {
                SetBuzzer(1,10,10,1);
            }
        }
    }
}



void Ctrl_Automatic_Toggle(void)
{
    _Ctrl.Auto.on = 1 -_Ctrl.Auto.on;
}

void Ctrl_LiquidSensor_Toggle(void)
{
    //_Ctrl.Sensors.AI.Liquidpresence
}

void Ctrl_JS_Commands(tControl*Ctrl)
{
    //Get Commands for A, C read and inter attachment
    int32_t press = 0;
    int32_t sum = 0;
    for(int32_t i = 0; i <= EQUIP_C; i++)
    {
        if(IOT_Button_IsDown(&Ctrl->Joystick.Button[i]))
        {
            sum++;
        }
    }
    Ctrl->ABC_Buttons_Pressed = sum;
    for(int32_t i = 0; i <= EQUIP_C; i++)
    {
        int32_t Key = 0;
        if(sum == 1)//only if one key is pressed.
            Key = (IOT_Button_IsDown(&Ctrl->Joystick.Button[i]) ? 1:0);
        Ctrl_JS_Cmd_SetCommands(&Ctrl->CmdABC[i], Key );
        if(Key)
        {
            press = 1;
        }
    }

    if(press)
    {
         Ctrl_JS_Cmd_SetCommands(&Ctrl->CmdNoKey, 0 );
    }
    else Ctrl_JS_Cmd_SetCommands(&Ctrl->CmdNoKey, 1);
}



void Ctrl_ResetMaxSpeed(void)
{
    _Ctrl.MaxSpeed = 0;
}

void Ctrl_SetMaxSpeed(uint32_t MaxSpeed)
{
  if(_Ctrl.MaxSpeed == 0)
  {
      _Ctrl.MaxSpeed = MaxSpeed;
  }
  if(MaxSpeed < _Ctrl.MaxSpeed)
  {
    _Ctrl.MaxSpeed = MaxSpeed;
  }
}

void Ctrl_CheckMaxSpeed(tControl *Ctrl)
{
  static int32_t ErrState;
  static int32_t count;

  if((_Ctrl.MaxSpeed == 0)
   ||(_Ctrl.Engine.speed < ((double) _Ctrl.MaxSpeed + 10)))
  {
    if(ErrState)//Reset Error
    {
      EList_ResetErr(ERRID_OVERSPEED);
      ErrState = 0;
    }
    count = GetMSTick();
  }
  else
  {
    if(ErrState == 0)//only if error is not set, yet
    {
      //Set Overspeed error if overspeed longer than 2 seconds
      if(2000 < GetMSTick() -count)
      {
        ErrState = 1;
        EList_SetErrLevByRCText(VBL_WARNING, ERRID_OVERSPEED, RCTEXT_T_OVERSPEED);
      }
    }
  }
}

void Ctrl_CheckMaximumRPM(tControl *Ctrl)
{
    const tParamData * Parameter = ParameterGet();
    //Check if Engine Take off is on
    int32_t pto_active = 0;
    SetVarIndexed(IDX_PTO_ON, 0); //AK: not tested
    if(1 == Parameter->Engine.PTOatEngine )
    {
        pto_active = 1;
    }
    else if( DI_SENSOR_ON == Ctrl->Sensors.DI.PTO_Active)
    {
        pto_active = 1;
        SetVarIndexed(IDX_PTO_ON, 1);//AK not tested
    }
    //check the actual rpm
    if((Ctrl->Engine.rpm >  (int16_t)Parameter->Engine.RPM_Max)
     &&(pto_active))
    {
        EList_SetErrLevByRCText(VBL_ERROR, ERRID_MAX_ENGINE_RPM, RCTEXT_T_MAXRPM, Parameter->Engine.RPM_Max);
    }
    else
    {
        EList_ResetErr( ERRID_MAX_ENGINE_RPM);
    }

    //set / reset the error.
}



void Ctrl_MaxSpeed_Draw(tControl *Ctrl)
{
    static int32_t DrawAttr_Old = -1;
    int32_t DrawAttr = 0;
    if(0 == Ctrl->MaxSpeed)
    {
        DrawAttr = 0;

    }
    else if(Ctrl->Engine.speed > (int16_t) Ctrl->MaxSpeed)
    {
         DrawAttr = ATTR_BLINKING | ATTR_VISIBLE | ATTR_TRANSPARENT | ATTR_SMOOTH;
    }
    else DrawAttr = ATTR_VISIBLE | ATTR_TRANSPARENT | ATTR_SMOOTH;

    if(DrawAttr != DrawAttr_Old)
    {
        DrawAttr_Old = DrawAttr;
        SetVarIndexed(IDX_MAXSPEEDATT, DrawAttr);
    }
    SetVarIndexed(IDX_MAXSPEED, Ctrl->MaxSpeed);
}

void Ctrl_DISensor_Activate(DI_Sensor *di)
{
    *di = DI_SENSOR_UNKNOWN;
}

void Ctrl_DISensor_DeActivate(DI_Sensor *di)
{
    *di = DI_SENSOR_INACTIVE;
}

int8_t Ctrl_IsDISensor_Active(DI_Sensor di)
{
    if (di == DI_SENSOR_INACTIVE)
        return 0;
    return 1;
}

void Ctrl_Init(void)
{
   // Ctrl_Brush_Init(&_Ctrl);
    J1939_Init(CAN1, &_Ctrl);

    //Init CAN-Modules
    IO_Init(&_Ctrl);

    ConveyorCreate(&_Ctrl.Top.Conveyor);
    SpreaderDiskCreate(&_Ctrl.Top.Disk);
    //Load characteristic curve for PVEP-Valv.
    GlobalInitValvePropPVEP();
    GlobalInitValveProp12V();

    LRS_Create(&_Ctrl.Top.LRS );
    /*******************
     * Maintanance and Runtime
     * ****************/
    //Create Runtime for tABCAttaches;
    Maint_Init(&_Ctrl.Maint);
    Maint_Init(&_Ctrl.MaintHourcounter);

    Brush_Maint_Create(&_Ctrl.ABC[EQUIP_A].ABC_Attach, &_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_BRUSH_FRONT      , RCTEXT_T_FRONTBRUSHWARN  , RCTEXT_T_FRONTBRUSHSERVICE      ,IDX_BRUSH_WARN_0     , IDX_BRUSH_SERVICE_0 );
    Brush_Maint_Create(&_Ctrl.ABC[EQUIP_C].ABC_Attach, &_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_Brush_SIDE       , RCTEXT_T_SIDEBRUSHWARN  , RCTEXT_T_SIDEBRUSHSERVICE       ,IDX_BRUSH_WARN_1     , IDX_BRUSH_SERVICE_1 );
    Brush_Maint_Create(&_Ctrl.ABC[EQUIP_B].ABC_Attach, &_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_BRUSH_INTERAXIS, RCTEXT_T_INTERAXISBRUSHWARN, RCTEXT_T_INTERAXISBRUSHWARN     ,IDX_BRUSH_WARN_2     , IDX_BRUSH_SERVICE_2 );
    Brush_Maint_Create(&_Ctrl.Rear.ABC_Attach, &_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_Brush_REAR          , RCTEXT_T_REARBRUSHWARN        , RCTEXT_T_REARBRUSHSERVICE       ,IDX_BRUSH_WARN_3     , IDX_BRUSH_SERVICE_3 );
    _Ctrl.Hydr.Load            = Maint_Runtime_Add(&_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_OILPUMP_LOAD    , RCTEXT_T_HYD_PUMP_LOAD_WARN   , RCTEXT_T_HYD_PUMP_LOAD_SERVICE  ,IDX_RUNTIME_HYDLOAD  , IDX_SERVICE_HYDLOAD, 100 );
    _Ctrl.Hydr.UnLoad          = Maint_Runtime_Add(&_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_OILPUMP_UNLOAD  , RCTEXT_T_HYD_PUMP_LOAD_WARN   , RCTEXT_T_HYD_PUMP_UNLOAD_SERVICE,IDX_RUNTIME_HYDNOLOAD, IDX_SERVICE_HYDNOLOAD, 100 );
    _Ctrl.Top.Conveyor.Runtime = Maint_Runtime_Add(&_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_RTR             , RCTEXT_T_RTR_WARN             , RCTEXT_T_RTR_SERVICE            ,IDX_RUNTIME_RTR      , IDX_SERVICE_RTR , 100 );
    //_Ctrl.Top.Conveyor.Runtime = Maint_Runtime_Add(&_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_RTR_HUMID     , RCTEXT_T_RTR_H_WARN           , RCTEXT_T_RTR_H_SERVICE          ,IDX_RUNTIME_RTRH     , IDX_SERVICE_RTRH, 100 );
    _Ctrl.Top.LRS.Runtime      = Maint_Runtime_Add(&_Ctrl.MaintHourcounter, ERRID_MAINTENANCE_LRS             , RCTEXT_T_LRS_WARN             , RCTEXT_T_LRS_SERVICE            ,IDX_RUNTIME_LRS      , IDX_SERVICE_LRS , 100 );
    _Ctrl.AnualMaint = Maint_Lifetime_Add(&_Ctrl.Maint,ERRID_MAINTENANCE_ANNUAL, RCTEXT_T_ANNUALMAINTENANCEWARN,RCTEXT_T_ANNUALMAINT, IDX_ANNUALSERVICE,  30 * 24);// Warn 30 days before

    _Ctrl.Auto.on = 1;
    const tParamData *Parameter = ParameterGet();

    if(Parameter->Engine.PTOatEngine)
        Maint_Runtime_SetDiv(_Ctrl.Hydr.UnLoad, 1/*10*/);

    /******************
     * Init analog Inputs
     * ***************/

    const tParamData *Param = ParameterGet();
    IOT_NormAI_Init(&_Ctrl.Sensors.AI.Liquidpresence
                    , Param->Sensors.LiquidPresence.x1
                    , Param->Sensors.LiquidPresence.x2
                    , Param->Sensors.LiquidPresence.y1
                    , Param->Sensors.LiquidPresence.y2
                    ,0,0);
    IOT_NormAI_Init(&_Ctrl.Sensors.AI.OilPressure
                    , Param->Sensors.OilPressure.x1
                    , Param->Sensors.OilPressure.x2
                    , Param->Sensors.OilPressure.y1
                    , Param->Sensors.OilPressure.y2
                    ,0,0);
    IOT_NormAI_Init(&_Ctrl.Sensors.AI.OilTemp
                    , Param->Sensors.OilTemp.x1
                    , Param->Sensors.OilTemp.x2
                    , Param->Sensors.OilTemp.y1
                    , Param->Sensors.OilTemp.y2
                    ,0,0);
    for(int32_t i = 0; i < 4;i++)
    {
        IOT_NormAI_Init(&_Ctrl.Sensors.AI.ValveErr[i]
                    , 0
                    , 1000
                    , 0
                    , 1000
                    ,0,0);
    }
    SQL_Close();
}

/**
 * @brief Checks if a brush is connected to the Attachment and if it is
 * turning. If so, it starts the Counter for the runtime.
 *
 * @param ABC_Attach
 */
void CtrlHourCounter_tABCAttachCycle(tABC_Attach * ABC_Attach)
{
    if(Brush_Active(ABC_Attach))
        Maint_Runtime_Start(ABC_Attach->MaintRunntime);
    else
        Maint_Runtime_Stop(ABC_Attach->MaintRunntime);
}


/**
 * @brief Controles the Hourcounters for the different Attachments and the Engine
 *
 * @param Ctrl
 */
void Ctrl_HourConter_Cycle(tControl *Ctrl)
{
    //Check tABCAttaches:
    CtrlHourCounter_tABCAttachCycle(&Ctrl->ABC[EQUIP_A].ABC_Attach);
    CtrlHourCounter_tABCAttachCycle(&Ctrl->ABC[EQUIP_C].ABC_Attach);
    CtrlHourCounter_tABCAttachCycle(&Ctrl->ABC[EQUIP_B].ABC_Attach);
    CtrlHourCounter_tABCAttachCycle(&Ctrl->Rear.ABC_Attach);

    //RTR Working hours
    if(Ctrl->Top.Disk.on
     ||Ctrl->Top.Conveyor.on)
    {
        Maint_Runtime_Start(Ctrl->Top.Conveyor.Runtime);
    }
    else Maint_Runtime_Stop(Ctrl->Top.Conveyor.Runtime);

   //LRS Working hours:
    if(0 < WaterPump_IsOn(&Ctrl->Top.Pump))
    {
        Maint_Runtime_Start(Ctrl->Top.LRS.Runtime);
    }
    Maint_Runtime_Stop(Ctrl->Top.LRS.Runtime);

    //Check if Hydraulik Pump is working and if it is under load or not.
    int32_t HydPumpOn = 0;
    const tParamData *Param = ParameterGet();
    if(Param->Engine.PTOatEngine)
    {
        if(Ctrl->Engine.rpm > 0)
        {
            HydPumpOn = 1;
        }
    }
    else
    {
        if(Ctrl->Engine.PTO_Engaged)
        {
            HydPumpOn = 1;
        }
    }
    if(HydPumpOn)//Hydraulic pump is running.
    {
        //does the pump work with load or without?
        if(Maint_Runtime_IsRunning(Ctrl->ABC[EQUIP_A].ABC_Attach.MaintRunntime)
         ||Maint_Runtime_IsRunning(Ctrl->ABC[EQUIP_C].ABC_Attach.MaintRunntime)
         ||Maint_Runtime_IsRunning(Ctrl->Rear.ABC_Attach.MaintRunntime)
         ||Maint_Runtime_IsRunning(Ctrl->ABC[EQUIP_B].ABC_Attach.MaintRunntime)
         ||Maint_Runtime_IsRunning(Ctrl->Top.LRS.Runtime)
         ||Maint_Runtime_IsRunning(Ctrl->Top.Conveyor.Runtime)
        )
        {
            Maint_Runtime_Start(Ctrl->Hydr.Load);
            Maint_Runtime_Stop(Ctrl->Hydr.UnLoad);

        }
        else
        {
            Maint_Runtime_Stop(Ctrl->Hydr.Load);
            Maint_Runtime_Start(Ctrl->Hydr.UnLoad);
            Maint_Runtime_SetDiv(Ctrl->Hydr.UnLoad , 1/*10*/);//count only every 10th second
        }
    }
    else
    {
        Maint_Runtime_Stop(Ctrl->Hydr.Load);
        Maint_Runtime_Stop(Ctrl->Hydr.UnLoad);
    }
    if(GetVarIndexed(IDX_BRUSHON_FRONT)
     ||GetVarIndexed(IDX_BRUSHON_SIDE)
     ||GetVarIndexed(IDX_BRUSHON_INTER))
    {
        Maint_Runtime_SetDiv(Ctrl->Hydr.Load , 1);//count every second
    }
    else
    {
        Maint_Runtime_SetDiv(Ctrl->Hydr.Load , 1/*10*/);//count only every 10th second
    }
    SetVarIndexed(IDX_RUNTIME_HYDLOAD_SHOW, GetVarIndexed(IDX_RUNTIME_HYDLOAD) );
    SetVarIndexed(IDX_RUNTIME_HYDNOLOAD_SHOW, GetVarIndexed(IDX_RUNTIME_HYDNOLOAD));
}

void Ctrl_Cycle(uint32_t evtc, tUserCEvt *evtv)
{

    /**********************
     * read inputs
     * ********************/
    IO_Cycle(&_Ctrl,evtc,evtv);
    //Read Buttons
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(MCM_StandardButtons); i++)
    {
        IOT_Button_SetDI(MCM_StandardButtons[i].Button,GSeMCM_GetDigitalIn(MCM_StandardButtons[i].Device, MCM_StandardButtons[i].idx));
    }
    //Read digital Inputs
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(MCM_DIs);i++)
    {
       if(*MCM_DIs[i].DI !=DI_SENSOR_INACTIVE)
        *MCM_DIs[i].DI =  GSeMCM_GetDigitalIn(MCM_DIs[i].Device, MCM_DIs[i].idx);
    }

    //Read analog Inputs
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(MCM_AIs);i++)
    {
       IOT_NormAI_Calc(MCM_AIs[i].AI, GSeMCM_GetAnalogIn(MCM_AIs[i].Device, MCM_AIs[i].idx));
    }


    //Read frequency Inputs
    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(MCM_FreqIn);i++)
    {
       *MCM_FreqIn[i].Freq =  GSeMCM_GetFrequencyInput(MCM_FreqIn[i].Device, MCM_FreqIn[i].idx);
    }

    Ctrl_EMCY_Cycle(&_Ctrl, evtc, evtv);

    //Read Engine BSNO
    J1939_Cycle(&_Ctrl.Engine);


    //Interpret Inputs

    //overwrite Joystick comands.
    if(1 == GSeMCM_GetActiveStatus(K2_PLOUGH))
    {
        //do something here
        if(IOT_Button_IsDown(&_Ctrl.PloughKeys.Up))
        {
            _Ctrl.CmdABC[EQUIP_A].Updown = 1000;
        }
        else if(IOT_Button_IsDown(&_Ctrl.PloughKeys.Down))
        {
            _Ctrl.CmdABC[EQUIP_A].Updown = -1000;
        }
        else
        {
            _Ctrl.CmdABC[EQUIP_A].Updown = 0;
        }
    }
    else
    {
        Ctrl_JS_Commands(&_Ctrl);
    }

    /***********************
     * Contorle code
     * *********************/
    Ctrl_Light(&_Ctrl.Light, evtc, evtv);
    Ctrl_ResetMaxSpeed();
    Prof_Cycle(&_Ctrl, evtc, evtv);



    /**************************
     * Write Outputs
     * ***********************/

    for(uint32_t i = 0; i < GS_ARRAYELEMENTS(MCM_StandardDOs); i++)
    {
        GSeMCM_SetDigitalOut(MCM_StandardDOs[i].Device, MCM_StandardDOs[i].idx, *MCM_StandardDOs[i].DO);
    }


    static uint32_t timer = 0;
    static int32_t init = 0;
    if(!init)
    {
        init = 1;
        timer = GetMSTick() + 10000;
    }
    if(500 < (GetMSTick() -timer))
    {
        timer = GetMSTick();
        Ctrl_SystemErrors(&_Ctrl);
        Ctrl_DeviceStates(&_Ctrl);
        Maint_Check(&_Ctrl.Maint);

        J1939_Timer(&_Ctrl);
        Maint_Runtime_Cycle();
        Ctrl_HourConter_Cycle(&_Ctrl);
        Ctrl_CheckMaxSpeed(&_Ctrl);
        Ctrl_MaxSpeed_Draw(&_Ctrl);
    }
    //Actualize data of the BNSO
    BNSO_SetData();

}


void Ctrl_Timer_MS(void)
{

    static uint32_t count = 0;
    count++;

    //Profiles
    Prof_Timer_10ms(&_Ctrl);

}


void Ctrl_DeInit(void)
{
    GSeMCM_DeInit();

}
