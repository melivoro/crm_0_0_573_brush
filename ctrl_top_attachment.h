﻿/************************************************************************
 *
 * File:         CTRL_TOP_ATTACHMENT.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CTRL_TOP_ATTACHMENT_H
#define CTRL_TOP_ATTACHMENT_H
#include "control.h"
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/

void RTR_Init(tControl *Ctrl);
void RTR_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);
void RTR_Timer_10ms(tControl *Ctrl);


void Ctrl_top_LRS_CK_Init(tControl *Ctrl);
void Ctrl_top_LRS_CK_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);
void Ctrl_top_LRS_CK_Timer_10ms(tControl *Ctrl);


void Ctrl_top_E2000_BP300_Init(tControl *Ctrl);
void Ctrl_top_E2000_BP300_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);

void Ctrl_top_E2000_C610H_Init(tControl *Ctrl);
void Ctrl_top_E2000_C610H_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);

void Ctrl_top_E2000_BP300_C610H_Init(tControl *Ctrl);
void Ctrl_top_E2000_BP300_C610H_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);

void Ctrl_top_E2000_C610H_RR300_Init(tControl *Ctrl);
void Ctrl_top_E2000_C610H_RR300_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);

void Ctrl_top_E2000_C610H_RR400_Init(tControl *Ctrl);
void Ctrl_top_E2000_C610H_RR400_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);

void Ctrl_top_E2000_BP300_RR300_Init(tControl *Ctrl);
void Ctrl_top_E2000_BP300_RR300_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);

void Ctrl_top_E2000_BP300_RR400_Init(tControl *Ctrl);
void Ctrl_top_E2000_BP300_RR400_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);

void Ctrl_top_CF_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);


void Ctrl_top_RTR_KH_Init(tControl *Ctrl);
void Ctrl_top_RTR_SH_Init(tControl *Ctrl);
void Ctrl_top_RTR_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv);
void Ctrl_top_RTR_SH_Timer_10ms(tControl *Ctrl);//needed to controle asymetry drive.


/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CTRL_TOP_ATTACHMENT_H
