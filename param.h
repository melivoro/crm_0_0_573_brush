﻿/************************************************************************
 *
 * File:         PARAM.h
 * Project:      CRM
 * Author(s):    Marc Schartel
 * Date:         29.11.2019
 *
 * Description: This File is used to define a structure for parameters which can
 * be saved in a file on the flash. At start up the parameter file is read and
 * the data written into a structure, which can be accessed by const tParamData * ParameterGet(void);
 * The file can be changed, In- an Exported form / to USB in the file Visu_param.c
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef PARAM_H
#define PARAM_H

/*************************************************************************/

/* macro definitions *****************************************************/
#define PATH_PARAM_FLASH "/gs/data/param.cfg"
#define PATH_PARAM_USB   "/gs/usb/param.cfg"
/* type definitions ******************************************************/

typedef struct tagParamEngine
{
    char Name[48];      //Name of the Engie for later, to differ between different engines
    uint32_t Id;        //ID of the Engine.
    uint32_t Id2;
    uint8_t  PTOatEngine;
    uint32_t RPM_Max;
}tParamEngine;

typedef struct tagParamMCM
{
  int32_t temp_max;     //Maximum temperature of MCM-Modules
}tParamMCM;

typedef struct tagParamJoystick
{
  uint32_t Id;          //J1939-Id of the Joystick
  char name[48];
}tParamJoystick;

typedef struct tagParamAI
{
  int32_t x1, x2;
  int32_t y1, y2;
}tParamAI;

typedef struct tagParamSensor
{
  tParamAI OilTemp;
  tParamAI OilPressure;
  tParamAI LiquidPresence;
}tParamSensor;

typedef struct tagParamAsymDrive
{
  int32_t I_Max;
}tParamAsymDrive;

/**
 * @brief Structure with all parameters saved in "/gs/data/param.cfg". You can
 * access to a variable of this structure by ParameterGet() in the programm code.
 * Use this structure to save parameters permanently.
 * In the C-File you can find the Array ParamArray. With this array you can define
 * how the informations of this structure are saved in the parameter file.
 *
 */
typedef struct tagParamData
{
  tParamEngine     Engine;
  tParamMCM        MCM;
  tParamJoystick   Joystick;
  tParamSensor     Sensors;
  tParamAsymDrive  AsymDrive;

}tParamData;
/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/


/**
 * @brief Reads the config file from"/gs/data/param.cfg" and fills the Parameters
 * defined in ParamArray. If an Entry is not found, default values will be used.
 * If the parameter file has not been found, a new one will be generated.
 *
 */
void InitParam(void);

/**
 * @brief returns a pointer to the filled parameter structure.
 *
 * @return const tParamData*
 */
const tParamData * ParameterGet(void);




/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef PARAM_H
