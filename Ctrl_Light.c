﻿/****************************************************************************
 *
 * File:         CTRL_LIGHT.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h> /* Events send to the Cycle function              */
#include <UserCAPI.h>    /* API-Function declarations                      */

#include "vartab.h" /* Variable Table definition                      */
                    /* include this file in every C-Source to access  */
                    /* the variable table of your project             */
                    /* vartab.h is generated automatically            */
#include "objtab.h" /* Object ID definition                           */
                    /* include this file in every C-Source to access  */
                    /* the object ID's of the visual objects          */
                    /* objtab.h is generated automatically            */
#include "control.h"
#include "Ctrl_Brush.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/

/****************************************************************************/

/* function code ************************************************************/

/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
/***********************************
 * Lights
 * *********************************/

/**
 * @brief TaskID 65
 *  Switch on Worklight if tABC_Attach on or floating mode
 *
 * @param tControl
 */
void Ctrl_Light_Worklight_AutoOn(tLight *Light)
{
    //switch on Worklight at the A, if tABC_Attach or floating
    static int32_t AutoPloughhead = 0;
    const tControl *Ctrl = CtrlGet();
    if (Brush_Active(&Ctrl->ABC[EQUIP_A].ABC_Attach) || (0 != Valv_GetState(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveFloat)))
    {
        //only switch ploughead on, if floating or rotating hasn't beem active before.
        if (!AutoPloughhead)
        {
            Light->On.PloughHead[0] = 1;
            Light->On.PloughHead[1] = 1;
            AutoPloughhead = 1;
        }
    }
    else
    {
        AutoPloughhead = 0;
    }

    //switch on Worklight at the C, if tABC_Attach floating
    static int32_t AutoWorklight_Side = 0;
   if (Brush_Active(&Ctrl->ABC[EQUIP_C].ABC_Attach) || (0 != Valv_GetState(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveFloat)))
    {
        if (!AutoWorklight_Side)
        {
            Light->On.WorkSide = 1;
            AutoWorklight_Side = 1;
        }
    }
    else
    {
        AutoWorklight_Side = 0;
    }
}

int32_t LightAlarm_Off = 0;
void Ctrl_Light_Alarm_On(tLight *Light)
{
    if (!LightAlarm_Off)
    {
        Light->On.FrontBeaconCab = 1;
        //Light->On.WorkSide = 1;
//        Light->On.PloughHead        = 1;

        Light->On.RTR_RearBeacon = 1;
        //Light->On.RTR_LeftRoadSign = 1;
        //Light->On.RTR_RightRoadSign = 1;
        //Light->On.RTR_WorkRear      = 1;

        Light->On.LRS_RearBeacon = 1;
        //Light->On.LRS_LeftRoadSign = 1;
        //Light->On.LRS_RightRoadSign = 1;
        //Light->On.LRS_WorkRear      = 1;
        LightAlarm_Off = 1; //prevent from switching on again, automatically.
    }
}

void Ctrl_Light_Alarm_Off(tLight *Light)
{
    Light->On.FrontBeaconCab = 0;
    Light->On.WorkSide = 0;
    Light->On.PloughHead[0] = 0;
    Light->On.PloughHead[1] = 0;

    Light->On.RTR_RearBeacon = 0;
    Light->On.RTR_LeftRoadSign = 0;
    Light->On.RTR_RightRoadSign = 0;
    Light->On.RTR_WorkRear = 0;

    Light->On.LRS_RearBeacon = 0;
    Light->On.LRS_LeftRoadSign = 0;
    Light->On.LRS_RightRoadSign = 0;
    Light->On.LRS_WorkRear = 0;
    LightAlarm_Off = 0; //prevent from switching on again, automatically.
}

/**
 * @brief Task 66
 *
 * @param Ctrl
 */
void Ctrl_Light_Beackon_AutoOn(tLight *Light)
{
    //check tABCAttaches:
    const tControl *Ctrl = CtrlGet();
    int32_t AlarmOn = 0;
    for (int32_t i = 0; i < 3; i++)
    {

        if (Brush_Active(&Ctrl->ABC[i].ABC_Attach) || (0 != Valv_GetState(&Ctrl->ABC[i].ABC_Attach.ValveFloat)))
        {
            Ctrl_Light_Alarm_On(Light);
            AlarmOn = 1;
            return;
        }
    }
    if (0 < WaterPump_IsOn(&Ctrl->Top.Pump))
    {
        Ctrl_Light_Alarm_On(Light);
        AlarmOn = 1;
        return;
    }
    else if (/*Ctrl->Top.Disk.running ||*/ Ctrl->Top.Conveyor.on)
    {
        AlarmOn = 1;
        Ctrl_Light_Alarm_On(Light);
    }
    if (0 == AlarmOn)
    {
        LightAlarm_Off = 0;
    }
}

void Ctrl_Light(tLight *Light, uint32_t evtc, tUserCEvt *evtv)
{
    //Toggle Lights
    if (IsMaskOn(MSK_MAIN) != 1)
        MsgContainerOff(CNT_LIGHTKEYS);
    else
    {
        if (IsMsgContainerOn(CNT_LIGHTKEYS))
        {
            if (IsKeyPressedNew(1))
            {
                Light->On.RTR_RearBeacon = 1 - Light->On.RTR_RearBeacon;
                Light->On.LRS_RearBeacon = 1 - Light->On.LRS_RearBeacon;
                Light->On.FrontBeaconCab = 1 - Light->On.FrontBeaconCab;
            }

           if (IsKeyPressedNew(4) && GetVarIndexed(IDX_ROADSIGN_LEFT) !=0)
            {
                Light->On.RTR_LeftRoadSign = 1 - Light->On.RTR_LeftRoadSign;
                Light->On.LRS_LeftRoadSign = 1 - Light->On.LRS_LeftRoadSign;
            }

            if (IsKeyPressedNew(5) && GetVarIndexed(IDX_ROADSIGN_RIGHT) !=0)
            {
                Light->On.RTR_RightRoadSign = 1 - Light->On.RTR_RightRoadSign;
                Light->On.LRS_RightRoadSign = 1 - Light->On.LRS_RightRoadSign;
            }

            if (IsKeyPressedNew(3))
            {

                Light->On.LRS_WorkRear = 1 - Light->On.LRS_WorkRear;
                Light->On.RTR_WorkRear = 1 - Light->On.RTR_WorkRear;
            }

            if (IsKeyPressedNew(2))
            {

                Light->On.WorkSide = 1 - Light->On.WorkSide;
            }
        }
    }
    /* {
      if(IOT_Button_IsPressedNew(&Light->Key.LeftRoadSign))
      {
        Light->On.RTR_LeftRoadSign = 1 - Light->On.RTR_LeftRoadSign;
        Light->On.LRS_LeftRoadSign = 1 - Light->On.LRS_LeftRoadSign;
      }


     if(IOT_Button_IsPressedNew(&Light->Key.RightRoadSign))
    {
        Light->On.RTR_RightRoadSign = 1 - Light->On.RTR_RightRoadSign;
        Light->On.LRS_RightRoadSign = 1 - Light->On.LRS_RightRoadSign;

    }


     if(IOT_Button_IsPressedNew(&Light->Key.RearBeaconTopEquip))
    {
        Light->On.RTR_RearBeacon = 1 - Light->On.RTR_RearBeacon;
        Light->On.LRS_RearBeacon = 1 - Light->On.LRS_RearBeacon;
    }


    if(IOT_Button_IsPressedNew(&Light->Key.PloughHead))
    {
        Light->On.PloughHead[0] = 1 - Light->On.PloughHead[0];
        Light->On.PloughHead[1] = 1 - Light->On.PloughHead[1];
    }


     if(IOT_Button_IsPressedNew(&Light->Key.FrontBeaconCab))
    {
        Light->On.FrontBeaconCab = 1 - Light->On.FrontBeaconCab;
    }


     if(IOT_Button_IsPressedNew(&Light->Key.Work))
    {

        Light->On.LRS_WorkRear = 1 - Light->On.LRS_WorkRear;
        Light->On.RTR_WorkRear = 1 - Light->On.RTR_WorkRear;
    }

     if(IOT_Button_IsPressedNew(&Light->Key.WorkSide))
    {

        Light->On.WorkSide = 1 - Light->On.WorkSide;
    }


     if(IOT_Button_IsPressedNew(&Light->Key.All))
    {
        //Check if more light are off or on
        uint32_t LightCount = 0;
        if(Light->On.LRS_LeftRoadSign)
            LightCount++;
        if(Light->On.LRS_RightRoadSign)
            LightCount++;
        if(Light->On.LRS_RearBeacon)
            LightCount++;
        if(Light->On.FrontBeaconCab)
            LightCount++;
        if(Light->On.LRS_WorkRear)
            LightCount++;
        if(Light->On.WorkSide)
            LightCount++;
        if(LightCount >= 3)//most lights are on
        {
            Light->On.RTR_LeftRoadSign   = 0;
            Light->On.LRS_LeftRoadSign   = 0;
            Light->On.RTR_RightRoadSign  = 0;
            Light->On.LRS_RightRoadSign  = 0;
            Light->On.RTR_RearBeacon     = 0;
            Light->On.LRS_RearBeacon     = 0;
            Light->On.FrontBeaconCab     = 0;
            Light->On.RTR_WorkRear       = 0;
            Light->On.LRS_WorkRear       = 0;
            Light->On.PloughHead[0]      = 0;
            Light->On.PloughHead[1]      = 0;
            Light->On.WorkSide           = 0;
        }
        else// most lights are off.
        {
            Light->On.RTR_LeftRoadSign   = 1;
            Light->On.LRS_LeftRoadSign   = 1;
            Light->On.RTR_RightRoadSign  = 1;
            Light->On.LRS_RightRoadSign  = 1;
            Light->On.RTR_RearBeacon     = 1;
            Light->On.LRS_RearBeacon     = 1;
            Light->On.FrontBeaconCab     = 1;
            Light->On.RTR_WorkRear       = 1;
            Light->On.LRS_WorkRear       = 1;
            Light->On.PloughHead[0]      = 1;
            Light->On.PloughHead[1]      = 1;
            Light->On.WorkSide           = 1;
        }
    }*/

    Ctrl_Light_Worklight_AutoOn(Light);
    Ctrl_Light_Beackon_AutoOn(Light);
}

void Light_Draw(const tLight *Light)
{
  if (              (Light->On.RTR_LeftRoadSign&& GetVarIndexed(IDX_ROADSIGN_LEFT) !=0)   ||   //added by A.K. to switch light indicator on Main mask
                    (Light->On.LRS_LeftRoadSign&& GetVarIndexed(IDX_ROADSIGN_LEFT) !=0)   ||
                    (Light->On.RTR_RightRoadSign&& GetVarIndexed(IDX_ROADSIGN_RIGHT) !=0)  ||
                    (Light->On.LRS_RightRoadSign&& GetVarIndexed(IDX_ROADSIGN_RIGHT) !=0)  ||
                    Light->On.RTR_RearBeacon     ||
                    Light->On.LRS_RearBeacon     ||
                    Light->On.FrontBeaconCab     ||
                    Light->On.RTR_WorkRear       ||
                    Light->On.LRS_WorkRear       /*||
                    Light->On.WorkSide*/)
       {
        SetVarIndexed(IDX_LIGHT_ON, 1);
       }
      else
        {
       SetVarIndexed(IDX_LIGHT_ON, 0);
        }
    //Blink
    static int32_t count = 0;
    if (500 < (GetMSTick() - count))
    {
        count = GetMSTick();
        //Back beacon blinking
        if (Light->On.RTR_RearBeacon)
        {
            SetVarIndexed(IDX_LIGHT_REARBEACON_0, 1 - GetVarIndexed(IDX_LIGHT_REARBEACON_0));
            SetVarIndexed(IDX_LIGHT_REARBEACON_1, 1 - GetVarIndexed(IDX_LIGHT_REARBEACON_0));
        }
        else
        {
            SetVarIndexed(IDX_LIGHT_REARBEACON_0, 0);
            SetVarIndexed(IDX_LIGHT_REARBEACON_1, 0);
        }

        //Front beacon blinking
        if (Light->On.FrontBeaconCab)
        {
            SetVarIndexed(IDX_LIGHT_FRONTBEACON, 1 - GetVarIndexed(IDX_LIGHT_FRONTBEACON));
        }
        else
        {
            SetVarIndexed(IDX_LIGHT_FRONTBEACON, 0);
        }
        if (Light->On.LRS_LeftRoadSign)
        {
            SetVarIndexed(IDX_ROADSIGN_LEFT_BLINK, GetVarIndexed(IDX_ROADSIGN_LEFT));
        }
        else
        {
            SetVarIndexed(IDX_ROADSIGN_LEFT_BLINK, 0);
        }

        if (Light->On.LRS_RightRoadSign)
        {
            SetVarIndexed(IDX_ROADSIGN_RIGHT_BLINK, GetVarIndexed(IDX_ROADSIGN_RIGHT));
        }
        else
        {
            SetVarIndexed(IDX_ROADSIGN_RIGHT_BLINK, 0);
        }
    }

    if (Light->On.RTR_WorkRear)
    {
        SetVarIndexed(IDX_LIGHT_REAR, 1);
    }
    else
    {
        SetVarIndexed(IDX_LIGHT_REAR, 0);
    }

    if (Light->On.WorkSide)
    {
        SetVarIndexed(IDX_LIGHT_SIDE, 1);
    }
    else
    {
        SetVarIndexed(IDX_LIGHT_SIDE, 0);
    }

    //Lights menu icons draw

    if (Light->On.FrontBeaconCab)
    {
        SetVarIndexed(IDX_LIGHT_BEACONON, 1);
    }
    else
    {
        SetVarIndexed(IDX_LIGHT_BEACONON, 0);
    }

    if (Light->On.LRS_LeftRoadSign)
    {
        SetVarIndexed(IDX_ROADSIGN_LEFTON, GetVarIndexed(IDX_ROADSIGN_LEFT));
    }
    else
    {
        SetVarIndexed(IDX_ROADSIGN_LEFTON, 0);
    }

    if (Light->On.LRS_RightRoadSign)
    {
        SetVarIndexed(IDX_ROADSIGN_RIGHTON, GetVarIndexed(IDX_ROADSIGN_RIGHT));
    }
    else
    {
        SetVarIndexed(IDX_ROADSIGN_RIGHTON, 0);
    }
}
