﻿/****************************************************************************
 *
 * File:         SUBCONSOLE.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */
#include "gseConsole.h"
#include "gsAnsiEsc.h"
#include "gsSocket.h"


/****************************************************************************/

/* macro definitions ********************************************************/
#define BUFISZE 128

#define ANSI_CLR_LN_FROM_CU       ANSI_ESC "[0K"                   //!< Clears line from cursor on
#define ANSI_CLR_LN_TO_CU         ANSI_ESC "[1K"                   //!< Clears line from home to cursor
#define ANSI_CLR_LN_ALL           ANSI_ESC "[2K"                   //!< Clears the whole string

/* type definitions *********************************************************/

/* prototypes ***************************************************************/
void CON_PrioMaskOn(uint32_t Hdl, char **data);
void CON_MaskOn(uint32_t Hdl, char **data);
void CON_MaskOff(uint32_t Hdl, char **data);
void CON_GetCurrentMaskShown(uint32_t Hdl, char **data);
void CON_Help(uint32_t Hdl, char **data);
void CON_GetVar(uint32_t Hdl, char **data);
void CON_SetVar(uint32_t Hdl, char **data);

void CON_SetTime(uint32_t Hdl, char **data);
void CON_GetTime(uint32_t Hdl, char **data);
void CON_SetDate(uint32_t Hdl, char **data);
void CON_GetDate(uint32_t Hdl, char **data);

void CON_Reboot(uint32_t Hdl, char **data);
void CON_Update(uint32_t Hdl, char **data);

void CON_GetIPConfig (uint32_t Hdl, char **data);

void CON_SetKeyBacklight(uint32_t Hdl, char **data);
void CON_SetDisplayBacklight(uint32_t Hdl, char **data);

void CON_GetOperatingTime(uint32_t Hdl, char **data);

typedef struct tagAdded_ComArr
{
  tsCon_ComArray *CArr;
  int32_t n_CArr;
}tAdded_ComArr;


typedef enum eagAnsiEsc
{
  ANSI_ESC_NO_SEQUENCE,
  ANSI_ESC_SEQUENCE_ACTIVE,
  ANSI_ESC_LINE_UP,
  ANSI_ESC_LINE_DOWN,
  ANSI_ESC_LEFT,
  ANSI_ESC_RIGHT,
}eAnsiEsc;

typedef struct tagAnsiEsc
{
  int8_t esc_active;
  int8_t con_active;
  char buffer[8];
  char buf_param[8];
  char buf_inter[8];
  int8_t final;
  int8_t  bufcount;

}tAnsiEsc;

typedef struct tagLastCommands
{
  char **Comands;
  int32_t numComands;
  int32_t LastSet;
  int32_t LastGet;
}tLastCommands;



typedef struct tagsCon
{
  uint32_t count;
  uint32_t count_max;
  char buf[BUFISZE];
  uint32_t BufSize;
  tCon_StreamOut sOut;
  tAdded_ComArr *Add_ComArr;
  int32_t       numAdd_ComArr;
  tAnsiEsc      Ansi;
  tLastCommands LC;
}tsCon;
/* global constants *********************************************************/

/* global variables *********************************************************/
tsCon *psCon    = NULL;
uint32_t NumsCon = 0;
/* local function prototypes*************************************************/
//int sscanf ( const char * s, const char * format, ...);

/****************************************************************************/

/* function code ************************************************************/


tsCon_ComArray GS_ComAr[] = {{"help"                , 0 , CON_Help               , "shows possible commands:"     },
                             {"priomaskon"          , 1 , CON_PrioMaskOn         , "PrioMaskOn( int32_t maskNum);"},
                             {"maskon"              , 1 , CON_MaskOn             , "MaskOn( int32_t maskNum);"},
                             {"maskoff"             , 1 , CON_MaskOff            , "MaskOff( int32_t maskNum);"},
                             {"getcurrentmaskshown" , 0 , CON_GetCurrentMaskShown, "int32_t PrioMaskOn( void);"    },
                             {"getvar"              , 1 , CON_GetVar             , "int32_t GetVar( int32_t Handle);"},
                             {"setvar"              , 2 , CON_SetVar             , "int32_t SetVar( int32_t Handle, int32_t Value);"},
                             {"reboot"              , 0 , CON_Reboot             , "device is restarting"},
                             {"update"              , 0 , CON_Update             , "device goes to update-mode"},
                             {"settime"             , 3 , CON_SetTime            , "void SetTime(int hh ,int mm ,int ss)"},
                             {"gettime"             , 0 , CON_GetTime            , "int32_t GetTime(void);"},
                             {"setdate"             , 3 , CON_SetDate            , "void SetDate (int DD,int MM, int YY"},
                             {"getdate"             , 0 , CON_GetDate            , "int32_t GetDate(void)"},
                             {"getipcfg"            , 0 , CON_GetIPConfig        , "returns ip of device"},
                             {"setkeybacklight"     , 2 , CON_SetKeyBacklight    , "brightness of a key (int num, int brightness)"},
                             {"setdisplaybacklight" , 1 , CON_SetDisplayBacklight, "brightness of the display (0 - 1000)"},
                             {"getophours"          , 0 , CON_GetOperatingTime   , "Get operating time of device (hh:mm:ss)"}
                             };


void LastCommands_Init(tLastCommands *LC, uint32_t numComands)
{
  memset(LC,0,sizeof(tLastCommands));
  LC->numComands = numComands;
  LC->Comands = malloc(sizeof(char*) * numComands);
  if(NULL == LC->Comands)
    return ;
  memset(LC->Comands, 0, sizeof(char*) * numComands);
}

/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : handle of the Stream.
**
*****************************************************************************/
int32_t sCon_InitStream(int32_t BufSize, tCon_StreamOut StreamOut)
{
  if( NULL == psCon)
  {
    psCon = (tsCon*)malloc(sizeof(tsCon));
  }
  else
  {
    tsCon * newCon;
    newCon = (tsCon*)realloc(psCon,sizeof(tsCon) * (NumsCon + 1));
    if(NULL != newCon)
    {
        psCon = newCon;
    }
  }
  if(NULL == psCon)
  {
    NumsCon =0;
    return -1;
  }
  NumsCon++;
  int32_t Index = NumsCon - 1;
  memset(&psCon[Index], 0, sizeof(tsCon));
  psCon[Index].sOut  = StreamOut;
  psCon[Index].BufSize = BufSize;
  LastCommands_Init(&psCon[Index].LC,10);
  return NumsCon - 1;
}


void LastCommands_Set(tLastCommands *LC, const char *string)
{
  LC->LastSet++;

  if(LC->LastSet >= LC->numComands)
    LC->LastSet = 0;

  LC->LastGet = LC->LastSet; //Liste abrufen fängt von diesem Punkt aus an.

  if(NULL != LC->Comands[LC->LastSet])
  {
    free(LC->Comands[LC->LastSet]);
  }
  LC->Comands[LC->LastSet] = strdup(string);

  return;
}

char * LastCommands_GetPrev(tLastCommands *LC)
{
  if(LC->Comands[LC->LastGet] != NULL)
  {
    int32_t idx = LC->LastGet;
    LC->LastGet--;
    if(LC->LastGet < 0)
    {
      LC->LastGet = LC->numComands - 1;
    }
    return LC->Comands[idx];
  }
  return NULL;
}

char * LastCommands_GetNext(tLastCommands *LC)
{
  int32_t idx = LC->LastGet + 1;
  if(idx >= LC->numComands)
  {
    idx = 0;
  }
  if(NULL != LC->Comands[idx])
  {
    LC->LastGet = idx;
    return LC->Comands[idx];
  }
  return NULL;
}

char  *cmd_strings[32] = {NULL};

int32_t sCon_CheckList(const uint32_t Hdl, tsCon_ComArray *ComArray, int32_t numComArray, char *command)
{
  int32_t cmdargc = -1;
  char    cmd[psCon[Hdl].BufSize];
  char * ptr;
  ptr = strtok(command, " ");

  if(NULL != ptr)
  {
    command = ptr;
    strncpy(cmd, ptr,psCon[Hdl].BufSize);
    ptr = strtok(NULL, " ");
    cmdargc = 0;
  }
  for(uint32_t i = 0; (i < GS_ARRAYELEMENTS(cmd_strings)) && (NULL != ptr); i++)
  {
    if(NULL != cmd_strings[i])
    {
      free(cmd_strings[i]);
      cmd_strings[i] = NULL;
    }
    cmd_strings[i] = strdup(ptr);
    cmdargc = i + 1;
    ptr = strtok(NULL, " ");
  }

  if(-1 == cmdargc)
  {
	  return -1;
  }
  //PrintToDebug("CMD: %s, %d, %d\r\n", cmd,cmd_i32[0],cmd_i32[1]);
  if(NULL == ComArray)
  {
    return -1;
  }
  tsCon_ComArray *HitComArray = NULL;
  for(int32_t i = 0; i < numComArray; i++)
  {
    if(0 == strcmp(ComArray[i].command, cmd))
    {
      HitComArray = &ComArray[i];
      break;
    }
  }
  if(NULL != HitComArray)
  {
    if(HitComArray->arg != (cmdargc))
    {
      psCon[Hdl].sOut("Wrong number of Arguments %d != %d\r\n",HitComArray->arg,cmdargc );
    }
    else HitComArray->function(Hdl, cmd_strings);
    return 0;
  }
  //check added comarrays:
  for(int32_t j = 0; j < psCon[Hdl].numAdd_ComArr; j++)
  {
    HitComArray = NULL;
    for(int32_t i = 0; i < psCon[Hdl].Add_ComArr[j].n_CArr; i++)
    {
      tsCon_ComArray *ActualArr;
      ActualArr = &psCon[Hdl].Add_ComArr[j].CArr[i];

      if(0 == strcmp(ActualArr->command, cmd))
      {
        HitComArray = ActualArr;
        break;
      }
    }
    if(NULL != HitComArray)
    {
      if(HitComArray->arg != (cmdargc))
      {
        psCon[Hdl].sOut("Wrong number of Arguments %d != %d\r\n",HitComArray->arg,cmdargc );
      }
      else HitComArray->function(Hdl, cmd_strings);
      return 0;
    }
  }
  psCon[Hdl].sOut("???\r\n");
  //psCon[Hdl].sOut("\r\n");
  return -1;
}


eAnsiEsc Scon_Ansii_Check(tAnsiEsc *ansi, char *param)
{
  if(ansi->con_active)
  {
    if('A' == ansi->final)
    {
      return ANSI_ESC_LINE_UP;
    }
    else if('B' == ansi->final)
    {
      return ANSI_ESC_LINE_DOWN;
    }
    else if('C' == ansi->final)
    {
      return ANSI_ESC_RIGHT;
    }
    else if('D' == ansi->final)
    {
      return ANSI_ESC_LEFT;
    }
    if(NULL != param)
    {
      //*param = ansi->buf_param;
    }
  }
  return ANSI_ESC_NO_SEQUENCE;
}

eAnsiEsc SCon_Ansii(tAnsiEsc *ansi, const char c)
{

    if(ansi->con_active)
    {
      if((c >= 48) & (c <= 63 ))
      {
        int len = strlen(ansi->buf_param);
        //PrintToDebug("Got Param %d\r\n", c);
        if(len > 6)
        {
          memset(ansi, 0, sizeof(tAnsiEsc));
          return 0;
        }
        else
        {
          ansi->buf_param[strlen(ansi->buf_param)] = c;
          ansi->buf_param[strlen(ansi->buf_param)+1 ] = '\0';
        }
      }
      else if((c >=32) & (c <= 47 ))
      {
        int len = strlen(ansi->buf_inter);
        //PrintToDebug("Got Inter %d\r\n", c);
        if(len > 6)
        {
          memset(ansi, 0, sizeof(tAnsiEsc));
          return 0;
        }
        else
        {
          ansi->buf_inter[strlen(ansi->buf_inter)] = c;
          ansi->buf_inter[strlen(ansi->buf_inter)+1 ] = '\0';
        }
      }
      else if((c >=64) & (c <= 126 ))
      {
        ansi->final = c;
        //PrintToDebug("Got final escape %c\r\n", c);
        //PrintToDebug("Inter: %d, Param: %d}\r\n", ansi->buf_inter[0], ansi->buf_param[0]);

        eAnsiEsc ret = Scon_Ansii_Check(ansi, NULL);
        memset(ansi, 0, sizeof(tAnsiEsc));
        return ret;
      }
      else
      {
        memset(ansi,0, sizeof(tAnsiEsc));
        return 0;
      }
    }
    else if(ansi->esc_active)
    {
      if(c == 91)
      {
        //PrintToDebug("Control active\r\n");
        ansi->con_active = 1;
      }
      else if((c >= 32) & (c <= 47 ))
      {
        if(ansi->bufcount < 8)
        {
          ansi->buffer[ansi->bufcount] = c;
          ansi->bufcount++;
        }
        else
        {
          //PrintToDebug("Got final escape %c\r\n", c);
          ansi->final = c;
          Scon_Ansii_Check(ansi, NULL);
          memset(ansi, 0, sizeof(tAnsiEsc));
          return 0;
        }
      }
      else if((c >= 48) & (c <= 126 ))
      {
        ansi->buffer[ansi->bufcount] = c;
        ansi->bufcount++;
        ansi->buffer[ansi->bufcount] = '\0';
        return 1;
      }
    }
    else if (c == 27)
    {
      PrintToDebug("Esc active\r\n");
      ansi->esc_active = 1;
      return 1;
    }
    else
    {
      return 0;
    }
    return 1;

}

int32_t sCon_GetString(uint32_t Hdl,  char * string, int32_t n)
{
  if(Hdl > NumsCon)
  {
    return -1;  //Handle nicht vorhanden.
  }




  for(int32_t i = 0; i < n; i++)
  {
    uint32_t *c     = &psCon[Hdl].count;
    uint32_t *c_max = &psCon[Hdl].count_max;
    //SetDebugWindow(1);
    //PrintToDebug("c = %X\r\n", string[i]);
    eAnsiEsc ret = SCon_Ansii(&psCon[Hdl].Ansi, string[i]);
    if(ret)
    {
      switch(ret)
      {
        case ANSI_ESC_LEFT:
        if((*c) > 0)
        {
          psCon[Hdl].sOut("\x1B[D");
          (*c)--;
        }
        break;

        case ANSI_ESC_RIGHT:
        if((*c) < (*c_max))
        {
          psCon[Hdl].sOut("\x1B[C");
          (*c)++;
        }
        break;

        case ANSI_ESC_LINE_DOWN:
        {
          char *str = LastCommands_GetNext(&psCon[Hdl].LC);
          if(NULL != str)
          {

            psCon[Hdl].sOut(ANSI_CLR_LN_ALL"\r%s",str);
            strncpy(psCon[Hdl].buf, str, sizeof(psCon[Hdl].buf));
            (*c) = strlen(psCon[Hdl].buf);
            *c_max = (*c);
          }
        }
        break;

        case ANSI_ESC_LINE_UP:
        {
          char *str = LastCommands_GetPrev(&psCon[Hdl].LC);
          if(NULL != str)
          {
            psCon[Hdl].sOut(ANSI_CLR_LN_ALL"\r%s",str);
            strncpy(psCon[Hdl].buf, str, sizeof(psCon[Hdl].buf));
            (*c) = strlen(psCon[Hdl].buf);
            *c_max = (*c);
          }
        }
        break;


        default:

        break;

      }
    }

    else if(('\n' == string[i])
          ||('\r' == string[i]))
    {
      //Local Echo
      psCon[Hdl].sOut("%c",string[i]);
      if(string[i] == '\r')//\r->\r\n to get return in terminal
      {
        psCon[Hdl].sOut("\n");
      }
      //no we command at \r\n
      if(i >= 1)
      {
        if(string[i-1] == '\r')
        return 0;
      }
      psCon[Hdl].buf[*c_max] = '\0';
      if(strlen(psCon[Hdl].buf) >= 1 )
      {
        char commando[ psCon[Hdl].BufSize];

        snprintf(commando, psCon[Hdl].BufSize,  "%s",psCon[Hdl].buf );
        //PrintToDebug("Commando: %s\r\n", commando);
        LastCommands_Set(&psCon[Hdl].LC, commando);

        sCon_CheckList( Hdl, GS_ComAr, GS_ARRAYELEMENTS(GS_ComAr), commando);
      }

      *c = 0;
      *c_max = 0;
    }

    else if(string[i] == 127)//backwards
    {
      psCon[Hdl].sOut("%c",string[i]);
      (*c)--;
      psCon[Hdl].buf[*c] = '\0';
    }
    else if(string[i] != '\0')
    {
      //In Kleinbuchstaben umwandeln
      if((string[i] >= 'A')
          &&(string[i] <= 'Z'))
      {
        string[i] = string[i] + 0x20;
      }
      psCon[Hdl].buf[*c] = string[i];
      psCon[Hdl].sOut("%c", psCon[Hdl].buf[*c]);
      //PrintToDebug("--'%c' == '%c'\r\n", psCon[Hdl].buf[*c], string[i]);
      (*c)++;
      //PrintToDebug("Actual String: %s ::len = %d\r\n",psCon[Hdl].buf, (*c) );
      if((*c) > (*c_max))
        (*c_max)++;
      if((*c) >  psCon[Hdl].BufSize)
      {
        (*c) = 0;
        (*c_max) = 0;
      }

    }
    if(*c >=  psCon[Hdl].BufSize)
    {
      *c = 0;
      return -1;
    }
  }

  return 0;
}


void sCon_AddCommandList(uint32_t Hdl,tsCon_ComArray *ComArray, int32_t numComArray)
{
  psCon[Hdl].numAdd_ComArr++;
  if(NULL == psCon[Hdl].Add_ComArr)
  {
    psCon[Hdl].Add_ComArr =(tAdded_ComArr*) malloc(sizeof(tAdded_ComArr));
  }
  else
  {
    psCon[Hdl].Add_ComArr = (tAdded_ComArr*) realloc(psCon[Hdl].Add_ComArr,sizeof(tAdded_ComArr) * psCon[Hdl].numAdd_ComArr);
  }
  int32_t idx = psCon[Hdl].numAdd_ComArr - 1;
  psCon[Hdl].Add_ComArr[idx].CArr  = ComArray;
  psCon[Hdl].Add_ComArr[idx].n_CArr= numComArray;
}




void    sCon_WriteString(uint32_t Hdl, const char * fmt,...)
{
  char buffer[256];
  va_list args;
  va_start (args, fmt);
  vsnprintf (buffer,256,fmt, args);
  va_end (args);
  psCon[Hdl].sOut(buffer);
}


void CON_Help(uint32_t Hdl, char **data)
{
  for(uint32_t i = 0; i < GS_ARRAYELEMENTS(GS_ComAr); i++)
  {
    char buffer[192];
    int32_t n;
    n = snprintf(buffer, 192,"%s:" , GS_ComAr[i].command);
    while(n < 22)
    {
      buffer[n] = ' '; //space
      n++;
    }
    buffer[n] = '\0';
    strncat(buffer,GS_ComAr[i].description, 192 - strlen(buffer));
    strncat(buffer,"\r\n", 192 - strlen(buffer));
    psCon[Hdl].sOut(buffer ,strlen(buffer));
  }
  for(int32_t j = 0; j < psCon[Hdl].numAdd_ComArr; j++)
  {
    tAdded_ComArr *ActArr;
    ActArr = &psCon[Hdl].Add_ComArr[j];
    for(int32_t i = 0; i < ActArr->n_CArr; i++)
    {
      char buffer[192];
      int32_t n;
      n = snprintf(buffer, 192,"%s:" , ActArr->CArr[i].command);
      while(n < 22)
      {
        buffer[n] = ' '; //space
        n++;
      }
      buffer[n] = '\0';
      strncat(buffer,ActArr->CArr[i].description, 192 - strlen(buffer));
      strncat(buffer,"\r\n", 192 - strlen(buffer));
      psCon[Hdl].sOut(buffer ,strlen(buffer));
    }
  }
}


void CON_PrioMaskOn(uint32_t Hdl, char **data)
{
  char string[48];
  int32_t value = strtol(data[0], NULL,0);
  PrioMaskOn(value);
  snprintf(string,GS_ARRAYELEMENTS(string), "PrioMaskOn(%d)\r\n", value);
}

void CON_MaskOn(uint32_t Hdl, char **data)
{
  char string[48];
  int32_t value = strtol(data[0], NULL,0);
  MaskOn(value);
  snprintf(string,GS_ARRAYELEMENTS(string), "MaskOn(%d)\r\n", value);
}

void CON_MaskOff(uint32_t Hdl, char **data)
{
  char string[48];
  int32_t value = strtol(data[0], NULL,0);
  MaskOff(value);
  snprintf(string,GS_ARRAYELEMENTS(string), "MaskOff(%d)\r\n",value);
}

void CON_GetCurrentMaskShown(uint32_t Hdl, char **data)
{
  char string[48];
  snprintf(string,48,"%d = GetCurrentMaskShown()\r\n",GetCurrentMaskShown());
  psCon[Hdl].sOut(string);
}

void CON_GetVar(uint32_t Hdl, char **data)
{
  int32_t value;
  int32_t iData = strtol(data[0], NULL,0);
  value = GetVar(iData);
  char string[48];
  snprintf(string,GS_ARRAYELEMENTS(string), "%d = GetVar(%d)\r\n", value,iData);
  psCon[Hdl].sOut(string);
}

void CON_SetVar(uint32_t Hdl, char **data)
{
  char string[48];
  int32_t iData[2];
  iData[0] = strtol(data[0], NULL,0);
  iData[1] = strtol(data[1], NULL,0);
  snprintf(string,GS_ARRAYELEMENTS(string), "%d = SetVar(%d, %d)\r\n", SetVar(iData[0],iData[1]), iData[0], iData[1]);
  psCon[Hdl].sOut(string);
}



void CON_Update(uint32_t Hdl, char **data)
{
  SetProjectUpdate(GS_PRJ_UPDATE_TRIGGER);
  psCon[Hdl].sOut("update-mode\r\n" ,strlen("update-mode\r\n"));
}

void CON_Reboot(uint32_t Hdl, char **data)
{
  PowerCommand (GS_POWER_CMD_REBOOT, 0);
  psCon[Hdl].sOut("Device rebooting\r\n" ,strlen("Device rebooting\r\n"));
}

void CON_GetTime(uint32_t Hdl, char **data)
{
  char string[48];
  tSysTime _time;
  RTCGetTime(&_time);
  snprintf(string, GS_ARRAYELEMENTS(string), "%02d:%02d:%02d = Actual _time (hh:mm:ss)\r\n", _time.Hours, _time.Minutes, _time.Seconds );
  psCon[Hdl].sOut(string);
}

void CON_SetTime(uint32_t Hdl, char **data)
{
  char string[48];
  tSysTime _time;
  int32_t iData[3];
  for(uint32_t i = 0; i < 3; i++)
  {
    iData[i] = strtol(data[i], NULL,0);
  }
  _time.Hours = iData[0];
  _time.Minutes = iData[1];
  _time.Seconds = iData[2];
  RTCSetTime(&_time);
  snprintf(string, GS_ARRAYELEMENTS(string), "%02d:%02d:%02d = New _time(hh:mm:ss)\r\n", _time.Hours, _time.Minutes, _time.Seconds );
  psCon[Hdl].sOut(string);
}

void CON_GetDate(uint32_t Hdl, char **data)
{
  char string[48];
  tSysDate date;
  RTCGetDate(&date);
  snprintf(string, GS_ARRAYELEMENTS(string), "%02d:%02d:%02d = Actual date (tt:mm:yy)\r\n", date.Day, date.Month, date.Year );
  psCon[Hdl].sOut(string);
}

void CON_SetDate(uint32_t Hdl, char **data)
{
  char string[48];
  tSysDate date;
  int32_t iData[3];
  for(uint32_t i = 0; i < 3; i++)
  {
    iData[i] = strtol(data[i], NULL,0);
  }
  date.Day   = iData[0];
  date.Month = iData[1];
  date.Year  = iData[2];
  RTCSetDate(&date);
  snprintf(string, GS_ARRAYELEMENTS(string), "%02d:%02d:%02d = New Date(tt:mm:yy)\r\n", date.Day, date.Month, date.Year);
  psCon[Hdl].sOut(string);
}

void CON_GetIPConfig(uint32_t Hdl, char **data)
{
  tGsSocketIntfInfo socketInfo;
  if(0 == gsSocketGetIntfInfo(NULL, &socketInfo))
  {
    psCon[Hdl].sOut("IP:      %s\r\n",socketInfo.mIpAddr);
    psCon[Hdl].sOut("Mac:     %s\r\n",socketInfo.mHwAddr);
    psCon[Hdl].sOut("Media:   %s\r\n",socketInfo.mMedia);
    psCon[Hdl].sOut("State:   %s\r\n",socketInfo.mStatus);
  }
  else psCon[Hdl].sOut("unknown IP-Config\r\n");
}

void CON_GetMac(uint32_t Hdl, char **data)
{
  tGsSocketIntfInfo socketInfo;
  if(0 == gsSocketGetIntfInfo(NULL, &socketInfo))
  {
    psCon[Hdl].sOut("%s\r\n",socketInfo.mHwAddr);
  }
  else psCon[Hdl].sOut("Got no valid Information\r\n");
}

void CON_SetKeyBacklight(uint32_t Hdl, char **data)
{
  uint32_t key        = strtol(data[0], NULL, 0);
  uint32_t brightness = strtol(data[0], NULL, 0);
  if(brightness)
    SetKeyBacklight(key,1 | GS_KEY_BACKLIGHT_BRIGHTNESS(brightness));
  else SetKeyBacklight(key, 0);
}
void CON_SetDisplayBacklight(uint32_t Hdl, char **data)
{
  uint32_t brightness = strtol(data[0], NULL, 0);
  if(brightness)
  {
    PowerCommand(GS_POWER_CMD_DISPLAY_ON,0);
    SetDisplayBacklight(0,brightness);
  }
  else
  {
    SetDisplayBacklight(0,brightness);
    PowerCommand(GS_POWER_CMD_DISPLAY_OFF,0);
  }
}

void CON_GetOperatingTime(uint32_t Hdl, char **data)
{
  int32_t OpTime;
  OpTime = HourCounterGet(0);
  psCon[Hdl].sOut("Operating Hours:  %02d:%02d:%02d h (hh:mm::ss)\r\n",
                                                      OpTime / 3600,
                                                     (OpTime / 60) % 60,
                                                     (OpTime % 60));
}

