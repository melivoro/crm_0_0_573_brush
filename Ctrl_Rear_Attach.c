﻿/****************************************************************************
 *
 * File:         CTRL_REAR_ATTACH.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "control.h"
#include "io.h"
#include "Ctrl_Brush.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

void Ctrl_Rear_OZP221_Init(tControl *Ctrl)
{
    tABC_Attach *  ABC_Attach = &Ctrl->Rear.ABC_Attach;
    Brush_UpDown_Init(&Ctrl->Rear.ABC_Attach,MCM250_2, 1,-1,-1);
    Brush_Float_Init( ABC_Attach, MCM250_2,8);
    Brush_Rotate_Init( ABC_Attach, MCM250_2, 3);
    Brush_LeftRight_Init(ABC_Attach,-1,-1,-1,-1 );
    Brush_ExRetract_Init(ABC_Attach, -1,-1 ,-1, -1);
    Brush_SideSections_Init(ABC_Attach, -1,-1,-1,-1);
}

void Ctrl_Rear_Brush_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
    Brush_Cycle(&Ctrl->Rear.ABC_Attach, &Ctrl->CmdABC[EQUIP_C]);
}
void Ctrl_Rear_Brush_Timer_10ms(tControl *Ctrl)
{
    static int32_t count = 0;
    count++;
    if(0 == (count %10))
    {
        Valv_Timer_100ms(&Ctrl->Rear.ABC_Attach.ValveUpDown);
    }
}
