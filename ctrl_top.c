﻿/****************************************************************************
 *
 * File:         CTRL_TOP.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "ctrl_top.h"
#include "control.h"
#include "gseDebug.h"
#include "RCText.h"
#include "gseMCM.h"
#include "io.h"
#include "GSe_MsgBox.h"
#include "errorlist.h"
#include "gsToVisu.h"
#include "ctrl_top_conveyor.h"


#define LRS_SECTION_BIN_RIGHT  1
#define LRS_SECTION_BIN_CENTER 2
#define LRS_SECTION_BIN_LEFT   4
/****************************************************************************/

/* macro definitions ********************************************************/


/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/




tSQLTable DiskTable = {
  .TableName = "Spreader Disk" ,
  .ColumnNames = {"Width", "RPM", "PWM Duty", "EMPTY"},
  .Cell = {
    {1  , 45,   102, 0},
    {2  , 88,   113 , 0},
    {3  , 152,  125, 0},
    {4  , 208,  136, 0},
    {5  , 248,  148, 0},
    {6  , 296,  160, 0},
    {7  , 360,  171, 0},
    {8  , 385,  183, 0},
    {10 , 500,  194, 0},
    {12 , 590,  206, 0},
    {13 , 630,  218, 0},
    }
};



int32_t SetBit(uint32_t *Byte, int8_t Pos, int8_t value )
{
  if(Pos >= 32)
    return -1;
  if(value)
  {
    *Byte = *Byte | (  0x01 << Pos );
  }
  else
  {
    *Byte = *Byte & ( ~( 0x01 << Pos ));
  }
  return 0;
}



/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

int32_t Liter = 0;

void WaterPump_Init(tWaterPump *Pump, int8_t *PumpOn, uint32_t Type)
{
  Valv_Init(&Pump->Valv, MCM250_2, 3, -1,-1);
  Pump->on = (uint8_t*) PumpOn;
  Pump->Type = Type;
}

void WaterPump_OnOff(tWaterPump *Pump, uint32_t value)
{
  if(Pump->on)
  {
   *Pump->on = value;
  }
}

void WaterPump_SetLiter(int32_t ml)
{
  Liter = ml;
}

void WaterPump_SetMaxLiter( int32_t ml)
{
  if(0 == Liter)
    WaterPump_SetLiter(ml);
  else if(ml < Liter)
  {
    WaterPump_SetLiter(ml);
  }
}

void WaterPump_ResetMaxLiter(void)
{
  Liter = 0;
}


void WaterPump_Cycle(tWaterPump *Pump)
{

  if(Pump->on)
  {
    if(*Pump->on)
    {
      if(Pump->Type == WATERPUMP_TYPE_BP300)
        ValvPropPVEP_SetLiter(&Pump->Valv.Valve.PVEH,Liter);
      else
       ValvPropPVEP_SetLiter(&Pump->Valv.Valve.PVEH,-Liter);//negative values for B-Valve
    }
    else
    {
      ValvPropPVEP_SetLiter(&Pump->Valv.Valve.PVEH,0);
    }
  }
}

int8_t WaterPump_IsOn(const tWaterPump *Pump)
{
  if(NULL != Pump->on)
  {
    return *Pump->on;
  }
  return -1;
}
/**
 * @brief checks if it is not to cold. If ambient temperature is colder then MinTemp
 * 1 is returned, else 0;
 *
 * @param MinTemp
 * @return itn32_t
 */
int32_t TemperatureToCold(int32_t MinTemp)
{
  const tControl *Ctrl = CtrlGet();
  if(MinTemp > Ctrl->Engine.AmbientAirTemp)
    return 1;
  else return 0;
}




void WaterPump_Draw(tTopEquip  *Top)
{
  static int32_t state = 1;

  if(Top->Pump.Valv.Type != VALVE_TYPE_NONE)
  {
    if(state != 1)
    {
      state = 1;
      SendToVisuObj(OBJ_WATERPUMP, GS_TO_VISU_SET_ATTR_VISIBLE, 1);
    }
    if(NULL != Top->Pump.on)
    {
      if(*Top->Pump.on)
      {
        SetVarIndexed(IDX_ICON_PUMP, 1);
      }
      else
      {
        SetVarIndexed(IDX_ICON_PUMP, 0);
      }
    }
  }
  else //make pump symbol invisible
  {
    if(state == 1)
    {
      state = 0;
      SendToVisuObj(OBJ_WATERPUMP, GS_TO_VISU_SET_ATTR_VISIBLE, 0);
    }
  }
}


#define MAT_SENSOR_OK     0
#define MAT_SENSOR_EMPTY  1
#define MAT_SENSOR_NO_ROT 2

void MaterialSensor_Cycle(void)
{
  const tControl * Ctrl = CtrlGet();
  static int32_t TimeOut_Material;
  static int32_t TimeOut_Rotation;

  //Set Timeout for rotation.
  if((0 == Ctrl->Sensors.Freq.ConveryRotation)
   &&(Ctrl->Top.Conveyor.on))
  {
    if( Ctrl->Auto.on)
    {
      //if auto mode and speed of vehicle = 0
      if(0 == Ctrl->Top.speed)
      {
        TimeOut_Rotation = GetMSTick();
      }
    }
  }
  else //conveyour is off-> roation don't have to be active.
  {
    TimeOut_Rotation = GetMSTick();
  }

  //Set Timeout for Material Sensor.
  if(DI_SENSOR_ON != Ctrl->Sensors.DI.MaterialPresense)
  {
    TimeOut_Material = GetMSTick();
  }

  //Rotation invalid for more then 2 seconds
  int32_t val;
  if(2000 < (GetMSTick() - TimeOut_Rotation))
  {
      val = MAT_SENSOR_NO_ROT; //Icon no Rotation
      TimeOut_Material = GetMSTick(); //this way no material error will come if rotation error goes away
  }
  else if(2000 < (GetMSTick() - TimeOut_Material))
  {
    val = MAT_SENSOR_EMPTY;
  }
  else
  {
    val = MAT_SENSOR_OK;
  }

  //only at event
  static int32_t val_old = -1;
  if(val != val_old)
  {
    val_old = val;
    if(val == MAT_SENSOR_NO_ROT)
    {
      EList_SetErrLevByRCText(VBL_WARNING,
                            ERRID_CONVEYOR_NO_ROTATION,
                            RCTEXT_T_NOCONVEYORROTATION);
    }
    else
    {
      EList_ResetErr(ERRID_CONVEYOR_NO_ROTATION);
      if(val == MAT_SENSOR_EMPTY)
      {
        MsgBoxOk(RCTextGetText(RCTEXT_T_WARNING, GetVarIndexed(IDX_SYS_LANGUAGE))
               ,RCTextGetText(RCTEXT_T_NOMATERIAL, GetVarIndexed(IDX_SYS_LANGUAGE))
               ,NULL, NULL);
      }
    }
    SetVarIndexed(IDX_ICON_DENSITY, val);
  }
  if(val == MAT_SENSOR_EMPTY)
  {
    //Blinking in 500 second steps
    if(500 > (GetMSTick() % 1000))
    {
      SetVarIndexed(IDX_ICON_DENSITY,MAT_SENSOR_OK);
    }
    else
    {
      SetVarIndexed(IDX_ICON_DENSITY,MAT_SENSOR_EMPTY);
    }
  }
}



void SpreadingSensor_Cycle(void)
{

  const tControl * Ctrl = CtrlGet();
  static int32_t timeout;
  static int32_t spreading = -1;
  if(Ctrl->Sensors.Freq.ConveryRotation)
  {
    if(DI_SENSOR_OFF == Ctrl->Sensors.DI.Spreading)
    {
      timeout = GetMSTick();
      if(DI_SENSOR_OFF != spreading)
      {
        spreading = DI_SENSOR_OFF;
        EList_ResetErr(ERRID_NO_REAGENT);
      }
    }
    else if((DI_SENSOR_ON == Ctrl->Sensors.DI.Spreading)
          &&(2000 < GetMSTick() - timeout))
    {
      if(DI_SENSOR_ON != spreading)
      {
        spreading = DI_SENSOR_ON;
        EList_SetErrLevByRCText(VBL_ERROR, ERRID_NO_REAGENT,  RCTEXT_T_NOREAGENT);
      }
    }
  }
}

void SpreaderDiskCreate(tSpreaderDisk *Disk)
{
  memset(Disk, 0,sizeof(tSpreaderDisk));
  Disk->Table = SQL_Tabel_Init(&DiskTable);
}

void SpreaderDiskInit(tSpreaderDisk *Disk, uint32_t Device, uint32_t idx)
{
  ValveProp_Init(&Disk->Valve, Device, idx, 1000, 10);
}



void SpreaderDiskCycle( tSpreaderDisk *Disk, uint32_t width)
{
    if(Disk->on)
    {
      if(width != Disk->width)
      {
        Disk->width = width;
        Disk->pwm   = TableGetLSEValue(Disk->Table,0,2,width);
      }
      if(Disk->on)
      {
        Disk->output = Disk->pwm;
        Disk->running = 1;
      }
      else
      {
        Disk->output  = 0;
        Disk->running = 0;
      }
      //ValvProp_Set(&Disk->Valve,Disk->output);
      ValvProp_Set(&Disk->Valve, Disk->output);
    }
}

void SpreaderDiskOn(tSpreaderDisk *Disk, uint32_t on)
{
  Disk->on = on;
}

void SpreaderDiskTimer(tSpreaderDisk *Disk)
{
  ValvProp_Timer_100ms(&Disk->Valve);
}


int32_t TopGetSpeed(void )
{
  const tControl *Ctrl = CtrlGet();
  if(Ctrl->Auto.on)
  {
    return Ctrl->Engine.speed;
  }
  else return 30;
}

void TopBoostActivate(tTopEquip *Top, uint8_t active)// /*tSpreaderDisk *Disk, uint32_t on*/)
{
  if(active)// Disk->on)
  {
    const tControl *Ctrl = CtrlGet();
    if(Ctrl->CmdNoKey.Boost == BOOST_ON)
      Top->Boost = BOOST_ON;
    else
      Top->Boost = BOOST_OFF;
  }
  else
    Top->Boost = BOOST_INVALID;
}

int32_t TopDrawBoost(void)
{
  const tControl *Ctrl = CtrlGet();
  if(Ctrl->Top.Boost == BOOST_ON)
  {
    if(!IsMsgContainerOn(CNT_BOOST))
    {
      MsgContainerOn(CNT_BOOST);
    }
  }
  else
  {
    if(IsMsgContainerOn(CNT_BOOST))
    {
      MsgContainerOff(CNT_BOOST);
    }
  }
  return 0;
}

void TopBoost_Cycle(tTopEquip *Top)
{
  const tControl *Ctrl = CtrlGet();
  if(Top->Boost == BOOST_INVALID)
    return;

  if(Ctrl->CmdNoKey.Boost == BOOST_ON)
    Top->Boost = BOOST_ON;
  else
    Top->Boost = BOOST_OFF;
}






double ShieldFactors[] =
{
  0.08,
  0.104,
  0.128,
  0.157,
  0.19,
  0.227
};

double Conveyor_GetShieldFactor(void)
{
  uint32_t val = GetVarIndexed(IDX_SHIELDPOSITION);
  if(val  < GS_ARRAYELEMENTS(ShieldFactors))
  {
    return ShieldFactors[val];
  }
  return 0;
}


tSQLTable LRS_Section_Left ={   .TableName = "LRS Section Left " ,
  .ColumnNames = {"l / m²", "EMPTY_0","PWM",  "EMPTY_1"},
  .Cell = {
    {10   ,  0, 20,   0},
    {20   ,  0, 88,   0},
    {50   ,  0, 152,  0},
    {75   ,  0, 208,  0},
    {100  ,  0, 248,  0},
    {250  ,  0, 296,  0},
    {500  ,  0, 360,  0},
    {1000 ,  0, 385,  0},
    {1500 ,  0, 500,  0},
    {2000 ,  0, 590,  0},
    {5000 ,  0, 1000, 0},
    }
};

tSQLTable LRS_Section_Left_Center ={   .TableName = "LRS Section Left and Center" ,
  .ColumnNames = {"l / m²", "EMPTY_0", "PWM", "EMPTY_1"},
  .Cell = {
    {10    , 0,20,    0},
    {20    , 0,88,    0},
    {50    , 0,152,   0},
    {75    , 0,208,   0},
    {100   , 0,248,   0},
    {250   , 0,296,   0},
    {500   , 0,360,   0},
    {1000  , 0,385,   0},
    {1500  , 0,500,   0},
    {2000  , 0,590,   0},
    {5000  , 0,1000,  0},
    }
};
tSQLTable LRS_Section_Center ={   .TableName = "LRS Section Center" ,
  .ColumnNames = {"l / m²", "EMPTY_0", "PWM", "EMPTY_1"},
  .Cell = {
    {10    ,  0,20,   0},
    {20    ,  0,88,   0},
    {50    ,  0,152,  0},
    {75    ,  0,208,  0},
    {100   ,  0,248,  0},
    {250   ,  0,296,  0},
    {500   ,  0,360,  0},
    {1000  ,  0,385,  0},
    {1500  ,  0,500,  0},
    {2000  ,  0,590,  0},
    {5000  ,  0,1000, 0},
    }
};

tSQLTable LRS_Section_Center_Right ={   .TableName = "LRS Section Center and Right " ,
  .ColumnNames = {"l / m²","EMPTY_0","PWM",   "EMPTY_1"},
  .Cell = {
    {10    ,  0,20,  0},
    {20    ,  0,88,  0},
    {50    ,  0,152, 0},
    {75    ,  0,208, 0},
    {100   ,  0,248, 0},
    {250   ,  0,296, 0},
    {500   ,  0,360, 0},
    {1000  ,  0,385, 0},
    {1500  ,  0,500, 0},
    {2000  ,  0,590, 0},
    {5000  ,  0,1000,0},
    }
};
tSQLTable LRS_Section_Right ={   .TableName = "LRS Section Right " ,
  .ColumnNames = {"l / m²", "EMPTY_0", "PWM", "EMPTY_1"},
  .Cell = {
    {10    ,  0, 20,  0},
    {20    ,  0, 88,  0},
    {50    ,  0, 152, 0},
    {75    ,  0, 208, 0},
    {100   ,  0, 248, 0},
    {250   ,  0, 296, 0},
    {500   ,  0, 360, 0},
    {1000  ,  0, 385, 0},
    {1500  ,  0, 500, 0},
    {2000  ,  0, 590, 0},
    {5000  ,  0, 1000,0},
    }
};

tSQLTable LRS_Section_Left_Center_Right ={   .TableName = "LRS Section Left, Center and Right " ,
  .ColumnNames = {"l / m²", "EMPTY_0","PWM", "EMPTY_1"},
  .Cell = {
    {10    , 0,  20,  0},
    {20    , 0,  88,  0},
    {50    , 0,  152, 0},
    {75    , 0,  208, 0},
    {100   , 0,  248, 0},
    {250   , 0,  296, 0},
    {500   , 0,  360, 0},
    {1000  , 0,  385, 0},
    {1500  , 0,  500, 0},
    {2000  , 0,  590, 0},
    {5000  , 0,  1000,0},
    }
};

void LRS_Create(tLRS *lrs)
{
  lrs->SectionTables[LRS_SECTION_LEFT]         =  SQL_Tabel_Init(&LRS_Section_Left);
  lrs->SectionTables[LRS_SECTION_LEFT_CENTER]  =  SQL_Tabel_Init(&LRS_Section_Left_Center);
  lrs->SectionTables[LRS_SECTION_CENTER]       =  SQL_Tabel_Init(&LRS_Section_Center);
  lrs->SectionTables[LRS_SECTION_CENTER_RIGHT] =  SQL_Tabel_Init(&LRS_Section_Center_Right);
  lrs->SectionTables[LRS_SECTION_RIGHT]        =  SQL_Tabel_Init(&LRS_Section_Right);
  lrs->SectionTables[LRS_SECTION_ALL]          =  SQL_Tabel_Init(&LRS_Section_Left_Center_Right);
}

void LRS_Init(tLRS *lrs)
{
  ValveProp_Init(&lrs->DensityValve.Valve.PVEH, MCM250_5, 6,1000,10);
  lrs->DensityValve.Type = VALVE_TYPE_PVE_AHS;
}


void LRS_Cycle(tLRS *lrs, tWaterPump *Pump)
{
  const tControl *Ctrl = CtrlGet();
  uint32_t LiterPerMin;
  if(Ctrl->Auto.on)
  {
    LiterPerMin = Ctrl->Engine.speed * lrs->Density / 60;
  }
  else
  {
    LiterPerMin = 30 * lrs->Density / 60; ;
  }

  switch (lrs->ActiveSection)
  {
    case LRS_SECTION_LEFT:
      lrs->pwm =  TableGetLSEValue(lrs->SectionTables[LRS_SECTION_LEFT], 0,2, LiterPerMin);
    break;
    case LRS_SECTION_LEFT_CENTER:
      lrs->pwm =  TableGetLSEValue(lrs->SectionTables[LRS_SECTION_LEFT_CENTER], 0,2, LiterPerMin);
    break;
    case LRS_SECTION_CENTER:
      lrs->pwm =  TableGetLSEValue(lrs->SectionTables[LRS_SECTION_CENTER], 0,2, LiterPerMin);
    break;
    case LRS_SECTION_CENTER_RIGHT:
      lrs->pwm =  TableGetLSEValue(lrs->SectionTables[LRS_SECTION_CENTER_RIGHT], 0,2, LiterPerMin);
    break;
    case LRS_SECTION_RIGHT:
      lrs->pwm =  TableGetLSEValue(lrs->SectionTables[LRS_SECTION_RIGHT], 0,2, LiterPerMin);
    break;
    case LRS_SECTION_ALL:
      lrs->pwm =  TableGetLSEValue(lrs->SectionTables[LRS_SECTION_ALL], 0,2, LiterPerMin);
    break;
  default:
    break;
  }

  if(WaterPump_IsOn(Pump))
  {
    Valv_Set(&lrs->DensityValve, lrs->pwm);
    //Valv_Set(&lrs->DensityValve, lrs->pwm);
  }
  else
  {
     Valv_Set(&lrs->DensityValve, 500);
  }
  SetVar(HDL_TESTVAR_0, LiterPerMin);
 // SetVar


}


void LRS_Timer_10ms(tLRS *lrs)
{
  static int32_t count = 0;
  count++;
  if(0 == (count%10))
  {
    ValvProp_Timer_100ms(&lrs->DensityValve.Valve.PVEH);
  }
}



void RR300_Init(tTopEquip *Top)
{
   Valv_Init(&Top->MainValve, MCM250_5, 5, -1,-1);

}


void RR400_Init(tTopEquip *Top)
{
   ValvDisk_Init (&Top->MainValve, MCM250_5, 5);
   ValvDisk_Init(&Top->LRS.LeftValve, MCM250_5, 7);
   ValvDisk_Init(&Top->LRS.RightValve, MCM250_5, 4);
}
void RR300_Cycle(tTopEquip *Top)
{
  const tControl *Ctrl = CtrlGet();
  //Switch on, if Key
  Valv_Set(&Top->MainValve,  Ctrl->CmdNoKey.D_S);
  tLRS * lrs = &Top->LRS;
  if(Ctrl->CmdNoKey.D_S)
  {
    lrs->ActiveSection    = LRS_SECTION_BIN_CENTER;
  }
  else lrs->ActiveSection = 0;

}

void RR400_Cycle(tTopEquip *Top)
{
  const tControl *Ctrl = CtrlGet();
  tLRS           *lrs  = &Top->LRS;
  Valv_Set(&Top->MainValve,  Ctrl->CmdNoKey.D_S);

  //Set State of middle valve
  SetBit(&lrs->ActiveSection,1, Ctrl->CmdNoKey.D_S);
  if(Ctrl->CmdNoKey.D_S)
  {
    if(Ctrl->CmdNoKey.Active)
    {
      //y up to activate all sections.
      if( 0 < IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.Y))
      {
        lrs->ActiveSection = LRS_SECTION_ALL;
      }
      //press right to deactivate left
      if(0 < IOT_AnaJoy_PressedNew(&Ctrl->Joystick.Z))
      {
        //toggle right valve
        if(lrs->ActiveSection & LRS_SECTION_BIN_LEFT)
          SetBit(&lrs->ActiveSection,2, 0);
        else
          SetBit(&lrs->ActiveSection,2, 1);
      }
      //press left to deactivate right
      else if(0 > IOT_AnaJoy_PressedNew(&Ctrl->Joystick.Z))
      {
        //toggle right valve
        if(lrs->ActiveSection & LRS_SECTION_BIN_RIGHT)
          SetBit(&lrs->ActiveSection,0, 0);
        else
          SetBit(&lrs->ActiveSection,0, 1);
      }
    }
  }
  else
  {
    lrs->ActiveSection = 0;
  }


  //left valve
  Valv_Set(&lrs->LeftValve,lrs->ActiveSection & LRS_SECTION_BIN_LEFT );

  //right valve
  Valv_Set(&lrs->RightValve,lrs->ActiveSection & LRS_SECTION_BIN_RIGHT );

}

void RR300_RR400_Draw(tTopEquip *Top)
{

  SetVarIndexed( IDX_ROADLINEM,(Top->LRS.ActiveSection & LRS_SECTION_BIN_CENTER) ? 1:0);
  SetVarIndexed( IDX_ROADLINEL,(Top->LRS.ActiveSection & LRS_SECTION_BIN_LEFT)   ? 1:0);
  SetVarIndexed( IDX_ROADLINER,(Top->LRS.ActiveSection & LRS_SECTION_BIN_RIGHT)  ? 1:0);
}


