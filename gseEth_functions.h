﻿/************************************************************************
 *
 * File:         GS_ETH_FUNCTIONS.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef GS_ETH_FUNCTIONS_H
#include <gsSocket.h>
#include <gsSocketTCPServer.h>
#include <gsDebug.h>
#define GS_ETH_FUNCTIONS_H


/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/

/**
 * @brief Open a Ethernet Server, which will allow update over Ethernet.
 *
 * @param evtc Number of Events
 * @param evtv Events
 * @param ip_obj_id   Object ID where the IP should be written in. -1 if no Object ID
 * @param mac_obj_id  Object ID where the mac should be written in. -1 if no Object ID
 * @return int32_t 1 if Ethernet is ready and Server is running
 *                 0 if Ethernet is not ready
 *                -1 if an error occured.
 */
int32_t Upload_over_ETH(uint32_t evtc, tUserCEvt * evtv, int32_t ip_obj_id, int32_t mac_obj_id);

/**
 * @brief Deinitalizes the Ethernetserver. Upload over Ethernet is not possible anymore.
 *
 */
void DeInit_Upload_over_ETH(void);

/**
 * @brief Returns the actual IP of the device as String
 *
 * @return char* IP-Address
 */
char * Upload_over_ETH_Get_IP (void);

/**
 * @brief Returns the actual Mac of the device as String
 *
 * @return char* Mac-Address
 */
char * Upload_over_ETH_Get_Mac(void);

/****************************************************************************
**
**    Function      : DeInit_Debug_ETH
**
**    Description   : destroy the TCP Server which is used for the ETH Debug
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void DeInit_Debug_ETH(void);




/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef GS_ETH_FUNCTIONS_H
