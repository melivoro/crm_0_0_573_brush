﻿/****************************************************************************
 *
 * File:         CTRL_SIDE_ATTACH.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */


#include "control.h"
#include "io.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/




void Ctrl_Side_CORMZ_OBA_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach   ,MCM250_2, 0, MCM250_2, 2 );
    Plough_WorkTransport(&Ctrl->ABC[EQUIP_C].ABC_Attach , -1,-1,-1,-1 );
    Plough_TiltUpDown(&Ctrl->ABC[EQUIP_C].ABC_Attach    , -1,-1,-1,-1);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach    ,MCM250_2, -1 );
}

void Ctrl_Side_AA_AC_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach   ,MCM250_2, 0, MCM250_2, 2 );
    Plough_Float_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach    ,MCM250_2, 8 );
    Plough_WorkTransport(&Ctrl->ABC[EQUIP_C].ABC_Attach , MCM250_2,7,MCM250_2,5 );
    Plough_TiltUpDown(&Ctrl->ABC[EQUIP_C].ABC_Attach    , MCM250_2,0,MCM250_2,2);
    Plough_Float_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach    ,MCM250_2, 8 );
    Ctrl->ABC[EQUIP_C].ABC_Attach.MaxSpeed = 40;
}

void Ctrl_Side_BUCHER_AK_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach   ,MCM250_1, 6, MCM250_1, 4 );
    Plough_UpDown_Second_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach   ,MCM250_2, 6, MCM250_2, 4 );//use this for additonal valves for up down movement
    Plough_Float_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach    ,MCM250_2, 2 );
    Plough_WorkTransport(&Ctrl->ABC[EQUIP_C].ABC_Attach ,MCM250_2,5,MCM250_2,7 );
    Plough_TiltUpDown(&Ctrl->ABC[EQUIP_C].ABC_Attach    ,-1,-1,-1,-1);
    Ctrl->ABC[EQUIP_C].ABC_Attach.MaxSpeed = 60;
}
void Ctrl_Side_MSN37_Init(tControl *Ctrl)
{
    Plough_UpDown_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach   ,MCM250_2, 0, MCM250_2, 2 );
    Plough_Float_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach    ,MCM250_2, 8 );
    Plough_WorkTransport(&Ctrl->ABC[EQUIP_C].ABC_Attach , MCM250_2,7,MCM250_2,5 );
    Plough_TiltUpDown(&Ctrl->ABC[EQUIP_C].ABC_Attach    , MCM250_2,0,MCM250_2,2);
    Ctrl->ABC[EQUIP_C].ABC_Attach.MaxSpeed = 40;
}

void Ctrl_Side_TE_Init(tControl *Ctrl)
{
    Plough_ExRetract_RightSection(&Ctrl->ABC[EQUIP_C].ABC_Attach, MCM250_1, 9 );
    Plough_UpDown_Init     (&Ctrl->ABC[EQUIP_C].ABC_Attach,MCM250_1, 1 ,-1,-1);
    Plough_Float_Init      (&Ctrl->ABC[EQUIP_C].ABC_Attach,MCM250_1, 8 );
    Plough_LeftRight_Init  (&Ctrl->ABC[EQUIP_C].ABC_Attach, MCM250_1,0,MCM250_1,2);

    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach,-1,-1,-1,-1);
    Plough_ExRetract_Init  (&Ctrl->ABC[EQUIP_C].ABC_Attach, MCM250_1,5,MCM250_1,7);
    Ctrl->ABC[EQUIP_C].ABC_Attach.MaxSpeed = 40;
}
void Ctrl_Side_VType_Init(tControl *Ctrl)
{
    Plough_ExRetract_RightSection(&Ctrl->ABC[EQUIP_C].ABC_Attach, MCM250_1, 9 );
    Plough_UpDown_Init     (&Ctrl->ABC[EQUIP_C].ABC_Attach,MCM250_1, 1 ,-1,-1);
    Plough_Float_Init      (&Ctrl->ABC[EQUIP_C].ABC_Attach,MCM250_1, 8 );
    Plough_LeftRight_Init  (&Ctrl->ABC[EQUIP_C].ABC_Attach, MCM250_1,0,MCM250_1,2);

    Plough_SteelRubber_Init(&Ctrl->ABC[EQUIP_C].ABC_Attach,-1,-1,-1,-1);
    Plough_ExRetract_Init  (&Ctrl->ABC[EQUIP_C].ABC_Attach, MCM250_1,5,MCM250_1,7);
    Ctrl->ABC[EQUIP_C].ABC_Attach.MaxSpeed = 40;
}


void Ctrl_Side_Plough_Draw(tControl *Ctrl)
{
    if(FLOAT_ON == Ctrl->CmdABC[EQUIP_C].Float)
    {
        SetVarIndexed(IDX_FLOATING_SIDE,1);
    }
    else
    {
        SetVarIndexed(IDX_FLOATING_SIDE,0);
    }
}

void Ctrl_Side_AK_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
    tABC_Attach *Plough = &Ctrl->ABC[EQUIP_C].ABC_Attach;

        int32_t FloatValve = 0;
        if(Ctrl->CmdABC[EQUIP_C].Float == FLOAT_ON)//FloatValve
        {
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[0], 1);
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[1], 1);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[0], 0);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[1], 0);
            Ctrl_SetMaxSpeed(Plough->MaxSpeed);
            Valv_Set(&Plough->ValveFloat, 0);

        }
        else if(Ctrl->CmdABC[EQUIP_C].Updown > 0 )//down
        {
            FloatValve = 1;
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[0], 0);
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[1], 1);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[0], 0);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[1], 1);
        }
        else if(Ctrl->CmdABC[EQUIP_C].Updown < 0)//up
        {
            FloatValve = 1;;
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[0], 1);
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[1], 0);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[0], 1);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[1], 0);

        }
        else//off
        {
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[0], 0);
            ValvDig_2_2_Set(&Plough->ValveUpDown.Valve.PVEO[1], 0);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[0], 0);
            ValvDig_2_2_Set(&Plough->ValveUpDownSecondary.Valve.PVEO[1], 0);
        }
        if(Ctrl->CmdABC[EQUIP_C].LeftRight)
        {
            FloatValve = 1;

        }
        Valv_Set(&Plough->ValveWorkTransport,Ctrl->CmdABC[EQUIP_C].LeftRight );
        Valv_Set(&Plough->ValveFloat,FloatValve);
        Ctrl_Side_Plough_Draw(Ctrl);
}


void Ctrl_Side_Plough_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
    tABC_Attach *Plough = &Ctrl->ABC[EQUIP_C].ABC_Attach;
    //MoveUpDown;
    Valv_Set(&Plough->ValveUpDown,Ctrl->CmdABC[EQUIP_C].Updown);
    Valv_Set(&Plough->ValveFloat, Ctrl->CmdABC[EQUIP_C].Float);
    Valv_Set(&Plough->ValveTilt, Ctrl->CmdABC[EQUIP_C].Z);
    Valv_Set(&Plough->ValveWorkTransport,Ctrl->CmdABC[EQUIP_C].LeftRight);
    Ctrl_Side_Plough_Draw(Ctrl);
}

void Ctrl_Side_Plough_Timer_10ms(tControl *Ctrl)
{
    static int32_t count = 0;
    count++;
    if(0 == (count %10))
    {
        Valv_Timer_100ms(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveTilt);
        Valv_Timer_100ms(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveUpDown);
        Valv_Timer_100ms(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveWorkTransport);
    }
}
