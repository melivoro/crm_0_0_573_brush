﻿/****************************************************************************
 *
 * File:         VISO_IO.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu.h"

#include "io.h"
#include "visu_io.h"
#include "gseMCM.h"
#include "control.h"
/****************************************************************************/

/* macro definitions ********************************************************/
#define DEVICE_TYPE_UNKNOWN -1
#define DEVICE_TYPE_MCM250   0
/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/



uint32_t CurrentDeviceType = DEVICE_TYPE_UNKNOWN;


/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Visu_IO_Init(const tVisuData* Data)
{
  VarEnableEvent(HDL_ACTUALIODEVICE);
  SetVarIndexed(IDX_ACTUALIODEVICE,1);

}

void Visu_IO_DrawMCM250(uint32_t CurrentDevice)
{

  InfoContainerOn(CNT_IO_MCM250);

  char string[32];
  sprintf(string, "MCM250_%u",CurrentDevice + 1);
  SetVisObjData(OBJ_DEVICENAME, string, strlen(string)+1);
  for(int32_t i = 0; i < 10; i++)
  {
    SetVar(HDL_IO_VIEW_DO_00 + i, GSeMCM_GetDigitalOut(CurrentDevice, i));
  }
  for(int32_t i = 0; i < 4; i++)
  {
    uint16_t duty;
    uint16_t frequency;
    GSeMCM_GetPWMOut(CurrentDevice,i, &duty, &frequency);
    SetVar(HDL_IO_VIEW_PWM_0 + i,duty);
  }
}

void Visu_IO_DrawJoystick(void)
{
  char string[32];
  sprintf(string, "Joystick");
  SetVisObjData(OBJ_DEVICENAME, string, strlen(string)+1);
  InfoContainerOn(CNT_IO_JOYSTICK);
}

void Visu_IO_DrawK2_4x3(uint32_t CurrentDevice)
{

  char string[32];
  sprintf(string, "Keyboard");
  SetVisObjData(OBJ_DEVICENAME, string, strlen(string)+1);
  InfoContainerOn(CNT_IO_K2_3X4);

}


void Visu_IO_Engine(uint32_t CurrentDevice)
{

  char string[32];
  sprintf(string, "Engine");
  SetVisObjData(OBJ_DEVICENAME, string, strlen(string)+1);
  InfoContainerOn(CNT_ENGINE);
}


void Visu_IO_Draw(void)
{
  InfoContainerOff(CNT_IO_MCM250);
  InfoContainerOff(CNT_IO_K2_3X4);
  InfoContainerOff(CNT_IO_JOYSTICK);
  uint32_t CurrentDevice = GetVarIndexed(IDX_ACTUALIODEVICE) - 1;
  if(CurrentDevice < K2_4X3)
  {
    Visu_IO_DrawMCM250(CurrentDevice);
  }
 /* else if(CurrentDevice == K2_4X3)
  {
    Visu_IO_DrawK2_4x3(CurrentDevice);
  }*/
  else if(CurrentDevice == JOYSTICK)
  {
    Visu_IO_DrawJoystick();
  }
  else
  {
    Visu_IO_Engine(CurrentDevice);
  }

}

void Visu_IO_MCM250_Cycle(uint32_t CurrentDevice, uint32_t evtc, tUserCEvt *evtv)
{
  SetVar(HDL_IO_VIEW_DEVICE_STATE, GSeMCM_GetActiveStatus(CurrentDevice));

  for(int32_t i = 0; i < 6; i++)
  {
    SetVar(HDL_IO_VIEW_DI_00 + i, GSeMCM_GetDigitalIn(CurrentDevice, i));
  }
  for(int32_t i = 0; i < 10; i++)
  {
    GSeMCM_SetDigitalOut(CurrentDevice, i, GetVar(HDL_IO_VIEW_DO_00 + i));
  }
  for(int32_t i = 0; i < 12; i++)
  {
    SetVar(HDL_IO_VIEW_AI_00 + i, GSeMCM_GetAnalogIn(CurrentDevice, i));
  }

  for(int32_t i = 0; i < 4; i++)
  {
    SetVar(HDL_IO_VIEW_FREQ_0 + i, GSeMCM_GetFrequencyInput(CurrentDevice, i));
  }

  for(int32_t i = 0; i < 4; i++)
  {
    uint16_t duty;
    uint16_t frequency;
    GSeMCM_GetPWMOut(CurrentDevice,i, &duty, &frequency);
    SetVar(HDL_IO_VIEW_PWM_0 + i,duty);
  }
}

void Viso_IO_Joystick_Cycle(void)
{
  const tControl *Ctrl = CtrlGet();
  SetVar(HDL_IO_VIEW_DI_00, IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_A]) ? 1:0);
  SetVar(HDL_IO_VIEW_DI_01, IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_B]) ? 1:0);
  SetVar(HDL_IO_VIEW_DI_02, IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_C]) ? 1:0);
  SetVar(HDL_IO_VIEW_DI_03, IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_D]) ? 1:0);
  SetVar(HDL_IO_VIEW_DI_04, IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_E]) ? 1:0);
  SetVar(HDL_IO_VIEW_DI_05, IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_F]) ? 1:0);
  SetVar(HDL_IO_VIEW_AI_00, IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.UpDown));
  SetVar(HDL_IO_VIEW_AI_01, IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.LeftRight));
  SetVar(HDL_IO_VIEW_AI_02, IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.X));
  SetVar(HDL_IO_VIEW_AI_03, IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.Y));
  SetVar(HDL_IO_VIEW_AI_04, IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.Z));
  SetVar(HDL_IO_VIEW_DEVICE_STATE, IO_Joystick_IsActive(&Ctrl->Joystick));
}


void Viso_IO_Engine_Cycle(void)
{
  const tControl *Ctrl = CtrlGet();
  SetVar(HDL_IO_VIEW_AI_00,Ctrl->Engine.rpm);
  SetVar(HDL_IO_VIEW_AI_01,Ctrl->Engine.AirPressure);
  SetVar(HDL_IO_VIEW_AI_02,Ctrl->Engine.OilTemp );
  SetVar(HDL_IO_VIEW_AI_03,Ctrl->Engine.OilLevel);
  SetVar(HDL_IO_VIEW_AI_04,Ctrl->Engine.CoolentTemp);
  SetVar(HDL_IO_VIEW_AI_05,Ctrl->Engine.CoolentPressure);
  SetVar(HDL_IO_VIEW_AI_06,Ctrl->Engine.CoolentLevel);
  SetVar(HDL_IO_VIEW_AI_07,Ctrl->Engine.AmbientAirTemp);
  SetVar(HDL_IO_VIEW_AI_08,Ctrl->Engine.FuelTemp);
  SetVar(HDL_IO_VIEW_AI_09,Ctrl->Engine.speed);
  SetVar(HDL_IO_VIEW_AI_10,Ctrl->Engine.PTO_Engaged);
  SetVar(HDL_IO_VIEW_DEVICE_STATE, Ctrl->Engine.Active);
}
/*void Visu_IO_Keyboard_Cycle(uint32_t CurrentDevice)
{
  for(int32_t i = 0; i < 12; i++)
  {
    SetVar(HDL_IO_VIEW_DI_00 + i, GSeMCM_IsKeyDown(CurrentDevice,i+1));
  }
  SetVar(HDL_IO_VIEW_DEVICE_STATE, GSeMCM_GetActiveStatus(CurrentDevice));
}*/


void Visu_IO_Open_cb(const tVisuData * Data)
{

  VarEnableEvent(HDL_ACTUALIODEVICE);
  SetVarIndexed(IDX_ACTUALIODEVICE, 1);

  PrioMaskOn(MSK_IO);
  Visu_IO_Draw();
}






void Visu_IO_Cycle_cb(const tVisuData * Data, uint32_t evtc, tUserCEvt *evtv)
{
  for(uint32_t i = 0 ; i < evtc; i++)
  {
    if(evtv[i].Type == CEVT_VAR_CHANGE)
    {
      if(evtv[i].Content.mVarChange.VarHandle == HDL_ACTUALIODEVICE)
      {
        if((uint32_t)(GetVarIndexed(IDX_ACTUALIODEVICE) - 1) >= ENGINE)
        {
          SetVarIndexed(IDX_ACTUALIODEVICE, ENGINE + 1);
        }
        Visu_IO_Draw();
      }
    }
  }
  uint32_t CurrentDevice = GetVarIndexed(IDX_ACTUALIODEVICE) - 1;
  if(CurrentDevice < K2_4X3)
  {
    Visu_IO_MCM250_Cycle(CurrentDevice, evtc, evtv);
  }
  /*else if(CurrentDevice == K2_4X3)
  {
    Visu_IO_Keyboard_Cycle(CurrentDevice);
  }*/
  else if(CurrentDevice == JOYSTICK)
  {
    Viso_IO_Joystick_Cycle();
  }
  else
  {
    Viso_IO_Engine_Cycle();
  }

  Visu_HomeKeyPressed();
}

void Visu_IO_Timer_cb(const tVisuData * Data)
{

  uint32_t CurrentDevice = GetVarIndexed(IDX_ACTUALIODEVICE) - 1;
  if(CurrentDevice < K2_4X3)
  {
    for(int32_t i = 0; i < 10; i++)
    {
      SetVar(HDL_IO_VIEW_DO_00 + i, GSeMCM_GetDigitalOut(CurrentDevice, i));
    }
  }
}



void Visu_IO_Close_cb(const tVisuData *Data)
{
  VarDisableEvent(HDL_ACTUALIODEVICE);
  InfoContainerOff(CNT_IO_MCM250);
  InfoContainerOff(CNT_IO_K2_3X4);
  InfoContainerOff(CNT_IO_JOYSTICK);
  InfoContainerOff(CNT_ENGINE);
}
