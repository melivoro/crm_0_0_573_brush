﻿/****************************************************************************
 *
 * File:         VISU_MAIN.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "visu_material.h"

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "visu.h"
#include "RCText.h"
#include "profile.h"
#include "gseErrorlist.h"
#include "gsToVisu.h"
#include "RCColor.h"
#include "gseDebug.h"
#include "visu_learning.h"

//#include "visu_material.c"


/****************************************************************************/

/* macro definitions ********************************************************/
#define PASSWORD_ADMIN    1
#define PASSWORD_MERKATOR 2
/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/



void Visu_Main_Open (const tVisuData * VData)
{
  PrioMaskOn(MSK_MAIN);
  db_out("Draw Mainscreen:\r\n");
  Prof_Draw();
  db_out("Mainscreen drawn\r\n");
}


void Visu_Main_Close (const tVisuData * VData)
{
  MsgContainerOff(CNT_MAINMENU);
  MsgContainerOff(CNT_PASSWORD);
  ProfHide();
  Visu_Learning_Close(VData);
}


uint32_t Password_Merkator = 0;

void Password_Open(const tVisuData *VData)
{
  SetVar(HDL_PASSWORD_ENTER, 0);
  MsgContainerOff(CNT_MAINMENU);
  MsgContainerOn(CNT_PASSWORD);

}

void Password_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{
  uint32_t Password = (uint32_t) GetVarIndexed(IDX_PASSWORD_ENTER);

  if(Password == (uint32_t)GetVar(HDL_PASSWORD_MERKATOR) )
  {
    MsgContainerOff(CNT_PASSWORD);
    SetVar(HDL_USER, RCTEXT_LI_USER_PASSWORD_MERKATOR);
    Visu_OpenScreen(SCREEN_MENU_MERKATOR);
  }
  else if(Password == (uint32_t) GetVar(HDL_PASSWORD_ADMIN))
  {
    MsgContainerOff(CNT_PASSWORD);
    SetVar(HDL_USER, RCTEXT_LI_USER_PASSWORD_ADMIN);
    Visu_OpenScreen(SCREEN_MENU_ADMIN);
  }
}


void MenuContainer_Cycle(const tVisuData * VData,uint32_t evtc, tUserCEvt *evtv )
{
  for(uint32_t i = 0; i < evtc ; i++)
  {
    if(CEVT_MENU_INDEX == evtv[i].Type)
    {
      uint32_t obj = evtv[i].Content.mMenuIndex.ObjID;
      if(OBJ_MENUTEXT_ERRORS == obj)
      {
        Visu_OpenScreen(SCREEN_ERROR);
      }
      else if(OBJ_MENU_TEXT_PROPERTIES == obj)
      {
        Visu_OpenScreen(SCREEN_PROPERTIES);
      }
      else if(OBJ_MENUTEXT_LEARNING == obj)
      {
        SetVar(HDL_LEARNING, 1 - GetVar(HDL_LEARNING));
        if(GetVar(HDL_LEARNING))
        {
          InfoContainerOn(CNT_LEARNING);
          MsgContainerOff(CNT_MAINMENU);
        }
        else
        {
          InfoContainerOff(CNT_LEARNING);
          MsgContainerOff(CNT_MAINMENU);
        }
      }
     else if(OBJ_MENUTEXT_SENSORS == obj)
      {
        Visu_OpenScreen(SCREEN_SENSORS);
      }
/*      else if (OBJ_MENUTEXT_SPREADINGSETTINGS==obj)
        {
          PrioMaskOn(MSK_SPREADINGSETTINGS);
        }*/
      else if(OBJ_MENUTEXT_SERVICEMENU_1 == obj)
      {

        if(GetVarIndexed(IDX_USER) == RCTEXT_LI_USER_PASSWORD_MERKATOR)
        {
          Visu_OpenScreen(SCREEN_MENU_MERKATOR);
        }
        else if(GetVarIndexed(IDX_USER) == RCTEXT_LI_USER_PASSWORD_ADMIN)
        {
          Visu_OpenScreen(SCREEN_MENU_ADMIN);
        }
        else
        {
          Password_Open(VData);
        }
      }
      else if(OBJ_MENUTEXT_SPREADSETTING == obj)
      {
        Visu_OpenScreen(SCREEN_SPREADINGSETTINGS);
      }
    }
  }


}

void Visu_Main_DrawProfiles(const tVisuData * VData)
{
  //tProfile *ActProfile = Profile_GetCurrentProfile();

}

#define IDXBITMAP_WETTINGSENSOR_NO_SENSOR 0
#define IDXBITMAP_WETTINGSENSOR_FULL      1
#define IDXBITMAP_WETTINGSENSOR_EMPTY     2

void VisuMain_Draw_LiquidSensor(const tControl *Ctrl)
{

  if(Ctrl->Top.LiquidSensorAvailable)
  {

    static int32_t LiquidSensorOld = -1;
    int32_t LiquidSensor;
    if(GetVarIndexed(IDX_WETTINGSENSORACTIVE))
    {
      if(Ctrl->Sensors.DI.Humidifier == DI_SENSOR_ON)
      {
        //No Water
        LiquidSensor = IDXBITMAP_WETTINGSENSOR_EMPTY;
      }
      else
        LiquidSensor = IDXBITMAP_WETTINGSENSOR_FULL;
    }
    else
    {
      LiquidSensor = IDXBITMAP_WETTINGSENSOR_NO_SENSOR;
    }
    if(LiquidSensorOld != LiquidSensor)
    {
      LiquidSensorOld = LiquidSensor;
      SetVarIndexed(IDX_WETTINGSENSORSTATE,LiquidSensor);
      if(IDXBITMAP_WETTINGSENSOR_EMPTY == LiquidSensor)
      {
        SendToVisuObj(OBJ_ICONLIQUIDSTATE, GS_TO_VISU_SET_ATTR_BLINK,1);
        SetBuzzer(1,100,100,3);
      }
      else
      {
        SendToVisuObj(OBJ_ICONLIQUIDSTATE, GS_TO_VISU_SET_ATTR_BLINK,0);
      }

    }
  }
  else
  {
    SetVarIndexed(IDX_WETTINGSENSORSTATE,0);
  }
  //hide liquidsensor if not available
  static int32_t LiquidSensorAvailable = -1;
  if(Ctrl->Top.LiquidSensorAvailable != LiquidSensorAvailable)
  {
    LiquidSensorAvailable = Ctrl->Top.LiquidSensorAvailable;
    {
      if(LiquidSensorAvailable)
      {
        SendToVisuObj(OBJ_ICONLIQUIDSTATE,  GS_TO_VISU_SET_ATTR_VISIBLE, 1);
      }
      else
      {
        SendToVisuObj(OBJ_ICONLIQUIDSTATE,  GS_TO_VISU_SET_ATTR_VISIBLE, 0);
      }

    }
  }
}

void Visu_Main_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{

 SetVarIndexed(IDX_WETTINGSENSORACTIVE, 0/*1*/); //Toggle automatic 0 because of KH, AK
  //db_out("Start Cycle of MainScreen\r\n");
if(IsKeyPressedNew(6))
{
  if(IsMsgContainerOn(CNT_LIGHTKEYS))
  {
      MsgContainerOff(CNT_LIGHTKEYS);
      }
      else MsgContainerOn(CNT_LIGHTKEYS);
}

if (IsMsgContainerOn(CNT_LIGHTKEYS)==0)
  {
    if(IsKeyPressedNew(3))
  {
    Ctrl_Automatic_Toggle();
  }
  /*if(IsKeyPressedNew(4))
  {
    SetVarIndexed(IDX_WETTINGSENSORACTIVE, 1 - GetVarIndexed(IDX_WETTINGSENSORACTIVE));
  }*/
/*
  if(IsKeyPressedNew(6))
    {
      Reagent_Next();
      Reagent_Draw();
    }
*/

  }
  if(IsKeyPressedNew(10))
  {
    if(GetVarIndexed(IDX_LEARNING))
    {
      SetVarIndexed(IDX_LEARNING, 0);
      Visu_Learning_Close(VData);
    }
    else if(IsMsgContainerOn(CNT_MAINMENU))
    {
      MsgContainerOff(CNT_MAINMENU);
    }
    else
    {
      MsgContainerOn(CNT_MAINMENU);
      MsgContainerOff(CNT_PASSWORD);
      SetVarIndexed(IDX_LEARNING, 0);
      Visu_Learning_Close(VData);
    }
  }

  if(IsMsgContainerOn(CNT_MAINMENU))
  {
    MenuContainer_Cycle(VData, evtc,evtv);
  }


  if(CNT_PASSWORD == GetCurrentMsgContainerShown())
  {
    Password_Cycle(VData, evtc,evtv);
  }

  const tControl *Ctrl = CtrlGet();
  /****************
   * Draw
   * *************/
  //Show container if Attachment is moved
  //db_out("Start draw\r\n");
  Prof_DrawMove();

  //Auto
  if(Ctrl->Auto.on)
  {
    SetVarIndexed(IDX_AUTOMANU, RCTEXT_LI_AUTOMANU_STANDARD_AUTO);
  }
  else
    SetVarIndexed(IDX_AUTOMANU, RCTEXT_LI_AUTOMANU_STANDARD_MANU);

  //Speed
  SetVarIndexed(IDX_SPEED,TopGetSpeed());
  //Temperature
  SetVarIndexed(IDX_TEMPOIL    , (IOT_NormAI(&Ctrl->Sensors.AI.OilTemp) + 5) /10*10);
  SetVarIndexed(IDX_TEMPOUTSIDE, Ctrl->Engine.AmbientAirTemp);

  //Icon OilLevel:
  static int32_t OilLevel_Old = -1;
  if(OilLevel_Old != GetVarIndexed(IDX_OILLEVEL))
  {
    OilLevel_Old = GetVarIndexed(IDX_OILLEVEL);
    if(-1 == OilLevel_Old)
    {
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_FG_COLOR,RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR), RCCOLOR_NAME_FRONT) );
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_ATTR_BLINK,0);
    }
    else if(0 == OilLevel_Old)
    {
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_FG_COLOR,RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR), RCCOLOR_NAME_GREEN) );
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_ATTR_BLINK,0);
    }
    else if(1 == OilLevel_Old)
    {
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_FG_COLOR,RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR), RCCOLOR_NAME_YELLOW) );
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_ATTR_BLINK,0);
    }
    else if(2 == OilLevel_Old)
    {
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_FG_COLOR,RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR), RCCOLOR_NAME_RED) );
      SendToVisuObj(OBJ_ICONOILLEVEL, GS_TO_VISU_SET_ATTR_BLINK,1);
    }
  }

  //Icon Oil Filter:
  static int32_t Oilfilter_Old = - 1;
  if(Oilfilter_Old != Ctrl->Sensors.DI.OilFilter)
  {
     Oilfilter_Old = Ctrl->Sensors.DI.OilFilter;
     if(Oilfilter_Old)
     {
       SendToVisuObj(OBJ_ICONFILTER, GS_TO_VISU_SET_FG_COLOR,RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR), RCCOLOR_NAME_RED) );
     }
     else
     {
       SendToVisuObj(OBJ_ICONFILTER, GS_TO_VISU_SET_FG_COLOR,RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR), RCCOLOR_NAME_GREEN) );
     }

  }
  //Icon Liquidsensor:
  //db_out("Draw LiquidSensor\r\n");
  VisuMain_Draw_LiquidSensor(Ctrl);

  if(GetVarIndexed(IDX_LEARNING))
  {
    Visu_Learning_Cycle(VData,  evtc, evtv);
  }



}

void Visu_Main_Timer(const tVisuData * VData)
{
  static int32_t count = 0;
  count++;
  //db_out("Call Main Timer\r\n");
  if(0 == (count % 10 ) )
  {
//    db_out("Get Errors\r\n");
    if(EList_GetErrors(ES_UNCHECKED_ERROR_ACTIVE | ES_UNCHECKED_ERROR_INACTIVE))
    {
      SetVarIndexed(IDX_ERRORICON, 2);
    }
    else if(EList_GetErrors(ES_CHECKED_ERROR_ACTIVE))
    {
      SetVarIndexed(IDX_ERRORICON, 1);
    }
    else
    {
      SetVarIndexed(IDX_ERRORICON, 0);
    }
  }
}
