﻿/************************************************************************
 *
 * File:         GSEOBJ.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef GSEOBJ_H
#define GSEOBJ_H
#include "stdint.h"
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
typedef struct tagGSeSquare
{
  uint32_t id;
  int32_t x;
  int32_t y;
  uint32_t dx;
  uint32_t dy;
}tGSeSquare;






/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef GSEOBJ_H
