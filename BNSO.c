﻿/****************************************************************************
 *
 * File:         BNSO.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "J1939.h"
#include "control.h"
#include "ctrl_top_adrive.h"
#include "BNSO.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/
int32_t BNSO_ValvToJ1939(const tValv *Valv)
{
    switch(Valv->Type)
    {
        case VALVE_TYPE_PVE_O:
            if(Valv->Valve.PVEO[0].val == 1)
            {
                return 0x01;
            }
            else if(Valv->Valve.PVEO[1].val == 1)
            {
                return 0x2;
            }
            else return 0;
        break;

        case VALVE_TYPE_DISK:
            if(Valv->Valve.PVEO[0].val ==1)
            {
                return 0x01;
            }
            else return 0;
        break;

        case VALVE_TYPE_PVE_AHS:
            if(Valv->Valve.PVEH.duty > 500)
            {
                return 0x01;
            }
            else if(Valv->Valve.PVEH.duty < 500)
            {
                return 0x02;
            }
            else if(Valv->Valve.PVEH.duty == 500)
            {
                return 0x00;
            }
            else return 0x03;
        break;
        default:

        break;

    }
    return 0x3;
}

int32_t  BNSO_ValvChangeToJ1939(const tValv *Valv, int8_t *value)
{
    if(NULL == value)
    {
        return 0;
    }
    switch(Valv->Type)
    {
        case VALVE_TYPE_PVE_O:
            if(Valv->Valve.PVEO[0].val == 1)
            {
               *value = 1;
               return 1;
            }
            else if(Valv->Valve.PVEO[1].val == 1)
            {
                *value = 0x2;
                return 1;
            }
            else
            {
                *value = 0x00;
                return 1;
            }
        break;

        case VALVE_TYPE_DISK:
            if(Valv->Valve.PVEO[0].val ==1)
            {
                *value = 1;
                return 0x01;
            }
            else
            {
                *value = 0;
                return 0x01;
            }

        break;

        case VALVE_TYPE_PVE_AHS:
            if(Valv->Valve.PVEH.duty > 550)  //replaced 500 with 550 to catch neutral position
            {
                *value = 0x01;
                return 1;
            }
            else if(Valv->Valve.PVEH.duty < 450) //replaced 500 with 450 to catch neutral position
            {
                *value = 0x02;
                return 1;
            }
            else
            {
                *value = 0x00;
                return 1;
            }

        break;
        default:

        break;

    }
    return 0 ;

}

/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void BNSO_SetData(void)
{
    tPGN_FF01 *FF01  = J1939_GetPGN_FF01();
    tPGN_FF02 *FF02  = J1939_GetPGN_FF02();
    const tControl *Ctrl  = CtrlGet();
    static int32_t count = 0;
    if(500 < GetMSTick() - count)
    {
        count = GetMSTick();
        int8_t v;
        //FRONT
        //Floating
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveFloat, &v))
            FF01->FrontAttach_FloatingMode =  v & 0x03;
        //UpDown
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveUpDown, &v))
            FF01->FrontAttach_VerticalPos =  v & 0x03;
        //LeftRight
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveLeftRight, &v))
            FF01->FrontAttach_HorizontalPos =  v & 0x03;
        //Rotation
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveRotation, &v))
            FF01->FrontAttach_Rotation      =  v & 0x03;
        //Bladetype
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_A].ABC_Attach.ValveSteelUpDown, &v))
            FF01->FrontAttach_BladeType     =  v & 0x03;
        //Interax

        //Interax
        //Floating
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveFloat, &v))
            FF01->Interaxle_FloatingMode =  v & 0x03;
        //UpDown
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveUpDown, &v))
            FF01->Interaxle_VerticalPos =  v & 0x03;
        //LeftRight
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveLeftRight, &v))
            FF01->Interaxle_HorizontalPos =  v & 0x03;
        //Rotation
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveRotation, &v))
            FF01->Interaxle_Rotation      =  v & 0x03;
        //Bladetype
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_B].ABC_Attach.ValveSteelUpDown, &v))
            FF01->Interaxle_BladeType     =  v & 0x03;
        //Interax


        //Side
        //Floating
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveFloat, &v))
            FF01->SideAttach_FloatingMode  =  v & 0x03;
        //UpDown
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveUpDown, &v))
            FF01->SideAttach_VerticalPos   =  v & 0x03;
        //Tilt
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveTilt, &v))
            FF01->SideAttach_Tilt          =  v & 0x03;
        //LeftRight
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveLeftRight, &v))
            FF01->SideAttach_HorizontalPos =  v & 0x03;
        else if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveWorkTransport, &v))
            FF01->SideAttach_HorizontalPos =  v & 0x03;
        //Rotation
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveRotation, &v))
            FF01->SideAttach_Rotation      =  v & 0x03;
        //Bladetype
        if(1 == BNSO_ValvChangeToJ1939(&Ctrl->ABC[EQUIP_C].ABC_Attach.ValveSteelUpDown, &v))
            FF01->SideAttach_BladeType     =  v & 0x03;
        //Interax
        FF01->SprayWidth = Ctrl->Top.Width;
        FF01->SprayDensity = Ctrl->Top.Density / 4;

        int32_t Pos = GetVarIndexed(IDX_SPREADINGSECTOR_ACT) - 1; //read actual positon from visu
        if(Pos < 0)
        {
            Pos = 4;
        }
        FF01->PositionAsymmetryDrive = Pos;

        FF01->ReagentFeedRate = Ctrl->Sensors.Freq.ConveryRotation;                      // 0 to 8000 rpm
        FF01->PresenceOfSprinklers           = 0x3;     // 0 No spraying, 1 spraying, 2 no sensor
        FF01->PresenceOfFluidHuidifier       = 0x3;     // 0 No Fluid, 1 Fluid, 2 no sensor

        FF02->HydSysTemp = (IOT_NormAI(& Ctrl->Sensors.AI.OilTemp) + 5) /10 * 10; // test for msg
        //FF02->HydSysTemp = (IOT_NormAI(& Ctrl->Sensors.AI.OilTemp) / 100 + 4) * 10;//roundend by last number
        FF02->HydSysPres = IOT_NormAI(& Ctrl->Sensors.AI.OilPressure) * 0.5;
        //hydraulic Oil level
        uint8_t Oillevel = 0;
        if(0 == Ctrl->Sensors.DI.CheckOilLevelSensor)
        {
            Oillevel = 0xFE; //Fault
        }
        else if((1 == Ctrl->Sensors.DI.UpperOilLevel)
              &&(0 == Ctrl->Sensors.DI.LowerOilLevel))
        {
            Oillevel = 100.0 * 2.5;
        }
        else if((0 == Ctrl->Sensors.DI.UpperOilLevel)
              &&(0 == Ctrl->Sensors.DI.LowerOilLevel))

        {
            Oillevel = 50.0 * 2.5;
        }
        else Oillevel = 0;

        FF02->HydSysOilLev = Oillevel;


        if(((int8_t)FF02->HydSysTemp) < -50) // Out of range
        {
            FF02->OilTemp = 0;
        }
        else if(((int8_t) FF02->HydSysTemp) < -20) // low temperature
        {
            FF02->OilTemp = 1;
        }
        else if(FF02->HydSysTemp < 30) // operating temperature range
        {
            FF02->OilTemp = 2;
        }
        else
        {
            FF02->OilTemp = 3;
        }
    }
}


void BNSO_SendErrMsg(uint16_t ErrMsgId, uint8_t state)
{
    uint16_t data = (ErrMsgId << 4) + (state & 0xF);
    J1939_SendPGN(J1939_ERR_MSG_ID, &data, sizeof(uint16_t));
}
