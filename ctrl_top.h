﻿/************************************************************************
 *
 * File:         CTRL_TOP.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CTRL_TOP_H
#define CTRL_TOP_H

/*************************************************************************/
#include "sql_table.h"
#include "HydValveCtrl.h"
#include "gseMaint_Runtime.h"
#include "DrawFunctions.h"
#include "io_types.h"
#include "Ctrl_Top_ADrive.h"
#include "ctrl_top_conveyor.h"
#define NUMREAGENTS 5


#define WATERPUMP_TYPE_BP300 0
#define WATERPUMP_TYPE_C610H 1
/* macro definitions *****************************************************/
/* type definitions ******************************************************/



typedef struct tagSpreaderDisk
{
  uint8_t running;
  uint8_t on;
  uint16_t pwm;
  tSQLTable *Table;
  uint32_t width;           //mm
  uint32_t rpm;
  uint32_t output;
  tValveProp Valve;
  tPaintText ColorWidth;

}tSpreaderDisk;




typedef struct tagWaterPump
{
  uint8_t *on;
  uint8_t  running;
  tValv    Valv;
  uint8_t  Type;
}tWaterPump;

typedef enum eagLRS_SECTIONS
{
  LRS_SECTION_NONE,           //000
  LRS_SECTION_RIGHT,          //001
  LRS_SECTION_CENTER,         //010
  LRS_SECTION_CENTER_RIGHT,   //011
  LRS_SECTION_LEFT,           //100
  LRS_SECTION_LEFT_RIGHt,     //101
  LRS_SECTION_LEFT_CENTER,    //110
  LRS_SECTION_ALL,            //111
  LRS_SECTION_NUM
}eLRS_SECTIONS;

typedef enum eagBoost
{
  BOOST_INVALID,
  BOOST_OFF,
  BOOST_ON

}eBoost;

typedef struct tagLRS
{
  eLRS_SECTIONS  ActiveSection;
  tSQLTable     *SectionTables[LRS_SECTION_NUM];
  uint32_t       Density;
  uint32_t       pwm;
  tValv          DensityValve;
  tValv          LeftValve, RightValve;
  tMaintRuntime *Runtime;
}tLRS;

typedef struct tagTopEquip
{
  uint32_t        Width;
  int32_t         Density;
  uint32_t        Distributor;
  uint8_t         LiquidSensorAvailable;
  uint32_t        speed;
  tSpreaderDisk   Disk;
  tConveyor       Conveyor;
  tWaterPump      Pump;
  tLRS            LRS;
  tAsymerticDrive ADrive;
  tMaintRuntime   RuntimeRTR_Humid;
  tValv           MainValve;
  eBoost          Boost;
}tTopEquip;

/**
 * @brief Activates the boost function. Without it, Boost cannot be set
 *
 * @param Top
 * @param active 1 to enable boost for the system , 0 to disable boost
 */
void TopBoostActivate(tTopEquip *Top, /*tSpreaderDisk *Disk,*/ uint8_t on/*uint8_t active*/);

/**
 * @brief Draws the boost container if boost is active
 *
 * @return int32_t
 */
int32_t TopDrawBoost(void);

void TopBoost_Cycle(tTopEquip *Top);
/**
 * @brief This sets the maximum speed of a top equoipment.
 *
 * @param Top
 * @param MaxSpeed allowed Maximum speed or 0 if no maximum for the equipment exists.
 */


int32_t TopGetSpeed(void );
int32_t TemperatureToCold(int32_t MinTemp);

/**
 * @brief Initialises the Conveyour and Creates/ loads parameter Tables form the SQL-File
 *
 * @param Conveyor
 */
void ConveyorCreate(tConveyor *Conveyor);

/**
 * @brief Inits the Conveyour and sets the Type of valve to contole it.
 *
 * @param Conveyor
 * @param device MCM-Device
 * @param pos   Position of the Output.
 */
void ConveyorInit(tConveyor *Conveyor, uint32_t device, uint32_t pos);

/**
 * @brief Controles the PWM of the conveyour.
 *
 * @param Conveyor
 * @param temp
 */
void ConveyorCycle(tConveyor *Conveyor, int32_t temp);
void ConveyorOn(tConveyor *Conveyor,uint32_t on);
void ConveyorTimer(tConveyor *Conveyor);
double Conveyor_GetShieldFactor(void);

void SpreaderDiskCreate(tSpreaderDisk *Disk);
void SpreaderDiskInit(tSpreaderDisk *Disk, uint32_t Device, uint32_t idx);
void SpreaderDiskCycle( tSpreaderDisk *Disk, uint32_t width);
void SpreaderDiskOn(tSpreaderDisk *Disk, uint32_t value);
void SpreaderDiskTimer(tSpreaderDisk *Disk);

void     MaterialSensor_Cycle(void);
void     SpreadingSensor_Cycle(void);
int32_t  ConveyorGetMaxDensity(tConveyor *Conveyor);


void LRS_Create(tLRS *lrs);
void LRS_Init(tLRS *lrs);
void LRS_Cycle(tLRS *lrs, tWaterPump *Pump);
void LRS_Timer_10ms(tLRS *lrs);


void WaterPump_Init(tWaterPump *Pump, int8_t *PumpOn, uint32_t Type);
void WaterPump_Cycle(tWaterPump *Pump);
void WaterPump_OnOff(tWaterPump *Pump, uint32_t value);
void WaterPump_SetLiter(int32_t ml);
void WaterPump_SetMaxLiter( int32_t ml);
void WaterPump_ResetLiter(void);
int8_t WaterPump_IsOn(const tWaterPump *Pump);


/**
 * @brief This will show the pump Icon, if the pump is active.
 *
 * @param Top
 */
void WaterPump_Draw(tTopEquip  *Top);
void RR300_RR400_Draw(tTopEquip *Top);

void RR300_Init(tTopEquip *Top);
void RR300_Cycle(tTopEquip *Top);

void RR400_Init(tTopEquip *Top);
void RR400_Cycle(tTopEquip *Top);

/*

*/


/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/






/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CTRL_TOP_H
