﻿/****************************************************************************
 *
 * File:         DRAWFUNCTIONS.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "gstovisu.h"
#include "drawfunctions.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

void PaintText_Init(tPaintText *pt,  uint32_t colour_active,  uint32_t colour_inactive,  uint32_t obj)
{
  pt->colour_active   = colour_active;
  pt->colour_inactive = colour_inactive;
  pt->obj             = obj;
  pt->val_old = 0xFF;
}

void PaintText_Cycle(tPaintText *pt,int8_t active)
{
  if(pt->val_old != (uint8_t) active)
  {
    pt->val_old = active;
    if(active)
    {
      SendToVisuObj(pt->obj, GS_TO_VISU_SET_FG_COLOR,pt->colour_active );
    }
    else
    {
      SendToVisuObj(pt->obj, GS_TO_VISU_SET_FG_COLOR,pt->colour_inactive );
    }
  }
}
