﻿/****************************************************************************
 *
 * File:         PARAM.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "gs_Easy_Config.h"

#include "gseDebug.h"
#include "param.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/


tParamData Parameter;

/**
 * @brief This array defines the parameter file. All entries will be saved in a file according to the tag.
 *
 */
tEasyConfigEntry ParamArray[] =
{
  //Type of Entry                   //Variable to save in the C-Code                  //Default value as string   //Tag name
  {.CType =  EASY_CONFIG_TYPE_STRING,.Entry =  Parameter.Engine.Name                  ,.Default = "DEFAULT"       ,.Tag = "Engine.Name" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Engine.Id                    ,.Default = "0x1E"          ,.Tag = "Engine.ID" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Engine.Id2                   ,.Default = "0x30"          ,.Tag = "Engine.ID2" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Engine.PTOatEngine           ,.Default = "1"             ,.Tag = "Engine.PTO" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Engine.RPM_Max               ,.Default = "1500"          ,.Tag = "Engine.RPM_Max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.MCM.temp_max                 ,.Default = "60"            ,.Tag = "MCM.Temp_Max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Joystick.Id                  ,.Default = "0x3"           ,.Tag = "Joystick.ID" },
  {.CType =  EASY_CONFIG_TYPE_STRING,.Entry =  Parameter.Joystick.name                ,.Default = "J1939"         ,.Tag = "Joystick.Type" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilTemp.x1           ,.Default = "400"           ,.Tag = "Sensors.OilTemperature.mA_min" }, // 1mA = 100, 20 mA = 2000;
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilTemp.x2           ,.Default = "2000"          ,.Tag = "Sensors.OilTemperature.mA_max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilTemp.y1           ,.Default = "0"             ,.Tag = "Sensors.OilTemperature.T_min" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilTemp.y2           ,.Default = "200"           ,.Tag = "Sensors.OilTemperature.T_max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilPressure.x1       ,.Default = "0"             ,.Tag = "Sensors.OilPressure.V_min" },  // 1 V = 100, 10 V = 1000;
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilPressure.x2       ,.Default = "1000"          ,.Tag = "Sensors.OilPressure.V_max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilPressure.y1       ,.Default = "0"             ,.Tag = "Sensors.OilPressure.mBar_min" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.OilPressure.y2       ,.Default = "250000"        ,.Tag = "Sensors.OilPressure.mBar_max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.LiquidPresence.x1    ,.Default = "0"             ,.Tag = "Sensors.LiquidSensor.mA_min" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.LiquidPresence.x2    ,.Default = "0"             ,.Tag = "Sensors.LiquidSensor.mA_max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.LiquidPresence.y1    ,.Default = "0"             ,.Tag = "Sensors.LiquidSensor.Liquid_min" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.Sensors.LiquidPresence.y2    ,.Default = "0"             ,.Tag = "Sensors.LiquidSensor.Liquid_max" },
  {.CType =  EASY_CONFIG_TYPE_INT   ,.Entry = &Parameter.AsymDrive.I_Max              ,.Default = "2500"          ,.Tag = "AsymetricDrive.I_Max" },

};
/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


void InitParam(void)
{
  gsEasyConfig_set_debug_out(db_out);
  db_out("Set Debug out for reading easy config");
  //Read parameters.
  if(0 != gsEasyConfig_Read(ParamArray, GS_ARRAYELEMENTS(ParamArray),PATH_PARAM_FLASH))
  {
    //if reading parameters failed, write parameters and use the default value, if a parameter doesn't exist.
    db_out("Write Parameters");
    gsEasyConfig_Write(ParamArray, GS_ARRAYELEMENTS(ParamArray), PATH_PARAM_FLASH);
  }
}

const tParamData * ParameterGet(void)
{
  return &Parameter;
}
