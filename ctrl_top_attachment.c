﻿/****************************************************************************
 *
 * File:         CTRL_TOP_ATTACHMENT.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "RCText.h"
#include "control.h"
#include "io.h"
#include "gseMCM.h"
#include "ctrl_top.h"
#include "gsToVisu.h"
#include "ctrl_light.h"
#include "DrawFunctions.h"
#include "GSe_MsgBox.h"
#include "RCColor.h"
#include "param.h"
#include "Ctrl_Top_ADrive.h"
#include "visu_material.h"

/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/

/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

void Draw_E_State(const tControl *Ctrl)
{
  int32_t t;
  if(Ctrl->CmdNoKey.E_Time > 1500)
  {
    t = 0;
  }
  else t = Ctrl->CmdNoKey.E_Time;
  if(Ctrl->CmdNoKey.E_L)
  {
    SetVar(HDL_E_PRESSTIME, 1500 - t); //changed 2 sec to 1 sec
  }
  else
  {
    SetVar(HDL_E_PRESSTIME, t);
  }
}

void RTR_Command(tControl *Ctrl)
{

  tConveyor *Conv     = &Ctrl->Top.Conveyor;
  tTopEquip *Top      = &Ctrl->Top;
  if((!IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_A]))
   &&(!IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_B]))
   &&(!IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_C])))
  {
    Ctrl->Top.Width  =  Ctrl->Top.Width + IOT_AnaJoy_PressedNew(&Ctrl->Joystick.Y) ;
    const tReagent *Reag= GetActualReagent();
    uint32_t SpreadingDensity_Step = 0;
    if(NULL != Reag)
    {
      SpreadingDensity_Step = Reag->SpreadingDensity_Step;
    }

    Top->Density = Top->Density + IOT_AnaJoy_PressedNew(&Ctrl->Joystick.X) * SpreadingDensity_Step;
    CheckRange((int32_t*)&Top->Density, 0,ConveyorGetMaxDensity(Conv));


    //Check Width of Plough 2-13
    CheckRange((int32_t*) (&(Ctrl->Top.Width)), 2,13);
    //Draw text:
    PaintText_Cycle(&Ctrl->Top.Conveyor.ColorDensity, IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.X));
    PaintText_Cycle(&Ctrl->Top.Disk.ColorWidth      , IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.Y));
  }
  else
  {
    PaintText_Cycle(&Ctrl->Top.Conveyor.ColorDensity, 0);
    PaintText_Cycle(&Ctrl->Top.Disk.ColorWidth, 0);
  }
}



/**
 * @brief  Draws the barpgraph showing the spreader width. min
 *
 * @param Width 0-13 m
 * @return uint32_t
 */
uint32_t RTR_DrawSpreaderBargraph(uint8_t Width)
{
  //Draw spreading width

  const int32_t MiddleXPos   = 110;
  const int32_t Wdith_Max = 220;
  const int32_t MeterMax  =13;
  static int8_t Width_old = -1;
  if(Width_old != Width)
  {
    SetVarIndexed(IDX_SPREADINGDUMMY, 100);

    int32_t width =  Width * Wdith_Max / MeterMax;
    int32_t pos_x = MiddleXPos - (width / 2 );
    SendToVisuObj(OBJ_BARGRAPHSPREADING, GS_TO_VISU_SET_ORIGIN_X, pos_x );
    SendToVisuObj(OBJ_BARGRAPHSPREADING, GS_TO_VISU_SET_WIDTH   ,width );
  }
  return 0;
}





/**
 * @brief Draws the container for the RTR
 *
 * @param Ctrl
 */
void RTR_Draw(const tControl *Ctrl)
{
  ConveyorDraw(&Ctrl->Top.Conveyor);
  RTR_DrawSpreaderBargraph(Ctrl->Top.Width);
  //Spreading Width
  SetVarIndexed(IDX_SPREADERWIDTH, Ctrl->Top.Width);
  SetVarIndexed(IDX_DENSITY, Ctrl->Top.Density);
  Draw_E_State(Ctrl);
  AsymetricDrive_Draw(&Ctrl->Top.ADrive);

}

void Ctrl_Top_LiquidSensorAvailable(tTopEquip *Top, int32_t value)
{
  Top->LiquidSensorAvailable = value;
}


void Ctrl_Top_RTR_Init(tControl *Ctrl)
{
  const tParamData *Param = ParameterGet();

  AsymerticDrive_Init(&Ctrl->Top.ADrive, MCM250_4, 5, MCM250_4,  7);
  AsymetricDrive_CurrentControle_Init(&Ctrl->Top.ADrive, Param->AsymDrive.I_Max);
  ConveyorInit(&Ctrl->Top.Conveyor, MCM250_4,1);
  SpreaderDiskInit(&Ctrl->Top.Disk, MCM250_4,3);
  //Main Valve
  Valv_Init(&Ctrl->Top.MainValve, MCM250_2, 3,-1,-1);
  //Show LiquidSensor:
  Ctrl_Top_LiquidSensorAvailable(&Ctrl->Top,1);

  //Read
  Ctrl->Top.Density = GetVarIndexed(IDX_DENSITY);
  Ctrl->Top.Width   = GetVarIndex(IDX_SPREADERWIDTH);

  PaintText_Init(&Ctrl->Top.Conveyor.ColorDensity,
                  RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR) ,RCCOLOR_NAME_SELECT),
                  RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR) ,RCCOLOR_NAME_FRONT),
                  OBJ_RTR_DENSITY );
  PaintText_Init(&Ctrl->Top.Disk.ColorWidth,
                  RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR) ,RCCOLOR_NAME_SELECT),
                  RCColorGetColor(GetVarIndexed(IDX_SYS_COLOR) ,RCCOLOR_NAME_FRONT),
                  OBJ_RTR_WIDTH );
  TopBoostActivate(&Ctrl->Top,1);
}


void Ctrl_top_RTR_KH_Init(tControl *Ctrl)
{
  Ctrl_Top_RTR_Init(Ctrl);
  Ctrl_Top_LiquidSensorAvailable(&Ctrl->Top,0);
  WaterPump_Draw(&Ctrl->Top);

}

void Ctrl_top_RTR_SH_Init(tControl *Ctrl)
{
  Ctrl_Top_RTR_Init(Ctrl);
  Ctrl_Top_LiquidSensorAvailable(&Ctrl->Top,1);
  WaterPump_Draw(&Ctrl->Top);
}


void Ctrl_top_RTR_KH_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  RTR_Draw(Ctrl);
  Light_Draw(&Ctrl->Light);
}

//
void Ctrl_top_RTR_SH_Timer_10ms(tControl *Ctrl)
{
  AsymerticDrive_Timer_10ms(&Ctrl->Top.ADrive);
  static int32_t count = 0;
  count++;
  if(0 == count%10)
  {
    ConveyorTimer(&Ctrl->Top.Conveyor);
    SpreaderDiskTimer(&Ctrl->Top.Disk);
    Valv_Timer_100ms(&Ctrl->Top.MainValve);
  }
}


void Ctrl_top_RTR_KH_Timer_10ms(tControl *Ctrl)
{

  static int32_t count = 0;
  count++;
  if(0 == count % 10)
  {
    ConveyorTimer(&Ctrl->Top.Conveyor);
    SpreaderDiskTimer(&Ctrl->Top.Disk);
    Valv_Timer_100ms(&Ctrl->Top.MainValve);
  }
}
//Cycle function for standard RTR
void Ctrl_top_RTR_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  RTR_Draw(Ctrl);
  //switch on components of RTR on / off
  if(Ctrl->CmdNoKey.E_L)
  {
    //check if it is to gold
    if(TemperatureToCold(-30))
    {
      //swich off
      Ctrl->CmdNoKey.E_L = 0;
      MsgBoxOK_RCText(RCTEXT_T_WARNING, RCTEXT_T_TOCOLDRTR, NULL, NULL);
    }
    else {
    Ctrl->Top.Disk.on      = 1;
    Ctrl->Top.Conveyor.on  = 1;
    AsymeticDriveSwitchOn(&Ctrl->Top.ADrive);
    ConveyorOn(&Ctrl->Top.Conveyor, 1);
    SpreaderDiskOn(&Ctrl->Top.Disk, 1);
    //ValvProp_25_50_75_Set(&Ctrl->Top.MainValve.Valve.PVEH, -900);
    ValvPropPVEP_SetLiter(&Ctrl->Top.MainValve.Valve.PVEH, 60000);//AK TESTED ok used to be 40000
    }
  }
  else
  {
    Ctrl->Top.Disk.on      = 0;
    Ctrl->Top.Conveyor.on  = 0;
    AsymeticDriveSwitchOff(&Ctrl->Top.ADrive);
    ConveyorOn(&Ctrl->Top.Conveyor, 0);
    SpreaderDiskOn(&Ctrl->Top.Disk, 0);
    ValvProp_25_50_75_Set(&Ctrl->Top.MainValve.Valve.PVEH, 0);//ValvPropPVEP_SetLiter(&Ctrl->Top.MainValve.Valve.PVEH, 0);
  }

  TopBoost_Cycle(&Ctrl->Top);
  //Changing Density
  RTR_Command(Ctrl);
  ConveyorCycle(&Ctrl->Top.Conveyor, IOT_NormAI(&Ctrl->Sensors.AI.OilTemp));
  SpreaderDiskCycle(&Ctrl->Top.Disk, Ctrl->Top.Width);
  AsymetricDrive_Command(&Ctrl->Top.ADrive, Ctrl->CmdNoKey.Z_Change);

  MaterialSensor_Cycle();
  SpreadingSensor_Cycle();

  TopDrawBoost();
  Light_Draw(&Ctrl->Light);


  SetVar(HDL_TESTVAR_0, Ctrl->Top.Conveyor.pwm);
  SetVar(HDL_TESTVAR_1, Ctrl->Top.Disk.pwm);
  SetVar(HDL_TESTVAR_2, Ctrl->Top.ADrive.PosActual);
  //if visu switches to the view of the vehicle

}



/**************************
 * LRS_CK_BP300 & LRS_CK
 * No difference in controle known till now
 * ***********************/

void Ctrl_top_LRS_CK_Init(tControl *Ctrl)
{
  //Pump switch on/off by long press D
  WaterPump_Init(&Ctrl->Top.Pump, &Ctrl->CmdNoKey.E_L, WATERPUMP_TYPE_BP300);
  WaterPump_SetMaxLiter( 40000);
  RR400_Init(&Ctrl->Top);
  LRS_Init(&Ctrl->Top.LRS);
}

void Ctrl_top_LRS_CK_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  Ctrl->Top.LRS.Density = Ctrl->Top.LRS.Density + Ctrl->CmdNoKey.X_Change * 4;
  CheckRange((int32_t*) &Ctrl->Top.LRS.Density, 20,70);
  SetVar(HDL_DENSITY, Ctrl->Top.LRS.Density);

  LRS_Cycle(&Ctrl->Top.LRS, &Ctrl->Top.Pump);

  //WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);

  RR400_Cycle(&Ctrl->Top);
  RR300_RR400_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);


}

void Ctrl_top_LRS_CK_Timer_10ms(tControl *Ctrl)
{
  LRS_Timer_10ms(&Ctrl->Top.LRS);

}




/**************************
 * E2000_BP300
 * ***********************/

void Ctrl_top_E2000_BP300_Init(tControl *Ctrl)
{
  WaterPump_Init(&Ctrl->Top.Pump, &Ctrl->CmdNoKey.E_L, WATERPUMP_TYPE_BP300);
  WaterPump_SetMaxLiter( 65000);
  RR300_Init(&Ctrl->Top);
}
void Ctrl_top_E2000_BP300_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);
}



/**************************
 * E2000_C610H
 * ***********************/

void Ctrl_top_E2000_C610H_Init(tControl *Ctrl)
{
  WaterPump_Init(&Ctrl->Top.Pump,  &Ctrl->CmdNoKey.E_L,WATERPUMP_TYPE_C610H);
  WaterPump_SetMaxLiter( 65000);
}

void Ctrl_top_E2000_C610H_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);
}



/**************************
 * E2000_BP300_C610H
 * ***********************/

void Ctrl_top_E2000_BP300_C610H_Init(tControl *Ctrl)
{
  Ctrl_top_E2000_C610H_Init(Ctrl);
  Ctrl_top_E2000_BP300_Init(Ctrl);
}
void Ctrl_top_E2000_BP300_C610H_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);
}

/**************************
 * E2000_C610H_RR300
 * ***********************/

void Ctrl_top_E2000_C610H_RR300_Init(tControl *Ctrl)
{
  //LRS_Init(&Ctrl->Top.LRS);
  RR300_Init(&Ctrl->Top);
  WaterPump_Init(&Ctrl->Top.Pump , &Ctrl->CmdNoKey.E_L,WATERPUMP_TYPE_C610H);
  WaterPump_SetMaxLiter( 65000);
}

void Ctrl_top_E2000_C610H_RR300_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);
  RR300_Cycle(&Ctrl->Top);
  RR300_RR400_Draw(&Ctrl->Top);
}

/**************************
 * E2000_C610H_RR400
 * ***********************/

void Ctrl_top_E2000_C610H_RR400_Init(tControl *Ctrl)
{
  RR400_Init(&Ctrl->Top);
  WaterPump_Init(&Ctrl->Top.Pump , &Ctrl->CmdNoKey.E_L,WATERPUMP_TYPE_C610H);
  WaterPump_SetMaxLiter(65000);
}

void Ctrl_top_E2000_C610H_RR400_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  RR400_Cycle(&Ctrl->Top);
  RR300_RR400_Draw(&Ctrl->Top);
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);
}

/**************************
 * E2000_BP300_RR400
 * ***********************/

void Ctrl_top_E2000_BP300_RR400_Init(tControl *Ctrl)
{
  //LRS_Init(&Ctrl->Top.LRS);
  RR400_Init(&Ctrl->Top);
  WaterPump_Init(&Ctrl->Top.Pump , &Ctrl->CmdNoKey.E_L,WATERPUMP_TYPE_BP300);
  WaterPump_SetMaxLiter(65000);
}

void Ctrl_top_E2000_BP300_RR400_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  RR400_Cycle(&Ctrl->Top);
  RR300_RR400_Draw(&Ctrl->Top);
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);
}

/**************************
 * E2000_BP300_RR300
 * ***********************/

void Ctrl_top_E2000_BP300_RR300_Init(tControl *Ctrl)
{
  //LRS_Init(&Ctrl->Top.LRS);
  RR300_Init(&Ctrl->Top);
  WaterPump_Init(&Ctrl->Top.Pump , &Ctrl->CmdNoKey.E_L,WATERPUMP_TYPE_BP300);
  WaterPump_SetMaxLiter( 65000);
}

void Ctrl_top_E2000_BP300_RR300_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  //LRS_Cycle(&Ctrl->Top.LRS,&Ctrl->Top.Pump_BP300);
  WaterPump_Cycle(&Ctrl->Top.Pump);
  WaterPump_Draw(&Ctrl->Top);
  RR300_Cycle(&Ctrl->Top);
  RR300_RR400_Draw(&Ctrl->Top);
  Light_Draw(&Ctrl->Light);
}


void Ctrl_top_CF_Cycle(tControl *Ctrl, uint32_t evtc, tUserCEvt *evtv)
{
  Light_Draw(&Ctrl->Light);
}
