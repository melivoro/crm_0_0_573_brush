﻿/************************************************************************
 *
 * File:         ERRORLIST.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef ERRORLIST_H
#define ERRORLIST_H

/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
typedef enum eagErrorID
{
    ERRID_MCM250_0_OFFLINE = 0x100,
    ERRID_MCM250_1_OFFLINE,
    ERRID_MCM250_2_OFFLINE,
    ERRID_MCM250_3_OFFLINE,
    ERRID_MCM250_4_OFFLINE,
    ERRID_K2_OFFLINE,
    ERRID_JOYSTICK_OFFLINE,
    ERRID_ENGINE_OFFLINE,
    ERRID_BNSO_OFFLINE,

    ERRID_MCM250_0_TEMP = 0x110,
    ERRID_MCM250_1_TEMP,
    ERRID_MCM250_2_TEMP,
    ERRID_MCM250_3_TEMP,
    ERRID_MCM250_4_TEMP,
    ERRID_K2_TEMP,
    ERRID_D3510_TEMP,

    ERRID_MAINTENANCE_BRUSH_FRONT,
    ERRID_MAINTENANCE_BRUSH_INTERAXIS,
    ERRID_MAINTENANCE_Brush_SIDE,
    ERRID_MAINTENANCE_Brush_REAR = 0x120, 
    ERRID_MAINTENANCE_PTO,
    ERRID_MAINTENANCE_OILPUMP_LOAD,
    ERRID_MAINTENANCE_OILPUMP_UNLOAD,
    ERRID_MAINTENANCE_RTR,
    ERRID_MAINTENANCE_RTR_HUMID,
    ERRID_MAINTENANCE_LRS,
    ERRID_MAINTENANCE_ANNUAL,

    ERRID_CRITICAL_OIL_LEVEL,
    ERRID_NO_OIL_SENSOR,
    ERRID_DECREASINGOILEVEL = 0x130,
    ERRID_HYDRAULICOILFILTER,
    ERRID_CRITICAL_LIQUID_LEVEL_E2000,
    ERRID_NO_REAGENT,
    ERRID_OVERLOAD,
    ERRID_HIGH_VOLTAGE,
    ERRID_LOW_VOLTAGE,
    ERRID_OVERSPEED,
    ERRID_NO_MATERIALSENSOR,
    ERRID_NO_WATERSENSOR,
    ERRID_HYDRAULIC_SYSTEM_NOT_AVAILABLE = 0x140,
    ERRID_ANNUAL_MAINTENANCE,
    ERRID_HYDRAULICVALVETIPPINGERROR_0,
    ERRID_HYDRAULICVALVETIPPINGERROR_1,
    ERRID_HYDRAULICVALVETIPPINGERROR_2,
    ERRID_HYDRAULICVALVETIPPINGERROR_3,

    ERRID_CONVEYOR_NO_ROTATION,
    ERRID_MAX_ENGINE_RPM,

}eErrorID;



/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef ERRORLIST_H
