﻿/************************************************************************
 *
 * File:         J1939.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef J1939_H
#define J1939_H
#include "control.h"
/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/
/**
 * @brief first PGN for sending data to the BNSO
 *
 */
typedef struct tagPGN_FF01
{
    unsigned int FrontAttach_FloatingMode      :2;     // 0 off , 1 on
    unsigned int FrontAttach_VerticalPos       :2;     // 1 raised, 2 pressed
    unsigned int FrontAttach_HorizontalPos     :2;     // 1 left, 2 right
    unsigned int FrontAttach_Rotation          :2;     // 0 no rot, 1 rot, not available
    unsigned int FrontAttach_BladeType         :2;     // 1 rubber, 2 steel
    unsigned int Interaxle_FloatingMode        :2;     // 0 on , 1 off
    unsigned int Interaxle_VerticalPos         :2;     // 1 raised, 2 pressed
    unsigned int Interaxle_HorizontalPos       :2;     // 1 left, 2 right
    unsigned int Interaxle_Rotation            :2;     // 0 no rot, 1 rot, not available
    unsigned int Interaxle_BladeType           :2;     // 1 rubber, 2 steel
    unsigned int SideAttach_FloatingMode       :2;     // 0 on , 1 off
    unsigned int SideAttach_VerticalPos        :2;     // 1 raised, 2 pressed
    unsigned int SideAttach_Tilt               :2;     // 1 raised, 2 pressed
    unsigned int SideAttach_HorizontalPos      :2;     // 1 left, 2 right
    unsigned int SideAttach_Rotation           :2;     // 0 no rot, 1 rot, not available
    unsigned int SideAttach_BladeType          :2;     // 1 rubber, 2 steel
    unsigned int ReagentFeedRate               :8;        // 0 to 8000 rpm
    unsigned int SprayDensity                  :8;        // 1 Bit = 10g/mÃ‚Â²
    unsigned int SprayWidth                    :8;        // 1 Bit = 1m
    unsigned int PresenceOfSprinklers          :2;     // 0 No spraying, 1 spraying, 2 no sensor
    unsigned int PresenceOfFluidHuidifier      :2;     // 0 No Fluid, 1 Fluid, 2 no sensor
    unsigned int PositionAsymmetryDrive        :4;     // 0 extreme left, 1 left, 2 slacker, 3 right, 4 extreme right
}tPGN_FF01;

/**
 *  @brief second PGN for sending data to the BNSO
 *
 */
typedef struct tagPGN_FF02
{
    unsigned int  HydSysTemp  :8;
    unsigned int  HydSysPres  :16;
    unsigned int  HydSysOilLev:8;
    unsigned int OilTemp      :2;
    unsigned int res_0        :6;
    unsigned int res_1        :24;

}tPGN_FF02;



/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/
tPGN_FF01 *J1939_GetPGN_FF01(void);
tPGN_FF02 *J1939_GetPGN_FF02(void);


void J1939_SendPGN(uint32_t id, void * data, size_t size_data);
/* global function prototypes ********************************************/

/**
 * @brief Initalizes the J1939 on CAN for the Engine and the BNSO
 *
 * @param CAN 0 for CAN0, 1 for CAN1
 * @param Control
 */
void J1939_Init(uint32_t CAN, tControl * Control);

/**
 * @brief Call this in the UserCCylce
 *
 * @param Ctrl
 */
void J1939_Cycle(tEngine * Ctrl);


void J1939_Timer(tControl * Ctrl);


/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef J1939_H
