﻿/************************************************************************
 *
 * File:         gseConDir.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/
#ifndef GSECONDIR_H
#define GSECONDIR_H

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */



#include "UserCapi.h"
#include "gseConsole.h"
/**
 * @brief Adds new commands to a console
 * Command Description               Paramerter
 * ------+-------------------------+-----------------------
 * "dir" | shows files in a folder | none
 * "cd"  | opens new folder path   | name of folder to be opened
 * "rm"  | removes file or folder  | filename or folder to be deleted
 *
 * @param Hdl Handle of the console.
 * @return int32_t acutal only 0
 */
int32_t gseConDirAdd(uint32_t Hdl);




#endif

