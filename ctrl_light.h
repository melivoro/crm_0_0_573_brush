﻿/************************************************************************
 *
 * File:         CTRL_LIGHT.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/

/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */
#ifndef CTRL_LIGHT_H
#define CTRL_LIGHT_H


#include "io_types.h"

/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/
typedef struct tagLight
{
  //Structurer of Light Switches
  struct
  {
    tIOT_Button PloughHead;
    tIOT_Button WorkSide;
    tIOT_Button FrontBeaconCab;
    tIOT_Button RearBeaconTopEquip;
    tIOT_Button LeftRoadSign;
    tIOT_Button RightRoadSign;
    tIOT_Button Work;
    tIOT_Button All;
  }Key;
  //Structure of different Light types
  struct
  {
    uint8_t PloughHead[2];
    uint8_t FrontBeaconCab;
    uint8_t RTR_RearBeacon;
    uint8_t LRS_RearBeacon;
    uint8_t RTR_LeftRoadSign;
    uint8_t LRS_LeftRoadSign;
    uint8_t RTR_RightRoadSign;
    uint8_t LRS_RightRoadSign;
    uint8_t WorkSide;
    uint8_t RTR_WorkRear;
    uint8_t LRS_WorkRear;
  }On;
}tLight;

void Ctrl_Light(tLight *Light,uint32_t evtc, tUserCEvt *evtv);
void Light_Draw(const tLight *Light);

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/



/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef CTRL_LIGHT_H
