﻿/****************************************************************************
 *
 * File:         SQL_TABLE.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "sqlite3.h"
#include "sql_table.h"
#include "gseDebug.h"
#include "gsToVisu.h"
#include "gseList.h"
#include "visu.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/
const char *path = "/gs/data/sqltables.sql";

typedef int (*SQL_Exec_cb)(void*,int,char**,char**);

sqlite3 *db;
int64_t LastInsertedRowId;

tVisu_SQL_Table VisuTable;

tGSList TableList = {NULL, NULL,NULL,0};
/*
tSQLTable Table_Test_1 =
{
  .TableName = "Test1" ,
  .ColumnNames = {"Spalte 1", "Spalte 2", "Spalte 3", "Spalte 4"},
  .Cell = {
    {0  , 10,  20, 30},
    {1  , 11,  21, 31},
    {2  , 12,  22, 32},
    {3  , 13,  23, 33},
    {4  , 14,  24, 34},
    {5  , 15,  25, 35},
    {6  , 16,  26, 36},
    {7  , 17,  27, 37},
    {8  , 18,  28, 38},
    {9  , 19,  29, 39},
    {10  , 20,  30, 40},
    }
};

tSQLTable Table_Test_2 =
{
  .TableName = "Test2" ,
  .ColumnNames = {"Spalte 1", "Spalte 2", "Spalte 3", "Spalte 4"},
  .Cell = {
    {0  , 10,  20, 30},
    {1  , 11,  21, 31},
    {2  , 12,  22, 32},
    {3  , 13,  23, 33},
    {4  , 14,  24, 34},
    {5  , 15,  25, 35},
    {6  , 16,  26, 36},
    {7  , 17,  27, 37},
    {8  , 18,  28, 38},
    {9  , 19,  29, 39},
    {10  , 20,  30, 40},
  }
};

*/

/**
 * @brief This function will interpolate a value between two given values in a table.
 * the first and the last line of the column y will give the maximum and minimum return value.
 *
 * @param Table Pointer to the table.
 * @param ci_x column of the x value
 * @param ci_y column of the y value
 * @param x
 * @return int32_t y = mx + b;
 */
int32_t TableGetLSEValue(tSQLTable *Table,uint32_t ci_x,uint32_t ci_y, int32_t x)
{
  //Max and Min:
  if(NULL == Table)
  {
    return -1;
  }
  if(x <= Table->Cell[0][ci_x])
    return Table->Cell[0][ci_y];

  if(x > Table->Cell[10][ci_x])
    return Table->Cell[10][ci_y];

  for(int32_t i = 1; i <= 10;i++)
  {
    if(x <= Table->Cell[i][ci_x])
    {
      if(Table->Cell[i][ci_x] ==  Table->Cell[i-1][ci_x])//would be division through 0
        return -1;
      double m = (double)(Table->Cell[i][ci_y]- (Table->Cell[i-1][ci_y])) /
                 (double)(Table->Cell[i][ci_x]- (Table->Cell[i-1][ci_x]));

      double b = (double)(Table->Cell[i][ci_y])  - m * (double)Table->Cell[i][ci_x];
      return m * x + b;
    }
  }
  return 0;
}

int32_t SQL_Exec(tSQLTable *Table, void *arg, const char *command, SQL_Exec_cb cb)
{
  char * sqErrMsg = NULL;
  if(NULL != db)
  {
    int32_t rc = sqlite3_exec(db, command, cb, arg, &sqErrMsg);
      if(rc != SQLITE_OK)
      {
        db_out("sqlite3_exec: %s\r\n", sqErrMsg);
        db_out("Command: %s\r\n", command);
        sqlite3_free(sqErrMsg);
        return -1;
      }
      else
      {
        LastInsertedRowId = sqlite3_last_insert_rowid( db);
      }
  }
  return 0;
}

/**
 * @brief Callback function to read from SQL-File
 *
 * @param arg Pointer to the Table
 * @param argc Number of Entries
 * @param argv Array of Entries in Textform
 * @param ColName Name of the column
 * @return int32_t
 */
int32_t SQL_Table_Read_cb(void*arg,int argc,char**argv,char**ColName)
{
  tSQLTable *Table = arg;
  if(argc  != 5)
  {
    db_out("SQL_Table_Read_cb gets wrong number of arguments\r\n");
    return -1;
  }
  if(Table->idx >= 11)
    return -2;
  Table->Cell[Table->idx][0] = strtol(argv[1], NULL, 0);
  Table->Cell[Table->idx][1] = strtol(argv[2], NULL, 0);
  Table->Cell[Table->idx][2] = strtol(argv[3], NULL, 0);
  Table->Cell[Table->idx][3] = strtol(argv[4], NULL, 0);
  Table->idx++;
  return 0;
}

//Opens SQL-File
uint32_t SQL_Table_Open(void)
{
  if(db != NULL)
    return 0;
  if( SQLITE_OK != sqlite3_open_v2(path,&db, SQLITE_OPEN_READWRITE, NULL))
  {
    db_out(sqlite3_errmsg(db));
    db_out("SQL_Open: Error opening SQL-Database: %s\r\n", sqlite3_errmsg(db));
    sqlite3_close(db);
    db = NULL;

    db_out("SQL_Open: Create new Databas.\r\n");

    int32_t err = sqlite3_open_v2(path, &db,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
    if(SQLITE_OK != err)
    {
      db_out("Error SQL_Create: %s\r\n",sqlite3_errmsg( db));
      return err;
    }
  }
  return 0;
}


/**
 * @brief Closes SQL-file
 *
 */
void SQL_Close(void)
{
  if(NULL != db)
  {
    sqlite3_close(db);
    db = NULL;
  }
}

uint32_t SQL_Tabel_ReadFromSQL(tSQLTable *Table)
{
  char cmd[128];
  snprintf(cmd, sizeof(cmd), "SELECT * FROM \"%s\"", Table->TableName);
  Table->idx = 0;
   SQL_Exec(Table,Table, cmd, SQL_Table_Read_cb);
  return 0;
}

/**
 * @brief This Function will check if the Table already exists in the SQL-Table.
 *
 * @param Table
 * @return tSQLTable*
 */
tSQLTable * SQL_Tabel_Init(tSQLTable *Table)
{
  if(!path)
  {
    return NULL;
  }
  //Create File
  SQL_Table_Open();

  //Check if table exists, else create Table
  char cmd[256];

  snprintf(cmd, sizeof(cmd), "CREATE TABLE IF NOT EXISTS \"%s\" (\"ID\" INTEGER PRIMARY KEY  NOT NULL", Table->TableName);
  for(int32_t i = 0; i < 4; i++)
  {
    if(Table->ColumnNames[i])
    {
      char string[48];
      snprintf(string, sizeof(string), ",\"%s\" INTEGER DEFAULT (0) ", Table->ColumnNames[i]);
      strncat(cmd, string, sizeof(cmd)-strlen(cmd));
    }
    else
    {
      char string[48];
      snprintf(string, sizeof(string), ",\"EMPTY_%d\" INTEGER DEFAULT (0) ", i);
      strncat(cmd, string, sizeof(cmd)-strlen(cmd));
    }

  }
  strncat(cmd, ")", sizeof(cmd)-strlen(cmd));
  SQL_Exec(Table, NULL,cmd, NULL );

  //Check if there are entrys
  sqlite3_stmt * sqlStmt;
  snprintf(cmd, sizeof(cmd), "SELECT * FROM \"%s\" WHERE ID = 1", Table->TableName);
  if(SQLITE_OK == sqlite3_prepare(db, cmd, -1, &sqlStmt, NULL))
  {
    //check if an entry has been found.
    if(SQLITE_DONE == sqlite3_step(sqlStmt))//NO Entries found
    {
      int32_t err = sqlite3_finalize(sqlStmt);
      if(err)
      {
        db_out(sqlite3_errmsg(db));
      }
      //Enter missing Entries
      db_out("Add Entries into empty table %s\r\n", Table->TableName);
      for(int32_t i = 0; i < 11 ; i++)
      {
        snprintf(cmd, sizeof(cmd), "INSERT INTO \"%s\" (\"%s\", \"%s\", \"%s\", \"%s\") VALUES(%d,%d,%d,%d)"
        ,Table->TableName
        ,Table->ColumnNames[0]
        ,Table->ColumnNames[1]
        ,Table->ColumnNames[2]
        ,Table->ColumnNames[3]
        ,Table->Cell[i][0]
        ,Table->Cell[i][1]
        ,Table->Cell[i][2]
        ,Table->Cell[i][3]
        );
        SQL_Exec(Table, NULL,cmd, NULL );
      }
    }
    else
    {
      int32_t err = sqlite3_finalize(sqlStmt);
      if(err)
      {
        db_out(sqlite3_errmsg(db));
      }
      SQL_Tabel_ReadFromSQL(Table);
    }
  }
  tSQLTable *TableReturn = GSList_AddData(&TableList, Table, sizeof(tSQLTable));
  //SQL_Close();
  return TableReturn;
}

/**
 * @brief This Function adds a Line to the Table. The Table has 4 rows.
 *
 * @param TableView
 * @param line Number of the line.
 * @param Obj_0 Object ID of the table entry, to make it visible
 * @param Hdl_0 Hdl of the Variable, to show the Table Value
 * @param Obj_1 Object ID of the table entry, to make it visible
 * @param Hdl_1 Hdl of the Variable, to show the Table Value
 * @param Obj_2 Object ID of the table entry, to make it visible
 * @param Hdl_2 Hdl of the Variable, to show the Table Value
 * @param Obj_3 Object ID of the table entry, to make it visible
 * @param Hdl_3 Hdl of the Variable, to show the Table Value
 */
void SetTableView_Line(tVisu_SQL_Table * TableView,  uint32_t line ,uint32_t Obj_0, uint32_t Hdl_0, uint32_t Obj_1, uint32_t Hdl_1, uint32_t Obj_2, uint32_t Hdl_2, uint32_t Obj_3, uint32_t Hdl_3 )
{
  TableView->ObjId[line][0] = Obj_0;
  TableView->Hdl[line][0]   = Hdl_0;
  TableView->ObjId[line][1] = Obj_1;
  TableView->Hdl[line][1]   = Hdl_1;
  TableView->ObjId[line][2] = Obj_2;
  TableView->Hdl[line][2]   = Hdl_2;
  TableView->ObjId[line][3] = Obj_3;
  TableView->Hdl[line][3]   = Hdl_3;
}








/**
 * @brief Writes the Values of a table-structure into a SQL-Table
 *
 * @param Table
 * @return uint32_t
 */
uint32_t SQL_Table_WriteToSQL(tSQLTable *Table)
{
  for(int32_t i = 0; i < 11; i++)
  {
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "Update \"%s\" SET \"%s\" = %d,\"%s\" = %d, \"%s\" = %d, \"%s\" = %d WHERE ID = %d"
    , Table->TableName
    , Table->ColumnNames[0], Table->Cell[i][0]
    , Table->ColumnNames[1], Table->Cell[i][1]
    , Table->ColumnNames[2], Table->Cell[i][2]
    , Table->ColumnNames[3], Table->Cell[i][3]
    , i + 1
    );
    SQL_Exec(Table,NULL, cmd, NULL);
  }
  return 0;
}

int32_t TableToVisu(tSQLTable *Table)
{
  SetVisObjData(OBJ_TABLE, Table->TableName, strlen(Table->TableName)+1);
  for(int32_t i = 0; i< 4 ; i++)
  {

    //don't Draw Column, if the Word "EMPTY" is part of the columnname.
    if(NULL == strstr(Table->ColumnNames[i],"EMPTY"))
    {

      if(Table->ColumnNames[i])
      {
        SetVisObjData(OBJ_DESCRIPTION_0 + i, Table->ColumnNames[i], strlen(Table->ColumnNames[i])+1);
      }
      for(int32_t j = 0; j < 11; j++)
      {
        SendToVisuObj(VisuTable.ObjId[j][i], GS_TO_VISU_SET_ATTR_ALL, ATTR_VISIBLE | ATTR_NOMINAL | ATTR_SMOOTH);
        SetVar(VisuTable.Hdl[j][i], Table->Cell[j][i]);
      }
    }
    else
    {
      //write empty string into column name.
      if(Table->ColumnNames[i])
      {
        SetVisObjData(OBJ_DESCRIPTION_0 + i, " ", strlen(" ")+1);
      }
      //make Table Entries of this row invisivble
      for(int32_t j = 0; j < 11; j++)
      {
        SendToVisuObj(VisuTable.ObjId[j][i], GS_TO_VISU_SET_ATTR_ALL, 0);
      }
    }
  }
  return 0;
}


/**
 * @brief Reads the Values from the visu and writes them into the table structure.
 *
 * @param Table Values from visu will be written into this table.
 * @return int32_t
 */
int32_t TableFromVisu(tSQLTable *Table)
{
  //for each column
  for(int32_t i = 0; i< 4 ; i++)
  {
    if(NULL == strstr(Table->ColumnNames[i],"EMPTY"))
    {
      //each cell in the column
      for(int32_t j = 0; j < 11; j++)
      {
        Table->Cell[j][i] = GetVar(VisuTable.Hdl[j][i]);
      }
    }
  }
  return 0;
}




void Visu_SQL_Table_Init(tVisuData const * Data)
{
  SetTableView_Line(&VisuTable, 0 , OBJ_COL_1_0, HDL_SQL_ZELL_00, OBJ_COL_1_10, HDL_SQL_ZELL_10,OBJ_COL_1_20, HDL_SQL_ZELL_20, OBJ_COL_1_30, HDL_SQL_ZELL_30 );
  SetTableView_Line(&VisuTable, 1 , OBJ_COL_1_1, HDL_SQL_ZELL_01, OBJ_COL_1_11, HDL_SQL_ZELL_11,OBJ_COL_1_21, HDL_SQL_ZELL_21, OBJ_COL_1_31, HDL_SQL_ZELL_31 );
  SetTableView_Line(&VisuTable, 2 , OBJ_COL_1_2, HDL_SQL_ZELL_02, OBJ_COL_1_12, HDL_SQL_ZELL_12,OBJ_COL_1_22, HDL_SQL_ZELL_22, OBJ_COL_1_32, HDL_SQL_ZELL_32 );
  SetTableView_Line(&VisuTable, 3 , OBJ_COL_1_3, HDL_SQL_ZELL_03, OBJ_COL_1_13, HDL_SQL_ZELL_13,OBJ_COL_1_23, HDL_SQL_ZELL_23, OBJ_COL_1_33, HDL_SQL_ZELL_33 );
  SetTableView_Line(&VisuTable, 4 , OBJ_COL_1_4, HDL_SQL_ZELL_04, OBJ_COL_1_14, HDL_SQL_ZELL_14,OBJ_COL_1_24, HDL_SQL_ZELL_24, OBJ_COL_1_34, HDL_SQL_ZELL_34 );
  SetTableView_Line(&VisuTable, 5 , OBJ_COL_1_5, HDL_SQL_ZELL_05, OBJ_COL_1_15, HDL_SQL_ZELL_15,OBJ_COL_1_25, HDL_SQL_ZELL_25, OBJ_COL_1_35, HDL_SQL_ZELL_35 );
  SetTableView_Line(&VisuTable, 6 , OBJ_COL_1_6, HDL_SQL_ZELL_06, OBJ_COL_1_16, HDL_SQL_ZELL_16,OBJ_COL_1_26, HDL_SQL_ZELL_26, OBJ_COL_1_36, HDL_SQL_ZELL_36 );
  SetTableView_Line(&VisuTable, 7 , OBJ_COL_1_7, HDL_SQL_ZELL_07, OBJ_COL_1_17, HDL_SQL_ZELL_17,OBJ_COL_1_27, HDL_SQL_ZELL_27, OBJ_COL_1_37, HDL_SQL_ZELL_37 );
  SetTableView_Line(&VisuTable, 8 , OBJ_COL_1_8, HDL_SQL_ZELL_08, OBJ_COL_1_18, HDL_SQL_ZELL_18,OBJ_COL_1_28, HDL_SQL_ZELL_28, OBJ_COL_1_38, HDL_SQL_ZELL_38 );
  SetTableView_Line(&VisuTable, 9 , OBJ_COL_1_9, HDL_SQL_ZELL_09, OBJ_COL_1_19, HDL_SQL_ZELL_19,OBJ_COL_1_29, HDL_SQL_ZELL_29, OBJ_COL_1_39, HDL_SQL_ZELL_39 );
  SetTableView_Line(&VisuTable, 10, OBJ_COL_1_A, HDL_SQL_ZELL_0A, OBJ_COL_1_1A, HDL_SQL_ZELL_1A,OBJ_COL_1_2A, HDL_SQL_ZELL_2A, OBJ_COL_1_3A, HDL_SQL_ZELL_3A );
  //SQL_Tabel_Init(&Table_Test_1);
  //SQL_Tabel_Init(&Table_Test_2);

}

tSQLTable * ActualTable;
void Visu_SQL_Table_Open(tVisuData const *Data)
{
  PrioMaskOn(MSK_SQL_LIST);
  ActualTable =  GSList_GetFirstData(&TableList);
  SQL_Table_Open();

  if(NULL != ActualTable)
  {
    SQL_Tabel_ReadFromSQL(ActualTable);
    TableToVisu(ActualTable);
  }
}

void Visu_SQL_Table_Close(tVisuData const *Data)
{
  SQL_Close();
}




void Table_CalibrateLinear(tSQLTable *Table, uint32_t column)
{
  //y = mx + b;
  //m = (y1-y2)/(x1-x2)
  //b = y1-mx1
  double m = (double)(Table->Cell[0][column]- (Table->Cell[10][column])) / -10.0;
  double b = (double)(Table->Cell[0][column]);
  for(int32_t i = 1; i < 11; i++)
  {
    Table->Cell[i][column] = (double)(m * (double) i + b);
  }
}


void Visu_SQL_Table_Cycle(const tVisuData *Data, uint32_t evtc, tUserCEvt *evtv)
{
  //previous table
  if(IsKeyPressedNew(1))
  {
    tSQLTable *  NewTable = GSList_GetPrevData(&TableList);
    if(NULL == NewTable)
    {
      NewTable = GSList_GetLastData(&TableList);
    }
    ActualTable = NewTable;
    SQL_Tabel_ReadFromSQL(ActualTable);
    TableToVisu(ActualTable);
  }
  //NextTable
  if(IsKeyPressedNew(2))
  {
    tSQLTable *  NewTable = GSList_GetNextData(&TableList);
    if(NULL == NewTable)
    {
      NewTable = GSList_GetFirstData(&TableList);
    }
    ActualTable = NewTable;
    SQL_Tabel_ReadFromSQL(ActualTable);
    TableToVisu(ActualTable);
  }
  //Save
  if(IsKeyPressedNew(3))
  {
    TableFromVisu(ActualTable);
    SQL_Table_WriteToSQL(ActualTable);
  }
  //linear calibration
  if(IsKeyPressedNew(4))
  {
    TableFromVisu(ActualTable);
    //linear 3. column. In this Column PWM is written down
    Table_CalibrateLinear(ActualTable, 2);

    TableToVisu(ActualTable);
  }
  Visu_HomeKeyPressed();

  SetVar(HDL_TESTVAR_1,  TableGetLSEValue(ActualTable, 0,1, GetVar(HDL_TESTVAR_0)));
}


