﻿/****************************************************************************
 *
 * File:         COMMANDS.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "commands.h"
#include "control.h"

/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Ctrl_JS_Cmd_SetCommands(tJSCommands *JS_Cmd, int32_t ActiveButtonPressed)
{
    const tControl *Ctrl = CtrlGet();
    if(ActiveButtonPressed)
    {
        JS_Cmd->Active    = 1;

        JS_Cmd->Updown    = -10 * IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.UpDown); //Inverted Y-Achses
        JS_Cmd->LeftRight = 10 * IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.LeftRight);
        JS_Cmd->X         = 10 * IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.X);
        JS_Cmd->Y         = 10 * IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.Y);
        JS_Cmd->Z         = 10 * IOT_AnaJoy_GetInPercent(&Ctrl->Joystick.Z);

        JS_Cmd->X_Change = IOT_AnaJoy_PressedNew(&Ctrl->Joystick.X);
        JS_Cmd->Y_Change = IOT_AnaJoy_PressedNew(&Ctrl->Joystick.Y);
        JS_Cmd->Z_Change = IOT_AnaJoy_PressedNew(&Ctrl->Joystick.Z);
        if(IOT_Button_IsPressedNew(&Ctrl->Joystick.Button[JSB_F]))
        {
            if(JS_Cmd->Float) JS_Cmd->Float = FLOAT_OFF;
            else              JS_Cmd->Float = FLOAT_ON;
            JS_Cmd->F_New = 1;

        }
        else
        {
            JS_Cmd->F_New = 0;
        }
        if(JS_Cmd->Updown > 0)//Push up->Deactivate Float and tABC_Attach
        {
            JS_Cmd->Float   = FLOAT_OFF; //Float off
        }
        else if(JS_Cmd->Updown < 0)//Push down->break for Float
        {
            if(JS_Cmd->Float == FLOAT_ON)
                JS_Cmd->Float = FLOAT_BREAK;
        }
        else
        {
            if(FLOAT_BREAK == JS_Cmd->Float)
            {
                JS_Cmd->Float = FLOAT_ON;
            }
        }

        //Boost Button
        if(IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_D]))
        {
            JS_Cmd->Boost = BOOST_ON;
        }
        else JS_Cmd->Boost = BOOST_OFF;
        //Power On/Off

        if(IOT_Button_IsReleasedShortNew(&Ctrl->Joystick.Button[JSB_E]))
        {
            if (JS_Cmd->E_L==1)
            {
                JS_Cmd->E_L=0;
            }
            else
            {
               JS_Cmd->E_S = 1 - JS_Cmd->E_S; /* code */
            }



        }
        if(IOT_Button_IsPressedLongNew(&Ctrl->Joystick.Button[JSB_E]))
        {
            JS_Cmd->E_L = 1 - JS_Cmd->E_L;
        }

        JS_Cmd->E_Time   = IOT_Button_IsDown(&Ctrl->Joystick.Button[JSB_E]);

        if(IOT_Button_IsReleasedShortNew(&Ctrl->Joystick.Button[JSB_D]))
        {
            JS_Cmd->D_S = 1 - JS_Cmd->D_S;
        }
        if(IOT_Button_IsPressedLongNew(&Ctrl->Joystick.Button[JSB_D]))
        {
            JS_Cmd->D_L = 1 - JS_Cmd->D_L;
        }
    }
    else //Key is not pressed anymore.
    {
        JS_Cmd->Updown     = 0;
        JS_Cmd->LeftRight  = 0;
        JS_Cmd->X          = 0;
        JS_Cmd->Y          = 0;
        JS_Cmd->Z          = 0;

        JS_Cmd->X_Change = 0;
        JS_Cmd->Y_Change = 0;
        JS_Cmd->Z_Change = 0;
        JS_Cmd->Active   = 0;

        JS_Cmd->E_Time   = 0;

        if(FLOAT_BREAK == JS_Cmd->Float)
        {
            JS_Cmd->Float = FLOAT_ON;
        }
    }

}




