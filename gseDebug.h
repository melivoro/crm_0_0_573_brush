﻿/************************************************************************
 *
 * File:         PARAMETER.h
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ************************************************************************/
 
 
 #include "gsSocketTCPServer.h"
 
/************************************************************************/
/* Protection against multiple includes.                                */
/* Do not code anything outside the following defines (except comments) */

#ifndef DEBUG_H
#define DEBUG_H



/*************************************************************************/

/* macro definitions *****************************************************/

/* type definitions ******************************************************/

/* prototypes ************************************************************/

/* global constants ******************************************************/

/* global variables ******************************************************/

/* global function prototypes ********************************************/
/**
 * @brief printf like funktion for displaying debug informations.  
 * Each line will be terminated with an '\n'
 * Before using this function db_init has to be called. 
 * The Output of the function can be configured by db_init
 * 
 * @param format formated string like in printf
 * @param ...    variable number of arguments like printf 
 */
void db_out(const char * format, ...);


/**
 * @brief printf like funktion for displaying debug infrmations. 
 * A timestamp will be set in the beginning of each message. 
 * Before using this function db_init has to be called. 
 * The Output of the function can be configured by db_init
 * 
 * @param format formated string like in printf
 * @param ...    variable number of arguments like printf 
 */
void db_out_time(const char * format, ...);
/**
 * @brief the output streams db_out and db_out_time will be configured
 * by this funciton. 
 * 
 * @param Console if 1 a bidirectional connection can be established by 
 *        ethernet and serial interface. See <sCon_AddCommandListfor>"()"" formore informations.  
 * @param EthPort   Ethernet Port for output over tcp. 0 if you don't want to use output over ethernet.
 * @param ComPort   Ethernet Port for output over serial interface. ComPort < 0 if you don't want to use output over ethernet.
 * @param usb       1 if you want to generate a debugfile on usb. 0 if not. 
 * @param window    1 if you want to display the text in the debugwindow opened by SetDebugWindow(1). 0 if you don't 
 * @param filepath  path to a logfile the debug data will be written in. e.g. "/gs/data/log.txt"
 * @return int32_t  currently allways 1
 */
int32_t db_init(int32_t Console, int32_t EthPort, int32_t ComPort, int32_t usb, int32_t window,  const char *filepath);

/**
 * @brief Cycle function for debug information. You have to call this function in your CCycle(), if you want to 
 * use ethernet or the console. 
 *
 * 
 * @param evtc 
 * @param evtv 
 */
void db_cycle(int32_t evtc,tUserCEvt * evtv);



int32_t GetEthConHdl(void);


/*************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !  */
/*************************************************************************/
#endif  // #ifndef PARAMETER_H
