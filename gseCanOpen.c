﻿/****************************************************************************
*
* File:         CANOPEN.c
* Project:
* Author(s):    Marc Schartel
* Date:
*
* Description:  Support lib for CanOpen
*
*
*
*
*
****************************************************************************/
#include <stdint.h>
    /* API-Function declarations                      */
#include "gseCanOpen.h"      /* include this file in every C-Source to access  */
     /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "gsMsgFiFo.h"
/****************************************************************************/
//CCS
#define INITIATE_DOMAIN_DOWNLOAD  0
#define DOWNLOAD_DOMAIN_SEGMENT   1
#define INITIATE_DOMAIN_UPLOAD    2
#define UPLOAD_DOMAIN_SEGMENT     3
#define DOMAIN_TRANSFER           4

#define MASK11BIT     0x7FF
#define MASK28BIT     0x1FFFFFFF
#define MASKCANOPEN   0x7F

#define NMT           0

/*****************
Master Slave
*****************/
#define CO_HOST 1
#define CO_NODE 0
/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/



typedef struct tagCO_Slave tCO_Slave;


typedef struct tagSdoBlockTransfer
{
  int32_t active;
  int32_t sc, cc; //support for csc
}tSdoBlockTransfer;


typedef struct tagSdoData
{
int8_t   CommandByte;
int16_t  Index;
int8_t   SubIndex;
void     *Data;
uint32_t  DataSize;
int32_t   DataCount;
int32_t  toggle;
int32_t  NewData;
int32_t  NewErrorCode;
int32_t  ErrorMsg;
tSdoBlockTransfer BT;

}tSdoData;


typedef struct tagCORxTx
{
  uint32_t rxID;
  uint32_t txID;
  tsData rData;
  int32_t DataReceived;
}tCORxTx;

typedef struct tagCO_PdoTX
{
  uint32_t id;
  uint8_t  len;
  tsData   Data;
}tCO_PdoTX;

typedef struct tagCO_PdoRX
{
  uint32_t id;
  uint8_t  len;
  tsData   Data;
  int32_t DataReceived;
}tCO_PdoRX;


typedef struct tagLSS
{
  int32_t ActNodeID;
  int32_t DataReceived;
}tLSS;

typedef struct tagTSM
{
  int32_t Producer;
  int32_t FiFo;
  int32_t Day;
  int32_t TimeMS;
}tTSM;

typedef struct tagCOHeartBeat
{
  uint32_t LastHB_ms;
  uint32_t DeltaHB;
  uint32_t factor;
}tCOHeartBeat;

typedef struct tagCO_SYNC
{
  uint32_t CycleTime;
  uint32_t TimeMS;
}tCO_SYNC;


typedef struct tagCO_Master{
  int32_t         deltaMS;
  int32_t         Channel;
  int32_t         Ext;
  int32_t         NMT_FiFo;   //NMT-Master
  tCanMessage     NMT_Msg;
  tCO_Slave     **Slaves;
  int32_t         NumSlaves;
  int32_t         TSM_FiFo;
  tTSM            TSM;        //Time Stamp Producer
  int32_t         LSS_FiFo;
  tLSS            Lss;
  tCO_SYNC        Sync;
  eCO_DebugLevel dbglvl;
  void    (*db_out)(const char *format, ...);
}tCO_Master;

struct tagCO_Slave
{
  uint32_t      id;
  uint32_t      channel;
  uint32_t      mask;
  uint8_t       ext;
  //GUARD
  uint8_t         toggle;
  //tSdoList        SdoList;
  eNodeState      NodeState;
  tCORxTx         Emcy;
  uint8_t         PdoTxNum;
  tCO_PdoTX      *PdoTX;
  uint8_t         PdoRxNum;
  tCO_PdoRX      *PdoRX;
  tCORxTx         Sdo;
  tCORxTx         Guard;
  int32_t         FiFo;
  uint8_t         SyncEvent;
  int16_t         Day;
  int32_t         TimeMS;
  int32_t         SdoBusy;
  uint32_t        SdoTimer;
  tSdoData        RxSdoData;
  tGsMsgFiFo      TxSdoFiFo;
  tSdoData        *pTxSdoAct;
  int32_t         MasterHdl;
  uint32_t        NodeGuarding;
  uint32_t        NodeGuardTime;
  tCOHeartBeat    HeartBeat;

};



tCO_Master **pCO_Master = NULL;
int32_t   numCO_Master      = 0;


/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/* global function prototypes ********************************************/
int32_t time(int32_t *seconds);

int32_t GetDayTimeMS(void)
{
  static uint32_t daytime_ms = 0;
  static uint32_t daytime_offset;
  if(0 == daytime_ms)
  {
    tSysTime mtime;
    RTCGetTime(&mtime);
    daytime_offset = (mtime.Seconds + mtime.Minutes * 60 + mtime.Hours * 3600) * 1000 - GetMSTick();
  }
  daytime_ms = (GetMSTick() + daytime_offset) & (60 * 60 *24* 1000);
  return daytime_ms;
}

void CO_Debug( tCO_Slave *pSlave,eCO_DebugLevel dbglvl, const char *fmt, ...)
{
char buffer[256];
  va_list args;
  va_start (args, fmt);
  vsnprintf (buffer,256,fmt, args);
  va_end (args);
  tCO_Master * thisMaster;
  if(NULL != pSlave)
  {
    thisMaster = pCO_Master[pSlave->MasterHdl];
  }
  else return;
  if(dbglvl >= thisMaster->dbglvl)
  {
    if(NULL != thisMaster->db_out)
    {
      thisMaster->db_out("CANOpen CAN %d Node 0x%02X: %s\r\n", thisMaster->Channel, pSlave->id, buffer);
    }
  }
}

tCO_Master * CO_GetMaster(tCanOpenMaster Hdl)
{
  if(Hdl < numCO_Master)
  {
    return pCO_Master[Hdl];
  }
  else return NULL;
}

/**
* Initialises the CAN-Open. Call this function befor adding CanOpen-Devices
 *
 *
 *
* @param number of a free timer which is internal used by the UserCMCQ-Timer functions
*
* @param CAN, on which CAN-Open is running. Use CAN0 or CAN1
*
* @param 11 bit extention used? use CBIT11ID or CBIT29ID
*/
tCanOpenMaster  CO_Init( int32_t  Channel, int32_t Ext, void (*fout)(const char *, ...))
{
  tCO_Master * thisMaster = malloc(sizeof(tCO_Master));
  if(NULL != thisMaster)
  {
    tCO_Master ** NewMasterList = realloc(pCO_Master, sizeof(tCO_Master*) * (numCO_Master + 1));
    if(NULL != NewMasterList)
    {
      pCO_Master = NewMasterList;
      pCO_Master[numCO_Master] = thisMaster;
      numCO_Master++;
    }
    else
      return -1;

    memset(thisMaster,0, sizeof(tCO_Master));
    thisMaster->Channel  = Channel;
    thisMaster->Ext      = Ext;
    thisMaster->NMT_FiFo = CANCreateFiFo(256);
    CANAddRxFilterFiFo(thisMaster->Channel, 0     , 0x7FF, thisMaster->Ext, thisMaster->NMT_FiFo);
    /*thisMaster->SYNC_FiFo = CANCreateFiFo(256);
    CANAddRxFilterFiFo(thisMaster->Channel, 0x80  , 0x7FF, thisMaster->Ext, thisMaster->SYNC_FiFo);*/
    thisMaster->TSM_FiFo = CANCreateFiFo(256);
    CANAddRxFilterFiFo(thisMaster->Channel, 0x100 , 0x7FF, thisMaster->Ext, thisMaster->TSM_FiFo);
    thisMaster->LSS_FiFo = CANCreateFiFo(256);
    CANAddRxFilterFiFo(thisMaster->Channel, 0x7E4 , 0x7FF, thisMaster->Ext, thisMaster->LSS_FiFo);
    thisMaster->Slaves    = NULL;
    thisMaster->NumSlaves = 0;
    thisMaster->db_out    = fout;
    thisMaster->dbglvl    = CO_DBGLVL_ERROR;
    return numCO_Master - 1;
  }
  return -1;
 }

int32_t CO_SetDebugLevel(tCanOpenMaster Hdl, eCO_DebugLevel DebugLevel)
{
  tCO_Master * thisMaster = CO_GetMaster(Hdl);
  if(NULL != thisMaster)
  {
    thisMaster->dbglvl = DebugLevel;
    return 1;
  }
  else return -1;
}

int32_t CO_AddTXPdo( tCanOpenSlave Slave,   int32_t Offset)
{
  tCO_Slave * this    = Slave;
  int32_t idx = this->PdoTxNum;
  tCO_PdoTX * pNewPdo = realloc(this->PdoTX, sizeof(tCO_PdoTX) * (idx + 1));
  if(NULL == pNewPdo)
  {
    CO_Debug(Slave,CO_DBGLVL_CRITICAL, "Error allocating memory for PDO");
    return -1;
  }
  memset(&pNewPdo[idx],0, sizeof(tCO_PdoTX));
  pNewPdo[idx].id  = this->id + Offset;
  pNewPdo[idx].len = 8;
  this->PdoTX = pNewPdo;
  this->PdoTxNum++;
  CO_Debug(this, CO_DBGLVL_INFO, "Added TX-PDO with ID 0x%X",pNewPdo[idx].id);
  return idx;
}

int32_t CO_AddRXPdo( tCanOpenSlave Slave, int32_t Offset)
{
  tCO_Slave * this    = Slave;
  int32_t idx = this->PdoRxNum;
  tCO_PdoRX * pNewPdo = realloc(this->PdoRX, sizeof(tCO_PdoRX) * (idx + 1));
  if(NULL == pNewPdo)
  {
    CO_Debug(Slave, CO_DBGLVL_CRITICAL,"Error  allocating memory for PDO");
    return -1;
  }
  memset(&pNewPdo[idx],0, sizeof(tCO_PdoRX));
  pNewPdo[idx].id  = this->id + Offset;
  pNewPdo[idx].len = 8;
  this->PdoRX = pNewPdo;
  this->PdoRxNum++;
  CO_Debug(this, CO_DBGLVL_INFO, "Added RX-PDO with ID 0x%X",pNewPdo[idx].id);
  return idx;
}


tCanOpenSlave CO_AddSlave(int32_t Id,tCanOpenMaster Hdl, ePDOConnectionSet Set)
{
  tCO_Master *thisMaster = CO_GetMaster( Hdl);
  if(thisMaster != NULL)
  {

    // Pointer in Data-Strutur speichern, wird für CO_Receive_Data benötigt.

    tCO_Slave * this = malloc(sizeof(tCO_Slave));
    if(NULL == this)
    {
      CO_Debug(NULL, CO_DBGLVL_CRITICAL, "Error allocating memory for new slave.");
    }
    tCO_Slave ** NewSlaveList = (tCO_Slave **)realloc(thisMaster->Slaves,sizeof(tCO_Slave *)* (thisMaster->NumSlaves + 1));
    if(NULL == NewSlaveList)
    {
      CO_Debug(this, CO_DBGLVL_CRITICAL, "Error reallocating memory for SlaveList.");
    }
    thisMaster->Slaves = NewSlaveList;
    thisMaster->Slaves[thisMaster->NumSlaves] = this;

    memset(this, 0, sizeof(tCO_Slave));
    thisMaster->NumSlaves++;

    this->MasterHdl = Hdl;

    this->ext         = thisMaster->Ext;
    this->mask        = 0x7F;
    this->channel     = thisMaster->Channel;
    this->id          = Id;


    int32_t returnvalue;
    returnvalue = 0;
    //Calculating adresses. If device is master, tx and rx are switched.
    this->Emcy.rxID = Id +   (1<<7);   //0x80
    this->Sdo.rxID    = Id + (11<<7);  //0x580
    this->Sdo.txID    = Id + (12<<7);  //0x600
    this->Guard.rxID  = Id + (14<<7);  //0x700
    this->Guard.txID  = Id + (14<<7);  //0x700

    if(PDO_CONNECTOINSET_DEFAULT == Set)
    {
      CO_AddRXPdo(this,(3<<7)); //0x180
      CO_AddTXPdo(this,(4<<7)); //0x200
      CO_AddRXPdo(this,(5<<7)); //0x280
      CO_AddTXPdo(this,(6<<7)); //0x300
      CO_AddRXPdo(this,(7<<7)); //0x380
      CO_AddTXPdo(this,(8<<7)); //0x400
      CO_AddRXPdo(this,(9<<7)); //0x480
      CO_AddTXPdo(this,(10<<7));//0x500
    }
    this->NodeState   = NODE_UNKNOWN;
    this->toggle      = 1;
    this->RxSdoData.Data      = NULL;
    this->RxSdoData.DataSize  = 0;
    this->RxSdoData.DataCount = 0;
    this->TxSdoFiFo = gsMsgFiFoCreate(sizeof(tSdoData*) * 32);
    if(NULL == this->TxSdoFiFo)
    {
      CO_Debug(this, CO_DBGLVL_CRITICAL, "Error creating Sdo TX Fifo");
    }
    this->pTxSdoAct           = NULL;

    //Init für Heartbeat:
    this->HeartBeat.DeltaHB   = 0;
    this->HeartBeat.LastHB_ms = 0;



    //erstelle Puffer
    if(-1 == (this->FiFo  = CANCreateFiFo(256)))
    {
    //gsDebugStreamsPrint("Error: Couldn't Create Emcy FiFo\n");
    returnvalue++;
    }
    this->RxSdoData.BT.active = 0;

    //Lege Filter an
    int32_t ret = CANAddRxFilterFiFo(thisMaster->Channel, Id, 0x7F, thisMaster->Ext,this->FiFo);
    if(1!= ret)
    {
      CO_Debug(this, CO_DBGLVL_CRITICAL, "Can't add Fifo for NodeID.CANAddRxFilterFiFo returned %d ", ret);
    }
    CO_Debug(this, CO_DBGLVL_INFO, "Added Slave");
    return this;

  }

  else return NULL;
}
/*************
allocate memory for subobjects
*************/


int32_t CO_NMT_Send( tCanOpenSlave Slave,eNMT_Command Command)
{
    tCanMessage CanMessage;
    tCO_Slave *this = Slave;
    CanMessage.id = 0;                        // Broadcast
    CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
    CanMessage.channel = this->channel;                  // CAN 0
    CanMessage.len = 2;                         // Data lenght
    CanMessage.res = 0;                         // everytime 0
    CanMessage.data.u8[0] = Command;
    //CanMessage.data.u8[1] = this->id;           // ID of Drehgeber
    CanMessage.data.u8[1] = 0;           // Broadcast. Alle sollen starten
    CanMessage.data.u8[2] = 0;
    CanMessage.data.u8[3] = 0;
    CanMessage.data.u8[4] = 0;
    CanMessage.data.u8[5] = 0;
    CanMessage.data.u8[6] = 0;
    CanMessage.data.u8[7] = 0;
    CANSendMsg(&CanMessage);
  return 0;
}

int32_t CO_Send_SDO_Command(tCanOpenSlave Slave,tsData *pData)
{
    tCO_Slave *this = (tCO_Slave*) Slave;
    tCanMessage CanMessage;
    CanMessage.id      = this->Sdo.txID;                        // Broadcast
    CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
    CanMessage.channel = this->channel;                  // CAN 0
    CanMessage.len = 8;                         // Data lenght
    CanMessage.res = 0;
    memcpy( &(CanMessage.data.u8[0]), &(pData->u8[0]), sizeof(tsData));
    CANSendMsg(&CanMessage);
    this->SdoBusy = 1;
    return 0;
}

int32_t CO_Sdo_Set(tCanOpenSlave Slave, int16_t Index, int8_t SubIndex, void *data, int32_t size)
{
  tCO_Slave *this = (tCO_Slave*) Slave;

  int32_t n, e, s;
  int32_t ccs, CommandByte;
  if(size <= 4)
  {
    e   = 1;
    /*
    n   = 4-size;
    s   = 1;*/
    n   = 0;
    s   = 0;
    ccs = 1;
    CommandByte = s + (e << 1) + (n << 2) + (ccs << 5);
  }
  else
  {
    ccs = 1;
    s   = 1;
    n = 0;
    e = 0;
    CommandByte = s + (e << 1) + (n << 2) + (ccs << 5);
  }
  tSdoData *Sdo = malloc(sizeof(tSdoData));
  memset(Sdo, 0, sizeof(tSdoData));
  Sdo->CommandByte = CommandByte;
  Sdo->Index       = Index;
  Sdo->SubIndex    = SubIndex;
  Sdo->Data = malloc(size);
  memcpy(Sdo->Data,data, size);
  Sdo->DataSize = size;
  if(0 == gsMsgFiFoWrite(this->TxSdoFiFo, &Sdo, sizeof(tSdoData*), 50))
  {
    CO_Debug(this, CO_DBGLVL_ERROR, "Error Writing SDO-Fifo. Fifo full or not initialized.");
    return 0;
  }
  this->SdoBusy = 1;
  return 1;
}

int32_t CO_Sdo_Req(tCanOpenSlave Slave, int16_t Index, int8_t SubIndex, int32_t size)
{
  tCO_Slave *this = (tCO_Slave*) Slave;

  int32_t n, e, s;
  int32_t ccs, CommandByte;
  ccs = 2;
  if(0 == size)
  {
    n = 0;
    e = 0;
    s = 0;
  }
  else if(size <=4)
  {
    e   = 1;
    n   = 4-size;
    s   = 1;
  }
  else
  {
    e = 0;
    n = 0;
    s = 1;
  }
  CommandByte = s + (e << 1) + (n << 2) + (ccs << 5);
  tSdoData *Sdo = malloc(sizeof(tSdoData));
  memset(Sdo, 0, sizeof(tSdoData));
  Sdo->CommandByte = CommandByte;
  Sdo->Index       = Index;
  Sdo->SubIndex    = SubIndex;
  Sdo->Data = malloc(size);
 Sdo->DataSize = size;
  if(0 == gsMsgFiFoWrite(this->TxSdoFiFo, &Sdo, sizeof(tSdoData*), 50))
  {
    CO_Debug(this,CO_DBGLVL_ERROR,  "Error Writing SDO-Fifo. Fifo full or not initialized.");
    return 0;
  }
  this->SdoBusy = 1;
  return 1;
}




int32_t CO_Send_TimeStamp(tCanOpenSlave Slave)
{
  tCanMessage CanMessage;
  tSysTime mtime;
  tCO_Slave *this = (tCO_Slave*) Slave;
  RTCGetTime(&mtime);
  CanMessage.id =      this->Guard.txID;                        // Broadcast
  CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
  CanMessage.channel = this->channel;                  // CAN 0
  CanMessage.len = 6;                         // Data lenght
  CanMessage.res = 0;                         // everytime 0
  CanMessage.data.u32[0] = GetDayTimeMS();
  CanMessage.data.u16[2] = time(NULL)/60/60/24;
  return CANSendMsg(&CanMessage);
}




int32_t CO_Pdo_Send(tCanOpenSlave Slave,uint16_t PDO_Idx, tsData *Data)
{

    tCanMessage CanMessage;
    tCO_Slave *this = (tCO_Slave*) Slave;
    if(PDO_Idx < this->PdoTxNum)
    {
      CanMessage.id      = this->PdoTX[PDO_Idx].id;                        // Broadcast
      CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
      CanMessage.channel = this->channel;                  // CAN
      CanMessage.len     = this->PdoTX[PDO_Idx].len;                         // Data lenght
      CanMessage.res     = 0;
      memcpy(&CanMessage.data,Data, sizeof(tsData));
      return CANSendMsg(&CanMessage);
    }
    else
    {
      CO_Debug(this, CO_DBGLVL_ERROR, "PDO with Hdl %d doesn't exist", PDO_Idx);
    }return -1;
}

int32_t CO_Pdo_Send_SetLen(tCanOpenSlave Slave,uint16_t PDO_Idx, uint8_t len)
{
    //tCanMessage CanMessage;
    tCO_Slave *this = (tCO_Slave*) Slave;
    if(len > 8)
      return -1;
    if(PDO_Idx < this->PdoTxNum)
    {
      this->PdoTX[PDO_Idx].len = len;
    }
    else return -1;
    return 0;
}


int32_t CO_Send_Guard(tCanOpenSlave Slave, int8_t Data)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  tCanMessage CanMessage;
  CanMessage.id      = this->Guard.txID;                        // Broadcast
  CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
  CanMessage.channel = this->channel;                  // CAN 0
  CanMessage.len = 1;                         // Data lenght
  CanMessage.res = 0;
  CanMessage.data.u8[0] = Data;

  return CANSendMsg(&CanMessage);
}



/************************
Node Guarding
************************/

//  MASTER
void CO_NodeGuarding_Start(tCanOpenSlave Slave, int32_t GuardTime, int32_t facotor)
{
  int32_t value = GuardTime;
  //Set GuardTime
  CO_Sdo_Set(Slave, CO_OBJ_GUARD_TIME,0,&value, sizeof(value));
  value = facotor;
  CO_Sdo_Set(Slave, CO_OBJ_GUARD_FACTOR,0,&value, sizeof(value));
  tCO_Slave *this = (tCO_Slave*) Slave;
  this->NodeGuarding = GuardTime;
}


/****************************************************************************
**
**    Function      : int32_t Request_Guard(tCanOpenSlave Slave);
**
**    Description   : Requests an Guard-Responce from a tCO_Slaves.
**
**
**
**    Returnvalues  : return 1 if the message was send succesfull.
**
*****************************************************************************/

int32_t CO_NodeGuard_Request(tCanOpenSlave Slave)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  tCanMessage CanMessage;
  int32_t i;
    CanMessage.id      = this->Guard.txID;                        // Broadcast
    CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
    CanMessage.channel = this->channel;                  // CAN

    CanMessage.len = 0;                         // Data lenght
    CanMessage.res = GS_CAN_RTR_FRAME;
    for(i = 0; i < 2; i++)
    {
      CanMessage.data.u32[i] = 0;
    }

    return CANSendMsg(&CanMessage);
}


/****************************************************************************
**
**    Function      : int32_t Receive_Guard(tCanOpenSlave Slave);
**
**    Description   : Looks for received Guard-response from the tCO_Slaves.
**
**
**
**    Returnvalues  : return 1 if message has been  received  and toggle is right.
                      return 0 if no message has been received.
                      return -1 if message has been received, but toggle is wrong.
**
*****************************************************************************/

int32_t CO_Receive_NodeGuard(tCanOpenSlave Slave)
{
  tCO_Slave *this = (tCO_Slave*) Slave;

  if(this->Guard.DataReceived == 1)
  {
    int32_t toggle_old;
    toggle_old = this->toggle;
    this->Guard.DataReceived = 1;
    if( this->Guard.rData.u8[0] == 0) this->NodeState = NODE_PREOPERATIONAL;//NMT Boot-up
    else
    {
      this->toggle = (this->Guard.rData.u8[0]>>7)&1;
      this->NodeState =  this->Guard.rData.u8[0]  & 0x7F;
    }
    if(toggle_old == this->toggle) return -1;
    else return 1;
  }
  return 0;
}

eNodeState CO_GetNodeState(tCanOpenSlave Slave)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  return this->NodeState;
}


/****************************************************************************
**
**    Function      : int32_t tCO_SlavesGuardCylce(tCanOpenSlave Slave);
**
**    Description   : If the slave received a request from the master, he responses with a guard-response.
**
**
**
**    Returnvalues  : return 1 if the message was send succesfull.
**
*****************************************************************************/


int32_t CO_tCO_SlavesGuardCylce(tCanOpenSlave Slave)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  tCanMessage CanMessage;
  CanMessage.id =      this->Guard.txID;                        // Broadcast
  CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
  CanMessage.channel = this->channel;                  // CAN 0
  CanMessage.len = 1;// Data lenght
  CanMessage.res = 0;// everytime 0

  //status = (((this->toggle<<7) && 0x80) + (this->state && 0x7F));

  CanMessage.data.u8[0] = this->NodeState;
  CanMessage.data.u8[1] = 0;       // ID of Drehgeber
  CanMessage.data.u8[2] = 0;
  CanMessage.data.u8[3] = 0;
  CanMessage.data.u8[4] = 0;
  CanMessage.data.u8[5] = 0;
  CanMessage.data.u8[6] = 0;
  CanMessage.data.u8[7] = 0;
  this->toggle = 1 - this->toggle;
  return CANSendMsg(&CanMessage);
}

/****************************
Heartbeat
****************************/

void CO_HeartBeat_Start  (tCanOpenSlave Slave, int32_t Time_ms, int32_t factor)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  this->HeartBeat.DeltaHB = Time_ms;
  this->HeartBeat.LastHB_ms = GetMSTick();
  this->HeartBeat.factor    = factor;
  //Set Hearbeat Time
  CO_Sdo_Set(Slave, CO_OBJ_HEARTBEAT,0,&Time_ms, sizeof(Time_ms));
}

int32_t CO_Heartbeat_Receive(tCanOpenSlave Slave)
{
    tCO_Slave *this = (tCO_Slave*) Slave;
    if(this->Guard.DataReceived == 1)
    {
      if((this->Guard.rData.u8[0]== 0)) this->NodeState = NODE_PREOPERATIONAL;//NMT Boot-up
      else (this->NodeState = this->Guard.rData.u8[0] & 0x7F);
      this->HeartBeat.LastHB_ms = GetMSTick();
      return 1;
    }
  return 0;
}

void CO_Heartbeat_Cycle(tCanOpenSlave Slave)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  tCOHeartBeat *hb = &this->HeartBeat;
  if(hb->DeltaHB)
  {
    if((hb->DeltaHB * hb->factor) < (GetMSTick() - hb->LastHB_ms))
    {
      this->NodeState = NODE_ERROR;
      hb->LastHB_ms = GetMSTick();
      CO_Debug(this , CO_DBGLVL_WARNING, "Heardbeat Timeout");
    }
  }
}

void CO_SYNC_Start(tCanOpenSlave Slave, int32_t CycleTime)
{
  tCO_Slave  *this = (tCO_Slave*) Slave;
  tCO_Master *thisMaster = CO_GetMaster(this->MasterHdl);

  int32_t cTime = CycleTime * 1000;
  CO_Sdo_Set(Slave, CO_OBJ_SYNC_COM_CYCLE,0,&cTime, sizeof(CycleTime));
  thisMaster->Sync.CycleTime = CycleTime;
  //thisMaster->Sync.TimeMS    = GetMSTick();
}


/***********************
Syncronisation
***********************/

int32_t CO_SYNC_Send(tCanOpenMaster Master)
{
  tCO_Master *thisMaster = (tCO_Master*) Master;
  tCanMessage CanMessage;
  CanMessage.id      = 0x80;                        // Broadcast
  CanMessage.ext     = thisMaster->Ext;                         // Extention 11 or 29 bit
  CanMessage.channel = thisMaster->Channel;                  // CAN 0
  CanMessage.len = 0;                         // Data lenght
  CanMessage.res = 0;

  return CANSendMsg(&CanMessage);
}


void CO_SYNC_Cycle(tCanOpenMaster Hdl)
{
  tCO_Master *thisMaster = CO_GetMaster(Hdl);
  tCO_SYNC *pSync = &thisMaster->Sync;
  if(pSync->CycleTime)
  {
    if((pSync->CycleTime) <= (GetMSTick() - pSync->TimeMS))
    {
      pSync->TimeMS = GetMSTick();
      CO_SYNC_Send(Hdl);
    }
  }
}

int32_t CO_Heartbeat_Send(tCanOpenSlave Slave)
{
    tCO_Slave *this = (tCO_Slave*) Slave;
    tCanMessage CanMessage;
    CanMessage.id =      this->Guard.txID;                        // Broadcast
    CanMessage.ext     = this->ext;                         // Extention 11 or 29 bit
    CanMessage.channel = this->channel;                  // CAN 0
    CanMessage.len = 1;                         // Data lenght
    CanMessage.res = 0;                         // everytime 0
    CanMessage.data.u8[0] = this->NodeState;
    CanMessage.data.u8[1] = 0;       // ID of Drehgeber
    CanMessage.data.u16[1] = 0;
    CanMessage.data.u32[1] = 0;
    return CANSendMsg(&CanMessage);
}

void CO_ReqSegData(tCanOpenSlave Slave)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  int32_t ccs = 3;
  int32_t CommandByte = 0 ;
  CommandByte |= ccs<<5;
  CommandByte |= this->toggle<<4;
  tsData Data;
  Data.u8[0] =  CommandByte;
  Data.u8[1] =  this->RxSdoData.Index       & 0xFF;
  Data.u8[2] = (this->RxSdoData.Index >> 8) & 0xFF;
  Data.u8[3] =  this->RxSdoData.SubIndex    & 0xFF;
  Data.u32[1]=  0;
  this->toggle = 1 - this->toggle;
  this->SdoBusy = 1;
  CO_Send_SDO_Command(this, &Data);
}

int32_t CO_Receive_SDO(tCanOpenSlave Slave)
{
  tCO_Slave  *this       = (tCO_Slave*) Slave;
  tCO_Master *thisMaster = CO_GetMaster(this->MasterHdl);
  tSdoData * pSdo = &this->RxSdoData;
  pSdo->NewData = 0;
  pSdo->NewErrorCode = 0;
  pSdo->CommandByte = this->Sdo.rData.u8[0];

  uint32_t scs, n,e,s;
  s =      (pSdo->CommandByte >> 0) & 0x01;
  e =      (pSdo->CommandByte >> 1) & 0x01;
  n =      (pSdo->CommandByte >> 2) & 0x03;

  scs =    (pSdo->CommandByte >> 5) & 0x07;
  this->SdoBusy = 1;
  switch(scs)
  {
    case 0: //reading segmented Data
    {

      this->TimeMS = GetMSTick();
      if(s == 1)   //last data of segmented download;
      {

        if(NULL != pSdo->Data)
        {
          n = 8 - (int32_t)((pSdo->CommandByte>>1)& 0x07);  //Anzahl der nicht benutzten Bytes

          if(pSdo->DataCount == -1)  //unknown data length
          {
            pSdo->Data = realloc(pSdo->Data, pSdo->DataSize + n);                             //vergrößere reservierten Speicher
            memcpy(((int8_t*)pSdo->Data)+pSdo->DataSize , &(this->Sdo.rData.u8[1]), n);  //kopiere die daten in den hinzugefügten Speicher.
            pSdo->DataSize = pSdo->DataSize + n;
            pSdo->DataCount = 0;
          }
          else //known data length.
          {
            if(pSdo->DataCount < 0) pSdo->DataCount = 0;
            uint32_t size = pSdo->DataSize - pSdo->DataCount;
            if(n > size) n = size;

            memcpy((int8_t*) pSdo->Data + pSdo->DataCount    //Pointer zu den noch nicht beschriebenen daten
                ,&(this->Sdo.rData.u8[1])                                                //Pointer zu den zu kopierenden Bytes
                ,n),

            pSdo->DataCount = pSdo->DataCount + n;   //berechne noch freie bytes.
            if(  pSdo->DataCount != (int32_t)pSdo->DataSize)
            {
              thisMaster->db_out("Data lost at semented download > 4Byte. Received Bytes: %d, expected: %d",pSdo->DataCount, pSdo->DataSize);
            }
            pSdo->NewData = 1;
          }
          //CO_Send_SDO_Command(this,pSdo->Index, pSdo->SubIndex, 0,0x60 | pSdo->toggle); //bestätige Empfang
          pSdo->toggle = 1 - pSdo->toggle;                              //nächste zu empfangende Nachricht muss toggle Bit anders sein.


          pSdo->DataCount = 0;
        }
      }
      else
      {
        if(NULL != pSdo->Data)
        {
          //Download, size unknown.
          if(pSdo->DataCount == -1)
          {
            pSdo->Data = realloc(pSdo->Data, pSdo->DataSize + 7);                             //vergrößere reservierten Speicher
            memcpy(((int8_t*)pSdo->Data)+pSdo->DataSize , &(this->Sdo.rData.u8[1]), 7);  //kopiere die daten in den hinzugefügten Speicher.
            pSdo->DataSize = pSdo->DataSize + 7;
            if(NULL == pSdo->Data)
            {
            thisMaster->db_out("Out of Memory\r\n");
            }
          }
          //Download, size known
          else
          {
          int32_t size = pSdo->DataSize - pSdo->DataCount;
          if(size < 0)
          {
            CO_Debug(Slave,CO_DBGLVL_ERROR,  "Error Calculating size!");
            size = 0;
          }
          if (size > 7) size = 7;

          memcpy(((int8_t*)pSdo->Data)+ pSdo->DataCount,&(this->Sdo.rData.u8[1]), size);
          pSdo->DataCount = pSdo->DataCount + 7;
          if(pSdo->DataCount < 0) pSdo->DataCount = 0;
          CO_ReqSegData(this);
          }


        }
      }
    }break;
    case 2:
    {
    pSdo->Index    = this->Sdo.rData.u8[1]
                  + (this->Sdo.rData.u8[2] << 8);
    pSdo->SubIndex = this->Sdo.rData.u8[3];
    if(NULL != pSdo->Data)
    {
                free(pSdo->Data);
                pSdo->Data = NULL;
    }
    if(NULL != this->pTxSdoAct)
    {
      if((pSdo->Index != this->pTxSdoAct->Index)
        ||(pSdo->SubIndex!= this->pTxSdoAct->SubIndex))
      {
        char string[128];
        snprintf(string,sizeof(string),  "Get wrong SDO. IDX %04X.%02X != %04X.%02X",pSdo->Index,pSdo->SubIndex, this->pTxSdoAct->Index, this->pTxSdoAct->SubIndex);
        CO_Debug(this,CO_DBGLVL_WARNING,string );
        free(this->pTxSdoAct->Data);
        free(this->pTxSdoAct);
        this->pTxSdoAct = NULL;
      }
    }


    if(e)
    {
      pSdo->Data = malloc(sizeof(int32_t));
      if(s)
      {

        pSdo->DataSize = 4 - n;
        memcpy( pSdo->Data, &this->Sdo.rData.u8[4],pSdo->DataSize);
      }
      else
      {
        memcpy( pSdo->Data, &this->Sdo.rData.u8[4],sizeof(int32_t));
        pSdo->DataSize = 4;
      }
      pSdo->NewData   = 1;
      pSdo->ErrorMsg = 0;
      return 1;
    }
    else   //Segmented message read start;
    {
      if(s)
      {
        pSdo->DataSize  = this->Sdo.rData.u32[1];
        pSdo->Data = malloc(pSdo->DataSize);
        pSdo->DataCount = 0;
        this->toggle = 0;
        //Request Segment
        CO_ReqSegData(this);
      }
    }


    }break;

  case 3: //Response message after writing
  {
    pSdo->NewData      = 0;
    pSdo->NewErrorCode = 0;
    tSdoData * pTSdo  = this->pTxSdoAct;
    if(NULL != pTSdo)
    {
      size_t DataToSend =  pTSdo->DataSize - pTSdo->DataCount;
      if(DataToSend > 0)
      {
        if(DataToSend > 7)
        {
          DataToSend = 7; //kann auch 8 sein, Doku wahrscheinlich fehlerhaft
          tsData Data;
          Data.u8[0] = (3 << 5) + ((pTSdo->toggle & 0x1)<<4);
          memcpy((int8_t*)pTSdo->Data + pTSdo->DataCount, &Data.u8[1], DataToSend);
          pTSdo->DataCount += DataToSend;
          CO_Send_SDO_Command(this,&Data);
          if((uint32_t)pTSdo->DataCount >= pTSdo->DataSize)
          {
            free(pTSdo->Data);
            free(pTSdo);
            this->pTxSdoAct = NULL;
          }
        }
        else
        {
          pTSdo->DataCount = 4 - n;
          DataToSend = pTSdo->DataSize - pTSdo->DataCount;
          //if(0 == DataToSend)
          {
            if((pSdo->Index != this->pTxSdoAct->Index)
              ||(pSdo->SubIndex!= this->pTxSdoAct->SubIndex))
            {
              free(this->pTxSdoAct->Data);
              free(this->pTxSdoAct);
              this->pTxSdoAct = NULL;
            }
          }
        }
      }
    }
    else
    {
      CO_Debug(Slave,CO_DBGLVL_WARNING,  "Got SDO-Messag without request. Index = %d, SubIndex = %d", pSdo->Index, pSdo->SubIndex);
    }
  }break;
  case 4: //Abort up/download
  {
    pSdo->NewErrorCode = 1;
    pSdo->ErrorMsg     = this->Sdo.rData.u32[1];
    if(NULL != this->pTxSdoAct)
    {
      free(this->pTxSdoAct->Data);
      free(this->pTxSdoAct);
      this->pTxSdoAct = NULL;
    }
    CO_Debug(this,CO_DBGLVL_WARNING,  "SDO Aborted by Node. ErrorCode: 0x%X" ,pSdo->ErrorMsg);
    return -1;
  }
  break;

    default: break;
  }
  return 0;
}

int32_t CO_Sdo_Read(tCanOpenSlave Slave,int32_t *Index, int32_t *SubIndex, void * Data, size_t DataSize)
{
  tCO_Slave  *this     = (tCO_Slave*) Slave;
  tSdoData   *pSdo     = &this->RxSdoData;
  if(pSdo->NewData)
  {

    if(DataSize > pSdo->DataSize)
    {
      DataSize = pSdo->DataSize;
    }
    memcpy(Data, pSdo->Data,DataSize);
    free(pSdo->Data);
    pSdo->Data = NULL;
    pSdo->DataSize = 0;

    pSdo->NewData = 0;
    *Index    = pSdo->Index;
    *SubIndex = pSdo->SubIndex;
    if(NULL != this->pTxSdoAct)
    {
                free(this->pTxSdoAct->Data);
                free(this->pTxSdoAct);
                this->pTxSdoAct = NULL;
    }
    return DataSize;
  }
  else if(pSdo->NewErrorCode)
  {
    return -1;
  }
  else return 0;
}

int32_t CO_Sdo_GetErrorCode(tCanOpenSlave Slave)
{
  if(NULL != Slave)
  {
    tCO_Slave  *this     = (tCO_Slave*) Slave;
    tSdoData   *pSdo     = &this->RxSdoData;
    if(NULL != pSdo)
    {
      return pSdo->ErrorMsg;
    }
    return -1;
  }
  else return -1;
}

int32_t CO_Emcy_Read(tCanOpenSlave Slave, tEmcy *EmcyMessage)
{

  tCO_Slave *this = (tCO_Slave*) Slave;
  if(this->Emcy.DataReceived)
  {
    EmcyMessage->Code     =  this->Emcy.rData.u16[0];
    EmcyMessage->Register =  this->Emcy.rData.u8[2];


    for(int32_t i = 3; i < 8; i++)
    {
      EmcyMessage->InfoByte[i-3] = this->Emcy.rData.u8[i];
    }
    return 1;
  }
  return 0;
}

int32_t CO_LSS_Read(tCanOpenMaster Master, tEmcy *EmcyMessage)
{

  tCO_Master *thisMaster = (tCO_Master*) Master;
  if(thisMaster->Lss.DataReceived)
  {
    return thisMaster->Lss.DataReceived;
  }
  return 0;
}


int32_t CO_Receive_Data(tCanOpenSlave Slave)
{
  tCO_Slave  *this = (tCO_Slave*) Slave;
  tCanMessage CanMessage;
  this->Emcy.DataReceived = 0;
  this->Sdo.DataReceived = 0;
  this->RxSdoData.NewData = 0;
  for(int32_t i = 0; i < this->PdoRxNum; i++)
  {
    this->PdoRX[i].DataReceived = 0;
  }
  this->Guard.DataReceived = 0;

  //_____________________
  //Node Guarding

  if(this->NodeGuarding)
  {
    if(this->NodeGuarding < (GetMSTick()- this->NodeGuardTime ))
    {
      this->NodeGuardTime = GetMSTick();
      CO_NodeGuard_Request(this);
    }
  }
  //_____________________
  //Heart beat

  CO_Heartbeat_Cycle(this);

  if(CANReadFiFo(this->FiFo,&CanMessage,1))
  {
    //NMT
    if(CanMessage.id == this->Emcy.rxID)
    {
      this->Emcy.DataReceived = 1;
      for(int32_t j = 0; j < 2 ;j++)
      {
        this->Emcy.rData.u32[j] = CanMessage.data.u32[j];
      }
    }
    //SDO
    else if (CanMessage.id == this->Sdo.rxID)
    {

      for(int32_t j = 0; j < 2 ;j++)
      {
        this->Sdo.rData.u32[j] = CanMessage.data.u32[j];
      }
      this->Sdo.DataReceived = 1;

      CO_Receive_SDO(this);
    }

    //Guard
    else if (CanMessage.id ==  this->Guard.rxID)
    {
      //Receive Master Node Guard Request
      if(CanMessage.res == GS_CAN_RTR_FRAME)
      {
        int8_t data = 0;

        data = (this->toggle << 7) + this->NodeState;
        CO_Send_Guard(this, data);
        this->toggle = 1 - this->toggle;
      }
      else
      {
        for(int32_t j = 0; j < 2 ;j++)
        {
          this->Guard.rData.u32[j] = CanMessage.data.u32[j];
        }
        this->Guard.DataReceived = 1;
        CO_Heartbeat_Receive(this);
      }
    }
    else
    {
      for(int32_t i = 0; i < this->PdoRxNum; i++)
      {
        if(CanMessage.id == this->PdoRX[i].id)
        {
          this->PdoRX[i].DataReceived = 1;
          for(int32_t j = 0; j < 2 ;j++)
          {
            this->PdoRX[i].Data.u32[j] = CanMessage.data.u32[j];
          }
        }

      }
    }
    return 1;
  }


  //NMT
  /*
  if(thisMaster->Host == CO_NODE)
  {
    if((NMT_Msg.data.u8[1] ==  this->id)
    ||(NMT_Msg.data.u8[1] == 0))
    {
      switch(NMT_Msg.data.u8[0])
      {
        case NMT_START_REMOTE_NODE:
          this->NodeState = NODE_OPERATIONAL;
        break;

        case NMT_STOP_REMOTE_NODE:
          this->NodeState = NODE_STOPPED;
        break;

        case NMT_ENTER_PRE_OPERATIONAL:
          this->NodeState = NODE_PREOPERATIONAL;
        break;

        case NMT_RESET_NODE:
          PowerCommand( GS_POWER_CMD_REBOOT,0);
        break;

        case NMT_RESET_COMMUNICATION:
          this->NodeState = NODE_INITIALISATION;
        break;

        default: break;
      }
    }
  }*/
  //Sync
  return 0;
}

int32_t CO_SDO_Busy(tCanOpenSlave Slave)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  return this->SdoBusy;
}


void CO_Cycle(tCanOpenMaster Hdl)
{


  tCO_Master *thisMaster = CO_GetMaster(Hdl);
  if(thisMaster == NULL)
  {
    return;
  }
  tCanMessage CanMessage;
  //NMT
  if(CANReadFiFo(thisMaster->NMT_FiFo,&CanMessage,1))
  {
    memcpy(&thisMaster->NMT_Msg,  &CanMessage, sizeof(tCanMessage));
  }
  //TSM
  if(CANReadFiFo(thisMaster->TSM_FiFo,&CanMessage,1))
  {
    if(0 == thisMaster->TSM.Producer)
    {
      thisMaster->TSM.TimeMS = CanMessage.data.u32[0];
      thisMaster->TSM.Day    = CanMessage.data.u16[2];
    }
  }
  //LSS
  if(CANReadFiFo(thisMaster->LSS_FiFo,&CanMessage,1))
  {
    switch(CanMessage.data.u8[0])
    {
      case LSS_ACT_NODE_ID:
      {
        thisMaster->Lss.ActNodeID = CanMessage.data.u8[1];
        thisMaster->Lss.DataReceived = 1;
      }break;
      default:
      {
        thisMaster->Lss.DataReceived = CanMessage.data.u8[1];
      }break;
    }
  }
  CO_SYNC_Cycle(Hdl);

  for(int32_t i = 0; i < thisMaster->NumSlaves;i++)
  {
    //Read Receive Buffers of all CO-Devices
    tCO_Slave *this = thisMaster->Slaves[i];
    CO_Receive_Data(this);
    // sending Heartbeat
    /*if((thisMaster->Host == CO_NODE)
     &&(0 != thisMaster->Slaves[i]->HeartBeat.DeltaHB))
    {
      if((thisMaster->Slaves[i]->NodeState == NODE_PREOPERATIONAL)
      ||(thisMaster->Slaves[i]->NodeState == NODE_OPERATIONAL))
      {
        if(thisMaster->Slaves[i]->HeartBeat.DeltaHB <= (GetMSTick() - thisMaster->Slaves[i]->HeartBeat.LastHB_ms))
        {
          thisMaster->Slaves[i]->HeartBeat.LastHB_ms = GetMSTick();
          CO_SendHeartbeat(thisMaster->Slaves[i]);
        }
      }
    }*/

    if(this->pTxSdoAct == NULL)
    {
      if(gsMsgFiFoRead(this->TxSdoFiFo, &this->pTxSdoAct, sizeof(tSdoData*), 0))
      {
        tsData Data;
        tSdoData *pSdoAct = this->pTxSdoAct;
        Data.u8[0] =  pSdoAct->CommandByte;
        Data.u8[1] =  pSdoAct->Index       & 0xFF;
        Data.u8[2] = (pSdoAct->Index >> 8) & 0xFF;
        Data.u8[3] =  pSdoAct->SubIndex    & 0xFF;
        uint32_t size = 4;
        if(pSdoAct->DataSize < size)
          size = pSdoAct->DataSize;
        memcpy(&Data.u32[1], pSdoAct->Data, size);
        CO_Send_SDO_Command(this,&Data);
        this->SdoTimer = GetMSTick();
      }
      else
      {
        this->SdoBusy = 0;
      }
    }
    //TimeOut
    else if(1000 < (uint32_t)(GetMSTick() - this->SdoTimer))
    {
      if(NULL != this->pTxSdoAct)
      {
        free(this->pTxSdoAct->Data);
        free(this->pTxSdoAct);
        this->pTxSdoAct = NULL;
      }

    }
  }


}


int32_t CO_Pdo_Read(tCanOpenSlave Slave, int32_t Pdo,tsData *Data)
{
  tCO_Slave *this = (tCO_Slave*) Slave;
  if(this->PdoRxNum <= Pdo) return -1;
  if(this->PdoRX[Pdo].DataReceived)
  {
    memcpy(Data, &(this->PdoRX[Pdo].Data), sizeof(tsData));
    return 1;
  }
  return 0;
}





/*
etLSS LSS_Receive(tCanOpenMaster Master, int16_t *data)
{
  //check if canopen is initialized
  tCO_Master *thisMaster = (tCO_Master*) Master;
  if(thisMaster->LSS_FiFo == 0)
  {
    return LSS_ERROR;
  }
  tCanMessage msg;
  if(CANReadFiFo(thisMaster->LSS_FiFo,&msg,1))
  {
    switch(msg.data.u8[0])
    {
      case LSS_ACT_NODE_ID:
        *data = msg.data.u8[1];
        return LSS_ACT_NODE_ID;
      break;

      case LSS_NEW_NODE_ID:
        *data = msg.data.u8[1];
        return LSS_ACT_NODE_ID;
      break;

      case LSS_NEW_BAUDRATE:
        *data = msg.data.u8[1];
        return LSS_NEW_BAUDRATE;
      break;

      case LSS_SAVE:
        *data = msg.data.u8[1];
        return LSS_NEW_BAUDRATE;
      break;

      default:
        *data = 0;
        return LSS_ERROR;
      break;
    }
  }
  return LSS_NO_MSG;
}*/

int32_t LSS_Start(tCanOpenMaster Master)
{
  tCO_Master *thisMaster = (tCO_Master*) Master;
  tCanMessage msg;
  msg.channel = thisMaster->Channel;
  msg.ext     = thisMaster->Ext;
  msg.id      = 0x7E5;
  msg.len     = 8;
  msg.res     = 0;
  msg.data.u32[0] = 0;
  msg.data.u32[1] = 0;
  msg.data.u8[0] = 0x04;
  msg.data.u8[1] = 0x01;
  return CANSendMsg(&msg);
}


int32_t LSS_Save(tCanOpenMaster Master)
{
  tCO_Master *thisMaster = (tCO_Master*) Master;
  tCanMessage msg;
  msg.channel     = thisMaster->Channel;
  msg.ext         = thisMaster->Ext;
  msg.id          = 0x7E5;
  msg.len         = 8;
  msg.res         = 0;
  msg.data.u32[0] = 0;
  msg.data.u32[1] = 0;
  msg.data.u8[0]  = 0x17;
  return CANSendMsg(&msg);
}

int32_t LSS_Stop(tCanOpenMaster Master)
{
  tCO_Master *thisMaster = (tCO_Master*) Master;
  tCanMessage msg;
  msg.channel = thisMaster->Channel;
  msg.ext     = thisMaster->Ext;
  msg.id      = 0x7E5;
  msg.len     = 8;
  msg.res     = 0;
  msg.data.u32[0] = 0;
  msg.data.u32[1] = 0;
  msg.data.u8[0] = 0x04;
  return CANSendMsg(&msg);
}

int32_t LSS_Req_ID(tCanOpenMaster Master)
{
  tCO_Master *thisMaster = (tCO_Master*) Master;
  tCanMessage msg;
  msg.channel = thisMaster->Channel;
  msg.ext     = thisMaster->Ext;
  msg.id      = 0x7E5;
  msg.len     = 8;
  msg.res     = 0;
  msg.data.u32[0] = 0;
  msg.data.u32[1] = 0;
  msg.data.u8[0] = 0x5E;
  CANSendMsg(&msg);
  return 0;
}

int32_t LSS_Set_ID(tCanOpenMaster Master, int8_t NodeId)
{
  tCO_Master *thisMaster = (tCO_Master*) Master;
  tCanMessage msg;
  msg.channel = thisMaster->Channel;
  msg.ext     = thisMaster->Ext;
  msg.id      = 0x7E5;
 msg.len     = 8;
  msg.res     = 0;
  msg.data.u32[0] = 0;
  msg.data.u32[1] = 1;
  msg.data.u8[0] = 0x11;
  msg.data.u8[1] = NodeId;
  CANSendMsg(&msg);

  return 0;
}



int32_t LSS_Set_Baudrate(tCanOpenMaster Master, int16_t Baudrate)
{
  tCO_Master *thisMaster = (tCO_Master*) Master;

  int32_t index;
  /*switch (Baudrate)
  {
    case 10:   index = 8;  break;
    case 20:   index = 7;  break;
    case 50:   index = 6;  break;
    case 100:  index = 5;  break;
    case 125:  index = 4;  break;
    case 250:  index = 3;  break;
    case 500:  index = 2;  break;
    case 1000: index = 0;  break;
    default: return -1; break;
  }*/
  index = Baudrate;
  tCanMessage msg;
  msg.channel = thisMaster->Channel;
  msg.ext     = thisMaster->Ext;
  msg.id      = 0x7E5;
  msg.len     = 8;
  msg.res     = 0;
  msg.data.u32[0] = 0;
  msg.data.u32[1] = 1;
  msg.data.u8[0]  = 0x13;
  msg.data.u8[2]  = index;
  CANSendMsg(&msg);

  return 0;
}




int32_t  CO_PdoCfg_SetParam(tCanOpenSlave Slave,  eConfigType eType,int32_t Param_1, int32_t Param_2, int32_t value)
{
  int32_t Index, SubIndex;
  switch (eType)
  {
    case PDO_CFG_TRANSMISSION_TYPE:
    {
      Index    = 0x1400 + Param_1 + 0x400 * Param_2 - 1;
      SubIndex = 2;
      CO_Sdo_Set(Slave, Index, SubIndex,&value, sizeof(value));
    }break;
    case PDO_CFG_TRANSMISSION_ID:
    {
      Index    = 0x1400 + Param_1 + 0x400 * Param_2 - 1;
      SubIndex = 1;
      value    = 0x40000000 + value;
      CO_Sdo_Set(Slave, Index, SubIndex,&value, sizeof(value));
    }break;
    case PDO_CFG_INHIBIT_TIME:
    {
      Index    = 0x1800 + Param_1 - 1;
      SubIndex = 3;
      int16_t val = value;
      CO_Sdo_Set(Slave, Index, SubIndex,&val, sizeof(val));
    }break;
    case PDO_CFG_EVENT_TIME:
    {
      Index    = 0x1800 + Param_1 - 1;
      SubIndex = 5;
      int16_t val = value;
      CO_Sdo_Set(Slave, Index, SubIndex,&val, sizeof(val));
      break;
    }
    case PDO_CFG_STORE_PARAMETERS:
    {
      Index    = 0x1010;
      SubIndex = Param_1;
      int32_t val = 0x65766173; // asci: SAVE
      CO_Sdo_Set(Slave, Index, SubIndex,&val, sizeof(val));
      break;
    }
    case PDO_CFG_RESTORE_PARAMETERS:
    {
      Index    = 0x1011;
      SubIndex = Param_1;
      int32_t val;
      val = 0x65766173; // asci: SAVE
      CO_Sdo_Set(Slave, Index, SubIndex,&val, sizeof(val));
      break;
    }
    default: return -1; break;
  }
  return 0;

}

typedef struct tagPdoMap
{
  int16_t Index;
  int8_t  SubIndex;
  int8_t  numBits;
}tPdoMap;

void CO_PdoCfgSetMap(tCanOpenSlave Slave, int32_t Pdo, int32_t SlaveTx, tPdoMap * MapArray,int8_t  numMapEntries)
{
  int16_t Index;
  int8_t  SubIndex;
  if(SlaveTx)
    Index    = 0x1A00 + Pdo;
  else
    Index    = 0x1600 + Pdo;
  SubIndex = 0;
  CO_Sdo_Set(Slave, Index, SubIndex, &numMapEntries, sizeof(int8_t));
  int32_t val;
  for(int32_t i = 0; i < numMapEntries; i++)
  {
    val =   (MapArray[i].numBits  & 0xFF)
          |((MapArray[i].SubIndex & 0xFF)   << 8)
          |((MapArray[i].Index    & 0xFFFF) << 16);
    CO_Sdo_Set(Slave, Index, i + 1, &val, sizeof(int32_t));
  }
}


void CO_DeInit(void)
{
  for(int32_t i = 0; i < numCO_Master; i++)
  {
    tCO_Master* thisMaster = pCO_Master[i];
    //free slavesB
    for(int32_t j = 0; j < thisMaster->NumSlaves; j++)
    {
      tCO_Slave *this = thisMaster->Slaves[i];
      if(NULL != this->PdoRX)
      {
        free(this->PdoRX);
      }
      if(NULL != this->PdoTX)
      {
        free(this->PdoTX);
      }
     CANRemoveFiFo(this->FiFo);
      if(this->RxSdoData.Data)
      {
        free(this->RxSdoData.Data);
      }
      if(this->pTxSdoAct)
      {
        free(this->pTxSdoAct->Data);
        this->pTxSdoAct->Data = NULL;
      }
      gsMsgFiFoDestroy(this->TxSdoFiFo);
    }
    free(thisMaster->Slaves);
    CANRemoveFiFo(thisMaster->LSS_FiFo);
    CANRemoveFiFo(thisMaster->NMT_FiFo);
    CANRemoveFiFo(thisMaster->TSM_FiFo);
  }
  free(pCO_Master);
  pCO_Master = NULL;
  numCO_Master = 0;
}

