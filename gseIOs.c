﻿/****************************************************************************
 *
 * File:         GSe_IOs.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */


#include "gseIOs.h"
#include "gseDebug.h"

/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


tDevices *DeviceList = NULL;
uint32_t numDevices = 0;




int32_t Notaus = 0;


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

tDevices * GSe_IOs_GetDevice( uint32_t Hdl)
{
  if((NULL != DeviceList)
   &&( Hdl <  numDevices))
  {
    return &DeviceList[Hdl];
  }
  else return NULL;
}



void GSe_IOs_SetName(char * dest, const char * name)
{
  if(NULL != dest)
    free(dest);
  if(NULL != name)
  {
    dest = strdup(name);
  }
  else
  {
    dest = NULL;
  }
}


int32_t GSe_IOs_SetPos( tGSe_IOs_Pos *IO_Pos, uint32_t idxDevice, int32_t idxIO)
{
  if(idxDevice < numDevices)
  {
    IO_Pos->idxDevice = idxDevice;
    IO_Pos->idxIO     = idxIO;
    return 0;
  }
  return -1;
}


int32_t GSe_IO_Add_DI(uint32_t DeviceHdl, const char * description)
{
  tDevices * Device = GSe_IOs_GetDevice(DeviceHdl);
  if(NULL != Device)
  {
    tGSe_IOs_DI * NewDI = realloc(Device->DI, sizeof(tGSe_IOs_DI) * (Device->numDI + 1));
    if(NULL == NewDI)
      return -1;

    Device->DI = NewDI;
    NewDI = &Device->DI[Device->numDI];
    memset(NewDI, 0, sizeof(tGSe_IOs_DI));
    NewDI->description = strdup(description);
    GSe_IOs_SetPos(&NewDI->Pos, DeviceHdl, Device->numDI);
    Device->numDI++;
    return Device->numDI - 1;
  }
  return -1;
}

int32_t GSe_IO_Add_DO(uint32_t DeviceHdl, const char * description)
{
  tDevices * Device = GSe_IOs_GetDevice(DeviceHdl);
  if(NULL != Device)
  {
    tGSe_IOs_DO * NewDO = realloc(Device->DO, sizeof(tGSe_IOs_DO) * (Device->numDO + 1));
    if(NULL == NewDO)
      return -1;

    Device->DO = NewDO;
    NewDO = &Device->DO[Device->numDO];
    memset(NewDO, 0, sizeof(tGSe_IOs_DO));
    NewDO->description = strdup(description);
    GSe_IOs_SetPos(&NewDO->Pos, DeviceHdl, Device->numDO);
    Device->numDO++;
    return Device->numDO - 1;
  }
  return -1;
}

int32_t GSe_IO_Add_AI(uint32_t DeviceHdl, const char * description)
{
  tDevices * Device = GSe_IOs_GetDevice(DeviceHdl);
  if(NULL != Device)
  {
    tGSe_IOs_AI * NewAI = realloc(Device->AI, sizeof(tGSe_IOs_AI) * (Device->numAI + 1));
    if(NULL == NewAI)
      return -1;

    Device->AI = NewAI;
    NewAI = &Device->AI[Device->numAI];
    memset(NewAI, 0, sizeof(tGSe_IOs_AI));
    NewAI->description = strdup(description);
    GSe_IOs_SetPos(&NewAI->Pos, DeviceHdl, Device->numAI);
    Device->numAI++;
    return Device->numAI - 1;
  }
  return -1;
}


int32_t GSe_IO_Add_PWM(uint32_t DeviceHdl, const char * description)
{
  tDevices * Device = GSe_IOs_GetDevice(DeviceHdl);
  if(NULL != Device)
  {
    tGSe_IOs_PWM * NewPWM = realloc(Device->PWM, sizeof(tGSe_IOs_PWM) * (Device->numPWM + 1));
    if(NULL == NewPWM)
      return -1;

    Device->PWM = NewPWM;
    NewPWM = &Device->PWM[Device->numPWM];
    memset(NewPWM, 0, sizeof(tGSe_IOs_PWM));
    NewPWM->description = strdup(description);
    GSe_IOs_SetPos(&NewPWM->Pos, DeviceHdl, Device->numPWM);
    Device->numPWM++;
    return Device->numPWM - 1;
  }
  return -1;
}


int32_t GSe_IO_Add_AO(uint32_t DeviceHdl, const char * description)
{
  tDevices * Device = GSe_IOs_GetDevice(DeviceHdl);
  if(NULL != Device)
  {
    tGSe_IOs_AO * NewAO = realloc(Device->AO, sizeof(tGSe_IOs_AO) * (Device->numAO + 1));
    if(NULL == NewAO)
      return -1;

    Device->AO = NewAO;
    NewAO = &Device->AO[Device->numAO];
    memset(NewAO, 0, sizeof(tGSe_IOs_AO));
    NewAO->description = strdup(description);
    GSe_IOs_SetPos(&NewAO->Pos, DeviceHdl, Device->numAO);
    Device->numAO++;
    return Device->numAO - 1;
  }
  return -1;
}

int32_t GSe_IO_Add_FreqIn(uint32_t DeviceHdl, const char * description)
{
  tDevices * Device = GSe_IOs_GetDevice(DeviceHdl);
  if(NULL != Device)
  {
    tGSe_IOs_FreqIn * NewFreq = realloc(Device->FreqIn, sizeof(tGSe_IOs_FreqIn) * (Device->numFreqIn + 1));
    if(NULL == NewFreq)
      return -1;

    Device->FreqIn = NewFreq;
    NewFreq = &Device->FreqIn[Device->numFreqIn];
    memset(NewFreq, 0, sizeof(tGSe_IOs_FreqIn));
    NewFreq->description = strdup(description);
    GSe_IOs_SetPos(&NewFreq->Pos, DeviceHdl, Device->numFreqIn);
    Device->numFreqIn++;
    return Device->numFreqIn - 1;
  }
  return -1;
}

int32_t GSe_IO_Add_Counter(uint32_t DeviceHdl, const char * description)
{
  tDevices * Device = GSe_IOs_GetDevice(DeviceHdl);
  if(NULL != Device)
  {
    tGSe_IOs_Count * NewCount = realloc(Device->Count, sizeof(tGSe_IOs_Count) * (Device->numCount + 1));
    if(NULL == NewCount)
      return -1;

    Device->Count = NewCount;
    NewCount = &Device->Count[Device->numCount];
    memset(NewCount, 0, sizeof(tGSe_IOs_Count));
    NewCount->description = strdup(description);
    GSe_IOs_SetPos(&NewCount->Pos, DeviceHdl, Device->numCount);
    Device->numCount++;
    return Device->numCount - 1;
  }
  return -1;
}


int32_t GSe_IOs_Add_Device(GSe_IOs_init_cb fpDeviceInit ,void * arg, GSe_IOs_cycle_cb fpDeviceCycle)
{
  //allocating memory for device


  tDevices *pNewDevice = (tDevices*) realloc(DeviceList, sizeof( tDevices) * (numDevices + 1));
  if(NULL != pNewDevice)
  {
    DeviceList = pNewDevice;
    numDevices++;
  }
  else return -1;

  int32_t idx = numDevices - 1;

  tDevices * ThisDevice = &DeviceList[idx];
  memset(ThisDevice, 0, sizeof(tDevices));

  ThisDevice->fpInit_IODevice  = fpDeviceInit;
  ThisDevice->fpCycle_IODevice = fpDeviceCycle;
  ThisDevice->idx = idx;





  //Rufe Gerätespezifische Initialisierungsfunktion auf.
  if(NULL != ThisDevice->fpInit_IODevice)
  {
    ThisDevice->fpInit_IODevice(ThisDevice, arg);
  }
  return idx;
}

void GSe_IOs_Cycle(void)
{
  if(NULL == DeviceList)
  {
    db_out("GSe_IOs_Cycle: DeviceList not initialized, Call GSe_IOs_Add_Device() first");
    return;
  }
  for(uint32_t i = 0; i < numDevices; i++)
  {

    tDevices * ThisDevice = &DeviceList[i];
    if(NULL != ThisDevice->fpCycle_IODevice)
    {
      if(Notaus)
      {
        for(uint32_t j = 0; j <ThisDevice->numDO; j++)
        {
          ThisDevice->DO[j].value = 0;
        }
      }
      ThisDevice->fpCycle_IODevice(ThisDevice);
    }
  }
}




tGSe_IOs_DI * GSe_IOs_GetDIp(int32_t Hdl_Device , uint32_t n)
{
  tDevices * pIODevice = GSe_IOs_GetDevice(  Hdl_Device);

  if(NULL == pIODevice)
  {
    db_out("Error GSe_IOs_MapDI: pIODevice == 0");
    return NULL;
  }
  else if(n >= pIODevice->numDI)
  {
    db_out("Error GSe_IOs_MapDI: n out of range");
    return NULL;
  }
  return &pIODevice->DI[n];
}

tGSe_IOs_DO * GSe_IOs_GetDOp(int32_t Hdl_Device , uint32_t n)
{
  tDevices * pIODevice = GSe_IOs_GetDevice(  Hdl_Device);
  if(NULL == pIODevice)
  {
    db_out("Error GSe_IOs_MapDO: pIODevice == 0");
    return NULL;
  }
  else if(n >= pIODevice->numDO)
  {
    db_out("Error GSe_IOs_MapDO: n out of range");
    return NULL;
  }
  return &pIODevice->DO[n];
}

tGSe_IOs_AI * GSe_IOs_GetAIp(int32_t Hdl_Device , uint32_t n)
{
  tDevices * pIODevice = GSe_IOs_GetDevice(  Hdl_Device);
  if(NULL == pIODevice)
  {
    db_out("Error GSe_IOs_MapAI: pIODevice == 0");
    return NULL;
  }
  else if(n >= pIODevice->numAI)
  {
    db_out("Error GSe_IOs_MapAI: n out of range");
    return NULL;
  }
  return &pIODevice->AI[n];
}

int32_t GSe_IOs_GetDI(tGSe_IOs_DI  *pDi)
{
  if(NULL != pDi)
    return pDi->value;
  else return -1;
}


int32_t GSe_IOs_GetAI(tGSe_IOs_AI  *pAi)
{
  if(NULL != pAi)
    return pAi->value;
  else return -1;
}





void GSe_IOs_SetDO(tGSe_IOs_DO  *pDo, int32_t value)
{

  if(NULL == pDo)
  {
    //db_out("Error GSe_IOs_SetDO: pDo == NULL");
  }
  else
  {
    if(value != pDo->value)
    {
      pDo->value = value;
      pDo->changed = 1;
    }
  }

}

void GSe_IOs_SetPWM(tGSe_IOs_PWM  *pPWM, int32_t duty, int32_t freq)
{

  if(NULL == pPWM)
  {
    //db_out("Error GSe_IOs_SetDO: pDo == NULL");
  }
  else
  {
    if((duty != pPWM->duty)  || (freq != pPWM->freq))
    {
      pPWM->enable = 1;
      pPWM->duty   = duty;
      pPWM->freq   = freq;
    }
  }

}

int32_t GSe_IOs_GetActiveState( int32_t Hdl_Device)
{
  tDevices *pActIODev = GSe_IOs_GetDevice(Hdl_Device);
  if(NULL != pActIODev)
    return pActIODev->active;
  else return -1;
}


int32_t GSe_IOs_GetTemp( int32_t Hdl_Device)
{
  tDevices *pActIODev = GSe_IOs_GetDevice(Hdl_Device);
  if(NULL != pActIODev)
    return pActIODev->temp;
  else return -1;
}

int32_t GSe_IOs_GetVolt( int32_t Hdl_Device)
{
  tDevices *pActIODev = GSe_IOs_GetDevice(Hdl_Device);
  if(NULL != pActIODev)
    return pActIODev->volt;
  else return -1;
}


uint32_t GSe_IO_GetNumDevices(void)
{
  return numDevices;
}

const char * GSe_IO_GetDeviceName(uint32_t DeviceHdl)
{
  if(DeviceHdl > numDevices)
  {
    return NULL;
  }
  return DeviceList[DeviceHdl].DeviceName;
}

const tDevices * GSe_IO_GetDevice(uint32_t DeviceHdl)
{
    if(DeviceHdl > numDevices)
  {
    return NULL;
  }
  return &DeviceList[DeviceHdl];
}
