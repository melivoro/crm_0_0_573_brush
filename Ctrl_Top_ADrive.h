﻿/****************************************************************************
 *
 * Project:   name
 *
 * @file      filename.h
 * @author    author
 * @date      [Creation date in format %02d.%02d.20%02d]
 *
 * @brief     description
 *
 ****************************************************************************/

/* Protection against multiple includes.                                    */
/* Do not code anything outside the following defines (except comments)     */
#ifndef CTRL_TOP_ADRIVE_H
#define CTRL_TOP_ADRIVE_H

/* macro definitions ********************************************************/
#define ASYM_DRIVE_TIME 1800
/* type definitions *********************************************************/

/* prototypes ***************************************************************/
typedef struct tagAsymerticDrive
{
  uint32_t On;
  uint32_t SwitchedOffTime;
  int32_t Pos;
  int32_t PosActual;
  int8_t Engine_Device;
  int8_t Engine_Output;
  int8_t Polarity_Device;
  int8_t Polarity_Output;
  int8_t Direction_Old;
  uint16_t I_Max;           //maximum current in mA
  uint32_t I_Max_Time;
  uint8_t MaxLeftPos;
  uint8_t MaxRightPos;
}tAsymerticDrive;

/* global constants *********************************************************/

/* global variables *********************************************************/

/* global function prototypes ***********************************************/
/**
 * @brief Initalises the asymmetrc drive Sets one output to drive the engine, and one to change the directoin of the engine
 *
 * @param ad
 * @param MCM_Device_Engine MCM-Device of the Engine output
 * @param MCM_Output_Engine Output for the Engine
 * @param MCM_Device_Direction MCM-Device for directoin output
 * @param MCM_Output_Direction Output to change directoin of the engine
 */
void AsymerticDrive_Init(tAsymerticDrive *ad, uint8_t MCM_Device_Engine, uint8_t MCM_Output_Engine, uint8_t MCM_Device_Direction, uint8_t MCM_Output_Direction);

/**
 * @brief Sets the maximum output current of the engine. If the current is higher then mA_Max
 * for more then 500 ms, the endpostion of the drive will be detected.
 *
 * @param ad
 * @param mA_Max maximum current in mA
 */
void AsymetricDrive_CurrentControle_Init(tAsymerticDrive *ad, int32_t mA_Max);

/**
 * @brief Call this Function every 10 ms.
 *
 * @param ad
 */
void AsymerticDrive_Timer_10ms(tAsymerticDrive *ad);

/**
 * @brief switches the asymmetry drive on. It will go to the center position.
 *
 * @param ad
 */
void AsymeticDriveSwitchOn(tAsymerticDrive *ad);

/**
 * @brief switches the asymmetry drive off. It will go to the left.
 *
 * @param ad
 */
void AsymeticDriveSwitchOff(tAsymerticDrive *ad);

/**
 * @brief Changes the wantet position. There are 5 positons.
 * Left, half left, middle, half right, right.
 *
 * @param ad
 * @param MovePos -1 one postion to the right, 1 one position to the left,
 * else do nothing
 */
void AsymetricDrive_Command(tAsymerticDrive *ad, int32_t MovePos );

/**
 * @brief Sets the Indexbitmaps of the asymetric drive.
 * The Indexbitmaps have 6 states.
 * 0 = inactive
 * 1 = left
 * ...
 * 6 = right
 * For the actual positon the variable IDX_SPREADINGSECTOR_ACT is used.
 * For the wanted positon the variable IDX_SPREADINGSECTOR is used.
 *
 * @param ad
 */
void AsymetricDrive_Draw(const tAsymerticDrive *ad);


/****************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !     */
/****************************************************************************/
#endif  // #ifndef CTRL_TOP_ADRIVE_H
