﻿/****************************************************************************
 *
 * Project:   name
 *
 * @file      filename.h
 * @author    author
 * @date      [Creation date in format %02d.%02d.20%02d]
 *
 * @brief     description
 *
 ****************************************************************************/

/* Protection against multiple includes.                                    */
/* Do not code anything outside the following defines (except comments)     */
#ifndef CTRL_TOP_CONVEYOR_H
#define CTRL_TOP_CONVEYOR_H


#include "gseList.h"
#include "sql_table.h"
/* macro definitions ********************************************************/
#define TEMPERATURE_PER_REAGENT 3

/* type definitions *********************************************************/

/* prototypes ***************************************************************/
typedef struct tagTemperatureTable
{
    int32_t    temp;
    tSQLTable  *Table;
}
tTemperatureTable;


typedef struct tagConveyor
{
  uint8_t            running;
  uint8_t            on;
  uint32_t           g_min;
  uint32_t           g_min_max;
  uint32_t           Density_max;
  uint32_t           pwm;                  //0 - 1000
  uint32_t           AsymetryGoHome;
  tMaintRuntime      *Runtime;
  tValv              Valve;
  tPaintText         ColorDensity;
  tTemperatureTable  TempTables[TEMPERATURE_PER_REAGENT];
}tConveyor;

void ConveyorCreate(tConveyor *Conveyor);
void ConveyorOn(tConveyor *Conveyor,uint32_t on);
void ConveyorInit(tConveyor *Conveyor,uint32_t Device, uint32_t Pos);
void ConveyorCycle(tConveyor *Conveyor, int32_t temp);
int32_t  ConveyorGetMaxDensity(tConveyor *Conveyor);
void ConveyorDraw(const tConveyor *Conveyor);
void ConveyorTimer(tConveyor *Conveyor);

/* global constants *********************************************************/

/* global variables *********************************************************/

/* global function prototypes ***********************************************/



/****************************************************************************/
/* Protection against multiple includes ends here ! Stop editing here !     */
/****************************************************************************/
#endif  // #ifndef CTRL_TOP_CONVEYOR_H
