﻿/****************************************************************************
 *
 * File:         HYDVALVCTRL.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "gseMCM.h"
#include "HydValveCtrl.h"
#include "sql_table.h"
/****************************************************************************/

/* macro definitions ********************************************************/
#define PVE_OFFSET (-40)
/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/
tSQLTable PVE_Table ={   .TableName = "PVE_Valves" ,
  .ColumnNames = {"Liters","Empty_0", "PWM",  "EMPTY"},
  .Cell = {
    {-66000    , 0,  230, 0},
    {-65000    , 0,  250, 0},
    {-45000    , 0,  280, 0},
    {-20000    , 0,  340, 0},
    {-1        , 0,  450, 0},
    {0         , 0,  500, 0},
    {1         , 0,  550, 0},
    {20000     , 0,  660, 0},
    {45000     , 0,  720, 0},
    {65000     , 0,  750, 0},
    {66000     , 0,  770, 0},
  }
};

tSQLTable Prop12V_Table ={   .TableName = "Prop12V_Valves" ,
  .ColumnNames = {"Percent","Empty_0", "PWM",  "EMPTY"},
  .Cell = {
    {0       , 0,  0,   0},
    {10      , 0,  25,  0},
    {20      , 0,  50,  0},
    {30      , 0,  75,  0},
    {40      , 0,  100, 0},
    {50      , 0,  125, 0},
    {60      , 0,  150, 0},
    {70      , 0,  175, 0},
    {80      , 0,  200, 0},
    {90      , 0,  225, 0},
    {100     , 0,  250, 0},
  }
};
/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/

int32_t _Valve_GetPWMOutput(uint32_t DO_Output)
{
  if(0 == (DO_Output % 2))//only uneven outputs are PWM outputs.
  {
    return -1;
  }

  int32_t out = (DO_Output - 1) / 2;

  if(out > 3)//maxmum = 3 PWM-Outputs
  {
    return -1;
  }
  return out;
}

void ValveProp_Init(tValveProp *valv, uint32_t MCM_Device, uint32_t Output, uint32_t freq, uint32_t wobble)
{
  valv->MCM_Device = MCM_Device;
  valv->freq = freq;
  valv->duty = 500;
  valv->output =Output;
  valv->wobble = wobble;
  valv->wobble_cnt = 0;
  int32_t output = _Valve_GetPWMOutput(valv->output);
  GSeMCM_SetPWMOut(valv->MCM_Device,output,valv->duty, valv->freq,1);

}


void ValveProp12V_Init(tValv *valv, uint32_t MCM_Device, uint32_t Output, uint32_t freq, uint32_t wobble)
{
  valv->Valve.PVEH.MCM_Device = MCM_Device;
  valv->Valve.PVEH.freq = freq;
  valv->Valve.PVEH.duty = 0;
  valv->Valve.PVEH.output =Output;
  valv->Valve.PVEH.wobble = wobble;
  valv->Valve.PVEH.wobble_cnt = 0;
  valv->Type = VALVE_TYPE_PROP_12V;
}



void ValvProp_25_50_75_Set(tValveProp *valv,int32_t value)
{
  const int32_t offset = PVE_OFFSET; // offset to eliminate offset voltage.
  const int32_t range = 1000 - offset;

  if(value < 0)
  {
    value = 450 + value *2 / 10;
  }
  else if(value > 0)
  {
    value = 550 + value * 2 / 10;
  }
  else value = 500;

  value = value * range / 1000;
  valv->duty = value + offset;


  int32_t output = _Valve_GetPWMOutput(valv->output);

  GSeMCM_SetPWMOut(valv->MCM_Device,output,valv->duty, valv->freq,1);
}


void ValvProp_Set(tValveProp *valv,uint32_t value)
{
  if(valv->duty != value)
  {
    valv->duty = value;

    int32_t output = _Valve_GetPWMOutput(valv->output);
    GSeMCM_SetPWMOut(valv->MCM_Device,output,valv->duty, valv->freq,1);
  }
}

int32_t ValvProp_Timer_100ms(tValveProp *valv)
{
  valv->wobble_cnt++;
  if(valv->wobble_cnt > 2)
    valv->wobble_cnt = -1;
  int32_t out =  valv->duty + valv->wobble_cnt * valv->wobble;
  if(out < 0)    out = 0;
  if(out > 1000) out = 1000;
  int32_t output = _Valve_GetPWMOutput(valv->output);
  if(output < 0)
    return -1;
  GSeMCM_SetPWMOut(valv->MCM_Device,output,valv->duty, valv->freq,1);
  return 0;
}




void ValvDig_2_2_Init(tValvDig_2_2 *Valv, uint8_t Device, uint8_t pos)
{
  Valv->MCM_Device = Device;
  Valv->pos        = pos;
  Valv->val = 0;
}

void ValvDig_2_2_Set(tValvDig_2_2 *Valv, uint8_t value)
{
  if(NULL == Valv)
    return;
  if(value !=Valv->val)
  {
    Valv->ChangeTime = GetMSTick();
  }
  Valv->val = value;
  GSeMCM_SetDigitalOut(Valv->MCM_Device, Valv->pos, value);
}



uint32_t ValvDig_2_2_CheckState(tValvDig_2_2 *Valv)
{
  if(Valv == NULL)
    return -1;
  if(500 < GetMSTick() -Valv->ChangeTime )
  {
    if(Valv->val)
    {
      if(20 >= GSeMCM_GetAnalogIn(Valv->MCM_Device, Valv->pos + 4))
      {
        return -1;
      }
      else return 1;
    }
    else
    {
      if(20 < GSeMCM_GetAnalogIn(Valv->MCM_Device, Valv->pos + 4))
      {
        return -1;
      }
      else return 0;
    }

  }
  return Valv->val;
}

int32_t ValvDig_2_2_GetState(const tValvDig_2_2 *Valv)
{
  return Valv->val;
}

int32_t ValvDig_GetState(const tValv *Valv)
{
  if(Valv->Valve.PVEO[0].val)
  {
    return -1;
  }
  else if(Valv->Valve.PVEO[1].val)
  {
    return 1;
  }
  else return 0;
}


void Valv_Init(tValv *Valv,  int32_t  Device1, int32_t Pos1, int32_t Device2, int32_t Pos2 )
{
  if(Device1 == -1)
  {
    //
    //Valv->Type = VALVE_TYPE_NONE;//would overwrite if a ventil is used.
  }
  else if(Device2 == -1)
  {
    Valv->Type = VALVE_TYPE_PVE_AHS;
    ValveProp_Init(&Valv->Valve.PVEH, Device1, Pos1, 200, 10);
  }
  else
  {
    Valv->Type = VALVE_TYPE_PVE_O;
    ValvDig_2_2_Init(&Valv->Valve.PVEO[0], Device1, Pos1);
    ValvDig_2_2_Init(&Valv->Valve.PVEO[1], Device2, Pos2);
  }
  Valv->state_old = 0;
}

void Valv_Set(tValv *Valv, int32_t value)
{
  Valv->state_old = Valv_GetState(Valv);
  if(Valv->Type == VALVE_TYPE_PVE_O)
  {
    if(value < 0)
    {
      ValvDig_2_2_Set(&Valv->Valve.PVEO[0], 1);
      ValvDig_2_2_Set(&Valv->Valve.PVEO[1], 0);
    }
    else if(value > 0)
    {
      ValvDig_2_2_Set(&Valv->Valve.PVEO[0], 0);
      ValvDig_2_2_Set(&Valv->Valve.PVEO[1], 1);
    }
    else
    {
      ValvDig_2_2_Set(&Valv->Valve.PVEO[0], 0);
      ValvDig_2_2_Set(&Valv->Valve.PVEO[1], 0);
    }
  }
  else if (Valv->Type == VALVE_TYPE_PVE_AHS)
  {
    ValvProp_25_50_75_Set(&Valv->Valve.PVEH, value);
  }
  else if(Valv->Type == VALVE_TYPE_DISK)
  {
    if(value)
      ValvDig_2_2_Set(&Valv->Valve.PVEO[0], 1);
    else
    {
      ValvDig_2_2_Set(&Valv->Valve.PVEO[0], 0);
    }
  }
  else if(Valv->Type == VALVE_TYPE_PROP_12V)
  {
    int32_t PWM =  TableGetLSEValue(&Prop12V_Table, 0,2, value);

    ValvProp_Set(&Valv->Valve.PVEH, PWM);
  }
}

int32_t ValvPVE_CheckState(const tValveProp *Valv)
{
  if(Valv->duty < (450 + PVE_OFFSET / 2))
    return PVE_PORT_A;
  else if(Valv->duty > (550 + PVE_OFFSET / 2))
    return PVE_PORT_B;
  else
    return PVE_PORT_NONE;
}

void Valv_Timer_100ms(tValv *Valv)
{
  if( Valv->Type == VALVE_TYPE_PVE_AHS)
  {
    ValvProp_Timer_100ms(&Valv->Valve.PVEH);
  }
}

int32_t  Valv_GetState(const tValv *Valv)
{
  switch (Valv->Type)
  {
  case VALVE_TYPE_PVE_O:
  case VALVE_TYPE_DISK:
    return ValvDig_GetState(Valv);
    break;
  case VALVE_TYPE_PVE_AHS:
    return ValvPVE_CheckState(&Valv->Valve.PVEH);
  default:
   return 0;
    break;
  }
  return 0;
}

int32_t Valv_GetValue(const tValv *Valv)
{
    switch (Valv->Type)
  {
  case VALVE_TYPE_PVE_O:
  case VALVE_TYPE_DISK:
    return Valv->Valve.PVEO[0].val;
    break;
  case VALVE_TYPE_PVE_AHS:
    return Valv->Valve.PVEH.output;
  default:
   return 0;
    break;
  }
  return 0;
}

int32_t Valv_GetPos(const tValv *Valv)
{
  switch (Valv->Type)
  {
  case VALVE_TYPE_PVE_O:
  case VALVE_TYPE_DISK:
    return Valv->Valve.PVEO[0].val;
    break;
  case VALVE_TYPE_PVE_AHS:
    return Valv->Valve.PVEH.duty;
  default:
   return -1;
    break;
  }
  return -1;
}

void ValvDisk_Init(tValv *Valv, uint32_t Device, uint32_t Pos)
{
  Valv->Type = VALVE_TYPE_DISK;
  Valv->Valve.PVEO[0].pos = Pos;
  Valv->Valve.PVEO[0].MCM_Device = Device;
  Valv->Valve.PVEO[0].val = 0;
}





tSQLTable *pPVE_Tab;
tSQLTable *pProp12_Tab;
void GlobalInitValvePropPVEP(void)
{
  pPVE_Tab = SQL_Tabel_Init(&PVE_Table);
}

void GlobalInitValveProp12V(void)
{
  pProp12_Tab = SQL_Tabel_Init(&Prop12V_Table);
}


void ValvPropPVEP_SetLiter(tValveProp *valv, int32_t ml)
{
  int32_t PWM =  TableGetLSEValue(pPVE_Tab, 0,2, ml);
  ValvProp_Set(valv, PWM);

}
