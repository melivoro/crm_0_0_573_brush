﻿/****************************************************************************
 *
 * File:         VISU_ERROR.c
 * Project:
 * Author(s):
 * Date:
 *
 * Description:
 *
 *
 *
 *
 *
 ****************************************************************************/
#include <stdint.h>
#include <UserCEvents.h>  /* Events send to the Cycle function              */
#include <UserCAPI.h>     /* API-Function declarations                      */

#include "vartab.h"       /* Variable Table definition                      */
                          /* include this file in every C-Source to access  */
                          /* the variable table of your project             */
                          /* vartab.h is generated automatically            */
#include "objtab.h"       /* Object ID definition                           */
                          /* include this file in every C-Source to access  */
                          /* the object ID's of the visual objects          */
                          /* objtab.h is generated automatically            */
#include "gseErrorlist.h"
#include "visu.h"
#include "gsedebug.h"
/****************************************************************************/

/* macro definitions ********************************************************/

/* type definitions *********************************************************/

/* prototypes ***************************************************************/

/* global constants *********************************************************/

/* global variables *********************************************************/

/* local function prototypes*************************************************/


/****************************************************************************/

/* function code ************************************************************/


/****************************************************************************
**
**    Function      :
**
**    Description   :
**
**
**
**    Returnvalues  : none
**
*****************************************************************************/
void Visu_Error_Init(const tVisuData * VData)
{
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_0,HDL_ERRCHECK_0 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_1,HDL_ERRCHECK_1 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_2,HDL_ERRCHECK_2 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_3,HDL_ERRCHECK_3 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_4,HDL_ERRCHECK_4 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_5,HDL_ERRCHECK_5 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_6,HDL_ERRCHECK_6 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_7,HDL_ERRCHECK_7 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_8,HDL_ERRCHECK_8 );
  ErrDraw_AddLine(0 ,OBJ_ERRORLINE_9,HDL_ERRCHECK_9 );
  ErrDraw_InitSetFrameObj(OBJ_ERRORFRAME);
  ErrDraw_InitErrorFrame( 58, 36,  50, 700);
  ErrDraw_SetTextColor(ES_NOERROR, 0x005500);
  ErrDraw_SetTextColor(ES_UNCHECKED_ERROR_ACTIVE, 0xEE2222);
  ErrDraw_SetTextColor(ES_UNCHECKED_ERROR_INACTIVE, 0x7F9646);
  ErrDraw_SetTextColor(ES_CHECKED_ERROR_ACTIVE    , 0x4F81BD);
  ErrDraw_SetTextColor(ES_CHECKED_ERROR_INACTIVE  , 0xAAAAAA);
  //EList_SetPrintErrorOut((tGsPrintErrOutCallback)db_out);


}

void Visu_Error_Open(const tVisuData * VData)
{
  //ErrDraw_Draw();
  PrioMaskOn(MSK_ERROR);
}


void Visu_Error_Cycle(const tVisuData * VData, uint32_t evtc, tUserCEvt *evtv)
{
  EList_AutoErrorRemove();
  ErrDraw_Cycle(evtc,evtv);
  Visu_HomeKeyPressed();
}

void Visu_Error_Close(const tVisuData * VData)
{
  MaskOff(MSK_ERROR);
}
